.. highlight:: bash

Scripts Overview
================

Display help and version information
....................................

All ``aoi`` scripts provide the following help and version command-line options:

* ``--help`` show the overall help, without examples

* ``--help-short`` show a short help, without description and examples

* ``--help-examples`` show examples for using a script

* ``--version`` display the script version

Examples::

  # works for all aio-scripts
  aio-cut --help
  aio-split --help-short
  aio-concat --help-examples
  aio-mix --version

The design of command-line options for aio-scripts can be found `in the wiki`_.

Script commands and options
...........................

Most of the ``aio`` scripts provide commands to select different functionality of a script. 

A command can be always given in a long or short form. Long form is usually a whole word, while the short form is just a single character, usually the first char of the long form command::

  # invoke settings command
  aio-fade settings
  # or its short form
  aio-fade s
  
  # invoke from and to-commands
  aio-cut from 10 to 20 audio.ogg
  # or their short forms
  aio-cut f 10 t 20 audio.ogg

The same holds true for script options and flags::

  # force overwrite of an existing file
  aio-cut --force-overwrite from 10 to 20 audio.ogg
  # or use the short form of the flag
  aio-cut -f from 10 to 20 audio.ogg

  # invoke fade-in with a custom duration, in long-form
  aio-fade in --duration-in 1 audio.ogg
  # or its short form
  aio-fade in --di 1 audio.ogg

.. _timestamp-format-label:

Timestamp format
................

Timestamps can be written as follows:

  ``hh:mm:ss.ms``, ie. ``22:13:59.33``

Timestamps can be given in the following value-range:

  ``00:00:00.000`` to ``23:59:59.999``

If the outer left hand part of a timestamp is 0, that part can be omitted:

  Instead of ``00:00:10.45``, just write ``10.45`` (10 seconds, 45 milliseconds)
  
  Instead of ``00:02:10.45``, just write ``2:10.45`` (2 minutes, 10 seconds, 45 milliseconds)
  
  Instead of ``00:00:00.000``, just write ``0``

If only the ``ms`` parts i provided, just write:

  ``0.45`` or ``.45`` (45 milliseconds)

If no ``ms`` part is used, then that part can be left out from timestamp:

  Instead of ``2:10:00.000``, just write ``2:10:00`` (two hours, 10 minutes)

Examples::  

  # split the input-file in two parts at 66 minutes
  aio-split at 01:06:00 audio.ogg

  # cut the first 12.5 seconds from the begin of the input-file  
  aio-cut from 12.5 audio.ogg

.. _output-file-format-label:

Output file generation
......................

All ``aoi`` scripts that process audio-files usually generate the name of the resulting ``<output-file>``.

The generated ``<output-file>`` is constructed as follows::

  <prefix>-<input-file>
  
The ``<prefix>`` reflects the processing type:

  =============  =============================
  script         prefix
  =============  =============================
  aio-cut        cut
  aio-split      split
  aio-concat     concat
  aio-mix        mix
  aio-fade       fade-in, fade-out, fade-inout
  aio-normalize  peak
  =============  =============================

Some ``aio`` scripts do produce several ``<output-files>``. In those cases, a <counter> is prepended to the generated ``<output-file>`` name::

  <counter>-<prefix>-<input-file>

Currently, the ``<counter>`` runs from ``a-z``. This might change `in the future`_

Let's see some examples::

  # cut audio.ogg once, resultings in one output-file
  aio-cut from 10 audio.ogg
  > cut-audio.ogg
  
  # cut audio.ogg twice, resulting in two output-files
  aio-cut stdin audio.ogg <<EOF
    audio.ogg 10
    audio.ogg 20 30
  EOF
  > a-cut-audio.ogg
  > b-cut-audio.ogg
  
  # always generates two output files
  aio-split at 10 audio.ogg
  > a-split-audio.ogg
  > b-split-audio.ogg
  
  # one output-file is generated, first input-file is used for output-file name
  aio-mix audio-1.ogg audio-2.ogg audio-3.ogg
  > mix-audio-1.ogg
  
  # one output-file is generated, first input-file is used for output-file name
  aio-concat audio-1.ogg audio-2.ogg audio-3.ogg
  > concat-audio-1.ogg
  
  # one output file is generated
  aio-fade in audio.ogg
  > fade-in-audio.ogg
  
  # for each input-file, a output-file is generated, but not counter is necessary here
  aio-fade inout audio-1.ogg audio-2.ogg audio-3.ogg
  > fade-inout-audio-1.ogg
  > fade-inout-audio-2.ogg
  > fade-inout-audio-3.ogg

.. _overview-ia-prepare-label:
  
Install the ia command-line tool for archive.org
................................................



Notes about about audio transcoding and quality
...............................................

.. note::

  ``aio-concat``, ``aio-cut`` and ``aio-split`` do not transcode audio when generating an ``<output-file>``. The ``<output-file>`` is stored with the same codec and codec parameters as the given ``<input-file>``.

.. note::
  
  During fading (``aio-fade``), mixing (``aio-mix``) and normalization (``aio-normalize``), each ``<input-file>`` is decoded, the audio-algorithm is applied and the processed audio-data is encoded into ``<output-files>``.
  
  When encoding ``<output-files>``, ``ffmpeg`` usually uses default quality settings of the corresponding audio-codec.

  For lossless audio (like flac or wav), this is no problem, as lossless audio quality is supposed to be that of the original audio.

  For compressed audio, the use of default quality settings by ``ffmpeg`` may result in an ``<output-file>`` that is encoded with a lower (variable) bitrate than the ``<input-file>``.

  In order to avoid that reduction of audio quality, custom default quality settings are used by ``aio-fade``, ``aio-mix`` and ``aio-normalize`` for a couple of common compressed audio-codecs. Those default values result in an ``<output-file>`` encoded with a relatively high bitrate.
  
  For a list of supported codecs and their custom quality settings run ``aio-fade settings``, ``aio-mix settings`` or ``aio-normalize settings``.

  For all other compressed audio codecs, no custom quality setting do exist yet. In that case, use the ``-q`` option of the ``aio`` scripts to specify a proper quality value. Otherwise, ``ffmpeg`` will use its own the default values for those codecs.
  
  For an overview about quality values accepted by ``ffmpeg``, take a look at https://trac.ffmpeg.org/wiki/Encode/HighQualityAudio .

.. _in the wiki: https://gitlab.com/zzeebdrraa/tools/cl-audio-processing/aio/-/wikis/Script-Design
.. _in the future: https://gitlab.com/zzeebdrraa/tools/cl-audio-processing/aio/-/wikis/Ideas/Naming-Schemes