.. highlight:: bash

aio-sec-to-ts
=============

Convert ``<seconds>`` to ``<timestamps>``.

::

  aio-sec-to-ts <second> [... <second>]

  aio-sec-to-ts stdin

Options
.......

``stdin``

  ``<seconds>`` will be read from stdin:

  * at least one ``<second>`` is expected on ``stdin``
  * one ``<second>`` per line
  * empty lines are ignored
  * lines that only contain spaces (whitespaces, tabs, etc) are ignored
  * lines that start with ``#`` are ignored
  
  If ``stdin`` is given, all ``<seconds>`` arguments will be ignored.
  
  The wiki shows internals about `providing data via stdin`_.

``<second> [... <second>]``

  At least one ``<second>`` value must be povided.

  If ``stdin`` is given, all ``<second>`` arguments will be ignored.

Seconds format
..............

  A ``<second>`` value ranges from ``0`` to any other positive float value, until and including ``86399.999``. ``86399.999`` represents ``23:59:59.999`` and is the maximum value that fits into a ``<timestamp>``:

    * ``0 >= <sec> <= 86399.999``

  ``<second>`` can be given like this:

    * ``<second>`` as ``<ss>.<ms>``
    * ``ss`` ranges from ``0`` to ``86399`` (``23:59:59``)
    * ``ms`` ranges from ``0 to 999``

Output
......

For each provided ``<second>`` (either given on cl or via stdin), the converted ``<timestamps>`` are printed to standard out, one value per line, in the same order as given ``<secsonds>``.

.. _providing data via stdin: https://gitlab.com/zzeebdrraa/tools/cl-audio-processing/aio/-/wikis/Script-Design#options-from-stdin