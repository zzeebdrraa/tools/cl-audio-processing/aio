.. highlight:: bash

aio-rename-files
================

Bulk renaming of ``<input-files>``.

::

  aio-rename-files help select
  aio-rename-files help in
  aio-rename-files help stdin
  aio-rename-files help out
  aio-rename-files help mix

  aio-rename-files examples select
  aio-rename-files examples in
  aio-rename-files examples stdin
  aio-rename-files examples out
  aio-rename-files examples mix

  aio-rename-files [-d|--dry-run]
                   [-s|--show-only]
                   [-f|--force-overwrite]
                   [--do|--delimiter-out <delimiter>]
                   <cmd> :::
                   <filepattern>

  aio-rename-files [global-options]
                   o|out
                     a|append <suffix|suffix-list> :::
                   <filepattern>

  aio-rename-files [global-options]
                   o|out
                     p|prepend <prefix|prefix-list> :::
                   <filepattern>

  aio-rename-files [global-options]
                   o|out
                     r|replace
                       <first|last>
                       <string-to-replace> :::
                       <replace-value|replace-value-list> :::
                   <filepattern>

  aio-rename-files [global-options]
                   o|out
                     r|replace
                       [--sb|--strip-begin] [--se|--strip-end] [-s|--squeeze-repeats]
                       all
                       <string-to-replace> :::
                       <replace-value|replace-value-list> :::
                   <filepattern>

  aio-rename-files [global-options]
                   i|in
                     [--di|--delimiter-in <delimiter>]
                     k|keep <filename-part-index-list> :::
                   <filepattern>

  aio-rename-files [global-options]
                   i|in
                     [--di|--delimiter-in <delimiter>]
                     c|cut <filename-part-index-list> :::
                   <filepattern>

  aio-rename-files [global-options]
                   i|in
                     s|split <index-list> :::
                   <filepattern>

  aio-rename-files [global-options]
                   stdin
                     v|value :::
                   <filepattern>
                   < value-list-stdin

  aio-rename-files [global-options]
                   stdin
                     kv|key-value <key-list> :::
                   <filepattern>
                   < key-value-list-stdin

  aio-rename-files [global-options]
                   s|select <index-list> :::
                   o|out ... :::
                   <filepattern>

  aio-rename-files [global-options]
                   s|select <index-list> :::
                   i|in ... :::
                   <filepattern>

  aio-rename-files [global-options]
                   s|select <index-list> :::
                   stdin ... :::
                   <filepattern>
                   < data-stdin

  aio-rename-files [global-options]
                   select ... :::
                   in ... :::
                   stdin ... :::
                   out ... :::
                   <filepattern>
                   < data-stdin

Description
...........

``aio-rename-files`` is meant for processing larger numbers of ``<input-files>`` but also works for just one or two files, even though it might then be faster/easier to directly use ``cp`` or ``mv``.

``aio-rename-files`` will never overwrite any ``<input-files>``. It always makes a copy of any ``<input-file>`` and store that copy under the new name.

``aio-rename-files`` will also never overwrite any existing renamed file, except it is told to via the ``--force-overwrite`` option.

``aio-rename-files`` can work on ``<input-files>`` whose names are structured in one way or another:

  it can extract parts of ``<input-file>`` names:

    see ``in cut`` and ``in keep`` commands.

  it can completely rename any ``<input-file>`` by using data passed via standard input:

    see ``stdin value`` and ``stdin key-value`` commands.

  it can prepend a prefix and/or append a suffix to any number of ``<input-files>`` and/or replace parts of a filename.

    see ``out prepend``, ``out append`` and ``out replace`` commands

  it can use any combination of commands, even though, things might become more complicated in understanding what will finally happen to the renamed files.

    see :ref:`aio-rename-multiple-commands-label`
  
Global Options
..............

Commands
........

.. _aio-rename-multiple-commands-label:

Using Multiple Commands At Once
...............................

Read From Stdin
...............

Output
......

.. _aio-rename-files-selectors-label:

Select Syntax
.............

Examples
........