.. highlight:: bash

aio-metadata-set
================

Writes audio metadata to ``<input-files>``. 

Metadata is read from stdin and can either be written to individual files or commonly to all files.

::

  # additional help topics
  aio-metadata-set --hm|--help-metadata

  aio-metadata-set s|status
  
  aio-metadata-set ls|list-files
                     <file-1> [... <file-x>]

  aio-metadata-set cn|count-files 
                     <file-1> [... <file-x>]

  aio-metadata-set [-d|--dry-run]
                   [--fff|--force-ffmpeg]
                   i|individual
                     <file-1> [... <file-x>]

  aio-metadata-set [-d|--dry-run]
                   [--fff|--force-ffmpeg]
                   c|common
                     <file-1> [... <file-x>]
                     
Global Options
..............

Commands
........

Supported Metadata-Standards
............................

``aio-metadata-set`` uses external tools to set metadata for specific audio formats. 

Those tools must have been installed. Use the ``status`` command to verify that all external tools are available.

Alternatively, ``ffmpeg`` can be enforced as metadata-tool for all audio-codecs by using the ``--fff`` option.

The following audio-codecs and metadata-standards are supported. The corresponding tools will be automatically selected, depending on the audio-codec(s) of ``<input-files>``:

  ============  =================   =============
  audio-format  metadata-standard   metadata-tool
  ============  =================   =============
    mp3           id3v2               id3v2
    flac          vorbis-comment      metaflac
    ogg           vorbis-comment      vorbiscomment
    opus          vorbis-comment      ffmpeg
    wav           riff                ffmpeg
  ============  =================   =============

The following audio-codecs are supported when using generic metadata ``<tag-names>`` supported by ffmpeg. Generic ``<tag-names>`` are converted by ``ffmpeg`` to the metadata-standard for particular audio-codecs:

  ============  =================   =============
  audio-format  metadata-standard   metadata-tool
  ============  =================   =============
    wav           ffmpeg              ffmpeg
    mp3           ffmpeg              ffmpeg
    ogg           ffmpeg              ffmpeg
    flac          ffmpeg              ffmpeg
    opus          ffmpeg              ffmpeg
  ============  =================   =============

For a detailed overview about metadata-standards, take a look at :ref:`metadata-standards-details-label`


.. note::

  ``ffmpeg`` might be able to set metadata for most audio-codecs. Try to set metadata with ``ffmpeg`` as tagging tool via the ``--fff`` option.
  
  Be aware that the generic ``<tag-names>`` supported by ``ffmpeg`` do only cover common tags of each metadata-standard. More specialized ``<tag-names>`` provided by particular standards must still be set with the corresponding tagging tools.

.. _metadata-standards-details-label:

In-Depth view into audio metadata standards
...........................................

