.. aio documentation master file, created by
   sphinx-quickstart on Sun Nov  7 15:09:08 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Usage
=====

.. toctree::
   :maxdepth: 1
	 
   overview

**Cut, split and concatenate audio files**

.. toctree::
   :maxdepth: 1

   aioconcat
   aiocut
   aiosplit
	
**Mix, fade and normalize audio files**
	
.. toctree::
   :maxdepth: 1

   aiofade
   aiomix
   aionormalize

**Detect silence in audio files**

.. toctree::
   :maxdepth: 1

   aiodetectsilence

**Generate and set audio metadata**

.. toctree::
   :maxdepth: 1

   aiometadatagenerate
   aiometadatatransform
   aiometadataset

**Bulk file renaming**

.. toctree::
   :maxdepth: 1

   aiorenamefiles

**Tools**

.. toctree::
   :maxdepth: 1

   aiotstosec
   aiosectots

**Work with archive.org**

.. toctree::
   :maxdepth: 1

   aioiametadata
   aioiaupload
