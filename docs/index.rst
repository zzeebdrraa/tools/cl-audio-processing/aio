.. aio documentation master file, created by
   sphinx-quickstart on Sun Nov  7 15:09:08 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to aio's documentation!
===============================

``aio`` contains a couple of ``bash`` scripts for bulk audio-processing, file tagging and renaming tasks.

``aio`` was developed in order to replace a number of repetitive steps usually done when cutting audio samples from videos, performing audio processing in ``Audacity``, tagging and renaming audio files. 

``aio`` is inspired by `Freds ImageMagic Scripts`_ in the sense that most of the ``aio`` scripts are wrappers around certain ffmpeg_ functionality. The scripts only expose a few of the many options of ``ffmpeg``. For complicated tasks, it might be better to directly use ``ffmpeg`` or any other audio tool.

Usage should be as simple as possible and is also aimed towards the casual command line user or, in the best case, towards people with no shell experience at all. Therefore it would be great to receive feedback if the documentation on this site lacks information or needs to be more comprehensive or if script usage could be improved or simplified. 

Where does it run
=================

You need ``bash`` version ``4.0.0`` or higher, as all aio-scripts make use of ``bash`` language features, that haven't been available in earlier ``bash`` versions (ie. associative arrays, declaration of array references, etc). ``aio`` was developed on a Debian system and scripts are not posix-compliant.

* **Linux** ``aio`` should run on any Linux that has a proper ``bash`` version installed. 

* **MacOS** ``aio`` should run on a recent MacOS, but some ``bash`` features and programs work differently than on Linux. It would be interesting to receive some feedback from Mac users.

* **Windows** ``aio`` might run under ``cygwin`` on Windows, but this is just a guess. For windows it would also be interessting to receive some feedback.

Please check out the :doc:`installation` instructions for detailed information about other dependencies (``ffmpeg``, ``ffprobe``, etc).

Download
========

``aio`` can be downloaded from https://gitlab.com/zzeebdrraa/tools/cl-audio-processing/aio

License
=======

``aio`` is licensed under `GNU General Public License v3.0`_.

Content
=======

.. toctree::
   :maxdepth: 2

   installation
   usage
   examples
   workflows
   releases

.. note::

   This project and the docs are under active development.

.. _Freds ImageMagic Scripts: http://www.fmwconcepts.com/imagemagick/index.php
.. _ffmpeg: https://ffmpeg.org
.. _GNU General Public License v3.0: https://gitlab.com/zzeebdrraa/tools/cl-audio-processing/aio/-/blob/master/LICENSE
