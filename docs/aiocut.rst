.. highlight:: bash

aio-cut
=======

Cut out a piece of a audio file.
::

  aio-cut status
  
  aio-cut [-f|--force-overwrite] 
          from <timestamp> <input-file> [<output-file>]

  aio-cut [-f|--force-overwrite]
          to <timestamp> <input-file> [<output-file>]
  
  aio-cut [-f|--force-overwrite]
          from <timestamp> to <timestamp> <input-file> [<output-file>]
  
  aio-cut [-f|--force-overwrite]
          stdin [<input-file>]

Commands
........

``status``
``s``

  Prints status of script internals.

``from <timestamp> <input-file>``
``f <timestamp> <input-file>``

  Cut from ``<timestamp>`` to the end of ``<input-file>``.

``to <timestamp> <input-file>``
``t <timestamp> <input-file>``

  Cut ``<input-file>`` from begin to ``<timestamp>``.

``from <timestamp> to <timestamp> <input-file>``
``f <timestamp> t <timestamp> <input-file>``

  Cut ``<input-file>`` from ``<timestamp>`` to ``<timestamp>``.

``stdin``

  One or more <input-files> can be cut several times at once. ``<input-files>`` and corresponding ``<from>`` and/or ``<to>`` timestamps are read from standard input.
  
  If ``stdin`` is used, all other commands are ignored.

``stdin <input-file>``

  One ``<input-file>`` can be cut more than once. One or more ``<from>`` and/or ``<to>`` timestamps are read from standard input.
  
  If ``stdin`` is used, all other commands are ignored.

Options
.......

``--force-overwrite``
``-f``

  Overwrite an existing ``<output-file>``. Is ignored if ``status`` is used.

``<input-file>``

  The audio-file to cut.
    
  Must be provided if ``from`` and/or ``to`` are used. Only one ``<input-file>`` can be given.
    
  Optional, if ``stdin`` is used.
  
``<output-file>``

  Optional, can be provided if ``from`` and/or ``to`` are used. Only one ``<output-file>`` can be given. If provided, then cut-result will be written to given ``<output-file>``. If not provided, then name of ``<output-file>`` will be generated from ``<input-file>``.
  
  Is ignored if ``stdin`` is used. Names of ``<output-files>`` will be generated from ``<input-files>``.

Stdin Format
............

If ``<stdin>`` is given, then one or more lines on stdin can be formated like this::

  # cut <input-file> from <timestamp> to <timestamp>
  <input-file> <from-timestamp> <to-timestamp>
  
  # cut <input-file> from <timestamp> to the end of the file
  <input-file> <from-timestamp>
    
  # cut <input-file> from begin of file to <timestamp>
  <input-file> 0 <to-timestamp>

Examples::

  # with a here-doc, several line can be provided

  # cut one file several times
  aio-cut stdin <<EOF
    audio-1.ogg 0 10.45
    audio-1.ogg 10.50 15
    audio-1.ogg 20
  EOF

  # cut several file several times
  aio-cut stdin <<EOF
    audio-1.ogg 0 10.45
    audio-1.ogg 10.50 15
    audio-1.ogg 20
    audio-2.ogg 0 1:20
    audio-2.ogg 2:00 5:00
    audio-2.ogg 10:00
  EOF

  # with a  here-string, only one line can be provided
  aio-cut stdin <<<'audio-1.ogg 0 10:45'

If ``<stdin> <input-file>`` is given, then one or more lines on stdin can be formated like this::

  # cut <input-file> from <timestamp> to <timestamp>
  <from-timestamp> <to-timestamp>
  
  # cut <input-file> from <timestamp> to the end of the file
  <from-timestamp>
  
  # cut <input-file> from begin of file to <timestamp>
  0 <to-timestamp>

Examples::

  # with a here-doc, several lines can be provided

  # cut one file several times
  aio-cut stdin <<EOF
    0 10.45
    10.50 15
    20
  EOF

  # with a  here-string, only one line can be provided
  aio-cut stdin audio-1.ogg <<<'0 10:45'

The wiki shows internals about `providing data via stdin`_.

.. _envvar-cut-label:

Environment Variables
.....................

``ENV_AIO_CUT_TS_TO_SEC_BIN_PATH``

  Can be used to set a custom path for the internally used ``aio-ts-to-sec`` script.
  
Use the ``status`` command to verify that envvar is set correctly.

Timestamps
..........

See :ref:`timestamp-format-label` for examples on how to write timestamps.

Output files
............

See :ref:`output-file-format-label` for more details.

.. _providing data via stdin: https://gitlab.com/zzeebdrraa/tools/cl-audio-processing/aio/-/wikis/Script-Design#options-from-stdin