.. highlight:: bash

aio-ia-generate-upload-file
===========================

Generate a ``<csv-file>`` that contains the ``<files>`` to be uploaded to an existing or new page at `archive.org`_.

::

  aio-ia-generate-upload-file -h|--help
  
  aio-ia-generate-upload-file help
  aio-ia-generate-upload-file help <topic>

  aio-ia-generate-upload-file new
                              [-d|--dry-run]
                              [-o|--output-file <csv-file>]
                              [-c|--creator <creator>]       | [-C|--placeholder-creator]
                              [-g|--collection <collection>] | [-G|--placeholder-collection] | [shortcuts]
                              [-t|--mediatype <media-type>]  | [shortcuts]
                              -i|--identifier <identifier>
                              <file-1> [... <file-x>]
                              <metadata-list
                                
  aio-ia-generate-upload-file append
                              [-d|--dry-run]
                              [-o|--output-file <csv-file>]
                              [-c|--creator <creator>]       | [shortcuts] | [-C|--placeholder-creator]
                              [-g|--collection <collection>] | [-G|--placeholder-collection] | [shortcuts]
                              [-t|--mediatype <media-type>]  | [shortcuts]
                              -i|--identifier <identifier>
                              <file-1> [... <file-x>]
                              <metadata-list

  aio-ia-generate-upload-file template
                              [-c|--creator <creator>]        | [-C|--placeholder-creator]
                              [-g|--collection <collection>]  | [-G|--placeholder-collection] | [shortcuts]
                              [-m|--mediatype <mediatype>]    | [-M|--placeholder-mediatype]  | [shortcuts]
                              [-i|--identifier <identifier>]  | [-I|--placeholder-identifier] |

Description
...........

A single page at `archive.org`_ usually represents a media-collection. A media-collection contains files like audios, movies, binaries or documents, a description, and other metadata.

In order to upload ``<files>`` to an existing or a new page at `archive.org`_, one can use the `ia tool`_. ``ia`` can read a ``<csv-file>`` that contains upload information like the ``<files>`` to upload and related metadata.

Writing a ``<csv-file>`` manually, especially if it contains many columns, is error prone, or one needs a spreadsheet software. This is where ``aio-ia-generate-upload-file`` aims to jump in. 

It transforms its file and metadata input into columns in order to generate a ``<csv-file>`` for the ``ia`` tool.

Check out :ref:`ia-upload-read-files-label` for reading file-names and file-metadata.

Check out :ref:`ia-upload-write-output-label` for the transformation of file-names and metadata into a ``<csv-file>``.

Check out :ref:`ia-upload-files-label` for using a ``<csv-file>`` with the `ia tool`_.

Archive.org Metadata Specification
..................................

When uploading files to a new or existing page at `archive.org`_, some specific ``<metadata>`` can be set for those files.
  
If a new page is about to be created through file-upload:
  
* provide the ``--identifier``, ``--mediatype``, ``--creator`` and ``--collection`` options on cl and/or the corresponding key-value pairs on stdin
  
If files are about to be uploaded to an existing page:
  
* provide the ``--identifier`` and ``--mediatype`` options and/or the corresponding key-value pairs on stdin

The detailed specification for each ``<metadata>`` item can be found at:

* https://archive.org/services/docs/api/metadata-schema/index.html

Commands
........

``new``

  Write generated content to a new ``<csv-file>``. In new-mode, column-headers and data are written.
  
  In order to write to the default ``<csv-file>``:
  
  * the value of the ``--identifier`` option is used to generate the name of a default ``<csv-file>``
  
  In order to write to a custom ``<csv-file>``:
  
  * use the ``--output-file <csv-file>`` option
  * ``--identifier`` option is not used for naming a default ``<csv-file>``
  
  If a default or custom ``<csv-file>`` does already exists, all generated column-headers and data will overwrite the existing file content.d
  
  If a default or custom ``<csv-file>`` does not exist, it will be created and all generated column-headers and data will be written to the new file.

``append``

  Appends generated content to an existing ``<csv-file>``. In append-mode, no column-headers are written, but just column-data.

  In order to append to the same default ``<csv-file>``:

  * use the same ``--identifier`` option in each call

  In order to append to a custom ``<csv-file>``:

  * use the ``--output-file <csv-file>`` option in each call
  * one may use different ``--identifier`` values in each call, as ``--identifier`` is not used for naming a default ``<csv-file>``

  If a default or custom ``<csv-file>`` does already exists, all generated column data will be appended to that file, without column-headers.

  If a default or custom ``<csv-file>`` does not exist, it will be created and all generated column data will be written to that file, without column-headers.

``template``
  
  Write the content of a key-value metadata template to standard output.
  
  Template content might be redirected to a file. Once the template file has been adjusted with custom metadata values, it can be used to pass that data via standard input to the ``new`` and ``append`` commands.

``help``

  List all help ``<topics>``.
  
``help <topic>``

  Print help for a single ``topic``.
  
Command Options - output
........................

``--dry-run``
``-d`` 

  Don't write to a file but print generated content to standard-output.

``--output-file <csv-file>``
``-o <csv-files>``

  Write the generated content to the given ``<csv-file>``.

  In ``new`` mode:
  
  * if default or custom ``<csv-file>`` does not exist, it wil be created first
  * generated column-headers and data do overwrite the default or custom ``<csv-file>``

  In ``append`` mode:
  
  * if default or custom ``<csv-file>`` does not exist, it wil be created first
  * generated column-data (but no column-headers) will be appended to the default or custom ``<csv-file>``
  * all existing content in default or custom ``<csv-file>`` will be preserved

Command Options - metadata
..........................

``--collection <collection>``
``-g <collection>``
  
  Several community collection exists. One can upload to pages/items in those collections without special access rights:
  
  * ``opensource_audio`` - the Community Audio collection
  * ``opensource_movies`` - the Community Video collection
  * ``opensource`` - the Community Texts collection
  * ``opensource_image`` - the Community Image collection
  * ``open_source_software`` - the Community Software collection
  
  In order to find the name of a collection, just browse to the about-page of a collection at `archive.org`_ and take a look at the ``identifier`` at that page. The ``identifier`` value is the collection name:
  
  * collection about page: https://archive.org/details/more_animation?tab=about
  * identifier: ``more_animation``

  Option should be used when a new page is about to be created at `archive.org`_.
  
  Option can be skipped when files are supposed to be uploaded to an existing page, as that page most likely is part of a collection already.
  
  .. note::
  
    Collection metadata is described at https://archive.org/services/docs/api/metadata-schema/index.html#collection

``--creator <creator>``
``-c <creator>``

  The name of the author/creator of the files to upload. If provided, it is written as ``creator`` column in the generated csv-data, for each given media-file to upload.

  Option should be used when a new page is about to be created at `archive.org`_.
  
  Option can be skipped when files are supposed to be uploaded to an existing page, as that page most likely has a creator already.
  
``--identifier <identifier>``
``-i <identifier>``

  The ``<identifier>`` is a mandatory option:
  
  * it is used for naming the default output ``<csv-file>``, if the ``--output-file <csv-file>`` option is not provided
  * it is used to select the page at `archive.org`_ to upload files to
  * it is written as ``identifier`` column in the generated csv-data, for each given media-file to upload
  
  The ``<identifier>`` is usually the endpoint of the url of a page at `archive.org`_:
  
  * page: https://archive.org/details/lac2019proceedings
  * identifier: ``lac2019proceedings``

  In ``append`` mode with ``--output-file <csv-file>``:
  
  * ``<identifier>`` might differ in each call, in order to list files to be uploaded to different pages in the same custom ``<csv-file>``

  Check out :ref:`ia-upload-write-output-label` for details about output-file naming and content generation.
  
  .. note::
  
    Identifiers are alse described at https://archive.org/services/docs/api/metadata-schema/#archive-org-identifiers

``--mediatype <mediatype>``
``-t <mediatype>``

  The media type of the files to be uploaded. All provided ``<input-files>`` must be of the same media-type.
  
  Common ``<mediatypes>`` are: 
  
  * ``audio``
  * ``image``
  * ``movies``
  * ``texts``
  
  If ``<input-files>`` with different ``<mediatypes>`` should be added to a ``<csv-file>`` and being uploaded **to the same page**:
  
  * run this script for initial ``<mediatype>`` in ``new`` mode
  * run this script for each additional ``<mediatype>`` in ``append`` mode
  * always use the same ``<identifier>`` in each call, in order to append generated content to the same default ``<csv-file>`` (:ref:`ia-upload-files-label`)
  
  If ``<input-files>`` with different ``<mediatypes>`` should be added to a ``<csv-file>`` and being uploaded **to different pages**:
  
  * provide the ``--output-file <csv-file>`` option, in order to always write to the same custom ``<csv-file>``
  * provide different ``<identifier>`` options for each ṕage you want to upload to
  * run this script for initial ``<mediatype>`` or ``<identifier>`` in ``new`` mode
  * run this script for each additional ``<mediatype>`` or ``<identifier>`` in ``append`` mode

  .. note::
  
    Accepted media-types are described at https://archive.org/services/docs/api/metadata-schema/index.html#mediatype
  
  .. note:: 
  
    ``aio-ia-generate-upload-file`` does not verify if given ``<input-files>`` do match the given mediatype.

Command Options - metadata shortcuts
....................................

.. note::

  Shortcuts are always inserted into a ``<csv-file>`` at the position they are specified on cl, thus, csv-column order rules apply for shortcuts as well. 
  
  If a shortcut option and its counterpart are both provided on cl, only the last occurence is taken into account.

``--ga``
``--collection-audio``

  a shortcut for ``--collection opensource_audio``

``--gi``
``--collection-image``

  a shortcut for ``--collection opensource_image``

``--gm``
``--collection-movies``

  a shortcut for ``--collection opensource_movies``

``--gp``
``--collection-podcast``

  a shortcut for ``--collection podcast``
  
``--gs``
``--collection-software``

  a shortcut for ``--collection open_source_software``
  
``--gt``
``--collection-text``

  a shortcut for ``--collection opensource``

``--ta``
``--mediatype-audio``

  a shortcut for ``--mediatype audio``

``--ti``
``--mediatype-image``

  a shortcut for ``--mediatype image``

``--tm``
``--mediatype-movie``

  a shortcut for ``--mediatype movies``

``--tt``
``--mediatype-text``

  a shortcut for ``--mediatype text``

Command Options - metadata placeholders
.......................................

.. note::

  Metada order rules apply for placeholders as well.
  
  If a placeholder option and its counterpart are both provided on cl, only the last occurence is taken into account.

``--placeholder-creator``
``-C``

  This option is basically a shortcut for the ``--creator ""`` option.

  Use a placeholder in ``new`` mode:
  
  * when the initial metadata that is written to a ``<csv-file>`` is for an existing page that already has a creator
  * subsequent appends of metadata are for new pages that need a ``creator`` column to be available in the ``<csv-file>``
  * then a placeholder just inserts the ``creator`` column-header and empty fields in the ``creator`` column for initial metadata
  
  Use a placeholder in ``append`` mode:
  
  * when an existing ``<csv-file>`` already contains a ``creator`` column
  * the data to be appended to the ``<csv-file>`` is for an existing page that already has a creator
  * then the placeholder just inserts empty fields in the ``creator`` column for the metadata to be appended
  
  In ``template`` mode, the placeholder causes the ``creator`` key to be included in generated template data.

``--placeholder-collection``
``-G``

  This option is basically a shortcut for the ``--collection ""`` option.

  Use a placeholder in ``new`` mode:
  
  * when the initial metadata that is written to the ``<csv-file>`` is for an existing page that is already part of a collection
  * subsequent appends of metadata are for new pages that needs a ``collection`` column to be available in the ``<csv-file>``
  * then the placeholder just inserts the ``collection`` column-header and empty fields in the ``collection`` column for initial metadata
  
  Use a placeholder in ``append`` mode:
  
  * when an existing ``<csv-file>`` already contains a ``collection`` column
  * the data to be appended to the ``<csv-file>`` is for an existing page that is already part of a collection
  * then the placeholder just inserts an empty fields in the ``collection`` column for the metadata to be appended
  
  In ``template`` mode, the placeholder causes the ``collection`` key to be included in generated template data.

``--placeholder-identifier``
``-I``

  In ``template`` mode, the placeholder causes the ``identifier`` key to be included in generated template data.
  
  One might use the placeholder in ``new`` and ``append`` mode as well, but it most likely causes the generated csv-file to be unusable for uploading.
  
``--placeholder-mediatype``
``-M``

  In ``template`` mode, the placeholder causes the ``mediatype`` key to be included in generated template data.
  
  One might use the placeholder in ``new`` and ``append`` mode as well, but it most likely causes the generated csv-file to be unusable for uploading.

.. _ia-upload-read-files-label:

Metadata on Standard Input
..........................

Input Files
...........

At least one ``<input-file>`` should be provided:

* if ``<input-file>`` is provided without path, then it must exists in the folder where ``aio-ia-generate-upload-file`` is being called from
* if ``<input-file>`` is provided with a absolte path, then is must exist in the given path
* if ``<input-file>`` is provided with a relative path, then the relative path must be reachable from where ``aio-ia-generate-upload-file`` is being called
* if ``<input-file>`` cannot be found, then the script returns with an error

Files can be provided in different forms on the command line:

* as list of individual files:: 

    audio-1.ogg audio-2.flac audio-3.wav

* as a file-pattern or a list of file-patterns:: 

    *.ogg
    *.ogg *.flac *.wav

* a combination of individual files and file-patterns::

    audio-1.ogg audio-2.flac audio-3.wav ../recordings/*.ogg

All ``<input-files>`` should match the used ``--mediatype``, ie.: if ``--mediatype`` is ``audio``, then ``<input-files>`` should be audio-files as well (no matter what kind of audio).

All ``<input-files>`` should match the used ``--collection``, ie.: if ``--collection`` is ``opensource_audio``, then ``<input-files>`` should be audio-files as well.

Allthough, it is possible to upload ``<input-files>`` with non-matching ``<mediatypes>`` to a page in a ``<collection>``, ie.: if ``--collection`` is ``opensource_audio``, one can also upload an image to a page, alongside audio-files. That image will be used as cover-image on that page.

.. note::

  ``aio-ia-generate-upload-file`` does not verify that ``<input-files>`` do match a given ``--mediatype`` or do fit into a given ``--collection``.

.. _ia-upload-write-output-label:

Output File
...........

All provided ``<input-files>`` and given ``<metadata>`` are transformed and written to a ``<csv-file>``. 

Default Output File
+++++++++++++++++++

If no ``--output-file`` option is used, then the name of a default ``<csv-file>`` is generated:

  Naming scheme::

    <identifier>-ia-upload.csv

  where ``<identifier>`` has to be provided by the ``--identifier <identifier>`` option

If the same ``<identifier>`` and ``new`` mode are used in several calls of this script:

  * generated column-headers and column-data will overwrite content of an existing ``<csv-file>``
  * or a new ``<csv-file>`` will be created, if not yet existing
  * in both cases, column-headers and column-data will be written to the default ``<csv-file>``

If the same ``<identifier>`` and ``append`` mode are used in several calls of this script:

  * generated column data will be appended to an existing ``<csv-file>``
  * or a new ``<csv-file>`` will be created once, if not existing
  * in both cases, no column-headers will be written to the ``<csv-file>``
  
If different ``<identifiers>`` and ``append`` mode are used in several calls of this script:

  * different ``<csv-file>`` names are generated and column data will be appended to those files
  * or new ``<csv-files>`` will be created, if not existing
  * in both cases, no column-headers will be written to the ``<csv-file>``

Custom Output File
++++++++++++++++++
  
If the ``--output-file <csv-file>`` option is used, then the custom ``<csv-file>`` is used

  * no default ``<csv-file>`` name is being generated
  * therefore ``--identifier`` option is not taken into account for default ``<csv-file>`` name generation

If ``new`` mode is used in several calls of this script:

  * generated column headers and data will overwrite existing content of the custom ``<csv-file>``
  * or the custom ``<csv-file>`` will be created, if not yet existing
  * in both cases, column-headers and column-data will be written to the custom ``<csv-file>``

If ``append`` mode is used in several calls of this script:

  * generated column data will be appended to the given ``<csv-file>``
  * or the given ``<csv-file>`` will be created, if not yet existing
  * in both cases, no column-headers will be written to the custom ``<csv-file>``

Generated Csv-Content
+++++++++++++++++++++
  
The generated content has the following csv-format, as expected by the `ia tool`_ ::

    identifier  ,mediatype  ,collection  ,creator  ,file
    <identifier>,<mediatype>,<collection>,<creator>,<input-file-1>
    <identifier>,<mediatype>,<collection>,<creator>,<input-file-2>
    ...
    <identifier>,<mediatype>,<collection>,<creator>,<input-file-x>

The order of column-headers and data currently depends on the order of metadata given on cl and/or standard-input.

1. if metadata is provided on stdin:

  * metadata on stdin is always read before metadata on cl
  * the order of metadata on stdin defines the initial order of csv-headers and columns

2. if metadata is provided on cl but no metadata is avialable on stdin

  * the order of metadata on cl defines the initial order of csv-headers and columns
  
3. if metadata is provided on cl and on stdin

  * if metadata on cl exists also on stdin, then cl-data overwrites corresponding stdin-data.
    the initial order of csv-headers and columns remains unchanged.
  * if metadata on cl does not exist on stdin, then for each new cl-metadata a new csv-column is appended to the initial csv-columns.
  * metadata on cl has always preceedence over metadata on stdin. this means that cl-metadata overwrites corresponding metadata read from stdin.

Pifalls
.......
    
In ``append`` mode, one can mix up the csv-column order if metadata is provided via stdin and cl:
    
* usually, given metadata is supposed to be appended to an existing ``<csv-file>``
* an existing ``<csv-file>`` defines the order of csv-headers and csv-columns
* when passing metadata in ``append`` mode, that order must be preserved

If metadata order is not preserved:

* metadata values might be written to the wrong columns in ``<csv-file>``

If not all metadata is provided:

* the final ``file`` column content is shifted to a column to its left

This might cause several failures during upload:

* complete failure, as an invalid ``<csv-file>`` has been generated with incomplete columns
* complete failure, if ``mediatype`` column has been mixed up
* upload might take place to the wrong page or collection if ``identifier`` column has been mixed up
* wrong page settings if upload succeeded but ``creator`` column has been mixed up

In order to mitigate chances to break the ``<csv-file>``:
    
* always check the existing ``<csv-file>`` for its metadata-order
* pass metadata in ``append`` mode in that order
* use placeholder-options where necessary in order prevent that metadata is written to the wrong columns
* consider metadata on stdin as a common dataset that should be always written in different calls to this script, therefore always pass common metadata to stdin
* mainly use metadata on cl to overwrite metadata on stdin
    
Examples
........

Metadata Is Only Provided On Cl
+++++++++++++++++++++++++++++++

::

  # provide 4 metadata items on cl, including the mandatory identifier
  # this is the initial csv-column order
  aio-ia-generate-upload-file new \
    --identifier myid \
    --creator me  \
    --collection podcast \
    --mediatype audio \
    podcast-1.ogg podcast-2.ogg

Generated csv-content::

  identifier  ,creator  ,collection  ,mediatype  ,file
  myid        ,me       ,podcast     ,audio      ,path/to/podcast-1.ogg
  myid        ,me       ,podcast     ,audio      ,path/to/podcast-2.ogg

Metadata Is Only Provided On Stdin
++++++++++++++++++++++++++++++++++

::

  # provides 4 metadata items on stdin, including the mandatory identifier
  # this is the initial csv-column order
  aio-ia-generate-upload-file new podcast-1.ogg podcast-2.ogg <<EOF
    creator me
    collection podcast
    mediatype audio
    identifier myid
  EOF

Generated csv-content::

  creator ,collection ,mediatype ,identifier ,file
  me      ,podcast    ,audio     ,myid       ,path/to/podcast-1.ogg
  me      ,podcast    ,audio     ,myid       ,path/to/podcast-2.ogg

::

  # provides 4 metadata items via a file to stdin, including the mandatory identifier
  # this is the initial csv-column order
  cat metadata.txt
  >creator me
  >collection podcast
  >mediatype audio
  >identifier myid

  aio-ia-generate-upload-file new podcast-1.ogg podcast-2.ogg <metadata.txt

Generated csv-content::

  creator ,collection ,mediatype ,identifier ,file
  me      ,podcast    ,audio     ,myid       ,path/to/podcast-1.ogg
  me      ,podcast    ,audio     ,myid       ,path/to/podcast-2.ogg
  

Metadata Is Provided On Cl And Stdin
++++++++++++++++++++++++++++++++++++

::

  # only contains two metadata pairs, without identifier
  # this is the initial csv-column order
  cat metadata.txt
  >collection podcast
  >mediatype audio
  
  # append creator and identifier from cl to initial csv-columns
  aio-ia-generate-upload-file new \
    --creator anotherme \
    --identifier anotherid \
    podcast-1.ogg podcast-2.ogg <metadata.txt

Generated csv-content::

  collection ,mediatype ,identifier ,creator   ,file
  podcast    ,audio     ,anotherid  ,anotherme ,path/to/podcast-1.ogg
  podcast    ,audio     ,anotherid  ,anotherme ,path/to/podcast-2.ogg

Metadata And Placeholders Are provided on Cl and Stdin
++++++++++++++++++++++++++++++++++++++++++++++++++++++

::

  # only contains two metadata pairs, without identifier
  # this is the initial csv-column order
  cat metadata.txt
  >collection podcast
  >mediatype audio
  
  # append creator and identifier from cl to initial csv-columns
  # default csv-file is created => anotherid-ia-upload.csv
  aio-ia-generate-upload-file new \
    --creator anotherme \
    --identifier anotherid \
    podcast-1.ogg podcast-2.ogg <metadata.txt

Generated csv-content::

  collection ,mediatype ,identifier ,creator   ,file
  podcast    ,audio     ,anotherid  ,anotherme ,path/to/podcast-1.ogg
  podcast    ,audio     ,anotherid  ,anotherme ,path/to/podcast-2.ogg

::

  # append to anotherid-ia-upload.csv
  # use placeholders for creator and collection
  # pass options on cl in the order defined by anotherid-ia-upload.csv
  aio-ia-generate-upload-file append \
    --placeholder-collection \
    --mediatype image \
    --identifier anotherid \
    --placeholder-creator \
    cover.jpg

Generated csv-content::

  collection ,mediatype ,identifier ,creator   ,file
  podcast    ,audio     ,anotherid  ,anotherme ,path/to/podcast-1.ogg
  podcast    ,audio     ,anotherid  ,anotherme ,path/to/podcast-2.ogg
             ,audio     ,anotherid  ,          ,path/to/cover.jpg

Append To An Existing Csv-File
++++++++++++++++++++++++++++++

::

  # append to this file
  # column-headers define the initial column-order
  cat metadata.csv
  >collection ,mediatype ,identifier ,creator   ,file
  >podcast    ,audio     ,anotherid  ,anotherme ,path/to/podcast-1.ogg
  >podcast    ,audio     ,anotherid  ,anotherme ,path/to/podcast-2.ogg
  
  # append new metadata for an existing page
  # we only need mediatype and identifier
  aio-ia-generate-upload-file append --output-file metadata.csv \
    --placeholder-collection \
    --mediatype image \
    --identifier xxx \
    --placeholder-creator \
    cover.jpg
    
  # append new metadata for yet another existing pages
  # we only need mediatype and identifier
  aio-ia-generate-upload-file append --output-file metadata.csv \
    --G \
    --tt \
    -i yyy \
    --C \
    book-1.pdf book-2.pdf

Generated csv-content::

  collection ,mediatype ,identifier ,creator   ,file
  podcast    ,audio     ,anotherid  ,anotherme ,path/to/podcast-1.ogg
  podcast    ,audio     ,anotherid  ,anotherme ,path/to/podcast-2.ogg
             ,image     ,xxx        ,          ,path/to/cover.jpg
             ,text      ,yyy        ,          ,path/to/book-1.ogg
             ,text      ,yyy        ,          ,path/to/book-2.ogg

Examples - Broken
.................
  
Metadata In Custom Csv-File Is Mixed Up After Append
++++++++++++++++++++++++++++++++++++++++++++++++++++

::

  # only contains two metadata pairs, without identifier
  # this is the initial csv-column order
  cat metadata.txt
  >mediatype audio
  >creator me
  
  # create or overwrite a custom csv-file => metadata.csv
  # append collection and identifier from cl to initial csv-column order
  aio-ia-generate-upload-file new --output-file metadata.csv \
    --collection podcast \
    --identifier someid \
    podcast-1.ogg podcast-2.ogg <metadata.txt

New csv-file content::

  mediatype  ,creator   ,collection ,identifier ,file
  audio      ,me        ,podcast    ,someid     ,path/to/podcast-1.ogg
  audio      ,me        ,podcast    ,someid     ,path/to/podcast-2.ogg

::

  # append to csv-file => metadata.csv
  # now, order of collection and identifier is swapped on cl
  # mediatype on cl overwrites mediatype from metadata.txt
  aio-ia-generate-upload-file append --output-file metadata.csv \
    --identifier anotherid \
    --collection opensource \
    --mediatype text \
    book-1.pdf book-2.pdf <metadata.txt

Extended csv-file with swapped column content for pdf-files::

  mediatype  ,creator   ,collection ,identifier ,file
  audio      ,me        ,podcast    ,someid     ,path/to/podcast-1.ogg
  audio      ,me        ,podcast    ,someid     ,path/to/podcast-2.ogg
  text       ,me        ,anotherid  ,opensource ,path/to/book-1.pdf
  text       ,me        ,anotherid  ,opensource ,path/to/book-2.pdf

Metadata In Default Csv-File Is Mixed Up After Append
+++++++++++++++++++++++++++++++++++++++++++++++++++++

::

  # only contains two metadata pairs, identifier and collection
  # this is the initial csv-column order
  cat metadata.txt
  >identifier someid
  >collection opensource_audio

  # create or overwrite a default csv-file => someid-ia-upload.csv
  # append mediatype and creator from cl to initial csv-column order
  aio-ia-generate-upload-file new \
    --media-type audio \
    --creator me \
    audio-1.ogg audio-2.ogg <metadata.txt

New csv-file content::

  identifier ,collection        ,mediatype  ,creator   ,file
  someid     ,opensource_audio  ,audio      ,me        ,path/to/audio-1.ogg
  someid     ,opensource_audio  ,audio      ,me        ,path/to/audio-2.ogg

::

  # append to default csv-file => someid-ia-upload.csv
  # only mediatype is provided on cl
  # creator is not given => file-colunm is written into creator column
  aio-ia-generate-upload-file append \
    --media-type image \
    cover.jpg <metadata.txt

Extended csv-file with column content for cover-image::

  identifier ,collection        ,mediatype  ,creator            ,file
  someid     ,opensource_audio  ,audio      ,me                 ,path/to/audio-1.ogg
  someid     ,opensource_audio  ,audio      ,me                 ,path/to/audio-2.ogg
  someid     ,opensource_audio  ,image      ,path/to/cover.jpg
  
Generate And Use A Metadata Template
++++++++++++++++++++++++++++++++++++

::

  aio-ia-generate-upload-file template --creator me --ma --ca | tee metadata.txt
  >creator me
  >mediatype audio
  >collection opensource_audio

  # generate csv-file => testitem-ia-upload.csv
  aio-ia-generate-upload-file new -i testitem audio-1.ogg audio-2.ogg audio-3.flac <metadata
  
New csv-file content::

   creator  ,mediatype  ,collection       ,identifier ,file
   me       ,audio      ,opensource_audio ,testitem   ,path/to/audio-1.ogg
   me       ,audio      ,opensource_audio ,testitem   ,path/to/audio-2.ogg
   me       ,audio      ,opensource_audio ,testitem   ,path/to/audio-3.flac

.. _ia-upload-files-label:

Upload To Archive.org
.....................

.. note::

  In order to upload files to a page at `archive.org`_, you need to :ref:`overview-ia-prepare-label`.

Generate a ``csv-file`` and upload files to a page like this::

  # all flac files in current directory are supposed to be uploaded to the 'my-item' page at archive.org
  # the generated csv-file is therefore 'my-item-ia-upload.csv'
  aio-ia-generate-upload-file --identifier my-item --creator me *.flac
  
  # upload to new or existing page at archive.org with ia
  ia upload --spreadsheet my-item-ia-upload.csv

The created or updated page can be accessed via the url:

  * ``https://archive.org/details/my-item``

.. _archive.org: https://archive.org

.. _ia tool: https://archive.org/services/docs/api/internetarchive/cli.html