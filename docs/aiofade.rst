.. highlight:: bash

aio-fade
========

Fade the begin and/or end of an audio-file.
::

  aio-fade s|settings

  aio-fade [-d|--dry-run]
           [-q|--quality <value>]
           [-f|--force-overwrite]
           i|in
             [--di <duration-sec>]
             [--ci <curve-type>]
             <input-file> [ ... <input-file-x>]

  aio-fade [-d|--dry-run]
           [-q|--quality <value>]
           [-f|--force-overwrite]
           o|out
             [--do <duration-sec>]
             [--co <curve-type>]
             <input-file> [ ... <input-file-x>]

  aio-fade [-d|--dry-run]
           [-q|--quality <value>]
           [-f|--force-overwrite]
           io|inout
             [--di <duration-sec>] [--do <duration-sec>]
             [--ci <curve-type>] [--co <curve-type>]
             <input-file> [ ... <input-file-x>]

Global Options
..............

``--dry-run``
``-d``

  Just show what would happen. No ``<output-file>`` is generated. Is ignored if ``settings`` is used.

``--force-overwrite``
``-f``

  Overwrite existing ``<output-file>``. Is ignored if ``settings`` is used.

``--quality <value>``
``-q <value>``

  Set the audio quality for the faded ``<output-file>``. ``<value>`` is usually a positive integer value. Is ignored if ``settings`` is used.
  
  If a quality setting is provided, all ``<input-files>`` should have been encoded with the same audio-codec. Otherwise, the quality value has different meanings for different codecs.
  
  If no quality setting is provided, then ``<output-files>`` are encoded with the default settings (those used by ``aio-fade`` or ``ffmpeg``).

.. note::
  
  Each compressed audio-codec uses its own value range. Please take a look at the ffmpeg documentation for the available codecs.

    https://trac.ffmpeg.org/wiki/Encode/HighQualityAudio

Commands
........

``settings``
``s``

  Print the default quality settings used by ``aio-fade`` for various compressed audio codecs.

``in [options] <input-file-1> ... <input-file-x>``
``i [options] <input-file-1> ... <input-file-x>``

  Fade-in all ``<input-files>``. A fade-in fades audio from silence to audio-level at the begin of an audio file.

  * Fade-in position starts at ``0s``
  * Fade-in duration is set via ``--duration-in-sec``
  * Fade-in curve/envelope is set via ``--curve-in``

``out [options] <input-file-1> ... <input-file-x>``
``o [options] <input-file-1> ... <input-file-x>``

  Fade-out all ``<input-files>``. A Fade-out fades audio from audio-level to silence at the end of an audio file.

  * Fade-out position starts at (audio-length - fade-out-duration)
  * Fade-out duration is set via ``--duration-out-sec``
  * fade-out curve/envelope is set via ``--curve-out``
  
``inout [options] <input-file-1> ... <input-file-x>``
``io [options] <input-file-1> ... <input-file-x>``

  Apply fade-in and fade-out to begin and end of all ``<input-files>``.

  * All options of ``in`` and ``out`` commands can be used.

Command specific options
........................

``--duration-in-sec <seconds>``
``--di <seconds>``
  
  Fade-in duration in seconds. The given value may also contain a milli-second part and should be larger ``0.000``.

  default [``0.01``] (10 milliseconds)

``--duration-out-sec``
``--do``

  Fade-out duration in seconds. The given value may also contain a milli-second part and should be larger ``0.000``.

  default [``0.01``] (10 milliseconds)

``--curve-in``
``--ci``

  Fade-in curve. Defines how the audio amplitude is faded-in.

  For accepted curve types, see

    * https://ffmpeg.org/ffmpeg-filters.html#afade

  default [``tri``] (linear fade)

``--curve-out``
``--co``

  Fade-out curve. Defines how the audio amplitude is faded-out.

  For accepted curve types, see

    * https://ffmpeg.org/ffmpeg-filters.html#afade

  default [``tri``] (linear fade)

Input files
...........

At least one ``<input-file>`` is expected for any of the ``in``, ``out`` or ``inout`` commands.

If more than one ``<input-file>`` is provided, then audio-codecs, codec-quality and number of channels can differ.

Output files
............

See :ref:`output-file-format-label` for more details.
