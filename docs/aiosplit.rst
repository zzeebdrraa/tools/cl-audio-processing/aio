.. highlight:: bash

aio-split
=========

Split a audio file into several pieces.
::

  aio-split status
  
  aio-split [-f|--force-overwrite] 
            at <timestamp> [at <timestamp> ...] <input-file>

  aio-split [-f|--force-overwrite]
            stdin

Commands
........

``status``
``s``

  Prints status of script internals.

``at <timestamp> <input-file>``
``a <timestamp> <input-file>``

  Split ``<input-file>`` at ``<timestamp>``. Results in two ``<output-files>``, the left-hand-side of the ``<timestamp>``, and the right hand side.

``at <timestamp> at <timestamp> ... <input-file>``
``a <timestamp> a <timestamp> ... <input-file>``
  
  ``at <timestamp>`` is given more than once. ``<input-file>`` is split into ``num(at) + 1`` parts. ``<timestamps>`` do not need to be ordered, as ``aio-split`` internally sorts all given ``<timestamps>`` chronologically.

Global Options
..............

``--force-overwrite``
``-f``

  Overwrite existing ``<output-files>``. Is ignored if ``status`` is used.

``<input-file>``

  The audio-file to split.
    
  Must be provided if ``at`` is used. Only one ``<input-file>`` can be given.
    
  Optional, if ``stdin`` is used.

Stdin Format
............

If ``<stdin>`` is given, then one or more lines on stdin can be formated like this::

  # split <input-file> at <timestamp>
  <input-file> <at-timestamp>

Examples::

  # split one file at several positions
  aio-split stdin <<EOF
    audio-1.ogg 10.45
    audio-1.ogg 15
    audio-1.ogg 20
  EOF

  # split several files at several positions
  aio-split stdin <<EOF
    audio-1.ogg 10.45
    audio-1.ogg 15
    audio-1.ogg 20
    audio-2.ogg 1:20
    audio-2.ogg 5:00
    audio-2.ogg 10:00
  EOF

  # with a here-string, only one line can be provided
  aio-split stdin <<<'audio-1.ogg 10:45'

If ``<stdin> <input-file>`` is given, then one or more lines on stdin can be formated like this::

  # cut <input-file> from <timestamp> to <timestamp>
  <at-timestamp>

Examples::

  # with a here-doc, several line can be provided

  # split one file at several positions
  aio-split stdin audio-1.ogg <<EOF
    10.45
    15
    20
  EOF

  # with a here-string, only one line can be provided
  aio-split stdin audio-1.ogg <<<'10:45'

The wiki shows internals about `providing data via stdin`_.

Environment Variables
.....................

``ENV_AIO_SPLIT_CUT_BIN_PATH``

  Can be used to set a custom path for the internally used ``aio-cut`` script.

``ENV_AIO_CUT_TS_TO_SEC_BIN_PATH``

  The environment variable used by ``aio-cut``. See :ref:`envvar-cut-label`.
  
Use the ``status`` command to verify that envvars are set correctly.

Timestamps
..........

See :ref:`timestamp-format-label` for examples on how to write timestamps.

Output files
............

See :ref:`output-file-format-label` for more details.

.. _providing data via stdin: https://gitlab.com/zzeebdrraa/tools/cl-audio-processing/aio/-/wikis/Script-Design#options-from-stdin