.. highlight:: bash

aio-concat
==========

Concatenate several ``<input-files>`` into one ``<output-file>``.
::

  aio-concat [-f|--force-overwrite]
             [-o|--output-file <output-file>]
             <input-file-1> <input-file-2> [... <input-file-x>]

Options
.......

``--force-overwrite``
``-f``

  Overwrite existing ``<output-file>``.
  
``--output-file <output-file>``
``-o <output-file>``

  Write result to ``<output-file>`` instead of generating the name of the resulting file.

``<input-file-1> <input-file-2> [... <input-file-x>]``

  The audio-files to concatenate.
  
  At least two ``<input-files>`` must be provided. ``<input-files>`` should be encoded wit the same codec and codec-parameters.

Output files
............

See :ref:`output-file-format-label` for more details.

About glitches
..............

Depending on how the audio of each ``<input-file>`` has been cut at the begin and end of each file, glitches might occure in ``<output-file>``, at the positions where two files have been glued together. So, always check the generated ``<output-file>``.

In case of audible glitches, you can try to fade each <input-file> at its begin and end to guarantee that the audio amplitute has a value of ``0`` at the file begin and end. A short fade duration should be used so that one cannot hear the faded parts::

  # apply fade-in and fade-out to each input-file. fade-in and fade-out duration is 1ms
  aio-fade inout --di 0.001 --do 0.001 audio-1.ogg audio-2.ogg audio-3.ogg
  # now concatenate the faded version
  aio-concat fade-inout-audio-1.ogg fade-inout-audio-2.ogg fade-inout-audio-3.ogg