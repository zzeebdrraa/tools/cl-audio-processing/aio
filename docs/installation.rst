Installation
============

Dependencies Overview
---------------------

Mandatory Dependencies
~~~~~~~~~~~~~~~~~~~~~~

* ``bash`` version ``4.0.0`` or higher
* ``ffmpeg`` and ``ffprobe`` version ``4.2.3`` or higher, with the following filters compiled in:
  ``afade``, ``loudnorm``, ``silencedetect``, ``volume``, ``volumedetect``, ``concat``
* ``realpath``, ``bc``, ``sort``

Optional Dependencies
~~~~~~~~~~~~~~~~~~~~~

If not installed, some ``aio`` scripts will provide less functionality.

* ``id3v2``, ``metaflac``, ``vorbis-comment`` for audio metadata tagging.
  If not installed, ``aio-metdata-set`` can be adjusted to use ``ffmpeg`` as tagging tool for supported audio metadata standards.

* ``bats``, ``mediainfo``, ``opusinfo`` for running ``unit-tests``.
  Only necessary when testing ``aio`` script with the ``bats`` framework, but not needed when using ``aio`` scripts.

* ``hexdump``, ``eyeD3`` for running ``unit-tests`` in ``debug mode``.
  Only necessary when debugging unit-tests, but not needed when using ``aio`` scripts.

Third-Party Tools
~~~~~~~~~~~~~~~~~

* ``ia`` for file uploading to `archive.org`_.

  ``ia`` is purely optional and only necessary if one uses `archive.org`_.

Install Dependencies
--------------------

Install dependencies via packet manager
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. note::

  We use ``apt-get`` but you Linux distribution might use another packet-manager.

::

  # ffmpeg package usually includes ffprobe. version must be 4.2.3 or higher
  sudo apt-get install ffmpeg realpath bc sort

  # metadata tagging packages for aio-metadata-scripts
  sudo apt-get install id3v2 flac vorbis-comment

  # mandatory packages used by unit tests
  sudo apt-get install mediainfo opustools

  # optional packages used by unit tests in debug mode
  sudo apt-get install eyeD3 hexdump

  # bats and aio are downloaded with git
  sudo apt-get install git

  # optional, ia needs python and is downloaded with wget
  sudo apt-get install python3 wget

.. _install-bats-label:

Install Bats from repository
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Detailed install instructions for ``bats``:

* https:/bats-core.readthedocs.io/en/stable/installation.html

::

  # clone bats to your local machine. this creates a folder named bats-core
  git clone https://github.com/bats-core/bats-core.git

  # go to the bats-core repository folder
  cd bats-core

  # run the install script. bats will be installed to /usr/local/bin folder
  sudo ./install.sh /usr/local

  # OPTIONAL: delete the bats-core folder

  cd ..
  rm -rf bats-core

  # test if bats is available. this should show the version info of bats
  bats --version

Install ia
~~~~~~~~~~

.. note::

   ``ia`` is purely optional. No need to install if you do not use `archive.org`_.

Detailed installation instructions for ``ia``:

* https://archive.org/services/docs/api/internetarchive/installation.html#binaries

::

  # download ia and install system-wide (or to any path in $PATH variable).
  wget https://archive.org/download/ia-pex/ia
  chmod +x ia

  # move ia to /usr/local/bin folder or to any other custom location like ${HOME}/bin
  sudo mv ia /usr/local/bin/

  # test if ia is available. this should show the version info of ia.
  ia --version

Once installed, ``ia`` needs to be configured, as `described in the wiki`_.

Install aio
-----------

Clone the aio repository on your local machine. This creates a folder named aio::

  git clone https://gitlab.com/zzeebdrraa/tools/cl-audio-processing/aio.git

Install ``aio`` system-wide to ``/usr/local/bin``::

  # go to the local aio repository folder
  cd aio

  # configure system-wide installation to /usr/local/bin
  ./configure

  # install all scripts
  sudo make install

Install ``aio`` at a custom location::

  # go to the local aio repository folder
  cd aio

  # here we want a installation to the bin folder in your home directory
  ./configure --prefix ${HOME}

  # install all scripts too  ${HOME}/bin
  make install

In order to use aio-scripts after installation, you need to add the installation directory to your ``PATH`` environment variable::

  export PATH=${HOME}/bin:${PATH}

Exporting ``PATH`` like that has the drawback that once you open a new shell or restart your computer, the changes in ``PATH`` will be gone.

Therefore better persist changes. How this is done is explained in detail in the following article

* https://opensource.com/article/17/6/set-path-linux


Uninstall aio
-------------

::

  # go to the local aio repository folder
  cd aio

  # remove all aio files from the installation directory
  make uninstall

After uninstalling ``aio`` you might want to remove the installation directory from your ``PATH`` environment variable as well, if it has been added previsouly.

Run script tests
----------------

.. note:: 

  ``bats`` must have been installed for running script-tests. See :ref:`install-bats-label` for install instructions.

Run all tests::

  # go to the local aio repository folder
  cd aio

  # run all tests. none of them should fail.
  make check

More examples for ``aio`` script testing can be found `in the wiki`_.

.. _archive.org: https://archive.org
.. _in the wiki: https://gitlab.com/zzeebdrraa/tools/cl-audio-processing/aio/-/wikis/Unit-Tests
.. _described in the wiki: https://gitlab.com/zzeebdrraa/tools/cl-audio-processing/aio/-/wikis/AIO-Archive.org