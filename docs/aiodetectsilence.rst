.. highlight:: bash

aio-detect-silence
==================

Detect sections in ``<input-file>`` that contain silence and print a list of detected sections with their duration, begin and end times in seconds.

A silence-section is detected if the audio amplitute in ``<input-files>`` drops below a given ``<treshold>`` for at least a given ``<duration>``, or longer.

::

  aio-detect-silence s|status

  aio-detect-silence [--ts|--as-timestamp]
                     [-t|--treshold <-db>] [-d|--duration <sec>]
                     [--ab|--at-begin-only] [--ae|--at-end-only]
                     <input-file> [... <input-file-x>]

Environment Variables
.....................

``ENV_AIO_DETECT_SILENCE_SEC_TO_TS_BIN_PATH``

  This envvar can be used to set a custom path for the internally used ``aio-sec-to-ts`` script.
  
  Use the ``status`` command to verify that envvars are set correctly.

Global options
..............

``--as-timestamp``
``--ts``

  Print position and duration of detected silence-sections as ``timestamps`` instead of ``seconds``.

``--at-begin-only``
``--ab``

  Only print position and duration of a detected silence-section at the begin of the ``<input-files>``, if any.

  Can be combined with ``--ae`` flag in order to detect the parts to be stripped from file begin and end.

``--at-end-only``
``--ae``

  Only print position and duration of a detected silence-section at the end of the ``<input-files>``, if any.

  Can be combined with ``--ab`` flag in order to detect the parts to be stripped from file begin and end.

``--treshold <db-value>``
``-t <db-value>``

  Silence is indicated, if the audio amplitute in ``<input-files>`` drops below the given ``<db-value>`` treshold.

  A treshold ``<db-value>`` ranges from a negative db-value to zero:
  
    ``<db-value> <= 0``.
  
  A treshold value can be given as integer or float value without ``db`` unit.

  **Default:** ``-35`` (-35db)

``-d <sec>``
``--duration <sec>``

  The duration defines the minimum length of an audio section before it is considered as silence.

  * a duration value is given as seconds and may contain a millisecond part: ie. ``0.5`` for 500 milliseconds
  * a duration value ranges from 0 to a positive integer or float value: ``<sec> >= 0.000``
  * a duration value longer than 1 minute (60 seconds), has to be passed in seconds as well: ie. ``120`` for two minutes.

  **Default:** ``0.5`` (500 millisonds)

Commands
........

``status``
``s``

  Prints status of script internals.

Output
......

``aio-detect-silence`` prints a list of silence sections for any input file to standard-output. Each detected silence section is printed on a individual line.

A single output line is formated as follows, where ``<from>``, ``<to>`` and ``<duration>`` values can be printed in seconds or as timestamps::

  <filename> <from> <to> <duration>

If ``--ab`` has been given, only the ``<duration>`` value of the detected silence section at the begin of the audio will be printed. The ``<duration>`` at file-begin is the same as the ``to`` value:: 

  <filename> <duration-at-begin>

If ``--ae`` has been given, only the ``<from>`` value of the detected silence section at the end of the audio will be printed::

  <filename> <from-at-end>

If ``--ab`` and ``--ae`` has been given, the ``<duration>`` value of the detected silence section at the begin of the audio and the ``<from>`` value of the detected silence section at the end of the audio will be printed::

  <filename> <duration-at-begin> <from-at-end>

If no silence has been detected, nothing is printed to stdout.

Removing silence sections
.........................

If ``--ab`` and ``--ts`` has been given, then the detected silence section can be piped to ``aio-cut`` or ``aio-split``::
  
  # aio-detect-silence may print
  # > audio-1.ogg 5.43
  # > audio-2.ogg 0.96
  # 
  # aio-split then splits both files at the end of the silence sections at file-begin
  #
  # the split results in:
  # - a file that contains the silence part
  # - a file that contains the remaining audio
  #
  aio-detect-silence --ts --ab audio-1.ogg audio-2.ogg | aio-split stdin
  
  # aio-detect-silence may print
  # > audio-1.ogg 5.43
  # > audio-2.ogg 0.96
  # 
  # aio-cut then cuts both files from the end of the silence sections at file-begin
  #
  # the cut results in:
  # - a file without silence at its begin
  #
  aio-detect-silence --ts --ab audio-1.ogg audio-2.ogg | aio-cut stdin

If ``--ae`` and ``--ts`` has been given, then the detected silence section can be piped to ``aio-split``::
  
  # aio-detect-silence may print
  # > audio-1.ogg 125.87
  # > audio-2.ogg 90.13
  # 
  # aio-split then splits both files at the begin of the silence sections at file end
  #
  # the split results in:
  # - a file that contains the audio part
  # - a file that contains the remaining silence
  #
  aio-detect-silence --ts --ae audio-1.ogg audio-2.ogg | aio-split stdin

If ``--ab``, ``--ae`` and ``--ts`` has been given, then the detected silence section can be piped to ``aio-cut`` or ``aio-split``::
  
  # aio-detect-silence may print
  # > audio-1.ogg 5.43 125.87
  # > audio-2.ogg 0.96 90.13
  # 
  # aio-split then splits both files at the end of the silence sections at file-begin 
  # and at the begin of the silence sections at file-end
  #
  # the split results in: 
  # - a file that contains the silence part from file-begin
  # - a file that contains the remaining audio
  # - a file that contains the silence part from file-end
  #
  aio-detect-silence --ts --ab audio-1.ogg audio-2.ogg | aio-split stdin
  
  # aio-detect-silence may print
  # > audio-1.ogg 5.43 125.87
  # > audio-2.ogg 0.96 90.13
  # 
  # aio-cut then cuts both files from the end of the silence sections at file-begin to begin of silence section at file-end
  #
  # the cut results in:
  # - a file without silence at its begin and end
  aio-detect-silence --ts --ab audio-1.ogg audio-2.ogg | aio-cut stdin
  
Performance
...........

Especially when using ``<input-files>`` with a long duration, like a 2 hours long recording, silence detection at file-begin and/or file-end is quite slow.

The reason is, that ``ffmpeg`` is always detecting silence sections in the whole file in the first place. For silence detection at file-begin and file-end, ``aio-detect-silence`` then just filters the ``ffmpeg`` results and tries to find a section that starts at file position ``0`` and a section that ends at file end.

In the future, `there should be a more efficient way to detect silence`_ particularily at file-begin and/or end, without analysing the whole file.

Glitches
........

Some audio files may have a very short amplitude peak at file begin and/or file end. Such peaks currently prevent silence-detection at file begin and/or end.

``aio-detect-silence`` should be able to `detect that situation in the future`_.

.. _there should be a more efficient way to detect silence: https://gitlab.com/zzeebdrraa/tools/cl-audio-processing/aio/-/wikis/To-Be-Implemented#detecting-silence-at-file-end-and-file-begin-and-end
.. _detect that situation in the future: https://gitlab.com/zzeebdrraa/tools/cl-audio-processing/aio/-/wikis/To-Be-Implemented#detecting-silence-at-file-end-and-file-begin-and-end