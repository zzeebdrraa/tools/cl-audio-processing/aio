.. highlight:: bash

aio-mix
=======

Mix all ``<input-files>`` into into a single ``<output-file>``.

::

  aio-mix s|settings

  aio-mix [-d|--dry-run]
          [-f|--force-overwrite]
          [-q|--quality <value>]
          [-o|--output-file <output-file>]
          <input-file-1> <input-file-2> [... <input-file-x>]

  aio-mix [-d|--dry-run]
          [-f|--force-overwrite]
          [-q|--quality <value>]
          [-o|--output-file <output-file>]
          <input-file-1> <input-file-2> [... <input-file-x>]

Global options
..............

``--dry-run``
``-d``

  Just show what would happen. No ``<output-file>`` is generated. Is ignored if ``settings`` is used.

``--force-overwrite``
``-f``

  Overwrite existing ``<output-file>``. Is ignored if ``settings`` is used.

``--quality <value>``
``-q <value>``

  Set the audio quality for the faded ``<output-file>``. ``<value>`` is usually a positive integer value. Is ignored if ``settings`` is used.
  
  If a quality setting is provided, all ``<input-files>`` should have been encoded with the same audio-codec. Otherwise, the quality value has different meanings for different codecs.
  
  If no quality setting is provided, then ``<output-files>`` are encoded with the default settings (those used by ``aio-fade`` or ``ffmpeg``).

  .. note::
  
    Each compressed audio-codec uses its own value range. Please take a look at the ffmpeg documentation for the corresponding codec.

    https://trac.ffmpeg.org/wiki/Encode/HighQualityAudio

``--output-file <output-file>``
``-o <output-file>``

  A custom file-name, where the mixed down audio will be written to.

  * ``<output-file>`` format extension can be of any audio format and must not necessarily be the same as the format(s) of ``<input-files>``
  * ``<output-file>`` cannot be one of the ``<input-files>``
  
Commands
........

``settings``
``s``

  Print the default quality settings used by ``aio-fade`` for various compressed audio codecs.

Input files
...........

At least two ``<input-files>`` are expected as script input.

  * ``<input-files>`` are expected to have the same number of channels, but audio-codecs and codec-quality may differ.
  * ``<input-files>`` may have different lengths. See :ref:`output-file-duration-label` on how the length of the ``<output-file>`` is determined.

Output file
...........

If no filename is explicitly set via ``-o`` option, then the resulting ``<output file>`` will be named as follows::

  <prefix>-<input-file>

where ``<prefix>`` is ``mix`` and ``<input-file>`` is the name of the first ``<input-file>``.

Also see :ref:`output-file-format-label` for more details.

.. _output-file-duration-label:

Output file duration
....................

The duration of ``<output-file>`` is determined by the length of the **first** ``<input-file>``.

If all following ``<input-files>`` have a shorter duration than the first ``<input-file>``, then ``<output-file>`` completely contains all ``<input-files>``.

If one or more of the following ``<input-files>`` have a longer duration than the first ``<input-file>``, then those files will be cut-off at the duration of the first ``<input-file>``. Therefore, always specify the longest ``<input-file>`` as first in the file-list.

Output file codecs
..................

If a ``<output-file>`` is explicitly specified via ``-o`` option, then the audio-codec of the ``<output-file>`` is internally determined as follows:
  
  * via ``<output-file>`` format extension
  * if ``<output-file>`` has no format extension, then the format of the **first** ``<input-file>`` will be automatically appended to ``<output-file>``
  * if ``<output-file>`` has no format extension, and **first** ``<input-file>`` has no format extension, then the audio format is determined by the audio-codec of the first ``<input-file>``.

If no ``<output-file>`` is explicitly specified via ``-o`` option, then the audio-codec of the first ``<input-file>`` will be used.