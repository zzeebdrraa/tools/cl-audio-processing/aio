.. highlight:: bash

aio-ia-generate-metadata-file
=============================

Generate a ``csv-file`` that contains the ``<metadata>`` for updating an existing page at `archive.org`_.

::

  aio-ia-generate-metadata-file [-d|--dry-run]
    <metadata-stdin

Description
...........

In order to write metadata at a page at `archive.org`_, one can use the `ia tool`_. A page at `archive.org`_ contains a collection of media items, like audio or document files, a description for those items, and other data.

For writting metadata at a page, ``ia`` accepts - among others - a ``<csv-file>`` that contains the metadata for that page.

Writing a ``<csv-file>`` manually, especially if it contains many metadata-fields, is error prone, or one needs a spreadsheet software. When just staying on the command line with a simple text editor like ``nano``, it seems easier to write vertically (a list of metadata ``<key> <value>`` pairs, without the need of delimiters) then to write horizontally (columns headers and rows with matching commas).

This is where ``aio-ia-generate-metadata-file`` aims to jump in. It reads a list of metadata ``<key> <value>`` pairs and transforms them to columns to generate a ``<csv-file>``.

Check out :ref:`ia-read-metadata-label` for reading ``<key> <value>`` pairs.

Check out :ref:`ia-write-output-label` for the transformation of metadata into a ``<csv-file>``.

Check out :ref:`ia-set-metadata-label` for using a ``<csv-file>`` with the `ia tool`_.

Global Options
..............

``--dry-run``
``-d`` 

  Don''t write to a file but print generated content to standard-output.

.. _ia-read-metadata-label:
  
Metadadata On Standard-Input
............................

The metadata content for generating a ``<csv-file`` is read from stdin.

Each line on stdin consists of a single ``<key> <value>`` pair:
  
* ``<key>`` and ``<value>`` are separated by one or more whitespaces
* ``<key>`` is the name of a valid archive.org metadata field and is written as column header to the ``<csv-file>``, see :ref:`ia-metadata-spec-label`
* ``<key>`` might also be the name of the extensions understood by this script, see :ref:`aio-metadata-spec-label`
* ``<key>`` must be a single word that does not contain whitespaces or any other special character, except the underscore ``_``
* ``<value>`` is the data that is written to the corresponding column in the ``<csv-file>``
* ``<value>`` may contain whitespaces and special characters, except a comma ``,``. A comma would corrupt the ``<csv-file>`` as it indiciates a new column and the number of column headers would not match anymore.

Each ``<key> <value>`` pair on a line is transformed into a column in the ``<csv-file>``. Input on stdin is expected to look like this::

  identifier <some identifier>
  <ia-key-1> <value-1>
  <ia-key-2> <value-2>
  ...
  <ia-key-x> <value-x>

There are just a few requirements for using ``<key> <value>`` pairs:
  
* at least the ``identifier`` key and its value must be always provided at stdin
* all other ``<keys>`` are optional
* if a ``<key>`` is used it should only be specified once
* if a ``<key>`` is specified more than once, then the value of the last occurence will be used
* the ``subject`` key is an exception and can be provided more than once, as a page might have several subjects/tags/categories
* if the metadata-extension provided by this script are used, then ``subject_file`` can be specified more than once

The wiki shows internals about `providing data via stdin`_.

.. _ia-write-output-label:  

Output
......

Once metadata pairs have been read from stdin, they are transformed and written to a ``<csv-file>``. 
  
The name of the ``<csv-file`` is generated like this::

  <identifier>-ia-metadata.csv

where ``<identifier>`` is taken from the data read from stdin.

The ``<csv-file`` content has the following format, as expected by the `ia tool`_::

  identifier  , <key-name-a>, <key-name-b>, <key-name-c>, ... <key-name-x>
  <identifier>, <data-a>    , <data-b>    , <data-c>    , ... <data-x>
  
where the ``identifier`` column must always be available, while all other keys/columns are optional.

----
    
Lets take the example data from :ref:`ia-read-metadata-label`.

Data on stdin looks like this::

  identifier lac2019proceedings
  date 2019-03-23
  description the text at the page
  subject linux
  subject audio
  subject software
  subject free software

The content of the generated ``<csv-file>`` then look like this::

  identifier        , date      , description         , subject[0], subject[1], subject[2], subject[3], subject[4]
  lac2019proceedings, 2019-03-23, the text at the page, linux     , audio     , software  , 2019      , free software

The generated ``csv-file`` can now be given to the `ia tool`_ to actually :ref:`ia-set-metadata-label`.

.. _ia-metadata-spec-label:

Archive.org Metadata Specification
..................................

.. note::

  In general, all ``<metadata>`` item can be passed to ``aio-ia-generate-metadata-file``.

  The detailed specification for each ``<metadata>`` item can be found at:

  * https://archive.org/services/docs/api/metadata-schema/index.html

Some commonly used ``<metadata>`` items are: ``identifier``, ``date``, ``description``, ``subject``

``identifier``

  * is used to select the page/item to update at `archive.org`_. 
  * is usually the endpoint of the url for a page at `archive.org`_, ie: for https://archive.org/details/lac2019proceedings, the identifier is ``lac2019proceedings``
  * must always be set in order to determine which page to update
  
``date``

  * defines when the items on a page have been created, ie. when a audio recording took place, or an album was released
  * optional, can be set once
  * date format is ``yyyy-mm-dd``
  
``description``

  * contains the full textual description of a page/item
  * may be formated as html
  * optional, can be set once
  * is clumsy to set in a ``csv-file``, especially if description-text is long, multiline, contains html and commas.
  * for an alternative, take a look at :ref:`aio-metadata-spec-label`

``subject``

  * contains a category for a page/item
  * optional, can be set more than once for several categories
  * has a bit clumsy syntax in ``csv-file``, ie columns headers are written like this: ``subject[0], subject[1], subject[2]``
  * for an alternative, take a look at :ref:`aio-metadata-spec-label`

Lets look at a page to see how ``<ia-metadata>`` items would be set:

  * https://archive.org/details/lac2019proceedings
  
If that page has to be updated, then ``<ia-metadata>`` items would be set as follows:

  * identifier: ``lac2019proceedings``
  * date: ``2019-03-23``
  * description: ``the text at the page``
  * subject: ``linux`` ``audio`` ``software`` ``2019`` ``free software``

.. _aio-metadata-spec-label:

Metadata Extension
..................

``aio-ia-generate-metadata`` defines a couple of new ``<keys>`` that can be passed to stdin in addition to the offical archive.org metadata.

``description_file <file>``

  The content of a local ``<file>`` is read line by line in order to generate a single ``description`` string.
  
  Each linebreak in ``<file>`` content is converted into a ``<br/>`` html-tag. This is necessary in order to fit the ``description`` text into a column field in the generated ``<csv-file>``. 

  ``description_file`` may only be used once. If several occurences are encountered, the content of the last occurence will be used.

  ``description_file`` content may contain any character, except a comma ``,``. A comma would corrupt the ``<csv-file>`` as it indiciates a new column and the number of column headers would not match anymore. 
  
  .. note::
  
    In the future, a comma should be automatically converted into a html code when reading ``<file>`` content.
    
    For now, this has to be done manually by using the html code ``&#44;``.
  
----
  
  ``<file>`` content is converted into a ``description`` string like this:

  A file ``description.txt`` contains::
    
    This is a
    
    multiline
    description.
  
  The generated ``description`` column in the ``csv-file`` looks like::

    description
    This is a<br/><br/>multiline<br/>description.<br/>

``subject_file <file>``

  The content of a local ``<file>`` is read line by line in order to generate a list of ``subject`` strings:
  
  * empty lines and lines starting with ``#`` are ignored
  * each line in ``<file>`` is considered as one ``subject`` value
  * a lines may contain one or more words, whitespaces and special characters, except a comma ``,``
    
  Even if ``subject_file`` is provided, all other ``subject`` keys read from stdin are still taken into account.
  
  ``subject_file`` can be specified more than once, ie. to separate ``subjects`` into a set that is commonly used and does not change (often), and a set of ``subjects`` that change more fequently.

----  
  
  The content of ``<file>`` is converted into a list of ``subjects`` like this:
  
  A file ``subjects-common.txt`` contains::
  
    linux
    audio
    free software
  
  A file ``subjects-special.txt`` contains::
  
    2019
    conference
    proceedings

  The generated ``subject`` columns in the ``csv-file`` look like::

    subject[0], subject[1], subject[2],    subject[3], subject[4], subject[5]
    linux,      audio,      free software, 2019,       conference, proceedings       

.. _ia-set-metadata-label:

Update Metadata At Archive.org
..............................

.. note::

  In order to update a page at `archive.org`_, you need to :ref:`overview-ia-prepare-label`.

Generate a ``csv-file`` and update a page like this::

  # metadata.txt contains an identifier 'my-item'.
  # the generated csv-file is therefore 'my-item-ia-metadata.csv'
  aio-ia-generate-metadata-file <metadata.txt

  # update the page at archive.org with ia
  ia metadata --spreadsheet my-item-ia-metadata.csv
  
The modified page can be accessed via the url:

  ``https://archive.org/details/my-item``

.. _archive.org: https://archive.org

.. _ia tool: https://archive.org/services/docs/api/internetarchive/cli.html

.. _providing data via stdin: https://gitlab.com/zzeebdrraa/tools/cl-audio-processing/aio/-/wikis/Script-Design#options-from-stdin