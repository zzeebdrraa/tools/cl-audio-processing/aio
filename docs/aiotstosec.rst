.. highlight:: bash

aio-ts-to-sec
=============

Convert ``<timestamps>`` to ``<seconds>``.

::

  aio-ts-to-sec <timestamp> [... <timestamp>]

  aio-ts-to-sec stdin

Options
.......

``stdin``

  ``<timestamps>`` will be read from stdin:

  * at least one ``<timestamp>`` is expected on ``stdin``
  * one ``<timestamp>`` per line
  * empty lines are ignored
  * lines that only contain spaces (whitespaces, tabs, etc) are ignored
  * lines that start with ``#`` are ignored
  
  If ``stdin`` is given, all ``<timestamps>`` arguments will be ignored.
  
  The wiki shows internals about `providing data via stdin`_.

``<timestamp> [... <timestamp>]``

  At least one ``<timestamp>`` value must be povided.

  If ``stdin`` is given, all ``<timestamps>`` arguments will be ignored.

Output
......

For each given ``<timestamp>`` (either given on cl or via stdin), the converted ``<seconds>`` are printed to standard out, one value per line, in the same order as given ``<timestamps>``.

Timestamp format
................  

  See :ref:`timestamp-format-label` for examples on how to write timestamps.

.. _providing data via stdin: https://gitlab.com/zzeebdrraa/tools/cl-audio-processing/aio/-/wikis/Script-Design#options-from-stdin