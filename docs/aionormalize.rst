.. highlight:: bash

aio-normalize
=============

Normalize audio to a certain level.
::

  aio-normalize s|settings

  aio-normalize [-d|--dry-run]
                [-q|--quality <value>]
                [-f|--force-overwrite]
                p|peak
                  [-l|--peak-level <peak-level-db>]
                  <input-file> [ ... <input-file-x>]

  aio-normalize [-d|--dry-run]
                [-q|--quality <value>]
                [-f|--force-overwrite]
                l|loudness
                  <input-file> [ ... <input-file-x>]

  aio-normalize v|volume
                  <input-file> [ ... <input-file-x>]

Global options
..............

``--dry-run``
``-d``

  Just show what would happen. No ``<output-file>`` is generated. Is ignored if ``settings`` is used.

``--force-overwrite``
``-f``

  Overwrite existing ``<output-file>``. Is ignored if ``settings`` is used.

``--quality <value>``
``-q <value>``

  Set the audio quality for the normalized ``<output-file>``. ``<value>`` is usually a positive integer value. Is ignored if ``settings`` is used.
  
  If a quality setting is provided, all ``<input-files>`` should have been encoded with the same audio-codec. Otherwise, the quality value has different meanings for different codecs.
  
  If no quality setting is provided, then ``<output-files>`` are encoded with the default settings (those used by ``aio-normalize`` or ``ffmpeg``).

  .. note::
  
    Each compressed audio-codec uses its own value range. Please take a look at the ffmpeg documentation for the corresponding codec.

    https://trac.ffmpeg.org/wiki/Encode/HighQualityAudio

Commands
........

``settings``
``s``

  Print the default quality settings used by ``aio-normalize`` for various compressed audio codecs.

``peak <db-value>``
``p <db-value>``

  Normalizes audio to the given ``<db-value>``. ``<db-value>`` is usually ``<= 0``. No ``db`` unit is necessary.
  
  The highest peak in input audio is the reference peak, thus overall audio amplitude is increased or decreased by the difference of the highest audio peak level to the provided peak value.

  * a ``0`` db-value means to change the amplitude of the audio to the maximum possible value
  * a ``-3`` db-value means to change the amplitude of the audio to half of the max value
  * a positive db-value would usually lead to clipped peaks
  
  .. note::
    
    If audio has only a few very high peaks, then the whole audio is amplified only until those peaks reach the desired ``<db-value>``.
    
    In that case, peak-normalization has a lesser effect on audio that has a wide dynamic range of amplitudes.
  
``loudness``
``l``

  .. note::

    Not yet implemented.

``volume``
``v``

  Only show volume infos of ``<input-files>``. No processing takes places.

Output files
............

See :ref:`output-file-format-label` for more details.