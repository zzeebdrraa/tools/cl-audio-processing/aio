.. highlight:: bash

aio-metadata-generate
=====================

Generate audio metadata ``<tag-names>`` for various audio-codecs.

::

  aio-metadata-generate c|common
                          [i3|vc|ff|ri]

  aio-metadata-generate c|common
                          [<tag-name-list>]

  aio-metadata-generate i|individual <num-groups>
                          [i3|vc|ff|ri]

  aio-metadata-generate i|individual <num-groups>
                          [<tag-name-list>]

Commands
........

``common [i3|vc|ff|ri]``
``c [i3|vc|ff|ri]``

  Generates one group of metadata tag-names.
  
  The resulting tag-names are supposed to be used for assigning common metadata  to several audio-files.

  If ``i3`` is given as argument, then a group with default ``id3v2`` tags for ``mp3`` files are generated::

    >TPE1
    >TALB
    >TYER
    >COMM
  
  If ``vc`` is given as argument, then a group with default ``vorbis-comment`` tags for ``vorbis`` audio files are generated::
  
    >ARTIST
    >ALBUM
    >DATE
    >DESCRIPTION
  
  If ``ri`` is given as argument, then a group with default ``riff`` tags for ``wav`` files are generated::
  
    >IART
    >IPRD
    >ICRD
    >ICMT

  If ``ff`` is given as argument, then a group with default ``ffmpeg`` tags are generated::
  
    >artist
    >album
    >date
    >comment
  
  If no argument is given, then the default group of ``id3v2`` tags is generated.

``common <tag-name-a> [... <tag-name-x>]``
``c <tag-name-a> [... <tag-name-x>]``

  Generates one group of metadata tag-names.
  
  The resulting tag-names are supposed to be used for assigning common metadata to several audio-files.
  
  ``<tag-name-a> [... <tag-name-x>]`` is a list of custom ``<tag-names>``:
  
    * at least one ``<tag-name>`` must be provided.
    * ``<tag-name>`` can be taken from :ref:`metadata-standards-label` and should match the codec of the audio-file(s) to be tagged.
    * ``<tag-name>`` can be any other names, except ``i3``, ``vc``, ``ff``, ``ri``. Most likely, tagging of audio-files with arbitrary names will fail.
    * ``<tag-name>`` must be single word, without whitespaces.
  
  If no argument is given, then the default group of ``id3v2`` tags is generated.
  
``individual <num-groups> [i3|vc|ff|ri]``
``i <num-groups> [i3|vc|ff|ri]``

  Generates several groups of metadata tag-names.
  
  The resulting tag-names are supposed to be used for assigning individual metadata groups to individual audio-files. One group per file.

  ``<num-groups>`` defines the number of groups to be generated. Each group is separated by an empty line on standard-output.
  
  if ``i3`` is given as argument, then a each group contains default ``id3v2`` tag-names for ``mp3`` files::

    >TIT2
    >TPE2
    >TRCK
  
  if ``vc`` is given as argument, then a each group contains default ``vorbis-comment`` tag-names for ``vorbis`` files::
  
    >TITLE
    >ALBUM_ARTIST
    >TRACKNUMBER
  
  if ``ri`` is given as argument, then a each group contains default ``riff`` tag-names for ``wav`` files::

    >INAM
    >ITRK

  if ``ff`` is given as argument, then a each group contains default ``ffmpeg`` tag-names::
  
    >title
    >album_artist
    >track
  
  If no argument is given, then ``<num-groups>`` of default ``id3v2`` tags are generated.

``individual <num-groups> <tag-name-a> [... <tag-name-x>]``
``i <num-groups> <tag-a> <tag-name-a> [... <tag-name-x>]``

  Generates several groups of metadata tag-names.
  
  The resulting tag-names are supposed to be used for assigning individual metadata groups to individual audio-files. One group per audio-file.

  ``<num-groups>`` defines the number of groups to be generated. Each group is separated by an empty line on standard-output.
  
  ``<tag-name-a> [... <tag-name-x>]`` is a list of custom ``<tag-names>``:
  
    * at least one ``<tag-name>`` must be provided.
    * ``<tag-name>`` can be taken from :ref:`metadata-standards-label` and should match the codec of the audio-file(s) to be tagged.
    * ``<tag-name>`` can be any other names, except ``i3``, ``vc``, ``ff``, ``ri``. Most likely, tagging of audio-files with arbitrary names will fail.
    * ``<tag-name>`` must be single word, without whitespaces.
  
  If no argument is given, then ``<num-groups>`` of default ``id3v2`` tags are generated.

Output
......

  ``common`` metadata tag-names are written as one group to standard output. A group does not contain empyt lines::
  
    # print the default id3v2 group
    aio-metadata-generate common
    >TPE1
    >TALB
    >TYER
    >COMM
    
    # print the default vorbis comment group
    aio-metadata-generate common vc
    >ARTIST
    >ALBUM
    >DATE
    >DESCRIPTION
    
    # print a custom group, here with a couple of tags supported by ffmpeg
    aio-metadata-generate common date album album_artist genre
    >date
    >album
    >album_artist
    >genre
  
  ``individual`` metadata tag-names are written as several groups to standard output. Each group is separated by one empty line::
  
    # print 3 default id3v2 groups
    aio-metadata-generate individual 3
    >TIT2
    >TPE2
    >TRCK
    >
    >TIT2
    >TPE2
    >TRCK
    >
    >TIT2
    >TPE2
    >TRCK

    # # print 3 default vorbis-comment groups
    aio-metadata-generate individual 3 vc
    >TITLE
    >ALBUM_ARTIST
    >TRACKNUMBER
    >
    >TITLE
    >ALBUM_ARTIST
    >TRACKNUMBER
    >
    >TITLE
    >ALBUM_ARTIST
    >TRACKNUMBER
    
    # print 3 custom groups, here with a couple of tags supported by ffmpeg
    aio-metadata-generate individual 3 title track disc performer
    >title
    >track
    >disc
    >performer
    >
    >title
    >track
    >disc
    >performer
    >
    >title
    >track
    >disc
    >performer

Set metadata
............

The result of ``aio-metadata-generate`` can be used to set metadata in audio-files.

Lets set common metadata for three mp3-files::

  # generate a common group with default id3v2 tag-names
  aio-metadata-generate common > common.txt
  
  # add values to each tag-name
  nano common.txt
  
  # set common metadata for several mp3-files
  aio-metadata-set common audio-1.mp3 audio-2.mp3 audio-3.mp3 <common.txt

Now, lets set individual metadata for each mp3-file::

  # generate the three groups of default id3v2 tag-names
  aio-metadata-generate individual 3 > individual.txt
  
  # add values to each tag-name
  nano individual.txt
  
  # set common metadata for several mp3-files
  aio-metadata-set individual audio-1.mp3 audio-2.mp3 audio-3.mp3 <individual.txt

.. _metadata-standards-label:

Official Metadata standards
...........................

Specification for each supported metadata standard can be found here:

  * ``id3v2`` https://id3.org/id3v2.3.0#Declared_ID3v2_frames
  * ``vorbis-comment`` https://www.xiph.org/vorbis/doc/v-comment.html
  * ``riff`` http://www.robotplanet.dk/audio/wav_meta_data
  * ``ffmpeg`` ``avformat.h:373`` in ffmpeg repository https://github.com/FFmpeg/FFmpeg/blob/master/libavformat/avformat.h