.. highlight:: bash

aio-metadata-transform
======================

``aio-metadata-transform`` helps to assign values from different sources to a list of metadata ``<tag-names>``.

::

  aio-metadata-transform kv|key-value
                           <custom-tag-name> as <tag-name>
                           <custom-tags-file>
                           [s|selector <line-selector-string>]
                        < metadata-file

  aio-metadata-transform c|column
                           <col-num> as <tag-name>
                           <custom-tags-file>
                           [d|delimiter <col-separator-char>]
                        < metadata-file

  aio-metadata-transform n|number
                           <tag-name> [<start-num>] [<stop-num>]
                        < metadata-file

  aio-metadata-transform v|value
                           <string> as <tag-name>
                           [s|select <metadata-tag-index-range>]
                        < metadata-file

  aio-metadata-transform <cmd> <args> ::: <cmd> <args> ::: ... <cmd> <args>
                        < metadata-file

Commands
........

.. note::

  Each ``aio-metadata-transform`` command reads a list of metadata ``<tag-names>`` from standard input.
  
  For details take a look at :ref:`metadata-stdin-label`

.. note::

  ``key-value`` and ``column`` commands additionally read a text-file from where metadata ``<values>`` are extracted. Those values are assigned to the metadata ``<tag-names>`` that are read from standard-input.
  
  For details take a look at :ref:`metadata-file-label`

.. note::

  ``number`` and ``value`` commands directly assign or generate metadata ``<values>`` to the metadata ``<tag-names>`` that are read from standard-input.
  
``key-value <custom-tag-name> as <tag-name> <custom-tags-file> [s|selector <line-selector>]``
``kv <custom-tag-name> as <tag-name> <custom-tags-file> [s|selector <line-selector>]``

  This command does the following:
  
  * read a ``<custom-tags-file>``
  * find all ``<custom-tag-name> <value>`` entries in ``<custom-tags-file>``
  * read metadata from standard-input (ie. metadata generated via ``aio-metadata-generate``)
  * find all ``<tag-name>`` entries in the data read from stdin
  * assign the ``<value>`` of each ``<custom-tag-names>`` entry to the corresponding ``<tag-name>`` entries from stdin
  
  Optionally it is possible to use the ``selector`` option to define a ``<line-selector>``:
  
  * ``<line-selector>`` is a character or word at the begin of a line in ``<custom-tags-file>``
  * ``<line-selector>`` can be a single word or a a list of words
  * ``<line-selector>`` cannot be a regex at the moment. If a regex is used, then it is matched literally
  * ``<line-selector>`` is expected at the very begin of a line
  * if a ``<line-selector>`` has been encountered, then a ``<custom-tag-name> <value>`` entry is expected to follow on the same line
  * a matching line in ``<custom-tags-file>`` is expected to look as follows::
  
    <line-selector> <custom-tag-name> <value>
  
  Data expected on stdin and custom-files is explained in :ref:`metadata-stdin-label` and :ref:`metadata-file-label`.
  
  Also take a look at :ref:`examples-keyvalue-label`.

``column <col-num> as <tag-name> <custom-tags-file> [d|delimiter <col-separator-char>]``
``c <col-num> as <tag-name> <custom-tags-file> [d|delimiter <col-separator-char>]``

  This command does the following:
  
  * read a ``<custom-tags-file>``
  * read column ``<col-num>`` from ``<custom-tags-file>``. ``<col-num>`` 
  * read metadata from standard-input (ie. metadata generated via ``aio-metadata-generate``)
  * find all ``<tag-name>`` entries in the data read from stdin
  * assign all entries of column ``<col-num>`` to the corresponding ``<tag-name>`` entries from stdin, in the order of their occurences.
  
  Optionally it is possible to use the ``delimiter`` option to define a custom ``<col-separator-char>``:
  
  * each column in ``<custom-tags-file>`` is expected to be separated by a ``<col-separator-char>``
  * by default, ``<col-separator-char>`` is set to ``,``, ie. for csv-files
  * ``<col-separator-char>`` can be any character
  * a line in ``<custom-tags-file>`` is expected to look as follows::
  
    <value-column-1> <separator> <value-column-2> <separator> <value-column-3> ...

  Data expected on stdin and custom-files is explained in :ref:`metadata-stdin-label` and :ref:`metadata-file-label`.
  
  Also take a look at :ref:`examples-column-label`.

``number <tag-name> [<start-num>] [<stop-num>]``
``n <tag-name> [<start-num>] [<stop-num>]``

  This command does the following:

  * read metadata from standard-input (ie. metadata generated via ``aio-metadata-generate``)
  * assign a continuous number to the corresponding ``<tag-name>`` entries from stdin
  
  Data expected on stdin is explained in :ref:`metadata-stdin-label`.
  
  Also take a look at :ref:`examples-number-label`.

``value <string> as <tag-name> [s|select <tag-index-range>]``
``v <string> as <tag-name> [s|select <tag-index-range>]``

  This command does the following:

  * read metadata from standard-input (ie. metadata generated via ``aio-metadata-generate``)
  * assign a single ``<string>`` to the all ``<tag-name>`` entries from stdin
  * optionally, specific occurences of ``<tag-name>`` can be selected with the ``select`` option
  
  Data expected on stdin is explained in :ref:`metadata-stdin-label`.
  
  Select option is explained in :ref:`metadata-select-label`.
  
  Also take a look at :ref:`examples-value-label`.
                           
Run Multiple Commands At Once
.............................

The general form to provide several commands at once is:

  ``aio-metadata-transform <cmd> ::: <cmd> ::: <cmd> <metadata-file``

  * each ``<cmd>`` is separated by command-delimiter ``:::``
  * a ``<cmd>`` can be any of ``key-value``, ``column``, ``number``, ``value``
  * the same ``<cmd>`` type can be given more than once

Take a look at :ref:`examples-multiple-label`.

There are also some :ref:`pitfalls-label`.

.. _metadata-stdin-label:

Read Metadata Tags From Standard-Input
......................................

All commands read metadata from standard-input. Data is expected in one of the following formats:

  A list of unique ``<tag-names>``::
    
    # metadata.txt
    <tag-name-a>
    <tag-name-b>
    <tag-name-c>
    <tag-name-d>
    ...

  A list of dublicated ``<tag-names>``::
    
    # metadata.txt
    <tag-name-a>
    <tag-name-a>
    <tag-name-a>
    <tag-name-a>
    ...

  A list of ``<tag-names>`` groups::

    # metadata.txt
    <tag-name-a>
    <tag-name-b>
    <tag-name-c>
    <tag-name-d>
    
    <tag-name-a>
    <tag-name-b>
    <tag-name-c>
    <tag-name-d>
    ...
  
  Another list of ``<tag-names>`` groups::
    
    # metadata.txt
    <tag-name-a>
    <tag-name-a>
    <tag-name-b>
    <tag-name-b>
    <tag-name-c>
    <tag-name-c>
    <tag-name-d>
    <tag-name-d>
    ...

One ``<tag-name>`` is expected per line:

  * ``<tag-name>`` is expected at the begin of a line
  * ``<tag-name>`` is usually a name of a audio metadata tag (ie, from ``vorbis-comment``, ``id3v2``, etc), but in general, ``<tag-name>`` can be an arbitrary word
  * ``<tag-name>`` must be one word, without whitespaces
  * ``<tag-name>`` can be followed by one or more words, separated by whitespaces. Those words will be preserved, if a command does not change a particular ``<tag-name>``. In that case, the existing words will be overwritten. 

The occurence of each ``<tag-name>`` is counted:

  * if each ``<tag-name>`` is unique, then the count for each ``<tag-name>`` is ``1``
  * if a ``<tag-name>`` occurs several times, then the count for a ``<tag-name>`` is ``<num-occurences>``

The wiki shows internals about `providing data via stdin`_.

.. _metadata-file-label:

Read Custom Metadata Values From A File
.......................................

``key-value`` and ``column`` commands read a custom metadata-file, from where metadata ``<values>`` are extracted. Extracted ``<values>`` are then assigned to selected ``<tag-names>`` read from standard-input.

``column`` command expects a file containing columns:

  * by default, columns are expected to be separated by ``,`` (ie. a csv-file)
  * column-separators can be adjusted by the ``delimiter`` option of the ``column`` command

  ::

    # use , as column separator
    # cat metadata-col.csv
    1981, Toy World, Cardiacs
    1987, Big Ship, Cardiacs
    1989, On Land And On The Sea, Cardiacs

  ::

    # use | as column separator
    # cat metadata-col.txt
    1981 | Toy World | Cardiacs
    1987 | Big Ship | Cardiacs
    1989 | On Land And On The Sea | Cardiacs
  
``key-value`` commands has no big expectations, except:

  * it should contain one or more custom metadata ``<tag-name> <value>`` pair(s)
  * a ``<tag-name>`` can be any word, followed by a ``<value>``, separated by a whitespace
  * ``<tag-name>`` is expected at the begin of a line, except when the ``selector`` option is used
  * a ``<value>`` can be one or more words on the remainig part of the line
  * the remaining file content can be arbitrary

  An example of a file that contains custom ``<tag-names>`` ``A``, ``T``, ``Y``::

    # custom-metadata.txt
    A Cardiacs
    T Toy World
    Y 1981
    A Cardiacs
    T The Big Ship
    Y 1987
    A Cardiacs
    T On Land And On The Sea
    Y 1989

  An example of a file that contains custom tag ``T`` as comments, and otherwise contains cut-points to cut samples from a recording:
  
  * custom ``<tag-name>`` ``T`` is used for giving each sample a title
  * the ``selector`` option of the ``key-value`` command should be set to ``#`` in order find ``T`` in comments:

  ::

    # cut-points.txt
    
    # T applaus at begin of speech
    1:20 1:30
  
    # T microphone click noise
    1:43.4 1:44
    
    # T speech about this and that
    2:00 3:00
    
    # T final applause
    3:00 3:20

.. _metadata-select-label:

Syntax For Selecting Specific Tag-Names
.......................................

A ``select`` option can be used with the ``value`` command (in the future, this option will be available for the ``number`` command as well).

This option accepts a ``<range>`` string in order to select specific occurences of ``<tag-name>`` that are read from stdin.

* select a single occurence of ``<tag-names>``: ``select <num>``::

    select 5

* select all ``<tag-names>`` starting from the selected one: ``<from>:``::

    select 5:

* select all ``<tag-names>`` withtin a range: ``<from>:<to>``::

    select 5:10

``<range>`` rules are as follows:

  * counters always starts at ``1``, ie. ``5`` selects the fifth occurence of a ``<tag-name>``
  * counters must be positive integers ``>= 1``
  * counters can be larger than the number of ``<tag-names>`` that are read from stdin. In that case, all values that are too high will be ignored.

.. note::

  Other ``aio`` scripts like ``aoi-renames-files`` implement selectors, that are a bit more powerfull. In the future, that selector-syntax should be the available in ``aio-metadata-transform`` as well.
  
  For details of more advanced selectors, take a look at :ref:`aio-rename-files-selectors-label`.   

.. _examples-keyvalue-label:

Examples: key-value command
...........................

The following examples use ``custom-metadata.txt`` and ``cut-points.txt`` as custom metadata source.

  ``custom-metadata.txt`` content::

    A Cardiacs
    T Toy World
    Y 1981
    A Cardiacs
    T The Big Ship
    Y 1987
    A Cardiacs
    T On Land And On The Sea
    Y 1989

  ``cut-point.txt`` content::

    # T applaus at begin of speech
    1:20 1:30
    
    # T microphone click noise
    1:43.4 1:44
    
    # T speech about this and that
    2:00 3:00
    
    # T final applause
    3:00 3:20
    
Example: extract the first occurence of custom tag-name ``T`` from ``custom-metadata.txt`` and assign its value to ``TALB`` read from stdin

  ::

    # generate a list with unique <tag-names> that only occure once
    aio-metadata-generate common | tee metadata.txt
    >TPE1
    >TALB
    >TYER
    >COMM

    aio-metadata-transform key-value T as TALB custom-metadata.txt <metadata.txt
    >TPE1
    >TALB Toy World
    >TYER
    >COMM

Example: extract three custom tag-names ``A`` from ``custom-metadata.txt`` and assign their values to ``TPE2`` in the order of occurence

  ::

    # generate 3 <tag-name> groups. each <tag-name> occures three times
    aio-metadata-generate individual 3 | tee metadata.txt
    >TIT2
    >TPE2
    >TRCK
    >
    >TIT2
    >TPE2
    >TRCK
    >
    >TIT2
    >TPE2
    >TRCK
    
    aio-metadata-transform key-value A as TPE2 custom-metadata.txt <metadata.txt
    >TIT2
    >TPE2 Cardiacs
    >TRCK
    >
    >TIT2
    >TPE2 Cardiacs
    >TRCK
    >
    >TIT2
    >TPE2 Cardiacs
    >TRCK

Example: extract four custom tag-names ``T`` from ``cut-points.txt`` by using a line selector ``#`` and assign their values to ``TIT2``

  ::

    # generate 4 <tag-name> groups. each <tag-name> occures four times
    aio-metadata-generate individual 4 | tee metadata.txt
    >TIT2
    >TPE2
    >TRCK
    >
    >TIT2
    >TPE2
    >TRCK
    >
    >TIT2
    >TPE2
    >TRCK
    >
    >TIT2
    >TPE2
    >TRCK
    
    aio-metadata-transform key-value A as TIT2 cut-points.txt selector '#' <metadata.txt
    >TIT2 applaus at begin of speech
    >TPE2 
    >TRCK
    >
    >TIT2 microphone click noise
    >TPE2
    >TRCK
    >
    >TIT2 speech about this and that
    >TPE2
    >TRCK
    >
    >TIT2 final applause
    >TPE2
    >TRCK

.. _examples-column-label:

Examples: column-command
........................

The following examples use ``metadata-col.csv`` and ``metadata-col.txt`` as custom metadata source and ``metadata.txt`` for assignment.

  ``metadata-col.csv`` content::
  
    1981, Toy World, Cardiacs
    1987, Big Ship, Cardiacs
    1989, On Land And On The Sea, Cardiacs

  ``metadata-col.txt`` content::

    1981 | Toy World | Cardiacs
    1987 | Big Ship | Cardiacs
    1989 | On Land And On The Sea | Cardiacs

  ``metadata.txt`` content::
  
    date
    album
    artist
    
    date
    album
    artist
    
    date
    album
    artist

Example: extract metadata ``<values>`` from first column in ``metadata-col.csv`` and assign them to ``date`` read from ``metadata.txt``

  ::

    # use the default column-separator ','
    aio-metadata-transform column 1 as date metadata-col.csv <metadata.txt
    >date 1981
    >album
    >artist
    >
    >date 1987
    >album
    >artist
    >
    >date 1989
    >album
    >artist

Example: extract metadata ``<values>`` from second column in ``metadata-col.txt`` and assign them to ``album`` read from ``metadata.txt``. A custom column-separator ``|`` is being used

  ::

    # use column-separator '|'
    aio-metadata-transform column 2 as date metadata-col.csv delimiter '|' <metadata.txt
    >date
    >album Toy World
    >artist
    >
    >date
    >album Big Ship
    >artist
    >
    >date
    >album On Land And On The Sea
    >artist

.. _examples-number-label:

Examples: number-command
........................

The following examples use ``metadata.txt``.

  ``metadata.txt`` content::
    
    title
    album_artist
    track
    
    title
    album_artist
    track
    
    title
    album_artist
    track

Example: generate continuous numbers for tag-name ``track``, starting from ``1``.

  ::

    # number count starts at 1 by default
    aio-metadata-transform number track <metadata.txt
    >title
    >album_artist
    >track 1
    >
    >title
    >album_artist
    >track 2
    >
    >title
    >album_artist
    >track 3

Example: generate continuous numbers for tag-name ``track``, starting from ``5``.

  ::

    aio-metadata-transform number track 5 <metadata.txt
    >title
    >album_artist
    >track 5
    >
    >title
    >album_artist
    >track 6
    >
    >title
    >album_artist
    >track 7

Example: generate continuous numbers for tag-name ``track``, starting from ``5`` to ``6``

  ::

    aio-metadata-transform number track 5 6 <metadata.txt
    >title
    >album_artist
    >track 5
    >
    >title
    >album_artist
    >track 6
    >
    >title
    >album_artist
    >track

.. _examples-value-label:

Examples: value-command
........................

Example: assign a single value to tag-name ``license``

  ::

    # here we use arbitrary words
    aio-metadata-transform value 'Licensed under GPL3' as album_artist <<EOF
      script aio-cut
      license
      
      script aio-split
      license
      
      script aio-concat
      license
    EOF

    >script aio-cut
    >license Licensed under GPL3
    >  
    >script aio-split
    >license Licensed under GPL3
    >  
    >script aio-concat
    >license Licensed under GPL3

Example: assign a single value to tag-name ``license``, but start from second occurence of ``license``

  ::

    # here we use arbitrary words
    aio-metadata-transform value 'Licensed under GPL3' as album_artist select 2: <<EOF
      script aio-cut
      license
      
      script aio-split
      license
      
      script aio-concat
      license
    EOF

    >script aio-cut
    >license
    >  
    >script aio-split
    >license Licensed under GPL3
    >  
    >script aio-concat
    >license Licensed under GPL3
    
.. _examples-multiple-label:

Examples: using multiple commands at once
.........................................

The following examples use ``metadata-col.csv`` and ``metadata-col.txt`` as metadata source and ``metadata.txt`` for assignment. 

  ``metadata-col.txt`` content::
  
    Big Ship | Big Ship | T.Smith
    Big Ship | Tarred and Feathered | T.Smith, W.D. Drake
    Big Ship | Burn Your House Down | T.Smith
    Big Ship | Stoneage Dinosaurs | T.Smith
    Big Ship | Plain Plain Against The Grain | T.Smith

  ``metadata.txt`` content::
  
    title
    track
    writer
    
    title
    track
    writer
    
    title
    track
    writer
    
    title
    track
    writer
    
    title
    track
    writer

Example: assign column ``2`` to tag-name ``title``, assign column ``3`` to tag-name ``writer``, generate ``track`` numbers

  ::

    aio-metadata-transform \
      column 2 as title metadata-col.txt delimiter '|' ::: \
      column 3 as writer metadata-col.txt delimiter '|' ::: \
      number track <metadata.txt
    
    >title Big Ship
    >track 1
    >writer T.Smith
    > 
    >title Tarred and Feathered
    >track 2
    >writer T.Smith, W.D. Drake
    >
    >title Burn Your House Down
    >track 3
    >writer T.Smith
    >
    >title Stoneage Dinosaurs
    >track 4
    >writer T.Smith
    > 
    >title Plain Plain Against The Grain
    >track 5
    >writer T.Smith

Example: assign several ``<values>`` to ``<tag-names>``

  ::

    aio-metadata-transform \
      v 'Big Ship' as TALB ::: \
      v 'Cardiacs' as TPE2 ::: \
      v '1985' as TYER ::: \
      v 'Vinyl, 12 inch, 45 rpm' as TCOM <EOF
        TALB
        TPE2
        TYER
        COMM
      EOF
      
      >TALB Big Ship
      >TPE2 Cardiacs
      >TYER 1985
      >COMM Vinyl, 12 inch, 45 rpm

Example: assign value ``Cardiacs`` to tag-name ``TPE2``, but only select occurences ``2, 3, 4`` of the tag-name

  ::

    aio-metadata-transform \
      value Cardiacs as TPE2 select 2:4 ::: \ <EOF
        TALB
        TPE2
        
        TALB
        TPE2
        
        TALB
        TPE2
        
        TALB
        TPE2
      EOF
      
      >TALB
      >TPE2
      >  
      >TALB
      >TPE2 Cardiacs
      >  
      >TALB
      >TPE2 Cardiacs
      > 
      >TALB
      >TPE2 Cardiacs

Example: transform custom tags to ``id3v2``, and assign a year and generate track-numbers

  The file ``custom.txt`` is used as metadata source::

    title Big Ship
    writer T.Smith
     
    title Tarred and Feathered
    writer T.Smith, W.D. Drake
    
    title Burn Your House Down
    writer T.Smith
    
    title Stoneage Dinosaurs
    writer T.Smith
     
    title Plain Plain Against The Grain
    writer T.Smith

  The file ``id3v2.txt`` is used to assign data::
    
    TIT2
    TRCK
    TCOM
    TYER
    
    TIT2
    TRCK
    TCOM
    TYER
    
    TIT2
    TRCK
    TCOM
    TYER
    
    TIT2
    TRCK
    TCOM
    TYER
    
    TIT2
    TRCK
    TCOM
    TYER

  ::

    aio-metadat-transform \
      kv title as TIT2 custom.txt ::: \
      kv writer as TCOM custom.txt ::: \
      v 1985 as TYER ::: \
      n TRCK <id3v2.txt
      
      >TIT2 Big Ship
      >TRCK 1
      >TCOM T.Smith
      >TYER 1985
      >
      >TIT2 Tarred and Feathered
      >TRCK 2
      >TCOM T.Smith, W.D. Drake
      >TYER 1985
      >
      >TIT2 Burn Your House Down
      >TRCK 3
      >TCOM T.Smith
      >TYER 1985
      >
      >TIT2 Stoneage Dinosaurs
      >TRCK 4
      >TCOM T.Smith
      >TYER 1985
      >
      >TIT2 Plain Plain Against The Grain
      >TRCK 5
      >TCOM T.Smith
      >TYER 1985
    
.. _pitfalls-label:

Pitfalls
........

Be aware that it is not possible to write the output of ``aio-metadata-transform`` to the file that is redirected to stdin.

In the next example, ``bash`` and ``tee`` would open and flush ``metadata.txt`` for writing, while it is read at the same time. This would result in corrupt input.

  ::

    # read metadata.txt from stdin and write to it at the same time
    aio-metadata-transform number track <metadata.txt >metadata.txt
    aio-metadata-transform number track <metadata.txt | tee metadata.txt

An alternative would be to store the output of ``aio-metadata-transform`` to a temporary file and move that file later to the original filename

  ::

    aio-metadata-transform number track <metadata.txt >tmp.txt
    mv tmp.txt metadata.txt
  
    aio-metadata-transform number track <metadata.txt | tee tmp.txt
    mv tmp.txt metadata.txt

Or use the ``sponge`` tool from ``moreutils`` package. ``sponge`` only opens a file for writing, once it received all data. Waiting for all the data should be ok, as we usually only have a few kbyte of data.

  ::

    aio-metadata-transform number track <metadata.txt | sponge metadata.txt    

In the current ``key-value`` and ``column`` command syntax, one needs to redundantly specify the ``<custom-tags-file>``

  ::
  
      aio-metadat-transform \
        kv title as TIT2 custom.txt ::: \
        kv writer as TCOM custom.txt <metadata.txt

Background
..........

The initial ideas for `metadata transformation can be found in the wiki`_.

.. _metadata transformation can be found in the wiki: https://gitlab.com/zzeebdrraa/tools/cl-audio-processing/aio/-/wikis/Ideas/Facilitate-Cutting-and-Splitting-of-many-files#when-cut-points-and-their-description-exist-in-one-file-and-an-additional-metadata-file-has-to-be-written

.. _providing data via stdin: https://gitlab.com/zzeebdrraa/tools/cl-audio-processing/aio/-/wikis/Script-Design#options-from-stdin