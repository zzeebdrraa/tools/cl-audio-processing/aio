#!/usr/bin/env bash

# This file is part of aio-proc
#
#     aio-proc is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     aio-proc is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with aio-proc. If not, see <http://www.gnu.org/licenses/>.

# features to add
# - implement loudness normalization
# - provide envvars to customise default values

declare -r __version="1.0.1"

# TODO set via envvars as well
declare -r __new_peak_level_default="0Db"
declare -r __norm_peak_prefix="norm-peak"
declare -r __norm_loudness_prefix="norm-loudness"

# the default quality settings for compressed audio codecs
# TODO add settings for more codecs
declare -r -A __codec_quality_settings=(
  [mp3]=2
  [vorbis]=7
  [opus]=7
)

print-help-short() {
  local -r reset=$(tput sgr0)
  local -r bold=$(tput bold)

  local -r scriptName="${bold}$(basename "$0")${reset}"

  printf '%s\n' "
${bold}Usage${reset}

  ${scriptName} -h|--help

  ${scriptName} h|help
  ${scriptName} h|help s|short
  ${scriptName} h|help d|details
  ${scriptName} h|help e|examples

  ${scriptName} --version

  ${scriptName} [-d|--dry-run]
                [-q|--quality <value>]
                [-f|--force-overwrite]
                p|peak
                  [-l|--peak-level <peak-level-db>]
                  <input-file> [ ... <input-file-x>]

  ${scriptName} [-d|--dry-run]
                [-q|--quality <value>]
                [-f|--force-overwrite]
                l|loudness
                  <input-file> [ ... <input-file-x>]

  ${scriptName} v|volume
                  <input-file> [ ... <input-file-x>]

  ${scriptName} s|settings
"
}

print-help-examples() {
  local -r reset=$(tput sgr0)
  local -r bold=$(tput bold)

  local -r scriptName="${bold}$(basename "$0")${reset}"

  printf '%s\n' "
${bold}Examples${reset}

  # peak normalisation of one file
  ${scriptName} p audio.ogg

  # peak normalisation of several file
  ${scriptName} p audio.flac audio.wav audio.mp3

  # peak normalisation of several files, where output files exist already
  ${scriptName} -f p audio.flac audio.wav

  # show default quality settings
  ${scriptName} s

  # show detected volumes of audio file
  ${scriptName} volume audio.ogg audio.wav
"
}

print-help-details() {
  local -r reset=$(tput sgr0)
  local -r bold=$(tput bold)

  printf '%s\n' "
${bold}Description${reset}

  ${scriptName} tries to normalize the amplitude of a audio to a
    certain peak value. one or more audio files can be normalized
    at once.

  ${bold}note
    
    audio normalization requires a audio file to be re-encoded after
      processing, if the audio codec is a compressed codec.
      
    therefore, it makes sense to figure out the quality of input
      audio in order to select a proper quality value for the
      normalized audio (via the --quality argument)${reset}.
    
    in future versions the quality might be auto-detected.

${bold}Commands${reset}

  ${bold}s|settings${reset}

    currently prints the custom quality settings for various
      compressed audio codecs that are used for encoding the
      <output-file>.

  ${bold}p|peak${reset}

    Normalizes the audio to the given db value. The highest peak in
      input audio is the reference peak, thus overall audio amplitude
      is increased by the difference of the highest audio peak level
      to the provided peak value.
      
    ${bold}-l|--peak-level <peak-level-db>${reset}
    
      the peak-level to normalize audio to. is given in db (decibel).
      
      peak-level can be 0 for maximum loudness, or a negative integer 
        value for more silent audio.
    
      ${bold}default [0]${reset} (decibel)

  ${bold}l|loudness${reset}

    Not yet implemented.

  ${bold}s|show${reset}

    Only show volume infos of input files. No processing takes places.

${bold}Global Options${reset}

  ${bold}-d|--dry-run${reset}

    just see what would happen. no audio processing takes places.

  ${bold}-f|--force-overwrite${reset}

    force overwriting an already existing output file.

    default [no overwrite]

  ${bold}-q|--quality <value>${reset}

    ${bold}note

      lossless audio codecs do not need a quality setting. only
        compressed codecs take quality values into account.${reset}
    
    ${bold}note
    
      if a quality setting is provided, all <input-files> should
        be of the same codec. otherwise, the same quality value
        has different meanings for different codecs.${reset}

    set the audio quality for the normalized <output-file>. <value> is
      usually a positive integer value ranging from 0 to 10.

    each compressed audio codec uses its own value range. please
      take a look at the ffmpeg documentation for the corresponding
      codec.
    
    for a couple of audio codecs, default quality settings will
      be used. see ${bold}settings${reset} command for defaults.
"
}

# TODO redundant code
handle-help() {
  [[ $# -eq 0 ]] && {
    print-help-short
    print-help-details
    return 0
  }

  while [ "$1" ]; do
    case "${1}" in
      s|short)
        print-help-short
        ;;
      d|details)
        print-help-details
        ;;
      e|examples)
        print-help-examples
        ;;
      *)
        printf 'error: unknown help topic [%s]\n' "$1" >&2
        return 1
        ;;
    esac
    shift
  done
  return 0
}

print-version() {
  printf '%s\n' "${__version}"
}

# TODO dublicated code aio-mix, aio-fade, aio-normalize
print-settings() {

  printf '%s\n' '
Default audio codec quality settings
--------
'
  for k in "${!__codec_quality_settings[@]}"; do
    printf ' %s: %s\n' "${k}" "${__codec_quality_settings[$k]}"
  done

  printf '%s\n' '
--------
  '
}

# TODO dublicated code aio-mix, aio-fade, aio-normalize
print-processing-data() {
  local -r cmd="$1"
  declare -r -n args="$2"
  local -r fout="$3"
  local -r fin="$4"

  local argList=()
  for k in "${!args[@]}"; do
    argList+=("${k} : ${args[${k}]}")
  done

  printf '%s\n' "${cmd}"
  printf '  %s\n' "${fin}"
  printf '  %s\n' "${fout}"
  printf '  %s\n' "${argList[@]}"
  printf '\n'
}

# @param $1 (not used) the name of an associative array that contains additional arguments
# @param $2 (not used) output file
# @param $3 input file
show-volume-stats() {
  local -r fin="$3"

  local vol=()
  vol=($(ffmpeg-detect-peak-volume "${fin}"))
  [[ $? -ne 0 ]] && return 1

  printf '%s\n' "
  file        : ${fin}
  mean-volume : ${vol[0]} db
  max-volume  : ${vol[1]} db
"
  return 0
}

# TODO dublicated code in aio-fade, aio-concat, aio-normalize, aio-mix
#
# @param $1 expected file codec type
#           can be one of [audio, video]
# @param $2 input-file path and name
#
# @return 0 input-file codec is as expected
# @return 1 input-file codec is not as expected
ffprobe-is-codec-type() {
  local -r expectedCodecType="$1"
  local -r fin="$2"

  local codecType=$(ffprobe -loglevel error -show_entries stream=codec_type -of default=nk=1:nw=1 "${fin}" 2>/dev/null)
  [[ "${codecType}" != "${expectedCodecType}" ]] && return 1
  return 0
}

# TODO dublicated code in aio-fade, aio-normalize, aio-mix, aio-concat
ffprobe-get-codec-name() {
  local -r fin="$1"

  local codecName
  codecName=$(ffprobe -loglevel error -show_entries stream=codec_name -of default=nk=1:nw=1 "${fin}" 2>/dev/null)

  [[ $? -ne 0 ]] && return 1
  [[ -z "${codecName}" ]] && return 1

  printf '%s\n' "${codecName}"
  return 0
}

ffmpeg-detect-peak-volume() {
  local volInfo

  volInfo=$(ffmpeg -i "$1" -af volumedetect -f null - 2>&1 | grep -E -o '(mean_volume|max_volume).*')
  [ $? -ne 0 ] && return 1

  local maxDb=$(printf '%s\n' "${volInfo}" | grep 'max')
  local meanDb=$(printf '%s\n' "${volInfo}" | grep 'mean')

  # extract the value after the colon
  max="${maxDb#*:}"
  mean="${meanDb#*:}"

  # remove all whitespaces
  max="${max// /}"
  mean="${mean// /}"

  # remove db
  max="${max//dB/}"
  mean="${mean//dB/}"

  printf '%s %s\n' "${mean}" "${max}"
}

# @param $1 the name of an associative array that contains additional arguments
# @param $2 output file
# @param $3 input file
normalize-peak() {
  # this is a reference to an associative array declared outside this function
  declare -r -n args="$1"
  local -r fOut="$2"
  local -r fIn="$3"

  local -r newPeak="${args[peak-level]}"
  local -r quality="${args[quality]}"

  local changeVal
  local meanDb
  local peakDb

  IFS=' ' read -r meanDb peakDb < <(ffmpeg-detect-peak-volume "${fIn}")

  # newPeak can be a db value ranging from -XXdb - 0db or a percentage value
  # ranging from 0 ro 100
  if [[ "${newPeak}" =~ [dD][bB] ]]; then
    # calculate the relative db change from difference between
    # new absolute db value and peak value of file
    local -r newPeakVal="${newPeak//[dD][bB]/}"
    local -r dbChange=$(bc -l <<<"(${peakDb} - ${newPeakVal}) * -1" )
    changeVal="${dbChange}dB"
  else
    # just normalize percentage value to 0-1 range
    changeVal=$( bc -l <<<"${newPeak} / 100" )
  fi

  local opts='-loglevel error'
  [[ ${args[force-overwrite]} -eq 1 ]] && opts+=" -y"

  local qualityOpts=''
  [[ ! -z "${quality}" ]] && qualityOpts="-aq ${quality}"

  ffmpeg ${opts} \
         -i "${fIn}" \
         -filter:a "volume=${changeVal}" \
         ${qualityOpts} \
         "${fOut}"

  [[ $? -ne 0 ]] && {
    printf 'error: audio peak normalization failed\n' >&2
    return 1
  }
  return 0
}

# @param $1 the name of an associative array that contains additional arguments
# @param $2 output file
# @param $3 input file
normalize-loudness() {
  # this is a reference to an associative array declared outside this function
  declare -r -n args="$1"
  local -r fOut="$2"
  local -r fIn="$3"

  local -r newPeak="${args[peak-level]}"
  local -r quality="${args[quality]}"

  return 1
}

verify-peak-level() {
  local -r peakLevel="$1"
  local -r lenPeakLevel="${#peakLevel}"

  local peakLevelStripped

  # db and percentage values are allowed to be fractions
  [[ "${peakLevel}" =~ [.] ]] && {
    peakLevelStripped="${peakLevel//./}"
    [[ ${#peakLevelStripped} -ne $((lenPeakLevel - 1)) ]] && return 1
  }

  if [[ "${peakLevel}" =~ [dDbB]+ ]]; then
    # test db value => can be negative as well
    [[ ! "${peakLevel}" =~ [dD][bB]$ ]] && return 1
    peakLevelStripped="${peakLevel//[dD][bB]/}"
    [[ ${#peakLevelStripped} -ne $((lenPeakLevel - 2)) ]] && return 1

    [[ "${peakLevel}" =~ [-] ]] && {
      [[ ! "${peakLevel}" =~ ^[-] ]] && return 1
      peakLevelStripped="${peakLevel//-/}"
      [[ ${#peakLevelStripped} -ne $((lenPeakLevel - 1)) ]] && return 1
    }

    # remove all allowed special chars => one or more numbers should be left
    peakLevelStripped="${peakLevel//[-.dDbB]/}"
    [[ ! "${peakLevelStripped}" =~ ^[0-9]+$ ]] && return 1
  else
    # test percentage value => cannot be negative
    [[ "${peakLevel}" =~ [-] ]] && return 1

    # remove all allowed special chars => one or more numbers should be left
    peakLevelStripped="${peakLevel//[.]/}"
    [[ ! "${peakLevelStripped}" =~ ^[0-9]+$ ]] && return 1
  fi

  return 0
}

do-run() {

  local cmd
  local codecQuality

  local files=()
  declare -A cmdArgs=(
    [dry-run]=0
    [force-overwrite]=0
  )

  local newPeakLevel="${__new_peak_level_default}"

  while [ "$1" ]; do
    case "$1" in
      -h|--help)
        ! handle-help && return 1
        return 0
        ;;
      h|help)
        shift
        ! handle-help "$@" && return 1
        return 0
        ;;
      --version)
        print-version
        return 0
        ;;
      -d|--dry-run)
        cmdArgs[dry-run]=1
        ;;
      -q|--quality)
        shift
        codecQuality="$1"
        ;;
      -f|--force-overwrite)
        cmdArgs[force-overwrite]=1
        ;;
      -p|--peak-level)
        shift
        newPeakLevel="$1"
        ;;
      p|peak)
        cmd="normalize-peak"
        ;;
      l|loudness)
        cmd="normalize-loudness"
        ;;
      # collides with version
      v|volume)
        cmd="show-volume-stats"
        ;;
      s|settings)
        print-settings
        return 0
        ;;
      *)
        files=("$@")
        break
        ;;
    esac
    shift
  done

  case "${cmd}" in
    normalize-peak)
      verify-peak-level "${newPeakLevel}"
      [[ $? -ne 0 ]] && {
        printf 'error: invalid peak level [%s] ... stopping\n' "${newPeakLevel}" >&2
        return 1
      }
      cmdArgs[peak-level]="${newPeakLevel}"
      cmdArgs[prefix]="${__norm_peak_prefix}-${newPeakLevel}"
      ;;
    normalize-loudness)
      verify-peak-level "${newPeakLevel}"
      [[ $? -ne 0 ]] && {
        printf 'error: invalid peak level [%s] ... stopping\n' "${newPeakLevel}" >&2
        return 1
      }
      cmdArgs[peak-level]="${newPeakLevel}"
      cmdArgs[prefix]="${__norm_loudness_prefix}-${newPeakLevel}"
      ;;
    show-volume-stats)
      ;;
    *)
      printf 'error: unknown command [%s] ... stopping\n' "${cmd:-none}" >&2
      return 1
  esac

  [[ "${#files[*]}" -eq 0 ]] && {
    printf 'error: no input files provided .. stopping' >&2
    return 1
  }

  local errorOccured=0

  for fin in "${files[@]}"; do
    [[ -f "${fin}" ]] && continue
    errorOccured=1
    printf 'error: file [%s] does not exist ... skipping\n' "${fin}" >&2
  done

  [[ ${errorOccured} -eq 1 ]] && return 1

  local fout
  local codecName

  for fin in "${files[@]}"; do

    # skip non-audio files
    ffprobe-is-codec-type audio "${fin}"
    [[ $? -ne 0 ]] && {
      printf 'error: input file [%s] is no audio file ... skipping\n' "${fin}" >&2
      errorsOccured=1
      continue
    }

    # get default quality of codec of current input file
    if [[ -z "${codecQuality}" ]]; then
      codecName=$(ffprobe-get-codec-name "${fin}")
      [[ $? -eq 0 ]] && cmdArgs[quality]="${__codec_quality_settings[${codecName}]}"
    else
      cmdArgs[quality]="${codecQuality}"
    fi

    fout="${cmdArgs[prefix]}-${fin}"

    if [[ ${args[dry-run]} -eq 1 ]]; then
      print-processing-data "${cmd}" cmdArgs "${fout}" "${fin}"
    else
      ${cmd} cmdArgs "${fout}" "${fin}"
      [[ $? -ne 0 ]] && errorsOccured=1
    fi
  done

  [[ ${errorsOccured} -eq 1 ]] && return 1
  return 0
}

do-run "$@"
