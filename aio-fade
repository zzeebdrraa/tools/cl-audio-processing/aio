#!/usr/bin/env bash

# This file is part of aio-proc
#
#     aio-proc is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     aio-proc is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with aio-proc. If not, see <http://www.gnu.org/licenses/>.

# features to add
# - run fade tasks in parallel
# - provide envvars to customise default values

declare -r __version="1.1.1"

# TODO set via envvars as well
declare -r __in_duration_sec_default='0.01'
declare -r __out_duration_sec_default='0.01'

declare -r __in_curve_default="tri"
declare -r __out_curve_default="tri"

# see also https://ffmpeg.org/ffmpeg-filters.html#afade
declare -r __out_file_prefix_in="fade-in"
declare -r __out_file_prefix_out="fade-out"
declare -r __out_file_prefix_inout="fade-inout"

# the default quality settings for compressed audio codecs
# TODO add settings for more codecs
declare -r -A __codec_quality_settings=(
  [mp3]=2
  [vorbis]=7
  [opus]=7
)

print-version() {
  printf '%s\n' "${__version}"
}

print-help-short() {
  local -r reset=$(tput sgr0)
  local -r bold=$(tput bold)

  local -r scriptName="${bold}$(basename "$0")${reset}"
  
  printf '%s\n' "
${bold}Usage${reset}

    ${scriptName} -h|--help

    ${scriptName} h|help
    ${scriptName} h|help s|short
    ${scriptName} h|help d|details
    ${scriptName} h|help e|examples

    ${scriptName} --version

    ${scriptName} s|settings

    ${scriptName} [-d|--dry-run]
             [-q|--quality <value>]
             [-f|--force-overwrite]
             i|in
               [--di <duration-sec>]
               [--ci <curve-type>]
               <input-file> [ ... <input-file-x>]

    ${scriptName} [-d|--dry-run]
             [-q|--quality <value>]
             [-f|--force-overwrite]
             o|out
               [--do <duration-sec>]
               [--co <curve-type>]
               <input-file> [ ... <input-file-x>]

    ${scriptName} [-d|--dry-run]
             [-q|--quality <value>]
             [-f|--force-overwrite]
             io|inout
               [--di <duration-sec>] [--do <duration-sec>]
               [--ci <curve-type>] [--co <curve-type>]
               <input-file> [ ... <input-file-x>]
"
}

print-help-details() {
  local -r reset=$(tput sgr0)
  local -r bold=$(tput bold)

  local -r scriptName="${bold}$(basename "$0")${reset}"

  printf '%s\n' "
${bold}Description${reset}

  apply fading effect to begin and/or end sections of audio.
    the audio is decoded and rencoded with most of its original
    properties (samplerate, samplesize, channels, codec).

${bold}Input${reset}

  one or more audio files. audiofile codecs may differ, as well as
    audio quality or number of channels.

${bold}Output${reset}

  a copy of each processed file is stored with a prefix
    indicating the faded audio file.

    <input-file> is processed and stored as:

      ${bold}<prefix>-<input-file>${reset}

    where prefix depends on given command:

      fade-in    [${__out_file_prefix_in}]
      fade-out   [${__out_file_prefix_out}]
      fade-inout [${__out_file_prefix_inout}]

${bold}Note${reset}

  fading needs decoding and re-encoding of each <input-file>. for
    re-encoding the <output-file>, ffmpeg usually uses default
    quality settings of the corresponding audio codec.

  for lossless audio (like flac or wav), this is no problem, as
    lossless audio quality is supposed to be that of the original
    audio in any case.

  for compressed audio, the use of default quality settings may
    result in an <output-file> that is encoded with a lower
    (variable) bitrate than the <input-file>.

  in order to avoid that reduction of audio quality, custom quality
    settings are used by ${scriptName} that results in an <output-file>
    encoded with a relatively high variable bitrate. one can use
    the ${bold}-q${reset} option to overwrite the quality settings of ${scriptName}.

  for some of the compressed audio codecs, no custom quality setting
    exists yet. in that case, one can use the ${bold}-q${reset} option to specify
    a proper quality value. otherwise, ffmpeg will use the default
    value for the corresponding codec.

  for a list of supported codecs and their custom quality settings,
    run:

      ${scriptName} settings

${bold}Commands${reset}:

  ${bold}s|settings${reset}

    currently prints the custom quality settings of various compressed
      audio codecs.

  ${bold}i|in${reset}

    apply fade-in to audio files. fade-in fades audio from
      silence to audio level at the begin of an audio file.

      - fade-in starts at position 0s
      - fade-in duration is set via ${bold}--duration-in-sec${reset}.
      - fade-in curve/envelope is set via ${bold}--curve-in${reset}.

  ${bold}o|out${reset}

    apply fade-out to audio files. fade-out fades audio from
      audio level to silence at the end of an audio file.

      - fade-out starts at position (audio-length - fade-out-duration)
      - fade-out duration is set via ${bold}--duration-out-sec${reset}.
      - fade-out curve/envelope is set via ${bold}--curve-out${reset}.

  ${bold}io|inout${reset}

    apply fade-in and fade-out to begin and end of an audio file.

      - all options of ${bold}i|in${reset} and ${bold}o|out${reset} commands can be used.

${bold}Global Options${reset}

  ${bold}-d|--dry-run${reset}

    just see what would happen. no audio processing takes places.

  ${bold}-f|--force-overwrite${reset}

    force overwriting an already existing <output file>.

    default [no overwrite]

  ${bold}-q|--quality <value>${reset}

    ${bold}Note${reset}

      - lossless audio codecs do not need a quality setting
      - if a quality setting is provided, all <input-files> should
        be of the same codec. otherwise, the quality value
        has different meanings for different codecs.

    set the audio quality for the faded <output-file>. <value> is
      usually a positive integer value.

    each compressed audio codec uses its own value range. please
      take a look at the ffmpeg documentation for the corresponding
      codec.

      ${bold}https://trac.ffmpeg.org/wiki/Encode/HighQualityAudio${reset}

${bold}Command specific options${reset}

  ${bold}--di|---duration-in-sec${reset}

    fade-in duration in seconds. the given value may also contain a
      milli-second part and should be larger 0.0.

    default [${__in_duration_sec_default}]

  ${bold}--do|---duration-out-sec${reset}

    fade-in duration in seconds. the given value may also contain a
      milli-second part and should be larger 0.0.

    default [${__out_duration_sec_default}]

  ${bold}--ci|--curve-in${reset}

    fade-in curve. defines how the audio amplitude is faded in.

    for accepted curve types, see

      ${bold}https://ffmpeg.org/ffmpeg-filters.html#afade${reset}

    default [${__in_curve_default}]

  ${bold}--co|--curve-out${reset}

    fade-out curve. defines how the audio amplitude is faded out.

    for accepted curve types, see

      ${bold}https://ffmpeg.org/ffmpeg-filters.html#afade${reset}

    default [${__out_curve_default}]
"
}

print-help-examples() {
  local -r reset=$(tput sgr0)
  local -r bold=$(tput bold)

  local -r scriptName="${bold}$(basename "$0")${reset}"

  printf '%s\n' "
${bold}Examples${reset}

  # use the default duration and curve for fade-in
  ${scriptName} in audio.ogg

  # use the default duration and curve for fade-in of several files
  ${scriptName} in audio-a.ogg audio-b.mp3 audio-c.flac

  # set a custom fade-out curve for inout fade and use defaults otherwise
  ${scriptName} inout --co losi audio.ogg

  # set a custom curves for inout fade and use defaults otherwise
  ${scriptName} inout --ci par --co losi audio.ogg

  # set a custom duration for fade-out
  ${scriptName} out --do 2.5 audio.ogg
"
}

# TODO redundant code
handle-help() {
  [[ $# -eq 0 ]] && {
    print-help-short
    print-help-details
    return 0
  }

  while [ "$1" ]; do
    case "${1}" in
      s|short)
        print-help-short
        ;;
      d|details)
        print-help-details
        ;;
      e|examples)
        print-help-examples
        ;;
      *)
        printf 'error: unknown help topic [%s]\n' "$1" >&2
        return 1
        ;;
    esac
    shift
  done
  return 0
}

# TODO dublicated code aio-mix, aio-fade, aio-normalize
print-settings() {

  printf '%s\n' '
Default audio codec quality settings
--------
'
  for k in "${!__codec_quality_settings[@]}"; do
    printf ' %s: %s\n' "${k}" "${__codec_quality_settings[$k]}"
  done

  printf '%s\n' '
--------
  '
}

# TODO dublicated code aio-mix, aio-fade, aio-normalize
print-processing-data() {
  local -r cmd="$1"
  declare -r -n argMapRef="$2"
  local -r fout="$3"
  local -r fin="$4"

  local argList=()
  local key

  for key in "${!argMapRef[@]}"; do
    argList+=("${key} : ${argMapRef[${key}]}")
  done

  printf '%s\n' "${cmd}"
  printf '  %s\n' "${fin}"
  printf '  %s\n' "${fout}"
  printf '  %s\n' "${argList[@]}" | sort
  printf '\n'
}

verify-duration() {
  local -r durationSec="$1"
  local -r lenDuration="${#durationSec}"

  # duration should only consists of numbers and maximum one dot
  [[ ! "${durationSec}" =~ ^[.0-9]+$ ]] && return 1

  if [[ "${durationSec}" =~ [.]+ ]]; then
    # remove all dots, if existing
    local durationSecStripped="${durationSec//./}"

    # duration consisted of dots only
    [[ -z "${durationSecStripped}" ]] && return 1
    # duration consisted of more than one dot
    [[ ${#durationSecStripped} -ne $((lenDuration - 1)) ]] && return 1
    # duration is zero. ie: .0 => 0, 0.0 => 00, 00.000 => 00000
    [[ ${durationSecStripped} -eq 0 ]] && return 1
  else
    # duration is zero. ie: 0, 00, 00000
    [[ ${durationSec} -eq 0 ]] && return 1
  fi

  return 0
}

# @param $1 input file an path
# @stdout audio duration in seconds
ffmpeg-get-audio-duration-sec() {
  ffprobe -loglevel error -show_entries format=duration -of default=nk=1:nw=1 "$1"
}

ffmpeg-fade-in() {
  # this is a reference to an associative array declared outside this function
  declare -r -n argsMapRef="$1"
  local -r fout="$2"
  local -r fin="$3"

  local -r fadeFilter="afade=t=in:d=${argsMapRef['duration-in']}:curve=${argsMapRef['curve-in']}"
  local -r quality="${argsMapRef['quality']}"

  local optsList=('-loglevel' 'error')
  [[ "${argsMapRef['force-overwrite']}" -eq 1 ]] && optsList+=('-y')

  local qualityOptsList=()
  [[ ! -z "${quality}" ]] && qualityOptsList+=('-aq' "${quality}")

  ffmpeg "${optsList[@]}" \
         -i "${fin}" \
         -avoid_negative_ts make_zero \
         -af "${fadeFilter}" \
         "${qualityOptsList[@]}" \
         "${fout}"
         
  # shellcheck disable=SC2181
  [[ $? -ne 0 ]] && {
    printf 'error: in-fading failed\n' >&2
    return 1
  }
  return 0
}

ffmpeg-fade-out() {
  # this is a reference to an associative array declared outside this function
  declare -r -n argsMapRef="$1"
  local -r fout="$2"
  local -r fin="$3"

  local -r quality="${argsMapRef['quality']}"
  local -r audioDurationSec="${argsMapRef['duration-audio']}"
  local -r fadeOutDurationSec="${argsMapRef['duration-out']}"

  local optsList=('-loglevel' 'error')
  [[ "${argsMapRef['force-overwrite']}" -eq 1 ]] && optsList+=('-y')

  local qualityOptsList=()
  [[ ! -z "${quality}" ]] && qualityOptsList+=('-aq' "${quality}")

  local -r fadeOutStartSec=$(bc -l <<<"${audioDurationSec} - ${fadeOutDurationSec}")
  local -r fadeFilter="afade=t=out:st=${fadeOutStartSec}:d=${fadeOutDurationSec}:curve=${argsMapRef[curve-out]}"

  ffmpeg "${optsList[@]}" \
         -i "${fin}" \
         -avoid_negative_ts make_zero \
         -af "${fadeFilter}" \
         "${qualityOptsList[@]}" \
         "${fout}"
         
  # shellcheck disable=SC2181 
  [[ $? -ne 0 ]] && {
    printf 'error: out-fading failed\n' >&2
    return 1
  }
  return 0
}

ffmpeg-fade-inout() {
  # this is a reference to an associative array declared outside this function
  declare -r -n argsMapRef="$1"
  local -r fout="$2"
  local -r fin="$3"

  local -r audioDurationSec="${argsMapRef['duration-audio']}"
  local -r fadeOutDurationSec="${argsMapRef['duration-out']}"
  local -r quality="${argsMapRef['quality']}"

  local optsList=('-loglevel' 'error')
  [[ "${argsMapRef['force-overwrite']}" -eq 1 ]] && optsList+=('-y')

  local qualityOptsList=()
  [[ ! -z "${quality}" ]] && qualityOptsList+=('-aq' "${quality}")

  local -r fadeOutStartSec=$(bc -l <<<"${audioDurationSec} - ${fadeOutDurationSec}")

  local -r fadeFilterIn="afade=t=in:d=${argsMapRef['duration-in']}:curve=${argsMapRef['curve-out']}"
  local -r fadeFilterOut="afade=t=out:st=${fadeOutStartSec}:d=${fadeOutDurationSec}:curve=${argsMapRef['curve-out']}"

  local -r fadeFilter="${fadeFilterIn},${fadeFilterOut}"

  ffmpeg "${optsList[@]}" \
         -i "${fin}" \
         -avoid_negative_ts make_zero \
         -af "${fadeFilter}" \
         "${qualityOptsList[@]}" \
         "${fout}"
  
  # shellcheck disable=SC2181 
  [[ $? -ne 0 ]] && {
   printf 'error: in-out-fading failed\n' >&2
   return 1
  }
  return 0
}

# TODO dublicated code in aio-fade, aio-concat, aio-normalize, aio-mix#
#
# @param $1 expected file codec type
#           can be one of [audio, video]
# @param $2 input-file path and name
#
# @return 0 input-file codec is as expected
# @return 1 input-file codec is not as expected
ffprobe-is-codec-type() {
  local -r expectedCodecType="$1"
  local -r fin="$2"

  local codecType
  codecType=$(ffprobe -loglevel error -show_entries stream=codec_type -of default=nk=1:nw=1 "${fin}" 2>/dev/null)
  
  # shellcheck disable=SC2181
  [[ $? -ne 0 ]] && return 1
  [[ "${codecType}" != "${expectedCodecType}" ]] && return 1
  
  return 0
}

# TODO dublicated code in aio-fade, aio-normalize, aio-mix, aio-concat
ffprobe-get-codec-name() {
  local -r fin="$1"

  local codecName
  codecName=$(ffprobe -loglevel error -show_entries stream=codec_name -of default=nk=1:nw=1 "${fin}" 2>/dev/null)

  # shellcheck disable=SC2181
  [[ $? -ne 0 ]] && return 1
  [[ -z "${codecName}" ]] && return 1

  printf '%s\n' "${codecName}"
  return 0
}

do-run() {

  local cmd
  local codecQuality

  local finList=()
  declare -A cmdArgs=(
    [force-overwrite]=0
    [dry-run]=0
  )

  while [ "$1" ]; do
    case "$1" in
      -h|--help)
        ! handle-help && return 1
        return 0
        ;;
      h|help)
        shift
        ! handle-help "$@" && return 1
        return 0
        ;;
      --version)
        print-version
        return 0
        ;;
      -d|--dry-run)
        cmdArgs["dry-run"]=1
        ;;
      -q|--quality)
        shift
        codecQuality="$1"
        ;;
      -f|--force-overwrite)
        cmdArgs["force-overwrite"]=1
        ;;
      s|settings)
        print-settings
        return 0
        ;;
      i|in)
        cmd=ffmpeg-fade-in
        ;;
      o|out)
        cmd=ffmpeg-fade-out
        ;;
      io|inout)
        cmd=ffmpeg-fade-inout
        ;;
      --di|---duration-in-sec)
        shift
        cmdArgs["duration-in"]="${1:-null}"
        ;;
      --do|---duration-out-sec)
        shift
        cmdArgs["duration-out"]="${1:-null}"
        ;;
      --ci|--curve-in)
        shift
        cmdArgs["curve-in"]="${1:-null}"
        ;;
      --co|--curve-out)
        shift
        cmdArgs["curve-out"]="${1:-null}"
        ;;
      *)
        finList=("$@")
        break
        ;;
    esac
    shift
  done

  local durationIn
  local durationOut

  case "${cmd}" in
    ffmpeg-fade-in)
      [[ -z "${cmdArgs['curve-in']}" ]] && cmdArgs['curve-in']="${__in_curve_default}"
      [[ -z "${cmdArgs['duration-in']}" ]] && cmdArgs['duration-in']="${__in_duration_sec_default}"

      durationIn="${cmdArgs['duration-in']}"
      ! verify-duration "${durationIn}" && {
        printf 'error: invalid fade-in duration time [%s] ... stopping\n' "${durationIn}" >&2
        return 1
      }
      
      # ffmpeg does not accept .5 but 0.5
      [[ "${durationIn}" =~ ^[.][0-9]+$ ]] && cmdArgs['duration-in']="0${durationIn}"
      cmdArgs['prefix']="${__out_file_prefix_in}"
      ;;
    ffmpeg-fade-out)
      [[ -z "${cmdArgs['curve-out']}" ]] && cmdArgs['curve-out']="${__out_curve_default}"
      [[ -z "${cmdArgs['duration-out']}" ]] && cmdArgs['duration-out']="${__out_duration_sec_default}"

      durationOut="${cmdArgs['duration-out']}"
      
      ! verify-duration "${durationOut}" && {
        printf 'error: invalid fade-out duration time [%s] ... stopping\n' "${durationOut}" >&2
        return 1
      }
      # ffmpeg does not accept .5 but 0.5
      [[ "${durationOut}" =~ ^[.][0-9]+$ ]] && cmdArgs['duration-out']="0${durationOut}"
      cmdArgs["prefix"]="${__out_file_prefix_out}"
      ;;
    ffmpeg-fade-inout)
      [[ -z "${cmdArgs["curve-in"]}" ]] && cmdArgs["curve-in"]="${__in_curve_default}"
      [[ -z "${cmdArgs["curve-out"]}" ]] && cmdArgs["curve-out"]="${__out_curve_default}"
      [[ -z "${cmdArgs["duration-in"]}" ]] && cmdArgs["duration-in"]="${__in_duration_sec_default}"
      [[ -z "${cmdArgs["duration-out"]}" ]] && cmdArgs["duration-out"]="${__out_duration_sec_default}"

      durationIn="${cmdArgs['duration-in']}"
      durationOut="${cmdArgs['duration-out']}"
      
      ! verify-duration "${durationIn}" && {
        printf 'error: invalid fade-in duration time [%s] ... stopping\n' "${durationIn}" >&2
        return 1
      }

      ! verify-duration "${durationOut}" && {
        printf 'error: invalid fade-out duration time [%s] ... stopping\n' "${durationOut}" >&2
        return 1
      }

      # ffmpeg does not accept .5 but 0.5
      [[ "${durationIn}" =~ ^[.][0-9]+$ ]] && cmdArgs['duration-in']="0${durationIn}"
      [[ "${durationOut}" =~ ^[.][0-9]+$ ]] && cmdArgs['duration-out']="0${durationOut}"
      cmdArgs['prefix']="${__out_file_prefix_inout}"
      ;;
    *)
      printf 'error: unknown command [%s] ... stopping\n' "${cmd:-none}" >&2
      return 1
      ;;
  esac
  
  [[ ! -z "${codecQuality}" ]] && [[ ! "${codecQuality}" =~ ^[0-9][0-9]*$ ]] && {
    printf 'error: value of codec-quality is invalid [%s] ... stopping\n' "${codecQuality}" >&2
    return 1
  }

  [[ "${#finList[*]}" -eq 0 ]] && {
    printf 'error: no input files provided ... stopping\n' >&2
    return 1
  }

  local fin
  local errorOccured=0

  for fin in "${finList[@]}"; do
    [[ -f "${fin}" ]] && continue

    errorOccured=1
    printf 'error: file [%s] does not exist ... skipping\n' "${fin}" >&2
  done

  [[ "${errorOccured}" -ne 0 ]] && return 1

  local fout
  local codecName

  for fin in "${finList[@]}"; do

    # skip non-audio files
    ! ffprobe-is-codec-type audio "${fin}" && {
      printf 'error: input file [%s] is no audio file ... skipping\n' "${fin}" >&2
      errorOccured=1
      continue
    }

    cmdArgs['duration-audio']=$(ffmpeg-get-audio-duration-sec "${fin}")
    
    # shellcheck disable=SC2181
    if [[ $? -ne 0 ]]; then
      errorOccured=1
      continue
    fi

    # get default quality for codec of current input file
    if [[ -z "${codecQuality}" ]]; then
      codecName=$(ffprobe-get-codec-name "${fin}")
      # shellcheck disable=SC2181
      [[ $? -eq 0 ]] && cmdArgs[quality]="${__codec_quality_settings[${codecName}]}"
    else
      cmdArgs[quality]="${codecQuality}"
    fi

    fout="${cmdArgs['prefix']}-${fin}"

    if [[ "${cmdArgs['dry-run']}" -eq 1 ]]; then
      print-processing-data "${cmd}" cmdArgs "${fout}" "${fin}"
    else
      ! ${cmd} cmdArgs "${fout}" "${fin}" && errorOccured=1
    fi
  done

  [[ "${errorOccured}" -ne 0 ]] && return 1
  return 0
}

do-run "$@"
