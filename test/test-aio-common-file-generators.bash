#!/usr/bin/env bash

# This file is part of aio-proc
#
#     aio-proc is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     aio-proc is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with aio-proc. If not, see <http://www.gnu.org/licenses/>.

# in seconds
declare -r -g __aio_signal_duration_sec_default=5

# normalized percentage from 0 to 1
declare -r -g __aio_signal_amplitude_norm_default=1

# in hertz
declare -r -g __aio_sine_frequency_hz_default=1

# in hertz
declare -r -g __aio_noise_samplerate_hz_default=44100

# one of [white, pink, ]
# see https://ffmpeg.org/ffmpeg-filters.html#anoisesrc
declare -r -g __aio_noise_color_default=white

# generates silence
# @param $1 output file, with ogg extension
# @param $2 duration - default 5 (seconds)
aio-generate-ogg-file-silence() {
  local -r duration="${2:-${__aio_signal_duration_sec_default}}"
  ffmpeg -f lavfi -i anullsrc -t "${duration}" -f ogg -c:a libvorbis "${1}"
}

# generates a sine wave, VBR encoding
# @param $1 output file, with ogg extension
# @param $2 duration  - default 5 (seconds)
# @param $3 amplitude - default 1 (100%)
# @param $4 frequency - default 1 (hz)
aio-generate-ogg-file-sine() {
  local -r duration="${2:-${__aio_signal_duration_sec_default}}"
  local -r amplitude="${3:-${__aio_signal_amplitude_norm_default}}"
  local -r frequency="${4:-${__aio_sine_frequency_hz_default}}"
  ffmpeg -f lavfi -i "sine=f=${frequency}:d=${duration}" -af "volume=${amplitude}" -f ogg -c:a libvorbis -aq 8 "${1}"
}

# generates noise, VBR encoding
# @param $1 output file, with ogg extension
# @param $2 duration    - default 5 (seconds)
# @param $3 amplitude   - default 1 (100%)
# @param $4 sample rate - default 44100 (hz)
# @param $5 noise color - default white
aio-generate-ogg-file-noise() {
  local -r duration="${2:-${__aio_signal_duration_sec_default}}"
  local -r amplitude="${3:-${__aio_signal_amplitude_norm_default}}"
  local -r rate="${4:-${__aio_noise_samplerate_hz_default}}"
  local -r color="${5:-${__aio_noise_color_default}}"
  ffmpeg -f lavfi -i "anoisesrc=d=${duration}:r=${rate}:color=${color}:a=${amplitude}" -f ogg -c:a libvorbis -aq 8 "$1"
}

# generates a sine wave, VBR encoding
# @param $1 output file, with mp3 extension
# @param $2 duration  - default 5 (seconds)
# @param $3 amplitude - default 1 (100%)
# @param $4 frequency - default 1 (hz)
aio-generate-mp3-file-sine() {
  local -r duration="${2:-${__aio_signal_duration_sec_default}}"
  local -r amplitude="${3:-${__aio_signal_amplitude_norm_default}}"
  local -r frequency="${4:-${__aio_sine_frequency_hz_default}}"
  ffmpeg -f lavfi -i "sine=f=${frequency}:d=${duration}" -af "volume=${amplitude}" -f mp3 -c:a libmp3lame -aq 1 "$1"
}

# generates noise, VBR encoding
# @param $1 output file, with mp3 extension
# @param $2 duration    - default 5 (seconds)
# @param $3 amplitude   - default 1 (100%)
# @param $4 sample rate - default 44100 (hz)
# @param $5 noise color - default white
aio-generate-mp3-file-noise() {
  local -r duration="${2:-${__aio_signal_duration_sec_default}}"
  local -r amplitude="${3:-${__aio_signal_amplitude_norm_default}}"
  local -r rate="${4:-${__aio_noise_samplerate_hz_default}}"
  local -r color="${5:-${__aio_noise_color_default}}"
  ffmpeg -f lavfi -i "anoisesrc=d=${duration}:r=${rate}:color=${color}:a=${amplitude}" -f mp3 -c:a libmp3lame -aq 1 "$1"
}

# generates noise, lossless
# @param $1 output file, with flac extension
# @param $2 duration    - default 5 (seconds)
# @param $3 amplitude   - default 1 (100%)
# @param $4 sample rate - default 44100 (hz)
# @param $5 noise color - default white
aio-generate-flac-file-noise() {
  local -r duration="${2:-${__aio_signal_duration_sec_default}}"
  local -r amplitude="${3:-${__aio_signal_amplitude_norm_default}}"
  local -r rate="${4:-${__aio_noise_samplerate_hz_default}}"
  local -r color="${5:-${__aio_noise_color_default}}"
  ffmpeg -f lavfi -i "anoisesrc=d=${duration}:r=${rate}:color=${color}:a=${amplitude}" -f flac -c:a flac "$1"
}

# generates a sine wave, lossless
# @param $1 output file, with flac extension
# @param $2 duration  - default 5 (seconds)
# @param $3 amplitude - default 1 (100%)
# @param $4 frequency - default 1 (hz)
aio-generate-flac-file-sine() {
  local -r duration="${2:-${__aio_signal_duration_sec_default}}"
  local -r amplitude="${3:-${__aio_signal_amplitude_norm_default}}"
  local -r frequency="${4:-${__aio_sine_frequency_hz_default}}"
  ffmpeg -f lavfi -i "sine=f=${frequency}:d=${duration}" -af "volume=${amplitude}" -f flac -c:a flac "${1}"
}

# generates a sine wave, VBR encoding
# @param $1 output file, with mp3 extension
# @param $2 duration  - default 5 (seconds)
# @param $3 amplitude - default 1 (100%)
# @param $4 frequency - default 1 (hz)
aio-generate-opus-file-sine() {
  local -r duration="${2:-${__aio_signal_duration_sec_default}}"
  local -r amplitude="${3:-${__aio_signal_amplitude_norm_default}}"
  local -r frequency="${4:-${__aio_sine_frequency_hz_default}}"
  ffmpeg -f lavfi -i "sine=f=${frequency}:d=${duration}" -af "volume=${amplitude}" -f opus -c:a libopus -ab 256000 "$1"
}

# generates a sine wave, VBR encoding
# @param $1 output file, with mp3 extension
# @param $2 duration  - default 5 (seconds)
# @param $3 amplitude - default 1 (100%)
# @param $4 frequency - default 1 (hz)
aio-generate-wav-file-sine() {
  local -r duration="${2:-${__aio_signal_duration_sec_default}}"
  local -r amplitude="${3:-${__aio_signal_amplitude_norm_default}}"
  local -r frequency="${4:-${__aio_sine_frequency_hz_default}}"
  ffmpeg -f lavfi -i "sine=f=${frequency}:d=${duration}" -af "volume=${amplitude}" -f wav -c:a pcm_s16le -ar 44100 -ac 1 "$1"
}

# generates a sine wave, VBR encoding
# @param $1 output file, with mp3 extension
# @param $2 duration  - default 5 (seconds)
# @param $3 amplitude - default 1 (100%)
# @param $4 frequency - default 1 (hz)
aio-generate-speex-file-sine() {
  local -r duration="${2:-${__aio_signal_duration_sec_default}}"
  local -r amplitude="${3:-${__aio_signal_amplitude_norm_default}}"
  local -r frequency="${4:-${__aio_sine_frequency_hz_default}}"
  ffmpeg -f lavfi -i "sine=f=${frequency}:d=${duration}" -af "volume=${amplitude}" -f spx -c:a libspeex -aq 10 "$1"
}
