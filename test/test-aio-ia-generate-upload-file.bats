#!/usr/bin/env bats

# This file is part of aio-proc
#
#     aio-proc is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     aio-proc is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with aio-proc. If not, see <http://www.gnu.org/licenses/>.

# TODO
#
#

###########
# Test Data
###########

declare -g -r -a __creator_collector_option_list=(
  '-c me'
  '--creator me'
  '-g podcast'
  '--collection podcast'
)

# same order as __creator_collector_option_list
declare -g -r -a __creator_collector_expected_column_header_list=(
  'creator'
  'creator'
  'collection'
  'collection'
)

declare -g -r -a __collection_short_option_shortcut_list=(
  '--ga'
  '--gm'
  '--ge'
  '--gt'
  '--gi'
  '--gs'
)

# same order as in __collection_short_option_shortcut_list
declare -g -r -a __collection_long_option_shortcut_list=(
  '--collection-audio'
  '--collection-movie'
  '--collection-test'
  '--collection-text'
  '--collection-image'
  '--collection-software'
)

# same order as in __collection_short_option_shortcut_list
declare -g -r -a __collection_row_data_list=(
  'opensource_audio'
  'opensource_movies'
  'test_collection'
  'opensource'
  'opensource_image'
  'open_source_software'
)

declare -g -r -a __mediatype_short_option_shortcut_list=(
  '--ma'
  '--md'
  '--mi'
  '--mm'
  '--mt'
)

# same order as in __mediatype_short_option_shortcut_list
declare -g -r -a __mediatype_long_option_shortcut_list=(
  '--mediatype-audio'
  '--mediatype-data'
  '--mediatype-image'
  '--mediatype-movie'
  '--mediatype-text'
)

# same order as in __mediatype_short_option_shortcut_list
declare -g -r -a __mediatype_row_data_list=(
  'audio'
  'data'
  'image'
  'movies'
  'texts'
)

# same order as __expected_placeholder_key_list
declare -g -r -a __mandatory_placeholder_option_list=(
  "-I"
  "--placeholder-identifier"
  "-M"
  "--placeholder-mediatype"
)

declare -g -r -a __optional_placeholder_option_list=(
  "-C"
  "--placeholder-creator"
  "-G"
  "--placeholder-collection"
)

# same order as __mandatory_placeholder_option_list
declare -g -r -a __expected_mandatory_placeholder_key_list=(
  "identifier"
  "identifier"
  "mediatype"
  "mediatype"
)

# same order as __optional_placeholder_option_list
declare -g -r -a __expected_optional_placeholder_key_list=(
  "creator"
  "creator"
  "collection"
  "collection"
)

declare -g -r -a __cl_metadata_key_list=(
  "identifier"
  "creator"
  "collection"
  "mediatype"
)

declare -g -r -a __creator_option_list=(
  '-c me'
  '--creator me'
)

declare -g -r -a __collection_option_list=(
  '-g podcast'
  '--collection podcast'
)

declare -g -r -a __identifier_option_list=(
  '-i testitem'
  '--identifier testitem'
)

declare -g -r -a __mediatype_option_list=(
  '-m data'
  '--mediatype data'
)

declare -g -r -a __metadata_key_with_special_char_list=(
  "a,identifier testitem"
  "a,collection podcast"
  "a,mediatype texts"
  "a,creator me"
  "a_identifier testitem"
  "a_collection podcast"
  "a_mediatype texts"
  "a_creator me"
  "a-identifier testitem"
  "a-collection podcast"
  "a-mediatype texts"
  "a-creator me"
)

declare -g -r -a __metadata_value_with_comma_list=(
  "identifier a,identifier"
  "collection a,collection"
  "mediatype a,text"
  "creator a,creator"
)

declare -g -r -a __metadata_value_with_whitespace_list=(
  "identifier a identifier"
  "collection a collection"
  "mediatype a text"
)

declare -g -r __line_with_whitespaces='      '
declare -g -r __line_with_tabs="$(printf '\t\t\t')"

###########
# Helpers
###########

declare -g -r __test_path="$(pwd)"
declare -g -r __bin_path="$(realpath ${__test_path}/..)"

# files are expected to reside in __workspace_path
declare -g -r __expected_csv_file="testitem-ia-upload.csv"
declare -g -r __expected_csv_file_default="ia-upload.csv"
declare -g -r __expected_csv_file_custom="my-upload.csv"
declare -g -r __metadata_file="custom-metadata.txt"

declare -g -r -a __files_to_cleanup=(
  "${__expected_csv_file}"
  "${__expected_csv_file_default}"
  "${__expected_csv_file_custom}"
  "${__metadata_file}"
)

declare -g __workspace_path
declare -g __file_to_upload_a
declare -g __file_to_upload_b
declare -g __upload_files

remove-temporary-files() {
  cd "${__workspace_path}"
  rm -f "${__files_to_cleanup[@]}"
}

###########
# Asserts
###########

assert-csv-file-is-complete() { 
  local -r csvFile="$1"
  local -r numExpectedLines="$2"
  
  [ -f "${csvFile}" ]

  local lineCount=$( wc -l <"${csvFile}" )
  [ "${lineCount}" -eq "${numExpectedLines}" ]
}

assert-csv-file-header() {
  local -r csvFile="$1"
  
  local expectedHeader
  read -r expectedHeader

  local -r availableHeader=$(head -1 "${csvFile}")
  [ "${availableHeader}" == "${expectedHeader}" ]
}

assert-csv-file-rows() {
  local -r csvFile="$1"
  local -r numRows="$2"
  
  local expectedLine
  local expectedLines=()

  while read -r expectedLine; do
    expectedLines+=("${expectedLine}")
  done
  
  local i=0
  local availableLine

  while read -r availableLine; do
    [ "${availableLine}" == "${expectedLines[$i]}" ]
    i=$((i+1))
  done < <(tail -"${numRows}" "${csvFile}")
}

###########
# Setup and Teardown
###########

setup() {
  remove-temporary-files
}

teardown() {
  remove-temporary-files
}

setup_file() {

  __workspace_path=$(mktemp -d -p "${XDG_RUNTIME_DIR}" "aio-test-XXX")
  __file_to_upload_a="${__workspace_path}/test-a.ogg"
  __file_to_upload_b="${__workspace_path}/test-b.ogg"  
  __upload_files="${__file_to_upload_a} ${__file_to_upload_b}"

  export __workspace_path
  export __file_to_upload_a
  export __file_to_upload_b
  export __upload_files
  
  mkdir -p "${__workspace_path}"
  touch ${__upload_files}
}

teardown_file() {
  local -r workspacePath=$(realpath "${__workspace_path}")

  [[ -d "${workspacePath}" ]] && [[ "${__workspace_path}" != '/' ]] && {
    rm -r "${__workspace_path}"
  }
}

###########
# Tests
###########


@test "invoke aio-ia-generate-upload-file - help" {
  run ${__bin_path}/aio-ia-generate-upload-file -h
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-ia-generate-upload-file --help
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-ia-generate-upload-file help
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-ia-generate-upload-file h
  [ ${status} -eq 0 ]
}

@test "invoke aio-ia-generate-upload-file - help short" {

  run ${__bin_path}/aio-ia-generate-upload-file help short
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-ia-generate-upload-file help s
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-ia-generate-upload-file h s
  [ ${status} -eq 0 ]
}

@test "invoke aio-ia-generate-upload-file - help details" {

  run ${__bin_path}/aio-ia-generate-upload-file help details
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-ia-generate-upload-file help d
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-ia-generate-upload-file h d
  [ ${status} -eq 0 ]
}

@test "invoke aio-ia-generate-upload-file - help archive" {

  run ${__bin_path}/aio-ia-generate-upload-file help archive
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-ia-generate-upload-file help a
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-ia-generate-upload-file h a
  [ ${status} -eq 0 ]
}

@test "invoke aio-ia-generate-upload-file - help metadata" {

  run ${__bin_path}/aio-ia-generate-upload-file help metadata
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-ia-generate-upload-file help m
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-ia-generate-upload-file h m
  [ ${status} -eq 0 ]
}

@test "invoke aio-ia-generate-upload-file - help output" {

  run ${__bin_path}/aio-ia-generate-upload-file help output
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-ia-generate-upload-file help o
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-ia-generate-upload-file h o
  [ ${status} -eq 0 ]
}

@test "invoke aio-ia-generate-upload-file - version" {
  run ${__bin_path}/aio-ia-generate-upload-file --version
  [ ${status} -eq 0 ]
}

@test "invoke aio-ia-generate-upload-file - no command" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-upload-file

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: no valid command provided [] ... stopping' ]
}

@test "invoke aio-ia-generate-upload-file - unknown command" {
  
  cd "${__workspace_path}"
  
  run ${__bin_path}/aio-ia-generate-upload-file generate

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: no valid command provided [generate] ... stopping' ]
}

@test "invoke aio-ia-generate-upload-file - no input file" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-upload-file new

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: no input files have been provided ... stopping' ]
  
  run ${__bin_path}/aio-ia-generate-upload-file append

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: no input files have been provided ... stopping' ]
}

@test "invoke aio-ia-generate-upload-file - unknown argument" {

  cd "${__workspace_path}"

  # -a does not exist as option and is interpreted as filename
  run ${__bin_path}/aio-ia-generate-upload-file new -i testitem -a image dummyIn1 ${__upload_files}
  
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: input file [-a] does not exist' ]
  [ "${lines[1]}" == 'error: input file [image] does not exist' ]

  # -a does not exist as option, script immediately exits and does not check remaining options
  run ${__bin_path}/aio-ia-generate-upload-file new -a image -i testitem dummyIn1 ${__upload_files}
  
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: input file [-a] does not exist' ]
  [ "${lines[1]}" == 'error: input file [image] does not exist' ]
  
  # -a does not exist as option and is interpreted as filename
  run ${__bin_path}/aio-ia-generate-upload-file append -i testitem -a image dummyIn1 ${__upload_files}
  
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: input file [-a] does not exist' ]
  [ "${lines[1]}" == 'error: input file [image] does not exist' ]

  # -a does not exist as option, script immediately exits and does not check remaining options
  run ${__bin_path}/aio-ia-generate-upload-file append -a image -i testitem dummyIn1 ${__upload_files}
  
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: input file [-a] does not exist' ]
  [ "${lines[1]}" == 'error: input file [image] does not exist' ]
}

@test "invoke aio-ia-generate-upload-file - cl - new - no identifier" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-upload-file new ${__upload_files}
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: you need to provide metadata item [identifier] ... stopping' ]
}

@test "invoke aio-ia-generate-upload-file - cl - new - no mediatype" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-upload-file new -i testitem ${__upload_files}
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: you need to provide metadata item [mediatype] ... stopping' ]
}

@test "invoke aio-ia-generate-upload-file - cl - new - metadata - values" {

  cd "${__workspace_path}"

  local expectedColumnHeader
  local option
  local value
  
  for index in "${!__creator_collector_option_list[@]}"; do

    expectedColumnHeader="${__creator_collector_expected_column_header_list[${index}]}"
    read -r option value <<<"${__creator_collector_option_list[${index}]}"

    run ${__bin_path}/aio-ia-generate-upload-file new \
      -i testitem \
      -m audio \
      "${option}" "${value}" \
      ${__upload_files}

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 3

    assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,mediatype,${expectedColumnHeader},file
EOF

    # one row for each file
    assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
testitem,audio,${value},${__file_to_upload_a}
testitem,audio,${value},${__file_to_upload_b}
EOF

  done
}

@test "invoke aio-ia-generate-upload-file - cl - new - metadata - value with whitespace" {

  cd "${__workspace_path}"
  
  run ${__bin_path}/aio-ia-generate-upload-file new \
      -i testitem -m audio \
      --creator 'its me' \
      ${__upload_files}

  [ "${status}" -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 3
      
  assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,mediatype,creator,file
EOF

    # one row for each file
    assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
testitem,audio,its me,${__file_to_upload_a}
testitem,audio,its me,${__file_to_upload_b}
EOF

}

@test "invoke aio-ia-generate-upload-file - cl - new - metadata - value with whitespace - not allowed" {

  cd "${__workspace_path}"
  
  local metadata
  local key
  local value

  for metadata in "${__metadata_value_with_whitespace_list[@]}"; do
  
    read -r key value <<<"${metadata}"
    # no need to provide identifier and mediatype as we expect an error during input validation
    run ${__bin_path}/aio-ia-generate-upload-file new \
      "--${key}" "${value}" \
      ${__upload_files}
    
    [ ${status} -eq 1 ]
    [ ! -f "a identifier-ia-upload.csv" ]
    [ "${output}" == "error: value for key [${key}] contains one or more whitespaces [${value}] ... stopping" ]

  done
}

@test "invoke aio-ia-generate-upload-file - cl - new - metadata - value with comma - not allowed" {

  cd "${__workspace_path}"
  
  local metadata
  local key
  local value

  for metadata in "${__metadata_value_with_comma_list[@]}"; do
  
    read -r key value <<<"${metadata}"
    # no need to provide identifier and mediatype as we expect an error during input validation
    run ${__bin_path}/aio-ia-generate-upload-file new \
      "--${key}" "${value}" \
      ${__upload_files}

    [ ${status} -eq 1 ]
    [ ! -f "a,identifier-ia-upload.csv" ]
    [ "${output}" == "error: value for key [${key}] contains a comma [${value}] ... stopping" ]

  done
}

@test "invoke aio-ia-generate-upload-file - cl - new - metadata - mediatype - shortcuts" {

  cd "${__workspace_path}"

  local index
  local shortcutOption
  local expectedValue

  for index in "${!__mediatype_short_option_shortcut_list[@]}"; do
    
    shortcutOption="${__mediatype_short_option_shortcut_list[${index}]}"
    expectedValue="${__mediatype_row_data_list[${index}]}"
    
    run ${__bin_path}/aio-ia-generate-upload-file new \
      --identifier testitem \
      --collection podcast \
      "${shortcutOption}" \
      ${__upload_files}
  
    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 3

    assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,collection,mediatype,file
EOF

    assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
testitem,podcast,${expectedValue},${__file_to_upload_a}
testitem,podcast,${expectedValue},${__file_to_upload_b}
EOF

  done
  
  for index in "${!__mediatype_long_option_shortcut_list[@]}"; do
    
    shortcutOption="${__mediatype_long_option_shortcut_list[${index}]}"
    expectedValue="${__mediatype_row_data_list[${index}]}"
    
    run ${__bin_path}/aio-ia-generate-upload-file new \
      --identifier testitem \
      --collection podcast \
      "${shortcutOption}" \
      ${__upload_files}
  
    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 3

    assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,collection,mediatype,file
EOF

    assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
testitem,podcast,${expectedValue},${__file_to_upload_a}
testitem,podcast,${expectedValue},${__file_to_upload_b}
EOF

  done

}

@test "invoke aio-ia-generate-upload-file - cl - new - metadata - mediatype - shortcuts - dublicates - not allowed" {
  
  cd "${__workspace_path}"
  
  local shortcutOption
  for shortcutOption in "${__mediatype_short_option_shortcut_list[@]}"; do
    
    run ${__bin_path}/aio-ia-generate-upload-file new \
      --identifier testitem \
      --collection podcast \
      "${shortcutOption}" \
      "${shortcutOption}" \
      dummyA dummyB
  
    [ ${status} -eq 1 ]
    [ ! -f "${__expected_csv_file}" ]
    [ "${output}" == 'error: the following metadata keys have been provided more than once on cl [mediatype] ... stopping' ]

  done
  
  for shortcutOption in "${__mediatype_long_option_shortcut_list[@]}"; do
    
    run ${__bin_path}/aio-ia-generate-upload-file new \
      --identifier testitem \
      --collection podcast \
      "${shortcutOption}" \
      "${shortcutOption}" \
      dummyA dummyB
  
    [ ${status} -eq 1 ]
    [ ! -f "${__expected_csv_file}" ]
    [ "${output}" == 'error: the following metadata keys have been provided more than once on cl [mediatype] ... stopping' ]

  done
}

@test "invoke aio-ia-generate-upload-file - cl - new - metadata - collection - shortcuts" {

  cd "${__workspace_path}"

  local index
  local shortcutOption
  local expectedValue

  for index in "${!__collection_short_option_shortcut_list[@]}"; do
    
    shortcutOption="${__collection_short_option_shortcut_list[${index}]}"
    expectedValue="${__collection_row_data_list[${index}]}"
    
    run ${__bin_path}/aio-ia-generate-upload-file new \
      --identifier testitem \
      --mediatype texts \
      "${shortcutOption}" \
      ${__upload_files}
  
    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 3

    assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,mediatype,collection,file
EOF

    assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
testitem,texts,${expectedValue},${__file_to_upload_a}
testitem,texts,${expectedValue},${__file_to_upload_b}
EOF

  done
  
  for index in "${!__collection_long_option_shortcut_list[@]}"; do
    
    shortcutOption="${__collection_long_option_shortcut_list[${index}]}"
    expectedValue="${__collection_row_data_list[${index}]}"
    
    run ${__bin_path}/aio-ia-generate-upload-file new \
      --identifier testitem \
      --mediatype texts \
      "${shortcutOption}" \
      ${__upload_files}
  
    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 3

    assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,mediatype,collection,file
EOF

    assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
testitem,texts,${expectedValue},${__file_to_upload_a}
testitem,texts,${expectedValue},${__file_to_upload_b}
EOF

  done

}

@test "invoke aio-ia-generate-upload-file - cl - new - metadata - collection - shortcuts - dublicates - not allowed" {

  cd "${__workspace_path}"

  local shortcutOption
  for shortcutOption in "${__collection_short_option_shortcut_list[@]}"; do

    run ${__bin_path}/aio-ia-generate-upload-file new \
      --identifier testitem \
      --mediatype texts \
      "${shortcutOption}" \
      "${shortcutOption}" \
      dummyA dummyB

    [ ${status} -eq 1 ]
    [ ! -f "${__expected_csv_file}" ]
    [ "${output}" == 'error: the following metadata keys have been provided more than once on cl [collection] ... stopping' ]

  done

  for shortcutOption in "${__collection_long_option_shortcut_list[@]}"; do

    run ${__bin_path}/aio-ia-generate-upload-file new \
      --identifier testitem \
      --mediatype texts \
      "${shortcutOption}" \
      "${shortcutOption}" \
      dummyA dummyB

    [ ${status} -eq 1 ]
    [ ! -f "${__expected_csv_file}" ]
    [ "${output}" == 'error: the following metadata keys have been provided more than once on cl [collection] ... stopping' ]

  done
}

@test "invoke aio-ia-generate-upload-file - cl - new - metadata - placeholders" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-upload-file new \
    -I -M \
    ${__upload_files}

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file_default}" 3

    assert-csv-file-header "${__expected_csv_file_default}" <<EOF
identifier,mediatype,file
EOF

    assert-csv-file-rows "${__expected_csv_file_default}" 2 <<EOF
,,${__file_to_upload_a}
,,${__file_to_upload_b}
EOF

    run ${__bin_path}/aio-ia-generate-upload-file new \
    --placeholder-identifier --placeholder-mediatype \
    ${__upload_files}

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file_default}" 3

    assert-csv-file-header "${__expected_csv_file_default}" <<EOF
identifier,mediatype,file
EOF

    assert-csv-file-rows "${__expected_csv_file_default}" 2 <<EOF
,,${__file_to_upload_a}
,,${__file_to_upload_b}
EOF

  local index
  local placeholderOption
  local expectedColumnHeader

  for index in "${!__optional_placeholder_option_list[@]}"; do
    
    placeholderOption="${__optional_placeholder_option_list[${index}]}"
    expectedColumnHeader="${__expected_optional_placeholder_key_list[${index}]}"
    
    run ${__bin_path}/aio-ia-generate-upload-file new \
    --placeholder-identifier --placeholder-mediatype \
    "${placeholderOption}" \
    ${__upload_files}

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file_default}" 3
    
    assert-csv-file-header "${__expected_csv_file_default}" <<EOF
identifier,mediatype,${expectedColumnHeader},file
EOF

    assert-csv-file-rows "${__expected_csv_file_default}" 2 <<EOF
,,,${__file_to_upload_a}
,,,${__file_to_upload_b}
EOF
  done

}

@test "invoke aio-ia-generate-upload-file - cl - append - no identifier" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-upload-file append ${__upload_files}
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: you need to provide metadata item [identifier] ... stopping' ]
}

@test "invoke aio-ia-generate-upload-file - cl - append - no mediatype" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-upload-file append -i testitem ${__upload_files}
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: you need to provide metadata item [mediatype] ... stopping' ]
}

@test "invoke aio-ia-generate-upload-file - cl - append - metadata - values" {

  cd "${__workspace_path}"

  local option
  local value
  local expectedRowList=()

  for index in "${!__creator_collector_option_list[@]}"; do

    read -r option value <<<"${__creator_collector_option_list[${index}]}"

    expectedRowList+=(
      "testitem,audio,${value},${__file_to_upload_a}"
      "testitem,audio,${value},${__file_to_upload_b}"
    )

    run ${__bin_path}/aio-ia-generate-upload-file append \
      -i testitem \
      -m audio \
      "${option}" "${value}" \
      ${__upload_files}

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" "${#expectedRowList[*]}"

    # one row for each file
    printf '%s\n' "${expectedRowList[@]}" | assert-csv-file-rows "${__expected_csv_file}" "${#expectedRowList[*]}"

  done

}

@test "invoke aio-ia-generate-upload-file - cl - append - metadata - value with whitespace" {

  cd "${__workspace_path}"
  
  run ${__bin_path}/aio-ia-generate-upload-file append \
      -i testitem -m audio \
      --creator 'its me' \
      ${__upload_files}

  [ "${status}" -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 2

    # one row for each file
    assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
testitem,audio,its me,${__file_to_upload_a}
testitem,audio,its me,${__file_to_upload_b}
EOF

}

@test "invoke aio-ia-generate-upload-file - cl - append - metadata - value with whitespace - not allowed" {

  cd "${__workspace_path}"

  local metadata
  local key
  local value

  for metadata in "${__metadata_value_with_whitespace_list[@]}"; do
  
    read -r key value <<<"${metadata}"
    # no need to provide identifier and mediatype as we expect an error during input validation
    run ${__bin_path}/aio-ia-generate-upload-file append \
      "--${key}" "${value}" \
      ${__upload_files}
    
    [ ${status} -eq 1 ]
    [ "${output}" == "error: value for key [${key}] contains one or more whitespaces [${value}] ... stopping" ]

  done
}

@test "invoke aio-ia-generate-upload-file - cl - append - metadata - value with comma - not allowed" {

  cd "${__workspace_path}"

  local metadata
  local key
  local value

  for metadata in "${__metadata_value_with_comma_list[@]}"; do
  
    read -r key value <<<"${metadata}"
    # no need to provide identifier and mediatype as we expect an error during input validation
    run ${__bin_path}/aio-ia-generate-upload-file append \
      "--${key}" "${value}" \
      ${__upload_files}

    [ ${status} -eq 1 ]
    [ ! -f "a,identifier-ia-upload.csv" ]
    [ "${output}" == "error: value for key [${key}] contains a comma [${value}] ... stopping" ]

  done
}

@test "invoke aio-ia-generate-upload-file - cl - append - metadata - mediatype - shortcuts" {

  cd "${__workspace_path}"

  local index
  local shortcutOption
  local expectedValue
  local expectedRowList=()

  for index in "${!__mediatype_short_option_shortcut_list[@]}"; do

    shortcutOption="${__mediatype_short_option_shortcut_list[${index}]}"
    expectedValue="${__mediatype_row_data_list[${index}]}"

    expectedRowList+=(
      "testitem,podcast,${expectedValue},${__file_to_upload_a}"
      "testitem,podcast,${expectedValue},${__file_to_upload_b}"
    )

    run ${__bin_path}/aio-ia-generate-upload-file append \
      --identifier testitem \
      --collection podcast \
      "${shortcutOption}" \
      ${__upload_files}

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" "${#expectedRowList[*]}"

    # one row for each file
    printf '%s\n' "${expectedRowList[@]}" | assert-csv-file-rows "${__expected_csv_file}" "${#expectedRowList[*]}"

  done

  for index in "${!__mediatype_long_option_shortcut_list[@]}"; do

    shortcutOption="${__mediatype_long_option_shortcut_list[${index}]}"
    expectedValue="${__mediatype_row_data_list[${index}]}"

    expectedRowList+=(
      "testitem,podcast,${expectedValue},${__file_to_upload_a}"
      "testitem,podcast,${expectedValue},${__file_to_upload_b}"
    )
    
    run ${__bin_path}/aio-ia-generate-upload-file append \
      --identifier testitem \
      --collection podcast \
      "${shortcutOption}" \
      ${__upload_files}
  
    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" "${#expectedRowList[*]}"

    # one row for each file
    printf '%s\n' "${expectedRowList[@]}" | assert-csv-file-rows "${__expected_csv_file}" "${#expectedRowList[*]}"

  done

}

@test "invoke aio-ia-generate-upload-file - cl - append - metadata - mediatype - shortcuts - dublicates - not allowed" {

  cd "${__workspace_path}"

  local shortcutOption

  for shortcutOption in "${__mediatype_short_option_shortcut_list[@]}"; do

    run ${__bin_path}/aio-ia-generate-upload-file append \
      --identifier testitem \
      --collection podcast \
      "${shortcutOption}" \
      "${shortcutOption}" \
      dummyA dummyB

    [ ${status} -eq 1 ]
    [ ! -f "${__expected_csv_file}" ]
    [ "${output}" == "error: the following metadata keys have been provided more than once on cl [mediatype] ... stopping" ]

  done
  
  for shortcutOption in "${__mediatype_long_option_shortcut_list[@]}"; do

    run ${__bin_path}/aio-ia-generate-upload-file append \
      --identifier testitem \
      --collection podcast \
      "${shortcutOption}" \
      "${shortcutOption}" \
      dummyA dummyB
  
    [ ${status} -eq 1 ]
    [ ! -f "${__expected_csv_file}" ]
    [ "${output}" == "error: the following metadata keys have been provided more than once on cl [mediatype] ... stopping" ]

  done
}

@test "invoke aio-ia-generate-upload-file - cl - append - metadata - collection - shortcuts" {

  cd "${__workspace_path}"

  local index
  local shortcutOption
  local expectedValue
  local expectedRowList=()

  for index in "${!__collection_short_option_shortcut_list[@]}"; do
    
    shortcutOption="${__collection_short_option_shortcut_list[${index}]}"
    expectedValue="${__collection_row_data_list[${index}]}"
    
    expectedRowList+=(
      "testitem,texts,${expectedValue},${__file_to_upload_a}"
      "testitem,texts,${expectedValue},${__file_to_upload_b}"
    )

    run ${__bin_path}/aio-ia-generate-upload-file append \
      --identifier testitem \
      --mediatype texts \
      "${shortcutOption}" \
      ${__upload_files}
  
    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" "${#expectedRowList[*]}"
    
    # one row for each file
    printf '%s\n' "${expectedRowList[@]}" | assert-csv-file-rows "${__expected_csv_file}" "${#expectedRowList[*]}"

  done
  
  rm -f "${__expected_csv_file}"
  expectedRowList=()
  
  for index in "${!__collection_long_option_shortcut_list[@]}"; do
    
    shortcutOption="${__collection_long_option_shortcut_list[${index}]}"
    expectedValue="${__collection_row_data_list[${index}]}"
    
    expectedRowList+=(
      "testitem,texts,${expectedValue},${__file_to_upload_a}"
      "testitem,texts,${expectedValue},${__file_to_upload_b}"
    )
    
    run ${__bin_path}/aio-ia-generate-upload-file append \
      --identifier testitem \
      --mediatype texts \
      "${shortcutOption}" \
      ${__upload_files}

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" "${#expectedRowList[*]}"

    # one row for each file
    printf '%s\n' "${expectedRowList[@]}" | assert-csv-file-rows "${__expected_csv_file}" "${#expectedRowList[*]}"
  
  done

}

@test "invoke aio-ia-generate-upload-file - cl - append - metadata - collection - shortcuts - dublicates - not allowed" {

  cd "${__workspace_path}"

  local shortcutOption
  for shortcutOption in "${__collection_short_option_shortcut_list[@]}"; do

    run ${__bin_path}/aio-ia-generate-upload-file append \
      --identifier testitem \
      --mediatype texts \
      "${shortcutOption}" \
      "${shortcutOption}" \
      dummyA dummyB
      
    [ ${status} -eq 1 ]
    [ ! -f "${__expected_csv_file}" ]
    [ "${output}" == 'error: the following metadata keys have been provided more than once on cl [collection] ... stopping' ]

  done

  for shortcutOption in "${__collection_long_option_shortcut_list[@]}"; do

    run ${__bin_path}/aio-ia-generate-upload-file append \
      --identifier testitem \
      --mediatype texts \
      "${shortcutOption}" \
      "${shortcutOption}" \
      dummyA dummyB
  
    [ ${status} -eq 1 ]
    [ ! -f "${__expected_csv_file}" ]
    [ "${output}" == 'error: the following metadata keys have been provided more than once on cl [collection] ... stopping' ]

  done

}

@test "invoke aio-ia-generate-upload-file - cl - append - placeholders" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-upload-file append \
    -I -M \
    ${__upload_files}

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file_default}" 2

    assert-csv-file-rows "${__expected_csv_file_default}" 2 <<EOF
,,${__file_to_upload_a}
,,${__file_to_upload_b}
EOF

    run ${__bin_path}/aio-ia-generate-upload-file append \
    --placeholder-identifier --placeholder-mediatype \
    ${__upload_files}

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file_default}" 4

    assert-csv-file-rows "${__expected_csv_file_default}" 4 <<EOF
,,${__file_to_upload_a}
,,${__file_to_upload_b}
,,${__file_to_upload_a}
,,${__file_to_upload_b}
EOF

  rm -f "${__expected_csv_file_default}"

  local index
  local placeholderOption
  local expectedRowList=()

  for index in "${!__optional_placeholder_option_list[@]}"; do
    
    placeholderOption="${__optional_placeholder_option_list[${index}]}"
    
    expectedRowList+=(
      ",,,${__file_to_upload_a}"
      ",,,${__file_to_upload_b}"
    )

    run ${__bin_path}/aio-ia-generate-upload-file append \
      --placeholder-identifier --placeholder-mediatype "${placeholderOption}" \
      ${__upload_files}

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file_default}" "${#expectedRowList[*]}"

    # one row for each file
    printf '%s\n' "${expectedRowList[@]}" | assert-csv-file-rows "${__expected_csv_file_default}" "${#expectedRowList[*]}"
  done

}

@test "invoke aio-ia-generate-upload-file - stdin - new - metadata - read timeout" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-upload-file new ${__upload_files}
  
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: you need to provide metadata item [identifier] ... stopping' ]
  [ ! -f "${__expected_csv_file}" ]

  run ${__bin_path}/aio-ia-generate-upload-file new ${__upload_files} < <(sleep 2; printf 'identifier testitem')
  
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: you need to provide metadata item [identifier] ... stopping' ]
  [ ! -f "${__expected_csv_file}" ]
}

@test "invoke aio-ia-generate-upload-file - stdin - new - metadata - comments" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-upload-file new ${__upload_files} <<EOF
# a single line comment
creator me
collection podcast
# a
${__line_with_tabs}# multi
${__line_with_whitespaces}# line
${__line_with_tabs}${__line_with_whitespaces}# comment
identifier testitem
mediatype audio
EOF

  [ "${status}" -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 3

  assert-csv-file-header "${__expected_csv_file}" <<EOF
creator,collection,identifier,mediatype,file
EOF

  assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
me,podcast,testitem,audio,${__file_to_upload_a}
me,podcast,testitem,audio,${__file_to_upload_b}
EOF

}

@test "invoke aio-ia-generate-upload-file - stdin - new - metadata - with empty lines" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-upload-file new ${__upload_files} <<EOF
# single empty line

creator me
collection podcast
# multiple empty lines with and without whitespaces and tabs

${__line_with_whitespaces}


${__line_with_tabs}
${__line_with_whitespaces}
${__line_with_tabs}${__line_with_whitespaces}
identifier testitem
mediatype audio
EOF

  [ "${status}" -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 3

  assert-csv-file-header "${__expected_csv_file}" <<EOF
creator,collection,identifier,mediatype,file
EOF

  assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
me,podcast,testitem,audio,${__file_to_upload_a}
me,podcast,testitem,audio,${__file_to_upload_b}
EOF

}

@test "invoke aio-ia-generate-upload-file - stdin - new - metadata - empty lines only" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-upload-file new ${__upload_files} <<EOF

${__line_with_whitespaces}
${__line_with_tabs}
${__line_with_whitespaces}${__line_with_tabs}
EOF

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: you need to provide metadata item [identifier] ... stopping' ]
  [ ! -f "${__expected_csv_file}" ]

  # empty value
  run ${__bin_path}/aio-ia-generate-upload-file new ${__upload_files} <<<""

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: you need to provide metadata item [identifier] ... stopping' ]
  [ ! -f "${__expected_csv_file}" ]

  # empty value - whitespaces only
  run ${__bin_path}/aio-ia-generate-upload-file new ${__upload_files} <<<"${__line_with_whitespaces}"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: you need to provide metadata item [identifier] ... stopping' ]
  [ ! -f "${__expected_csv_file}" ]

  # empty value - tabs only
  run ${__bin_path}/aio-ia-generate-upload-file new ${__upload_files} <<<"${__line_with_tabs}"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: you need to provide metadata item [identifier] ... stopping' ]
  [ ! -f "${__expected_csv_file}" ]

  # empty value - whitespaces and tabs only
  run ${__bin_path}/aio-ia-generate-upload-file new ${__upload_files} <<<"${__line_with_whitespaces}${__line_with_tabs}"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: you need to provide metadata item [identifier] ... stopping' ]
  [ ! -f "${__expected_csv_file}" ]

}

@test "invoke aio-ia-generate-upload-file - stdin - new - metadata - value with whitespace" {
  
  cd "${__workspace_path}"
  
  run ${__bin_path}/aio-ia-generate-upload-file new ${__upload_files} <<EOF
identifier
mediatype
creator a creator
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_default}" 3

  assert-csv-file-header "${__expected_csv_file_default}" <<EOF
identifier,mediatype,creator,file
EOF

  assert-csv-file-rows "${__expected_csv_file_default}" 2 <<EOF
,,a creator,${__file_to_upload_a}
,,a creator,${__file_to_upload_b}
EOF

}

@test "invoke aio-ia-generate-upload-file - stdin - new - metadata - value with whitespace - not allowed" {

  cd "${__workspace_path}"

  local metadata
  local key
  local value

  for metadata in "${__metadata_value_with_whitespace_list[@]}"; do

    # no need to provide identifier and mediatype as we expect an error during input validation
    run ${__bin_path}/aio-ia-generate-upload-file new ${__upload_files} <<<"${metadata}"
    
    [ ${status} -eq 1 ]
    [ ! -f "${__expected_csv_file}" ]

    read -r key value <<<"${metadata}"
    [ "${output}" == "error: value for key [${key}] contains one or more whitespaces [${value}] ... stopping" ]

  done
}

@test "invoke aio-ia-generate-upload-file - stdin - new - metadata - value with comma - not allowed" {
  
  cd "${__workspace_path}"

  local metadata
  local key
  local value

  for metadata in "${__metadata_value_with_comma_list[@]}"; do

    # no need to provide identifier and mediatype as we expect an error during input validation
    run ${__bin_path}/aio-ia-generate-upload-file new ${__upload_files} <<<"${metadata}"
    
    [ ${status} -eq 1 ]
    [ ! -f "${__expected_csv_file}" ]

    read -r key value <<<"${metadata}"
    [ "${output}" == "error: value for key [${key}] contains a comma [${value}] ... stopping" ]

  done
}

@test "invoke aio-ia-generate-upload-file - stdin - new - metadata - key with special char - not allowed" {
  
  cd "${__workspace_path}"
  
  local metadata
  local key
  local value

  for metadata in "${__metadata_key_with_special_char_list[@]}"; do

    # no need to provide identifier and mediatype as we expect an error during input validation
    run ${__bin_path}/aio-ia-generate-upload-file new ${__upload_files} <<<"${metadata}"
    
    [ ${status} -eq 1 ]

    read -r key value <<<"${metadata}"
    [ "${output}" == "error: key [${key}] contains special char(s) ... stopping" ]

  done
}

@test "invoke aio-ia-generate-upload-file - stdin - new - metadata - with indented data" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-upload-file new ${__upload_files} <<EOF
creator${__line_with_whitespaces}me
${__line_with_whitespaces}${__line_with_tabs}collection${__line_with_whitespaces}podcast${__line_with_tabs}
${__line_with_tabs}identifier testitem
${__line_with_whitespaces}${__line_with_tabs}mediatype audio${__line_with_whitespaces}
EOF

  [ "${status}" -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 3

  assert-csv-file-header "${__expected_csv_file}" <<EOF
creator,collection,identifier,mediatype,file
EOF

  assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
me,podcast,testitem,audio,${__file_to_upload_a}
me,podcast,testitem,audio,${__file_to_upload_b}
EOF

}

@test "invoke aio-ia-generate-upload-file - stdin - new - metadata - values" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-upload-file new ${__upload_files} <<EOF
identifier testitem
creator me
mediatype audio
collection podcast
EOF
  
  [ ${status} -eq 0 ]
  
  assert-csv-file-is-complete "${__expected_csv_file}" 3
  assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,creator,mediatype,collection,file
EOF

  assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
testitem,me,audio,podcast,${__file_to_upload_a}
testitem,me,audio,podcast,${__file_to_upload_b}
EOF
}

@test "invoke aio-ia-generate-upload-file - stdin - new - metadata - file" {

  cd "${__workspace_path}"

  # write a custom metadata file
  printf '%s\n' "
identifier testitem
creator me
mediatype audio
collection podcast
" >"${__metadata_file}"

  # all metadata is read from metadata_file
  run ${__bin_path}/aio-ia-generate-upload-file new ${__upload_files} <"${__metadata_file}"
  
  [ ${status} -eq 0 ]
  
  assert-csv-file-is-complete "${__expected_csv_file}" 3
  assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,creator,mediatype,collection,file
EOF

  assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
testitem,me,audio,podcast,${__file_to_upload_a}
testitem,me,audio,podcast,${__file_to_upload_b}
EOF
}

@test "invoke aio-ia-generate-upload-file - stdin - append - metadata - read timeout" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-upload-file append ${__upload_files}
  
  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file}" ]
  [ "${output}" == 'error: you need to provide metadata item [identifier] ... stopping' ]

  run ${__bin_path}/aio-ia-generate-upload-file append ${__upload_files} < <(sleep 2; printf 'identifier testitem')
  
  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file}" ]
  [ "${output}" == 'error: you need to provide metadata item [identifier] ... stopping' ]
}

@test "invoke aio-ia-generate-upload-file - stdin - append - metadata - comments" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-upload-file append ${__upload_files} <<EOF
# a single line comment
creator me
collection podcast
# a
${__line_with_tabs}# multi
${__line_with_whitespaces}# line
${__line_with_tabs}${__line_with_whitespaces}# comment
identifier testitem
mediatype audio
EOF

  [ "${status}" -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 2
      
  assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
me,podcast,testitem,audio,${__file_to_upload_a}
me,podcast,testitem,audio,${__file_to_upload_b}
EOF

}

@test "invoke aio-ia-generate-upload-file - stdin - append - metadata - with empty lines" {
  
  cd "${__workspace_path}"
  
  run ${__bin_path}/aio-ia-generate-upload-file append ${__upload_files} <<EOF
# single empty line

creator me
collection podcast
# multiple empty lines with and without whitespaces and tabs

${__line_with_whitespaces}


${__line_with_tabs}
${__line_with_whitespaces}
${__line_with_tabs}${__line_with_whitespaces}
identifier testitem
mediatype audio
EOF

  [ "${status}" -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 2
      
  assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
me,podcast,testitem,audio,${__file_to_upload_a}
me,podcast,testitem,audio,${__file_to_upload_b}
EOF

}

@test "invoke aio-ia-generate-upload-file - stdin - append - metadata - empty lines only" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-upload-file append ${__upload_files} <<EOF

${__line_with_whitespaces}
${__line_with_tabs}
${__line_with_whitespaces}${__line_with_tabs}
EOF

  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file}" ]
  [ "${output}" == 'error: you need to provide metadata item [identifier] ... stopping' ]

  # empty value
  run ${__bin_path}/aio-ia-generate-upload-file append ${__upload_files} <<<""

  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file}" ]
  [ "${output}" == 'error: you need to provide metadata item [identifier] ... stopping' ]

  # empty value - whitespaces only
  run ${__bin_path}/aio-ia-generate-upload-file append ${__upload_files} <<<"${__line_with_whitespaces}"

  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file}" ]
  [ "${output}" == 'error: you need to provide metadata item [identifier] ... stopping' ]

  # empty value - tabs only
  run ${__bin_path}/aio-ia-generate-upload-file append ${__upload_files} <<<"${__line_with_tabs}"

  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file}" ]
  [ "${output}" == 'error: you need to provide metadata item [identifier] ... stopping' ]

  # empty value - whitespaces and tabs only
  run ${__bin_path}/aio-ia-generate-upload-file append ${__upload_files} <<<"${__line_with_whitespaces}${__line_with_tabs}"

  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file}" ]
  [ "${output}" == 'error: you need to provide metadata item [identifier] ... stopping' ]
}

@test "invoke aio-ia-generate-upload-file - stdin - append - metadata - value with whitespace" {
  
  cd "${__workspace_path}"
  
  run ${__bin_path}/aio-ia-generate-upload-file append ${__upload_files} <<EOF
identifier
mediatype
creator a creator
EOF

  [ "${status}" -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_default}" 2
      
  assert-csv-file-rows "${__expected_csv_file_default}" 2 <<EOF
,,a creator,${__file_to_upload_a}
,,a creator,${__file_to_upload_b}
EOF

}

@test "invoke aio-ia-upload-metadata-file - stdin - append - metadata - value with whitespace - not allowed" {
  
  cd "${__workspace_path}"
  
  local metadata
  local key
  local value

  for metadata in "${__metadata_value_with_whitespace_list[@]}"; do

    # no need to provide identifier and mediatype as we expect an error during input validation
    run ${__bin_path}/aio-ia-generate-upload-file append ${__upload_files} <<<"${metadata}"
    
    [ ${status} -eq 1 ]
    [ ! -f "${__expected_csv_file}" ]

    read -r key value <<<"${metadata}"
    [ "${output}" == "error: value for key [${key}] contains one or more whitespaces [${value}] ... stopping" ]

  done
}

@test "invoke aio-ia-generate-upload-file - stdin - append - metadata - value with comma - not allowed" {
  
  cd "${__workspace_path}"

  local metadata
  local key
  local value

  for metadata in "${__metadata_value_with_comma_list[@]}"; do
  
    read -r key value <<<"${metadata}"
    run ${__bin_path}/aio-ia-generate-upload-file append ${__upload_files} <<<"${key} ${value}"

    [ ${status} -eq 1 ]
    [ ! -f "${__expected_csv_file}" ]
    [ "${output}" == "error: value for key [${key}] contains a comma [${value}] ... stopping" ]

  done
}

@test "invoke aio-ia-generate-upload-file - stdin - append - metadata - key with special char - not allowed" {
  
  cd "${__workspace_path}"

  local metadata
  local key
  local value

  for metadata in "${__metadata_key_with_special_char_list[@]}"; do
  
    read -r key value <<<"${metadata}"
    run ${__bin_path}/aio-ia-generate-upload-file append ${__upload_files} <<<"${key} ${value}"

    [ ${status} -eq 1 ]
    [ ! -f "${__expected_csv_file}" ]
    [ "${output}" == "error: key [${key}] contains special char(s) ... stopping" ]

  done
}

@test "invoke aio-ia-generate-upload-file - stdin - append - metadata - with indented data" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-upload-file append ${__upload_files} <<EOF
creator${__line_with_whitespaces}me
${__line_with_whitespaces}${__line_with_tabs}collection${__line_with_whitespaces}podcast${__line_with_tabs}
${__line_with_tabs}identifier testitem
${__line_with_whitespaces}${__line_with_tabs}mediatype audio${__line_with_whitespaces}
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 2
  
  assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
me,podcast,testitem,audio,${__file_to_upload_a}
me,podcast,testitem,audio,${__file_to_upload_b}
EOF

}

@test "invoke aio-ia-generate-upload-file - stdin - append - metadata - values" {

  cd "${__workspace_path}"
  
  # generate new file first
  run ${__bin_path}/aio-ia-generate-upload-file new "${__file_to_upload_a}" <<EOF
identifier testitem
creator me
mediatype audio
collection podcast
EOF

  [ ${status} -eq 0 ]
  
  assert-csv-file-is-complete "${__expected_csv_file}" 2
  assert-csv-file-header "${__expected_csv_file}" <<<'identifier,creator,mediatype,collection,file'

  run ${__bin_path}/aio-ia-generate-upload-file append "${__file_to_upload_b}" <<EOF
identifier testitem
creator me
mediatype audio
collection podcast
EOF
  
  [ ${status} -eq 0 ]
  
  assert-csv-file-is-complete "${__expected_csv_file}" 3

  assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
testitem,me,audio,podcast,${__file_to_upload_a}
testitem,me,audio,podcast,${__file_to_upload_b}
EOF
}

@test "invoke aio-ia-generate-upload-file - stdin - append - metadata - file" {

  cd "${__workspace_path}"

  # write a custom metadata file
  printf '%s\n' "
identifier testitem
creator me
mediatype audio
collection podcast
" >"${__metadata_file}"
  
  # generate new file first
  run ${__bin_path}/aio-ia-generate-upload-file new "${__file_to_upload_a}" <"${__metadata_file}"

  [ ${status} -eq 0 ]
  
  assert-csv-file-is-complete "${__expected_csv_file}" 2
  assert-csv-file-header "${__expected_csv_file}" <<<'identifier,creator,mediatype,collection,file'

  # now append
  run ${__bin_path}/aio-ia-generate-upload-file append "${__file_to_upload_b}" <"${__metadata_file}"

  [ ${status} -eq 0 ]
  
  assert-csv-file-is-complete "${__expected_csv_file}" 3
  assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
testitem,me,audio,podcast,${__file_to_upload_a}
testitem,me,audio,podcast,${__file_to_upload_b}
EOF

}

@test "invoke aio-ia-generate-upload-file - mixed - new - metadata - values" {

  local option
  local value
  local index

  cd "${__workspace_path}"
  
  for index in "${!__identifier_option_list[@]}"; do

    read -r option value <<<"${__identifier_option_list[${index}]}"

    run ${__bin_path}/aio-ia-generate-upload-file new \
      "${option}" "${value}" \
      ${__upload_files} <<EOF
identifier someid
creator somecreator
collection somecollection
mediatype data
EOF

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 3

    assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,creator,collection,mediatype,file
EOF

    assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
${value},somecreator,somecollection,data,${__file_to_upload_a}
${value},somecreator,somecollection,data,${__file_to_upload_b}
EOF

  done

  for index in "${!__collection_option_list[@]}"; do

    read -r option value <<<"${__collection_option_list[${index}]}"

    run ${__bin_path}/aio-ia-generate-upload-file new \
      "${option}" "${value}" \
      ${__upload_files} <<EOF
identifier testitem
creator somecreator
collection somecollection
mediatype data
EOF

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 3

    assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,creator,collection,mediatype,file
EOF

    assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
testitem,somecreator,${value},data,${__file_to_upload_a}
testitem,somecreator,${value},data,${__file_to_upload_b}
EOF

  done

  for index in "${!__creator_option_list[@]}"; do

    read -r option value <<<"${__creator_option_list[${index}]}"

    run ${__bin_path}/aio-ia-generate-upload-file new \
      "${option}" "${value}" \
      ${__upload_files} <<EOF
identifier testitem
creator somecreator
collection somecollection
mediatype data
EOF

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 3

    assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,creator,collection,mediatype,file
EOF

    assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
testitem,${value},somecollection,data,${__file_to_upload_a}
testitem,${value},somecollection,data,${__file_to_upload_b}
EOF

  done
  
  for index in "${!__mediatype_option_list[@]}"; do

    read -r option value <<<"${__mediatype_option_list[${index}]}"

    run ${__bin_path}/aio-ia-generate-upload-file new \
      "${option}" "${value}" \
      ${__upload_files} <<EOF
identifier testitem
creator somecreator
collection somecollection
mediatype somedata
EOF

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 3

    assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,creator,collection,mediatype,file
EOF

    assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
testitem,somecreator,somecollection,${value},${__file_to_upload_a}
testitem,somecreator,somecollection,${value},${__file_to_upload_b}
EOF

  done
  
  local identifierOption identifierValue
  local creatorOption creatorValue
  local collectionOption collectionValue
  local mediatypeOption mediatypeValue
  
  for index in "${!__creator_option_list[@]}"; do

    read -r identifierOption identifierValue <<<"${__identifier_option_list[${index}]}"
    read -r creatorOption creatorValue <<<"${__creator_option_list[${index}]}"
    read -r collectionOption collectionValue <<<"${__collection_option_list[${index}]}"
    read -r mediatypeOption mediatypeValue <<<"${__mediatype_option_list[${index}]}"

    run ${__bin_path}/aio-ia-generate-upload-file new \
      "${identifierOption}" "${identifierValue}" \
      "${creatorOption}" "${creatorValue}" \
      "${collectionOption}" "${collectionValue}" \
      "${mediatypeOption}" "${mediatypeValue}" \
      ${__upload_files} <<EOF
identifier someid
creator somecreator
collection somecollection
mediatype somedata
EOF

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 3

    assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,creator,collection,mediatype,file
EOF

    assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
${identifierValue},${creatorValue},${collectionValue},data,${__file_to_upload_a}
${identifierValue},${creatorValue},${collectionValue},data,${__file_to_upload_b}
EOF

  done
}

@test "invoke aio-ia-generate-upload-file - mixed - new - metadata - shortcuts" {

  cd "${__workspace_path}"

  local index
  local collectionShortcutOption
  local expectedCollectionName

  for index in "${!__collection_short_option_shortcut_list[@]}"; do
    
    collectionShortcutOption="${__collection_short_option_shortcut_list[${index}]}"
    expectedCollectionName="${__collection_row_data_list[${index}]}"
    
    run ${__bin_path}/aio-ia-generate-upload-file new \
      --identifier testitem --ma "${collectionShortcutOption}" \
      ${__upload_files} <<EOF
identifier someid
creator somecreator
collection somecollection
EOF
  
    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 3

    assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,creator,collection,mediatype,file
EOF

    assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
testitem,somecreator,${expectedCollectionName},audio,${__file_to_upload_a}
testitem,somecreator,${expectedCollectionName},audio,${__file_to_upload_b}
EOF

  done
  
  for index in "${!__collection_long_option_shortcut_list[@]}"; do
    
    collectionShortcutOption="${__collection_long_option_shortcut_list[${index}]}"
    expectedCollectionName="${__collection_row_data_list[${index}]}"
    
    run ${__bin_path}/aio-ia-generate-upload-file new \
      --identifier testitem --ma "${collectionShortcutOption}" \
      ${__upload_files} <<EOF
identifier someid
creator somecreator
collection somecollection
EOF
  
    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 3

    assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,creator,collection,mediatype,file
EOF

    assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
testitem,somecreator,${expectedCollectionName},audio,${__file_to_upload_a}
testitem,somecreator,${expectedCollectionName},audio,${__file_to_upload_b}
EOF

  done
}

@test "invoke aio-ia-generate-upload-file - mixed - new - metadata - placeholders" {

  cd "${__workspace_path}"

  # placeholders on cl
  run ${__bin_path}/aio-ia-generate-upload-file new \
    --placeholder-creator \
    --placeholder-collection \
    --placeholder-identifier \
    --placeholder-mediatype \
    ${__upload_files} <<EOF
identifier someid
creator somecreator
collection somecollection
mediatype somedata
EOF
  
  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_default}" 3

  assert-csv-file-header "${__expected_csv_file_default}" <<EOF
identifier,creator,collection,mediatype,file
EOF

  assert-csv-file-rows "${__expected_csv_file_default}" 2 <<EOF
,,,,${__file_to_upload_a}
,,,,${__file_to_upload_b}
EOF
  
  # placeholders on cl
  run ${__bin_path}/aio-ia-generate-upload-file new \
    -C -G -I -M \
    ${__upload_files} <<EOF
identifier someid
creator somecreator
collection somecollection
mediatype somedata
EOF
  
  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_default}" 3

  assert-csv-file-header "${__expected_csv_file_default}" <<EOF
identifier,creator,collection,mediatype,file
EOF

  assert-csv-file-rows "${__expected_csv_file_default}" 2 <<EOF
,,,,${__file_to_upload_a}
,,,,${__file_to_upload_b}
EOF
  
  # placeholders on cl and stdin
  run ${__bin_path}/aio-ia-generate-upload-file new \
    -C -G -I -M \
    ${__upload_files} <<EOF
identifier
creator
collection
mediatype
EOF
  
  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_default}" 3

  assert-csv-file-header "${__expected_csv_file_default}" <<EOF
identifier,creator,collection,mediatype,file
EOF

  assert-csv-file-rows "${__expected_csv_file_default}" 2 <<EOF
,,,,${__file_to_upload_a}
,,,,${__file_to_upload_b}
EOF
  
  # placeholders on stdin
  run ${__bin_path}/aio-ia-generate-upload-file new \
    -c me -g podcast -i testitem -m audio \
    ${__upload_files} <<EOF
identifier
creator
collection
mediatype
EOF
  
  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 3

  assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,creator,collection,mediatype,file
EOF

  assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
testitem,me,podcast,audio,${__file_to_upload_a}
testitem,me,podcast,audio,${__file_to_upload_b}
EOF

}

@test "invoke aio-ia-generate-upload-file - mixed - new - metadata - dublicates" {

  cd "${__workspace_path}"

  # dublicates on stdin
  local key
  for key in "${__cl_metadata_key_list[@]}"; do

    run ${__bin_path}/aio-ia-generate-upload-file new \
      -I -C -G -M \
      ${__upload_files} <<EOF
${key}
${key}
EOF

    [ ${status} -eq 1 ]
    [ ! -f "${__expected_csv_file_default}" ]
    [ "${lines[0]}" == "error: key [${key}] can only be provided once ... stopping" ]
  
  done
  
  local option
  local expectedKey
  
  # dublicates on cl
  for index in "${!__mandatory_placeholder_option_list[@]}"; do
  
    option="${__mandatory_placeholder_option_list[${index}]}"
    expectedKey="${__expected_mandatory_placeholder_key_list[${index}]}"

    run ${__bin_path}/aio-ia-generate-upload-file new \
      "${option}" "${option}" \
      ${__upload_files} <<EOF
creator me
collection podcast
identifier testitem
mediatype data
EOF

    [ ${status} -eq 1 ]
    [ ! -f "${__expected_csv_file}" ]
    [ "${output}" == "error: the following metadata keys have been provided more than once on cl [${expectedKey}] ... stopping" ]
  
  done
  
  for index in "${!__optional_placeholder_option_list[@]}"; do
  
    option="${__optional_placeholder_option_list[${index}]}"
    expectedKey="${__expected_optional_placeholder_key_list[${index}]}"

    run ${__bin_path}/aio-ia-generate-upload-file new \
      "${option}" "${option}" \
      ${__upload_files} <<EOF
creator me
collection podcast
identifier testitem
mediatype data
EOF

    [ ${status} -eq 1 ]
    [ ! -f "${__expected_csv_file}" ]
    [ "${lines[0]}" == "error: the following metadata keys have been provided more than once on cl [${expectedKey}] ... stopping" ]
  
  done
  
  # dublicates on cl and stdin
  for index in "${!__mandatory_placeholder_option_list[@]}"; do

    option="${__mandatory_placeholder_option_list[${index}]}"
    expectedKey="${__expected_mandatory_placeholder_key_list[${index}]}"

    run ${__bin_path}/aio-ia-generate-upload-file new \
      "${option}" "${option}" \
      ${__upload_files} <<EOF
${expectedKey}
${expectedKey}
EOF

    [ ${status} -eq 1 ]
    [ ! -f "${__expected_csv_file}" ]
    [ "${lines[0]}" == "error: the following metadata keys have been provided more than once on cl [${expectedKey}] ... stopping" ]
  
  done
}

@test "invoke aio-ia-generate-upload-file - mixed - append - metadata - values" {

  cd "${__workspace_path}"

  local option
  local value
  local index
  local expectedRowList=()
  
  for index in "${!__identifier_option_list[@]}"; do

    read -r option value <<<"${__identifier_option_list[${index}]}"
    
    expectedRowList+=(
      "${value},somecreator,somecollection,data,${__file_to_upload_a}"
      "${value},somecreator,somecollection,data,${__file_to_upload_b}"
    )

    run ${__bin_path}/aio-ia-generate-upload-file append \
      "${option}" "${value}" \
      ${__upload_files} <<EOF
identifier someid
creator somecreator
collection somecollection
mediatype data
EOF

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" "${#expectedRowList[*]}"

    printf '%s\n' "${expectedRowList[@]}" \
    | assert-csv-file-rows "${__expected_csv_file}" "${#expectedRowList[*]}"

  done

  rm -f "${__expected_csv_file}"
  expectedRowList=()
  
  for index in "${!__collection_option_list[@]}"; do

    read -r option value <<<"${__collection_option_list[${index}]}"
    
    expectedRowList+=(
      "testitem,somecreator,${value},data,${__file_to_upload_a}"
      "testitem,somecreator,${value},data,${__file_to_upload_b}"
    )
    
    run ${__bin_path}/aio-ia-generate-upload-file append \
      "${option}" "${value}" \
      ${__upload_files} <<EOF
identifier testitem
creator somecreator
collection somecollection
mediatype data
EOF

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" "${#expectedRowList[*]}"

    printf '%s\n' "${expectedRowList[@]}" \
    | assert-csv-file-rows "${__expected_csv_file}" "${#expectedRowList[*]}"

  done
  
  rm -f "${__expected_csv_file}"
  expectedRowList=()

  for index in "${!__creator_option_list[@]}"; do

    read -r option value <<<"${__creator_option_list[${index}]}"
    
    expectedRowList+=(
      "testitem,${value},somecollection,data,${__file_to_upload_a}"
      "testitem,${value},somecollection,data,${__file_to_upload_b}"
    )

    run ${__bin_path}/aio-ia-generate-upload-file append \
      "${option}" "${value}" \
      ${__upload_files} <<EOF
identifier testitem
creator somecreator
collection somecollection
mediatype data
EOF

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" "${#expectedRowList[*]}"

    printf '%s\n' "${expectedRowList[@]}" \
    | assert-csv-file-rows "${__expected_csv_file}" "${#expectedRowList[*]}"

  done
  
  rm -f "${__expected_csv_file}"
  expectedRowList=()
  
  for index in "${!__mediatype_option_list[@]}"; do

    read -r option value <<<"${__mediatype_option_list[${index}]}"
    
    expectedRowList+=(
      "testitem,somecreator,somecollection,${value},${__file_to_upload_a}"
      "testitem,somecreator,somecollection,${value},${__file_to_upload_b}"
    )

    run ${__bin_path}/aio-ia-generate-upload-file append \
      "${option}" "${value}" \
      ${__upload_files} <<EOF
identifier testitem
creator somecreator
collection somecollection
mediatype somedata
EOF

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" "${#expectedRowList[*]}"

    printf '%s\n' "${expectedRowList[@]}" \
    | assert-csv-file-rows "${__expected_csv_file}" "${#expectedRowList[*]}"

  done
  
  rm -f "${__expected_csv_file}"
  expectedRowList=()
  
  local identifierOption identifierValue
  local creatorOption creatorValue
  local collectionOption collectionValue
  local mediatypeOption mediatypeValue
  
  for index in "${!__creator_option_list[@]}"; do

    read -r identifierOption identifierValue <<<"${__identifier_option_list[${index}]}"
    read -r creatorOption creatorValue <<<"${__creator_option_list[${index}]}"
    read -r collectionOption collectionValue <<<"${__collection_option_list[${index}]}"
    read -r mediatypeOption mediatypeValue <<<"${__mediatype_option_list[${index}]}"
    
    expectedRowList+=(
      "${identifierValue},${creatorValue},${collectionValue},data,${__file_to_upload_a}"
      "${identifierValue},${creatorValue},${collectionValue},data,${__file_to_upload_b}"
    )

    run ${__bin_path}/aio-ia-generate-upload-file append \
      "${identifierOption}" "${identifierValue}" \
      "${creatorOption}" "${creatorValue}" \
      "${collectionOption}" "${collectionValue}" \
      "${mediatypeOption}" "${mediatypeValue}" \
      ${__upload_files} <<EOF
identifier someid
creator somecreator
collection somecollection
mediatype somedata
EOF

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" "${#expectedRowList[*]}"

    printf '%s\n' "${expectedRowList[@]}" \
    | assert-csv-file-rows "${__expected_csv_file}" "${#expectedRowList[*]}"

  done
}

@test "invoke aio-ia-generate-upload-file - mixed - append - metadata - shortcuts" {

  local index
  local collectionShortcutOption
  local expectedCollectionName
  local expectedRowList=()
  
  cd "${__workspace_path}"

  for index in "${!__collection_short_option_shortcut_list[@]}"; do
    
    collectionShortcutOption="${__collection_short_option_shortcut_list[${index}]}"
    expectedCollectionName="${__collection_row_data_list[${index}]}"
    
    expectedRowList+=(
      "testitem,somecreator,${expectedCollectionName},audio,${__file_to_upload_a}"
      "testitem,somecreator,${expectedCollectionName},audio,${__file_to_upload_b}"
    )
    
    run ${__bin_path}/aio-ia-generate-upload-file append \
      --identifier testitem --ma "${collectionShortcutOption}" \
      ${__upload_files} <<EOF
identifier someid
creator somecreator
collection somecollection
EOF
  
    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" "${#expectedRowList[*]}"

    printf '%s\n' "${expectedRowList[@]}" \
    | assert-csv-file-rows "${__expected_csv_file}" "${#expectedRowList[*]}"

  done
  
  for index in "${!__collection_long_option_shortcut_list[@]}"; do
    
    collectionShortcutOption="${__collection_long_option_shortcut_list[${index}]}"
    expectedCollectionName="${__collection_row_data_list[${index}]}"
    
    expectedRowList+=(
      "testitem,somecreator,${expectedCollectionName},audio,${__file_to_upload_a}"
      "testitem,somecreator,${expectedCollectionName},audio,${__file_to_upload_b}"
    )
    
    run ${__bin_path}/aio-ia-generate-upload-file append \
      --identifier testitem --ma "${collectionShortcutOption}" \
      ${__upload_files} <<EOF
identifier someid
creator somecreator
collection somecollection
EOF
  
    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" "${#expectedRowList[*]}"

    printf '%s\n' "${expectedRowList[@]}" \
    | assert-csv-file-rows "${__expected_csv_file}" "${#expectedRowList[*]}"

  done
}

@test "invoke aio-ia-generate-upload-file - mixed - append - metadata - placeholders" {

  cd "${__workspace_path}"

  # placeholders on cl
  run ${__bin_path}/aio-ia-generate-upload-file append \
    --placeholder-creator \
    --placeholder-collection \
    --placeholder-identifier \
    --placeholder-mediatype \
    ${__upload_files} <<EOF
identifier someid
creator somecreator
collection somecollection
mediatype somedata
EOF
  
  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_default}" 2

  assert-csv-file-rows "${__expected_csv_file_default}" 2 <<EOF
,,,,${__file_to_upload_a}
,,,,${__file_to_upload_b}
EOF
  
  # placeholders on cl
  run ${__bin_path}/aio-ia-generate-upload-file append \
    -C -G -I -M \
    ${__upload_files} <<EOF
identifier someid
creator somecreator
collection somecollection
mediatype somedata
EOF
  
  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_default}" 4

  assert-csv-file-rows "${__expected_csv_file_default}" 4 <<EOF
,,,,${__file_to_upload_a}
,,,,${__file_to_upload_b}
,,,,${__file_to_upload_a}
,,,,${__file_to_upload_b}
EOF
  
  # placeholders on cl and stdin
  run ${__bin_path}/aio-ia-generate-upload-file append \
    -C -G -I -M \
    ${__upload_files} <<EOF
identifier
creator
collection
mediatype
EOF
  
  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_default}" 6

  assert-csv-file-rows "${__expected_csv_file_default}" 6 <<EOF
,,,,${__file_to_upload_a}
,,,,${__file_to_upload_b}
,,,,${__file_to_upload_a}
,,,,${__file_to_upload_b}
,,,,${__file_to_upload_a}
,,,,${__file_to_upload_b}
EOF
  
  # placeholders on stdin but now a valid identifier on cl, thus csv-file name changes
  run ${__bin_path}/aio-ia-generate-upload-file append \
    -c me -g podcast -i testitem -m audio \
    ${__upload_files} <<EOF
identifier
creator
collection
mediatype
EOF
  
  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 2

  assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
testitem,me,podcast,audio,${__file_to_upload_a}
testitem,me,podcast,audio,${__file_to_upload_b}
EOF

}

@test "invoke aio-ia-generate-upload-file - mixed - append - metadata - dublicates" {

  cd "${__workspace_path}"

  # dublicates on stdin
  local key
  for key in "${__cl_metadata_key_list[@]}"; do

    run ${__bin_path}/aio-ia-generate-upload-file append \
      -I -C -G -M \
      ${__upload_files} <<EOF
${key}
${key}
EOF

    [ ${status} -eq 1 ]
    [ ! -f "${__expected_csv_file_default}" ]
    [ "${lines[0]}" == "error: key [${key}] can only be provided once ... stopping" ]
  
  done
  
  local option
  local expectedKey
  
  # dublicates on cl
  for index in "${!__mandatory_placeholder_option_list[@]}"; do
  
    option="${__mandatory_placeholder_option_list[${index}]}"
    expectedKey="${__expected_mandatory_placeholder_key_list[${index}]}"

    run ${__bin_path}/aio-ia-generate-upload-file append \
      "${option}" "${option}" \
      ${__upload_files} <<EOF
creator me
collection podcast
identifier testitem
mediatype data
EOF

    [ ${status} -eq 1 ]
    [ ! -f "${__expected_csv_file}" ]
    [ "${output}" == "error: the following metadata keys have been provided more than once on cl [${expectedKey}] ... stopping" ]
  
  done
  
  for index in "${!__optional_placeholder_option_list[@]}"; do
  
    option="${__optional_placeholder_option_list[${index}]}"
    expectedKey="${__expected_optional_placeholder_key_list[${index}]}"

    run ${__bin_path}/aio-ia-generate-upload-file append \
      "${option}" "${option}" \
      ${__upload_files} <<EOF
creator me
collection podcast
identifier testitem
mediatype data
EOF

    [ ${status} -eq 1 ]
    [ ! -f "${__expected_csv_file}" ]
    [ "${lines[0]}" == "error: the following metadata keys have been provided more than once on cl [${expectedKey}] ... stopping" ]
  
  done
  
  # dublicates on cl and stdin
  for index in "${!__mandatory_placeholder_option_list[@]}"; do

    option="${__mandatory_placeholder_option_list[${index}]}"
    expectedKey="${__expected_mandatory_placeholder_key_list[${index}]}"

    run ${__bin_path}/aio-ia-generate-upload-file append \
      "${option}" "${option}" \
      ${__upload_files} <<EOF
${expectedKey}
${expectedKey}
EOF

    [ ${status} -eq 1 ]
    [ ! -f "${__expected_csv_file}" ]
    [ "${lines[0]}" == "error: the following metadata keys have been provided more than once on cl [${expectedKey}] ... stopping" ]
  
  done
}

@test "invoke aio-ia-generate-upload-file - cl - new - dry-run" {
  
  cd "${__workspace_path}"
  
  run ${__bin_path}/aio-ia-generate-upload-file new --dry-run \
    --identifier testitem --creator me --collection podcast --mt \
    ${__upload_files}

  [ ${status} -eq 0 ]
  [ ! -f "${__expected_csv_file}" ]

  # header on stdout
  [ "${lines[0]}" == 'identifier,creator,collection,mediatype,file' ]
  # rows on stdout
  [ "${lines[1]}" == "testitem,me,podcast,texts,${__file_to_upload_a}" ]
  [ "${lines[2]}" == "testitem,me,podcast,texts,${__file_to_upload_b}" ]

  run ${__bin_path}/aio-ia-generate-upload-file new -d \
    --identifier testitem --creator me --collection podcast --mt \
    ${__upload_files}

  [ ${status} -eq 0 ]
  [ ! -f "${__expected_csv_file}" ]

  # header on stdout
  [ "${lines[0]}" == 'identifier,creator,collection,mediatype,file' ]
  # rows on stdout
  [ "${lines[1]}" == "testitem,me,podcast,texts,${__file_to_upload_a}" ]
  [ "${lines[2]}" == "testitem,me,podcast,texts,${__file_to_upload_b}" ]

}

@test "invoke aio-ia-generate-upload-file - cl - append - dry-run" {

  cd "${__workspace_path}"

  # only append, do not create column headers
  run ${__bin_path}/aio-ia-generate-upload-file append --dry-run \
    --identifier testitem --creator me --collection podcast -M \
    ${__upload_files}

  [ ! -f "${__expected_csv_file}" ]

  # rows on stdout
  [ "${lines[0]}" == "testitem,me,podcast,,${__file_to_upload_a}" ]
  [ "${lines[1]}" == "testitem,me,podcast,,${__file_to_upload_b}" ]

  run ${__bin_path}/aio-ia-generate-upload-file append -d \
    --identifier testitem --creator me --collection podcast -M \
    ${__upload_files}

  [ ${status} -eq 0 ]
  [ ! -f "${__expected_csv_file}" ]

  # rows on stdout
  [ "${lines[0]}" == "testitem,me,podcast,,${__file_to_upload_a}" ]
  [ "${lines[1]}" == "testitem,me,podcast,,${__file_to_upload_b}" ]
}

@test "invoke aio-ia-generate-upload-file - stdin - new - dry-run" {
  
  cd "${__workspace_path}"
  
  run ${__bin_path}/aio-ia-generate-upload-file new --dry-run ${__upload_files} <<EOF
creator me
collection podcast
identifier testitem
mediatype data
EOF

  [ ${status} -eq 0 ]
  [ ! -f "${__expected_csv_file}" ]
  
  # header on stdout
  [ "${lines[0]}" == 'creator,collection,identifier,mediatype,file' ]
  
  # rows on stdout
  [ "${lines[1]}" == "me,podcast,testitem,data,${__file_to_upload_a}" ]
  [ "${lines[2]}" == "me,podcast,testitem,data,${__file_to_upload_b}" ]

  run ${__bin_path}/aio-ia-generate-upload-file new -d ${__upload_files} <<EOF
creator me
collection podcast
identifier testitem
mediatype data
EOF

  [ ${status} -eq 0 ]
  [ ! -f "${__expected_csv_file}" ]

  # header on stdout
  [ "${lines[0]}" == 'creator,collection,identifier,mediatype,file' ]
  
  # rows on stdout
  [ "${lines[1]}" == "me,podcast,testitem,data,${__file_to_upload_a}" ]
  [ "${lines[2]}" == "me,podcast,testitem,data,${__file_to_upload_b}" ]
}

@test "invoke aio-ia-generate-upload-file - stdin - append - dry-run" {

  cd "${__workspace_path}"

  # only append, do not create column headers
  run ${__bin_path}/aio-ia-generate-upload-file append --dry-run ${__upload_files} <<EOF
creator me
collection podcast
identifier testitem
mediatype data
EOF

  [ ${status} -eq 0 ]
  [ ! -f "${__expected_csv_file}" ]

  # rows on stdout
  [ "${lines[0]}" == "me,podcast,testitem,data,${__file_to_upload_a}" ]
  [ "${lines[1]}" == "me,podcast,testitem,data,${__file_to_upload_b}" ]

  run ${__bin_path}/aio-ia-generate-upload-file append -d ${__upload_files} <<EOF
creator me
collection podcast
identifier testitem
mediatype data
EOF

  [ ${status} -eq 0 ]
  [ ! -f "${__expected_csv_file}" ]

  # rows on stdout
  [ "${lines[0]}" == "me,podcast,testitem,data,${__file_to_upload_a}" ]
  [ "${lines[1]}" == "me,podcast,testitem,data,${__file_to_upload_b}" ]
}

@test "invoke aio-ia-generate-upload-file - cl - new - custom output file" {
  
  cd "${__workspace_path}"
  
  run ${__bin_path}/aio-ia-generate-upload-file new -o "${__expected_csv_file_custom}" \
    --identifier someitem --creator me --collection podcast --ma \
    ${__upload_files}

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 3

  assert-csv-file-header "${__expected_csv_file_custom}" <<EOF
identifier,creator,collection,mediatype,file
EOF

  assert-csv-file-rows "${__expected_csv_file_custom}" 2 <<EOF
someitem,me,podcast,audio,${__file_to_upload_a}
someitem,me,podcast,audio,${__file_to_upload_b}
EOF

  run ${__bin_path}/aio-ia-generate-upload-file new --output-file "${__expected_csv_file_custom}" \
    --identifier anotheritem --creator you --collection test_collection --ma \
    ${__upload_files}

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 3

  assert-csv-file-header "${__expected_csv_file_custom}" <<EOF
identifier,creator,collection,mediatype,file
EOF

  assert-csv-file-rows "${__expected_csv_file_custom}" 2 <<EOF
anotheritem,you,test_collection,audio,${__file_to_upload_a}
anotheritem,you,test_collection,audio,${__file_to_upload_b}
EOF
  
}

@test "invoke aio-ia-generate-upload-file - cl - append - custom output file" {

  cd "${__workspace_path}"

  # only append, do not create column headers
  run ${__bin_path}/aio-ia-generate-upload-file append -o "${__expected_csv_file_custom}" \
    --identifier someitem --creator me --collection podcast --ma \
    ${__upload_files}
  
  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 2

  run ${__bin_path}/aio-ia-generate-upload-file append --output-file "${__expected_csv_file_custom}" \
    --identifier anotheritem --creator you --collection test_collection --mt \
    ${__upload_files}

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 4

  assert-csv-file-rows "${__expected_csv_file_custom}" 4 <<EOF
someitem,me,podcast,audio,${__file_to_upload_a}
someitem,me,podcast,audio,${__file_to_upload_b}
anotheritem,you,test_collection,texts,${__file_to_upload_a}
anotheritem,you,test_collection,texts,${__file_to_upload_b}
EOF

  # create a new file with column headers first
  run ${__bin_path}/aio-ia-generate-upload-file new -o "${__expected_csv_file_custom}" \
    --identifier someitem --creator me --collection podcast --ma \
    ${__upload_files}

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 3

  run ${__bin_path}/aio-ia-generate-upload-file append --output-file "${__expected_csv_file_custom}" \
    --identifier anotheritem --creator you --collection test_collection --mt \
    ${__upload_files}

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 5
  
  assert-csv-file-header "${__expected_csv_file_custom}" <<EOF
identifier,creator,collection,mediatype,file
EOF

  assert-csv-file-rows "${__expected_csv_file_custom}" 4 <<EOF
someitem,me,podcast,audio,${__file_to_upload_a}
someitem,me,podcast,audio,${__file_to_upload_b}
anotheritem,you,test_collection,texts,${__file_to_upload_a}
anotheritem,you,test_collection,texts,${__file_to_upload_b}
EOF
  
}

@test "invoke aio-ia-generate-upload-file - cl - append - custom output file - existing - no linefeed" {
  
  cd "${__workspace_path}"
  
  # just write column headers into custom csv-file
  # NOTE the written line does not end with a linefeed => printf '%s'
  printf '%s' "identifier,creator,collection,mediatype,file" >"${__expected_csv_file_custom}"

  run ${__bin_path}/aio-ia-generate-upload-file append -o "${__expected_csv_file_custom}" \
    --identifier someitem --creator me --collection podcast --ma \
    ${__upload_files}

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 3

  run ${__bin_path}/aio-ia-generate-upload-file append --output-file "${__expected_csv_file_custom}" \
    --identifier anotheritem --creator you --collection test_collection --mt \
    ${__upload_files}

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 5
  
  assert-csv-file-header "${__expected_csv_file_custom}" <<EOF
identifier,creator,collection,mediatype,file
EOF

  assert-csv-file-rows "${__expected_csv_file_custom}" 4 <<EOF
someitem,me,podcast,audio,${__file_to_upload_a}
someitem,me,podcast,audio,${__file_to_upload_b}
anotheritem,you,test_collection,texts,${__file_to_upload_a}
anotheritem,you,test_collection,texts,${__file_to_upload_b}
EOF

}

@test "invoke aio-ia-generate-upload-file - cl - append - custom output file - existing - with linefeed" {
  
  cd "${__workspace_path}"
  
  # just write column headers into custom csv-file
  # NOTE the written line does end with a linefeed => printf '%s\n'
  printf '%s\n' "identifier,creator,collection,mediatype,file" >"${__expected_csv_file_custom}"

  run ${__bin_path}/aio-ia-generate-upload-file append -o "${__expected_csv_file_custom}" \
    --identifier someitem --creator me --collection podcast --ma \
    ${__upload_files}

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 3

  run ${__bin_path}/aio-ia-generate-upload-file append --output-file "${__expected_csv_file_custom}" \
    --identifier anotheritem --creator you --collection test_collection --mt \
    ${__upload_files}

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 5
  
  assert-csv-file-header "${__expected_csv_file_custom}" <<EOF
identifier,creator,collection,mediatype,file
EOF

  assert-csv-file-rows "${__expected_csv_file_custom}" 4 <<EOF
someitem,me,podcast,audio,${__file_to_upload_a}
someitem,me,podcast,audio,${__file_to_upload_b}
anotheritem,you,test_collection,texts,${__file_to_upload_a}
anotheritem,you,test_collection,texts,${__file_to_upload_b}
EOF

}

@test "invoke aio-ia-generate-upload-file - cl - append - custom output file - mixed up order" {

  cd "${__workspace_path}"

  # just write column headers into custom csv-file
  # NOTE the written line does end with a linefeed => printf '%s\n'
  printf '%s\n' "identifier,creator,collection,mediatype,file" >"${__expected_csv_file_custom}"

  run ${__bin_path}/aio-ia-generate-upload-file append --output-file "${__expected_csv_file_custom}" \
    --ma --identifier someitem \
    ${__upload_files}

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 3
    
  run ${__bin_path}/aio-ia-generate-upload-file append --output-file "${__expected_csv_file_custom}" \
    --collection test_collection --creator you --identifier anotheritem --mt \
    ${__upload_files}

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 5
  
  assert-csv-file-header "${__expected_csv_file_custom}" <<EOF
identifier,creator,collection,mediatype,file
EOF

  # csv-file row content does not match the column headers
  assert-csv-file-rows "${__expected_csv_file_custom}" 4 <<EOF
audio,someitem,${__file_to_upload_a}
audio,someitem,${__file_to_upload_b}
test_collection,you,anotheritem,texts,${__file_to_upload_a}
test_collection,you,anotheritem,texts,${__file_to_upload_b}
EOF
}

@test "invoke aio-ia-generate-upload-file - stdin - new - custom output file" {
  
  cd "${__workspace_path}"
  
  run ${__bin_path}/aio-ia-generate-upload-file new \
    -o "${__expected_csv_file_custom}" \
    ${__upload_files} <<EOF
creator me
collection podcast
identifier testitem
mediatype data
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 3
      
  assert-csv-file-header "${__expected_csv_file_custom}" <<EOF
creator,collection,identifier,mediatype,file
EOF

  assert-csv-file-rows "${__expected_csv_file_custom}" 2 <<EOF
me,podcast,testitem,data,${__file_to_upload_a}
me,podcast,testitem,data,${__file_to_upload_b}
EOF

  run ${__bin_path}/aio-ia-generate-upload-file new \
    --output-file "${__expected_csv_file_custom}" \
    ${__upload_files} <<EOF
creator you
collection podcast
identifier anotheritem
mediatype audio
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 3
      
  assert-csv-file-header "${__expected_csv_file_custom}" <<EOF
creator,collection,identifier,mediatype,file
EOF

  assert-csv-file-rows "${__expected_csv_file_custom}" 2 <<EOF
you,podcast,anotheritem,audio,${__file_to_upload_a}
you,podcast,anotheritem,audio,${__file_to_upload_b}
EOF
  
}

@test "invoke aio-ia-generate-upload-file - stdin - append - custom output file" {

  cd "${__workspace_path}"

  # only append, do not create column headers
  run ${__bin_path}/aio-ia-generate-upload-file append \
    -o "${__expected_csv_file_custom}" \
    ${__upload_files} <<EOF
creator me
collection podcast
identifier testitem
mediatype data
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 2

  run ${__bin_path}/aio-ia-generate-upload-file append \
    --output-file "${__expected_csv_file_custom}" \
    ${__upload_files} <<EOF
creator you
collection podcast
identifier anotheritem
mediatype audio
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 4

  assert-csv-file-rows "${__expected_csv_file_custom}" 4 <<EOF
me,podcast,testitem,data,${__file_to_upload_a}
me,podcast,testitem,data,${__file_to_upload_b}
you,podcast,anotheritem,audio,${__file_to_upload_a}
you,podcast,anotheritem,audio,${__file_to_upload_b}
EOF

  # create a new file with column headers first
  run ${__bin_path}/aio-ia-generate-upload-file new \
    -o "${__expected_csv_file_custom}" \
    ${__upload_files} <<EOF
creator me
collection podcast
identifier testitem
mediatype data
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 3

  run ${__bin_path}/aio-ia-generate-upload-file append \
    --output-file "${__expected_csv_file_custom}" \
    ${__upload_files} <<EOF
creator you
collection podcast
identifier anotheritem
mediatype audio
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 5
  
  assert-csv-file-header "${__expected_csv_file_custom}" <<EOF
creator,collection,identifier,mediatype,file
EOF

  assert-csv-file-rows "${__expected_csv_file_custom}" 4 <<EOF
me,podcast,testitem,data,${__file_to_upload_a}
me,podcast,testitem,data,${__file_to_upload_b}
you,podcast,anotheritem,audio,${__file_to_upload_a}
you,podcast,anotheritem,audio,${__file_to_upload_b}
EOF
  
}

@test "invoke aio-ia-generate-upload-file - stdin - append - custom output file - existing - no linefeed" {
  
  cd "${__workspace_path}"
  
  # just write column headers into custom csv-file
  # NOTE the written line does not end with a linefeed => printf '%s'
  printf '%s' "identifier,creator,collection,mediatype,file" >"${__expected_csv_file_custom}"

  run ${__bin_path}/aio-ia-generate-upload-file append \
    -o "${__expected_csv_file_custom}" \
    ${__upload_files} <<EOF
creator me
collection podcast
identifier testitem
mediatype data
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 3

  run ${__bin_path}/aio-ia-generate-upload-file append \
    --output-file "${__expected_csv_file_custom}" \
    ${__upload_files} <<EOF
creator you
collection podcast
identifier anotheritem
mediatype audio
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 5
  
  assert-csv-file-header "${__expected_csv_file_custom}" <<EOF
identifier,creator,collection,mediatype,file
EOF

  assert-csv-file-rows "${__expected_csv_file_custom}" 4 <<EOF
me,podcast,testitem,data,${__file_to_upload_a}
me,podcast,testitem,data,${__file_to_upload_b}
you,podcast,anotheritem,audio,${__file_to_upload_a}
you,podcast,anotheritem,audio,${__file_to_upload_b}
EOF

}

@test "invoke aio-ia-generate-upload-file - stdin - append - custom output file - existing - with linefeed" {
  
  cd "${__workspace_path}"
  
  # just write column headers into custom csv-file
  # NOTE the written line does end with a linefeed => printf '%s\n'
  printf '%s\n' "identifier,creator,collection,mediatype,file" >"${__expected_csv_file_custom}"

  run ${__bin_path}/aio-ia-generate-upload-file append \
    -o "${__expected_csv_file_custom}" \
    ${__upload_files} <<EOF
creator me
collection podcast
identifier testitem
mediatype data
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 3

  run ${__bin_path}/aio-ia-generate-upload-file append \
    --output-file "${__expected_csv_file_custom}" \
    ${__upload_files} <<EOF
creator you
collection podcast
identifier anotheritem
mediatype audio
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 5
  
  assert-csv-file-header "${__expected_csv_file_custom}" <<EOF
identifier,creator,collection,mediatype,file
EOF

  assert-csv-file-rows "${__expected_csv_file_custom}" 4 <<EOF
me,podcast,testitem,data,${__file_to_upload_a}
me,podcast,testitem,data,${__file_to_upload_b}
you,podcast,anotheritem,audio,${__file_to_upload_a}
you,podcast,anotheritem,audio,${__file_to_upload_b}
EOF

}

@test "invoke aio-ia-generate-upload-file - stdin - append - custom output file - mixed up order" {

  cd "${__workspace_path}"

  # just write column headers into custom csv-file
  # NOTE the written line does end with a linefeed => printf '%s\n'
  printf '%s\n' "identifier,creator,collection,mediatype,file" >"${__expected_csv_file_custom}"

  run ${__bin_path}/aio-ia-generate-upload-file append \
    --output-file "${__expected_csv_file_custom}" \
    ${__upload_files} <<EOF
mediatype audio
identifier someitem
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 3

  run ${__bin_path}/aio-ia-generate-upload-file append \
    --output-file "${__expected_csv_file_custom}" \
    ${__upload_files} <<EOF
collection test_collection
creator you
identifier anotheritem
mediatype texts
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 5

  assert-csv-file-header "${__expected_csv_file_custom}" <<EOF
identifier,creator,collection,mediatype,file
EOF

  # csv-file row content does not match the column headers
  assert-csv-file-rows "${__expected_csv_file_custom}" 4 <<EOF
audio,someitem,${__file_to_upload_a}
audio,someitem,${__file_to_upload_b}
test_collection,you,anotheritem,texts,${__file_to_upload_a}
test_collection,you,anotheritem,texts,${__file_to_upload_b}
EOF
}

@test "invoke aio-ia-generate-upload-file - cl - template - default" {

  run ${__bin_path}/aio-ia-generate-upload-file template

  [ ${status} -eq 0 ]

  [ "${lines[0]}" == "identifier" ]
  [ "${lines[1]}" == "creator" ]
  [ "${lines[2]}" == "collection" ]
  [ "${lines[3]}" == "mediatype" ]
  [ "${lines[4]}" == "file" ]
}

@test "invoke aio-ia-generate-upload-file - cl - template - values" {  

  run ${__bin_path}/aio-ia-generate-upload-file template \
    -i testitem \
    -m audio \
    -g my_collection \
    -c 'its me'

  [ ${status} -eq 0 ]

  [ "${lines[0]}" == "identifier testitem" ]
  [ "${lines[1]}" == "mediatype audio" ]
  [ "${lines[2]}" == "collection my_collection" ]
  [ "${lines[3]}" == "creator its me" ]

  run ${__bin_path}/aio-ia-generate-upload-file template \
    --identifier testitem \
    --mediatype audio \
    --collection my_collection \
    --creator 'its me'

  [ ${status} -eq 0 ]

  [ "${lines[0]}" == "identifier testitem" ]
  [ "${lines[1]}" == "mediatype audio" ]
  [ "${lines[2]}" == "collection my_collection" ]
  [ "${lines[3]}" == "creator its me" ]
  
  # in template mode, identifier and mediatype are not required
  run ${__bin_path}/aio-ia-generate-upload-file template \
    --collection my_collection \
    --creator 'its me'

  [ ${status} -eq 0 ]

  [ "${lines[0]}" == "collection my_collection" ]
  [ "${lines[1]}" == "creator its me" ]
}

@test "invoke aio-ia-generate-upload-file - cl - template - placeholders" {  

  run ${__bin_path}/aio-ia-generate-upload-file template \
    -I -M -G -C

  [ ${status} -eq 0 ]

  [ "${lines[0]}" == "identifier" ]
  [ "${lines[1]}" == "mediatype" ]
  [ "${lines[2]}" == "collection" ]
  [ "${lines[3]}" == "creator" ]

  run ${__bin_path}/aio-ia-generate-upload-file template \
    --placeholder-identifier \
    --placeholder-creator \
    --placeholder-mediatype \
    --placeholder-collection

  [ ${status} -eq 0 ]

  [ "${lines[0]}" == "identifier" ]
  [ "${lines[1]}" == "creator" ]
  [ "${lines[2]}" == "mediatype" ]
  [ "${lines[3]}" == "collection" ]
}

@test "invoke aio-ia-generate-upload-file - cl - template - shortcuts" {

  local index
  local collectionShortcutOption
  local expectedCollectionValue

  for index in "${!__collection_short_option_shortcut_list[@]}"; do
    collectionShortcutOption="${__collection_short_option_shortcut_list[${index}]}"
    expectedCollectionValue="${__collection_row_data_list[${index}]}"
    
    run ${__bin_path}/aio-ia-generate-upload-file template \
        "${collectionShortcutOption}" -i someitem
        
    [ ${status} -eq 0 ]

    [ "${lines[0]}" == "collection ${expectedCollectionValue}" ]
    [ "${lines[1]}" == "identifier someitem" ]
    
  done
  
  for index in "${!__collection_long_option_shortcut_list[@]}"; do
    collectionShortcutOption="${__collection_long_option_shortcut_list[${index}]}"
    expectedCollectionValue="${__collection_row_data_list[${index}]}"
    
    run ${__bin_path}/aio-ia-generate-upload-file template \
        "${collectionShortcutOption}" -i someitem
        
    [ ${status} -eq 0 ]

    [ "${lines[0]}" == "collection ${expectedCollectionValue}" ]
    [ "${lines[1]}" == "identifier someitem" ]
    
  done
  
  local mediatypeShortcutOption
  local expectedMediatypeValue

  for index in "${!__mediatype_short_option_shortcut_list[@]}"; do
    mediatypeShortcutOption="${__mediatype_short_option_shortcut_list[${index}]}"
    expectedMediatypeValue="${__mediatype_row_data_list[${index}]}"
    
    run ${__bin_path}/aio-ia-generate-upload-file template \
        "${mediatypeShortcutOption}" -i someitem
        
    [ ${status} -eq 0 ]

    [ "${lines[0]}" == "mediatype ${expectedMediatypeValue}" ]
    [ "${lines[1]}" == "identifier someitem" ]
    
  done
  
  for index in "${!__mediatype_long_option_shortcut_list[@]}"; do
    mediatypeShortcutOption="${__mediatype_long_option_shortcut_list[${index}]}"
    expectedMediatypeValue="${__mediatype_row_data_list[${index}]}"
    
    run ${__bin_path}/aio-ia-generate-upload-file template \
        "${mediatypeShortcutOption}" -i someitem
        
    [ ${status} -eq 0 ]

    [ "${lines[0]}" == "mediatype ${expectedMediatypeValue}" ]
    [ "${lines[1]}" == "identifier someitem" ]
    
  done

}

@test "invoke aio-ia-generate-upload-file - cl - template - mixed" {

  run ${__bin_path}/aio-ia-generate-upload-file template \
    -I -c me --gi --mediatype audio

  [ ${status} -eq 0 ]

  [ "${lines[0]}" == "identifier" ]
  [ "${lines[1]}" == "creator me" ]
  [ "${lines[2]}" == "collection opensource_image" ]
  [ "${lines[3]}" == "mediatype audio" ]

}

@test "invoke aio-ia-generate-upload-file - cl - template - dublicates" {
  
  # in template mode, dublicates are allowed
  run ${__bin_path}/aio-ia-generate-upload-file template \
    -I -c me -c you --gi -i anotheritem --collection podcast --mm -M

  [ ${status} -eq 0 ]

  [ "${lines[0]}" == "identifier" ]
  [ "${lines[1]}" == "creator me" ]
  [ "${lines[2]}" == "creator you" ]
  [ "${lines[3]}" == "collection opensource_image" ]
  [ "${lines[4]}" == "identifier anotheritem" ]
  [ "${lines[5]}" == "collection podcast" ]
  [ "${lines[6]}" == "mediatype movies" ]
  [ "${lines[7]}" == "mediatype" ]

}