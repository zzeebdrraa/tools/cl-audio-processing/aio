#!/usr/bin/env bats

# This file is part of aio-proc
#
#     aio-proc is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     aio-proc is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with aio-proc. If not, see <http://www.gnu.org/licenses/>.

###########
# TODO
#
# add tests for:
#
#   - test input files with no extension
#   - test output files with no extension
#   - fix all tests that are skipped right now
#
# more:
#
#   - use bats hooks setup_file() amd teardown_file() in order to create temporary working dirs
#
###########

###########
# Test Data
###########

declare -g -r testInFileOgg="test-in.ogg"

declare -g -r testInFileWithWhitespacesOgg="test in 2000.ogg"
declare -g -r testInFileWithEscapedWhitespacesOgg="test\ in\ 2000.ogg"
declare -g -r testInFileWithSpecialCharsOgg="test-(in).2000.ogg"
declare -g -r testInFileWithSpecialCharsAndWhitespacesOgg="test-(in).2000 01.ogg"

declare -g -r testOneLevelSubdirPath="test-subdir"
declare -g -r testThreeLevelSubdirPath="test/sub/dir"

declare -g -r cutOutputFiles=(
  "a-cut-${testInFileOgg}"
  "b-cut-${testInFileOgg}"
  "c-cut-${testInFileOgg}"
  "d-cut-${testInFileOgg}"
  "a-cut-${testInFileWithWhitespacesOgg}"
  "b-cut-${testInFileWithWhitespacesOgg}"
  "a-cut-${testInFileWithEscapedWhitespacesOgg}"
  "b-cut-${testInFileWithEscapedWhitespacesOgg}"
  "a-cut-${testInFileWithSpecialCharsOgg}"
  "b-cut-${testInFileWithSpecialCharsOgg}"
  "a-cut-${testInFileWithSpecialCharsAndWhitespacesOgg}"
  "b-cut-${testInFileWithSpecialCharsAndWhitespacesOgg}"
)

###########
# Envvars
###########

export ENV_AIO_SPLIT_CUT_BIN_PATH='..'
export ENV_AIO_CUT_TS_TO_SEC_BIN_PATH='..'

###########
# Helpers
###########

declare -g -r __bin_path='..'

load test-aio-common-file-generators
load test-aio-common-asserts

###########
# Setup
###########

setup() {
  rm -f "${testInFileOgg}"

  rm -f "${testInFileWithWhitespacesOgg}"
  rm -f "${testInFileWithEscapedWhitespacesOgg}"
  rm -f "${testInFileWithSpecialCharsOgg}"
  rm -f "${testInFileWithSpecialCharsAndWhitespacesOgg}"

  rm -f "${cutOutputFiles[@]}"

  if [[ -d "${testOneLevelSubdirPath}" ]]; then
    rm -r "${testOneLevelSubdirPath}"
  fi
  if [[ -d "${testThreeLevelSubdirPath}" ]]; then
    rm -v -f ${testThreeLevelSubdirPath}/*
    rmdir -p "${testThreeLevelSubdirPath}"
  fi
}

teardown() {
  rm -f "${testInFileOgg}"

  rm -f "${testInFileWithWhitespacesOgg}"
  rm -f "${testInFileWithEscapedWhitespacesOgg}"
  rm -f "${testInFileWithSpecialCharsOgg}"
  rm -f "${testInFileWithSpecialCharsAndWhitespacesOgg}"

  rm -f "${cutOutputFiles[@]}"

  if [[ -d "${testOneLevelSubdirPath}" ]]; then
    rm -r -f "${testOneLevelSubdirPath}"
  fi
  if [[ -d "${testThreeLevelSubdirPath}" ]]; then
    rm -v -f ${testThreeLevelSubdirPath}/*
    rmdir -p "${testThreeLevelSubdirPath}"
  fi
}

###########
# Tests
###########


@test "invoke aio-split - help" {
  run ${__bin_path}/aio-split -h
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-split --help
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-split help
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-split h
  [ ${status} -eq 0 ]
}

@test "invoke aio-split - help short" {

  run ${__bin_path}/aio-split help short
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-split help s
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-split h s
  [ ${status} -eq 0 ]
}

@test "invoke aio-split - help details" {

  run ${__bin_path}/aio-split help details
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-split help d
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-split h d
  [ ${status} -eq 0 ]
}

@test "invoke aio-split - version" {
  run ${__bin_path}/aio-split --version
  [ ${status} -eq 0 ]
}

@test "invoke aio-split - status" {
  run ${__bin_path}/aio-split status
  [ ${status} -eq 0 ]
}

@test "invoke aio-split - cl - input file - none specified" {
  run ${__bin_path}/aio-split at 1

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: not a file [] ... stopping' ]
}

@test "invoke aio-split - cl - input file - non-existing" {
  run ${__bin_path}/aio-split at 1 dummyIn1

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: not a file [dummyIn1] ... stopping' ]
}

@test "invoke aio-split - cl - input file - name with whitespaces" {
  aio-generate-ogg-file-sine "${testInFileWithWhitespacesOgg}"

  run ${__bin_path}/aio-split at 2 "${testInFileWithWhitespacesOgg}"

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "a-cut-${testInFileWithWhitespacesOgg}" 2
  aio-assert-duration-ffmpeg "b-cut-${testInFileWithWhitespacesOgg}" 3
}

@test "invoke aio-split - cl - input file - name with escaped whitespaces" {
  aio-generate-ogg-file-sine "${testInFileWithEscapedWhitespacesOgg}"

  run ${__bin_path}/aio-split at 2 "${testInFileWithEscapedWhitespacesOgg}"

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "a-cut-${testInFileWithEscapedWhitespacesOgg}" 2
  aio-assert-duration-ffmpeg "b-cut-${testInFileWithEscapedWhitespacesOgg}" 3
}

@test "invoke aio-split - cl - input file - name with special chars" {
  aio-generate-ogg-file-sine "${testInFileWithSpecialCharsOgg}"

  run ${__bin_path}/aio-split at 2 "${testInFileWithSpecialCharsOgg}"

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "a-cut-${testInFileWithSpecialCharsOgg}" 2
  aio-assert-duration-ffmpeg "b-cut-${testInFileWithSpecialCharsOgg}" 3
}

@test "invoke aio-split - cl - input file - name with special chars and whitespaces" {
  aio-generate-ogg-file-sine "${testInFileWithSpecialCharsAndWhitespacesOgg}"

  run ${__bin_path}/aio-split at 2 "${testInFileWithSpecialCharsAndWhitespacesOgg}"

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "a-cut-${testInFileWithSpecialCharsAndWhitespacesOgg}" 2
  aio-assert-duration-ffmpeg "b-cut-${testInFileWithSpecialCharsAndWhitespacesOgg}" 3
}

@test "invoke aio-split - cl - input file in sub-directory - one level" {

  mkdir "${testOneLevelSubdirPath}"

  aio-generate-ogg-file-sine "${testOneLevelSubdirPath}/${testInFileOgg}"
  touch "a-cut-${testInFileOgg}"
  touch "b-cut-${testInFileOgg}"

  # NOTE outpufiles will be created in this director and not in the sub-directory
  run ${__bin_path}/aio-split -f at 2 "${testOneLevelSubdirPath}/${testInFileOgg}"

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "a-cut-${testInFileOgg}" 2
  aio-assert-duration-ffmpeg "b-cut-${testInFileOgg}" 3
}

@test "invoke aio-split - cl - input file in sub-directory - more levels" {

  mkdir -p "${testThreeLevelSubdirPath}"

  aio-generate-ogg-file-sine "${testThreeLevelSubdirPath}/${testInFileOgg}"
  touch "a-cut-${testInFileOgg}"
  touch "b-cut-${testInFileOgg}"

  # NOTE outpufiles will be created in this director and not in the sub-directory
  run ${__bin_path}/aio-split -f at 2 "${testThreeLevelSubdirPath}/${testInFileOgg}"

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "a-cut-${testInFileOgg}" 2
  aio-assert-duration-ffmpeg "b-cut-${testInFileOgg}" 3
}

@test "invoke aio-split - cl - already existing output files - no overwrite" {

  aio-generate-ogg-file-sine "${testInFileOgg}"
  touch "a-cut-${testInFileOgg}"
  touch "b-cut-${testInFileOgg}"

  run ${__bin_path}/aio-split at 2 "${testInFileOgg}"

  [ ${status} -eq 1 ]

  [ "${lines[0]}" == "error: output file [a-cut-${testInFileOgg}] is already existing ... skipping" ]
  [ "${lines[1]}" == "error: output file [b-cut-${testInFileOgg}] is already existing ... skipping" ]
}

@test "invoke aio-split - cl - already existing output files - force overwrite" {

  aio-generate-ogg-file-sine "${testInFileOgg}"
  touch "a-cut-${testInFileOgg}"
  touch "b-cut-${testInFileOgg}"

  run ${__bin_path}/aio-split -f at 2 "${testInFileOgg}"

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "a-cut-${testInFileOgg}" 2
  aio-assert-duration-ffmpeg "b-cut-${testInFileOgg}" 3
}

@test "invoke aio-split - cl - missing split position" {
  touch "${testInFileOgg}"

  run ${__bin_path}/aio-split "${testInFileOgg}"

  [ ${status} -eq 1 ]
  [ "${output}" == "error: no split position provided for [${testInFileOgg}] ... stopping" ]
}

@test "invoke aio-split - cl - split at begin" {
  touch "${testInFileOgg}"

  run ${__bin_path}/aio-split at 0 "${testInFileOgg}"

  [ ${status} -eq 1 ]
  [ "${output}" == "error: timestamp value [00:00:00] cannot be zero or max ... skipping" ]

  run ${__bin_path}/aio-split at 1 at 0 "${testInFileOgg}"

  [ ${status} -eq 1 ]
  [ "${output}" == "error: timestamp value [00:00:00] cannot be zero or max ... skipping" ]
}

@test "invoke aio-split - cl - invalid split positions" {
  touch "${testInFileOgg}"

  run ${__bin_path}/aio-split at 00.00.0 "${testInFileOgg}"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: timestamp [00.00.0] is not formated correctly ... skipping' ]

  run ${__bin_path}/aio-split at 00.00.0 at 1:0:0:0:0 "${testInFileOgg}"

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: timestamp [00.00.0] is not formated correctly ... skipping' ]
  [ "${lines[1]}" == 'error: timestamp [1:0:0:0:0] is not formated correctly ... skipping' ]

  run ${__bin_path}/aio-split at "${testInFileOgg}" "${testInFileOgg}"

  [ ${status} -eq 1 ]
  [ "${output}" == "error: timestamp [${testInFileOgg}] is not formated correctly ... skipping" ]
}

@test "invoke aio-split - cl - invalid split positions - undetected" {
  skip "not yet detected as invalid timestamps"
  touch "${testInFileOgg}"

  run ${__bin_path}/aio-split at 1:: "${testInFileOgg}"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: timestamp [1::] is not formated correctly ... skipping' ]

  run ${__bin_path}/aio-split at 00::00:1 "${testInFileOgg}"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: timestamp [ 00::00:1] is not formated correctly ... skipping' ]
}

@test "invoke aio-split - cl - valid split positions - sec - single" {
  aio-generate-ogg-file-sine "${testInFileOgg}"

  run ${__bin_path}/aio-split at 2 "${testInFileOgg}"

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "a-cut-${testInFileOgg}" 2
  aio-assert-duration-ffmpeg "b-cut-${testInFileOgg}" 3
}

@test "invoke aio-split - cl - valid split positions - sec - single-digit - ordered" {
  aio-generate-ogg-file-sine "${testInFileOgg}"

  run ${__bin_path}/aio-split at 1 at 2 at 3 "${testInFileOgg}"

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "a-cut-${testInFileOgg}" 1
  aio-assert-duration-ffmpeg "b-cut-${testInFileOgg}" 1
  aio-assert-duration-ffmpeg "c-cut-${testInFileOgg}" 1
  aio-assert-duration-ffmpeg "d-cut-${testInFileOgg}" 2
}

@test "invoke aio-split - cl - valid split positions - sec - single-digit - unordered" {
  aio-generate-ogg-file-sine "${testInFileOgg}"

  run ${__bin_path}/aio-split at 3.5 at 1 at 2 "${testInFileOgg}"

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "a-cut-${testInFileOgg}" 1
  aio-assert-duration-ffmpeg "b-cut-${testInFileOgg}" 1
  aio-assert-duration-ffmpeg "c-cut-${testInFileOgg}" 1.5
  aio-assert-duration-ffmpeg "d-cut-${testInFileOgg}" 1.5
}

@test "invoke aio-split - cl - valid split positions - sec - multi-digit - ordered" {
  aio-generate-ogg-file-sine "${testInFileOgg}" 22

  # with alphabetical ordering in aio-split, 3 would be larger than 20
  # if timestamp is not normalized correctly in aio-split:normalize-timestamp(a)
  # 00normalized: 00:1.5 00:00:3 00:00:20 => alphabetical sorted: 00:00:1.5 00:00:20 00:00:3
  run ${__bin_path}/aio-split at 01.5 at 3 at 20 "${testInFileOgg}"

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "a-cut-${testInFileOgg}" 1.5
  aio-assert-duration-ffmpeg "b-cut-${testInFileOgg}" 1.5
  aio-assert-duration-ffmpeg "c-cut-${testInFileOgg}" 17
  aio-assert-duration-ffmpeg "d-cut-${testInFileOgg}" 2
}

@test "invoke aio-split - stdin - all output files already existing - no overwrite" {
  aio-generate-ogg-file-sine "${testInFileOgg}"
  touch "a-cut-${testInFileOgg}"
  touch "b-cut-${testInFileOgg}"

  run ${__bin_path}/aio-split stdin "${testInFileOgg}" <<<2

  [ ${status} -eq 1 ]

  [ "${lines[0]}" == "error: output file [a-cut-${testInFileOgg}] is already existing ... skipping" ]
  [ "${lines[1]}" == "error: output file [b-cut-${testInFileOgg}] is already existing ... skipping" ]
}

@test "invoke aio-split - stdin - one of output files already existing - no overwrite" {
  aio-generate-ogg-file-sine "${testInFileOgg}"
  touch "b-cut-${testInFileOgg}"

  run ${__bin_path}/aio-split stdin "${testInFileOgg}" <<<2

  [ ${status} -eq 1 ]

  [ "${lines[0]}" == "error: output file [b-cut-${testInFileOgg}] is already existing ... skipping" ]
}

@test "invoke aio-split - stdin - output files already existing - force overwrite" {
  aio-generate-ogg-file-sine "${testInFileOgg}"
  touch "a-cut-${testInFileOgg}"
  touch "b-cut-${testInFileOgg}"

  run ${__bin_path}/aio-split -f stdin "${testInFileOgg}" <<<2

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "a-cut-${testInFileOgg}" 2
  aio-assert-duration-ffmpeg "b-cut-${testInFileOgg}" 3
}

@test "invoke aio-split - stdin - input file - name with whitespaces - herestring" {
  skip "WHITESPACES IN FILENAME NOT YET HANDLED CORRECTLY"

  aio-generate-ogg-file-sine "${testInFileWithWhitespacesOgg}"

  run ${__bin_path}/aio-split stdin  <<<"${testInFileWithWhitespacesOgg} 2"

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "a-cut-${testInFileWithWhitespacesOgg}" 2
  aio-assert-duration-ffmpeg "b-cut-${testInFileWithWhitespacesOgg}" 3
}

@test "invoke aio-split - stdin - input file - name with escaped whitespaces - herestring" {
  skip "WHITESPACES IN FILENAME NOT YET HANDLED CORRECTLY"

  aio-generate-ogg-file-sine "${testInFileWithEscapedWhitespacesOgg}"

  run ${__bin_path}/aio-split stdin  <<<"${testInFileWithEscapedWhitespacesOgg} 2"

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "a-cut-${testInFileWithEscapedWhitespacesOgg}" 2
  aio-assert-duration-ffmpeg "b-cut-${testInFileWithEscapedWhitespacesOgg}" 3
}

@test "invoke aio-split - stdin - input file - name with special chars - herestring" {
  aio-generate-ogg-file-sine "${testInFileWithSpecialCharsOgg}"

  run ${__bin_path}/aio-split stdin <<<"${testInFileWithSpecialCharsOgg} 2"

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "a-cut-${testInFileWithSpecialCharsOgg}" 2
  aio-assert-duration-ffmpeg "b-cut-${testInFileWithSpecialCharsOgg}" 3
}

@test "invoke aio-split - stdin - input file - name with special chars and whitespaces - herestring" {
  skip "WHITESPACES IN FILENAME NOT YET HANDLED CORRECTLY"

  aio-generate-ogg-file-sine "${testInFileWithSpecialCharsAndWhitespacesOgg}"

  run ${__bin_path}/aio-split stdin <<<"${testInFileWithSpecialCharsAndWhitespacesOgg} 2"

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "a-cut-${testInFileWithSpecialCharsAndWhitespacesOgg}" 2
  aio-assert-duration-ffmpeg "b-cut-${testInFileWithSpecialCharsAndWhitespacesOgg}" 3
}

@test "invoke aio-split - stdin - valid split positions - sec - herestring" {
  aio-generate-ogg-file-sine "${testInFileOgg}"

  run ${__bin_path}/aio-split -- "${testInFileOgg}" <<<2

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "a-cut-${testInFileOgg}" 2
  aio-assert-duration-ffmpeg "b-cut-${testInFileOgg}" 3
}

@test "invoke aio-split - stdin - with comments - heredoc" {
  aio-generate-ogg-file-sine "${testInFileOgg}"

  local -r indentWhitespaces='      '
  local -r indentTabs="$(printf '\t\t')"

  run ${__bin_path}/aio-split -- "${testInFileOgg}" <<EOF
# a single line comment
1
2
# a
${indentTabs}# multi
${indentWhitespaces}# line
${indentTabs}${indentWhitespaces}# comment
3
EOF

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "a-cut-${testInFileOgg}" 1
  aio-assert-duration-ffmpeg "b-cut-${testInFileOgg}" 1
  aio-assert-duration-ffmpeg "c-cut-${testInFileOgg}" 1
  aio-assert-duration-ffmpeg "d-cut-${testInFileOgg}" 2
}

@test "invoke aio-split - stdin - with empty lines - heredoc" {
  aio-generate-ogg-file-sine "${testInFileOgg}"

  local -r lineWithWhitespaces='      '
  local -r lineWithTabs="$(printf '\t\t\t')"

  run ${__bin_path}/aio-split -- "${testInFileOgg}" <<EOF
# single empty line

1
# empty line containing only whitespaces
${lineWithWhitespaces}
# empty line containing only tabs
${lineWithTabs}
2
# multiple empty lines with and without whitespaces and tabs

${lineWithWhitespaces}


${lineWithTabs}
${lineWithWhitespaces}
${lineWithTabs}${lineWithWhitespaces}
3
EOF

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "a-cut-${testInFileOgg}" 1
  aio-assert-duration-ffmpeg "b-cut-${testInFileOgg}" 1
  aio-assert-duration-ffmpeg "c-cut-${testInFileOgg}" 1
  aio-assert-duration-ffmpeg "d-cut-${testInFileOgg}" 2
}

@test "invoke aio-split - stdin - with indented data - at - heredoc" {
  aio-generate-ogg-file-sine "${testInFileOgg}"

  local -r indentWhitespaces='      '
  local -r indentTabs="$(printf '\t\t')"

  run ${__bin_path}/aio-split -- "${testInFileOgg}" <<EOF
${indentWhitespaces}1
${indentTabs}2${indentWhitespaces}
${indentTabs}${indentWhitespaces}3${indentTabs}
EOF

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "a-cut-${testInFileOgg}" 1
  aio-assert-duration-ffmpeg "b-cut-${testInFileOgg}" 1
  aio-assert-duration-ffmpeg "c-cut-${testInFileOgg}" 1
  aio-assert-duration-ffmpeg "d-cut-${testInFileOgg}" 2
}

@test "invoke aio-split - stdin - with indented data - file at - heredoc" {
  aio-generate-ogg-file-sine "${testInFileOgg}"

  local -r indentWhitespaces='      '
  local -r indentTabs="$(printf '\t\t')"

  run ${__bin_path}/aio-split --  <<EOF
${indentWhitespaces}${testInFileOgg} 1
${indentTabs}${testInFileOgg}${indentTabs}2${indentWhitespaces}
${indentTabs}${indentWhitespaces}${testInFileOgg}${indentTabs}3${indentTabs}
EOF

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "a-cut-${testInFileOgg}" 1
  aio-assert-duration-ffmpeg "b-cut-${testInFileOgg}" 1
  aio-assert-duration-ffmpeg "c-cut-${testInFileOgg}" 1
  aio-assert-duration-ffmpeg "d-cut-${testInFileOgg}" 2
}

@test "invoke aio-split - stdin - valid split positions - sec - single-digit - ordered" {
  aio-generate-ogg-file-sine "${testInFileOgg}"

  run ${__bin_path}/aio-split -- "${testInFileOgg}" <<EOF
1
2
3
EOF
  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "a-cut-${testInFileOgg}" 1
  aio-assert-duration-ffmpeg "b-cut-${testInFileOgg}" 1
  aio-assert-duration-ffmpeg "c-cut-${testInFileOgg}" 1
  aio-assert-duration-ffmpeg "d-cut-${testInFileOgg}" 2
}

@test "invoke aio-split - stdin - valid split positions - sec - single-digit - unordered" {
  aio-generate-ogg-file-sine "${testInFileOgg}"

  run ${__bin_path}/aio-split -- "${testInFileOgg}" <<EOF
3.5
1
2
EOF
  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "a-cut-${testInFileOgg}" 1
  aio-assert-duration-ffmpeg "b-cut-${testInFileOgg}" 1
  aio-assert-duration-ffmpeg "c-cut-${testInFileOgg}" 1.5
  aio-assert-duration-ffmpeg "d-cut-${testInFileOgg}" 1.5
}

@test "invoke aio-split - stdin - valid split positions - sec - multi-digit - ordered" {
  aio-generate-ogg-file-sine "${testInFileOgg}" 22

  run ${__bin_path}/aio-split -- "${testInFileOgg}" <<EOF
01.5
3
20
EOF

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "a-cut-${testInFileOgg}" 1.5
  aio-assert-duration-ffmpeg "b-cut-${testInFileOgg}" 1.5
  aio-assert-duration-ffmpeg "c-cut-${testInFileOgg}" 17
  aio-assert-duration-ffmpeg "d-cut-${testInFileOgg}" 2
}

@test "invoke aio-split - stdin - valid split positions - sec - multi-digit - ordered - long" {

  skip "INVALID TS FORMAT NOT YET DETECTED"

  aio-generate-ogg-file-sine "${testInFileOgg}" 70

  # timestamps with a missing 0 in front of single digits are not yet handled properly
  run ${__bin_path}/aio-split -- "${testInFileOgg}" <<EOF
3
1:4
1:05
EOF

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "a-cut-${testInFileOgg}" 3
  aio-assert-duration-ffmpeg "b-cut-${testInFileOgg}" 61
  aio-assert-duration-ffmpeg "c-cut-${testInFileOgg}" 1
  aio-assert-duration-ffmpeg "d-cut-${testInFileOgg}" 5
}
