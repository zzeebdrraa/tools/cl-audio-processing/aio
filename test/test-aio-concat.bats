#!/usr/bin/env bats

# This file is part of aio-proc
#
#     aio-proc is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     aio-proc is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with aio-proc. If not, see <http://www.gnu.org/licenses/>.

###########
# TODO
#
# add more tests:
#
#   - test output file with no extension
#   - test input files with no extension
#   - test --force-overwrite cl-option
#
# more:
#
#   - use bats hooks setup_file() amd teardown_file() in order to create temporary working dirs
#
###########

###########
# Test Data
###########

declare -g -r testInFileOgg="test-in.ogg"
declare -g -r testInFileOggA="test-in-a.ogg"
declare -g -r testInFileOggB="test-in-b.ogg"
declare -g -r testInFileFlac="test-in.flac"
declare -g -r invalidInFile="invalid.ogg"
declare -g -r testOutFileOgg="concat-test-in.ogg"
declare -g -r testOutFileFlac="concat-test-in.flac"

###########
# Helpers
###########

load test-aio-common-asserts
load test-aio-common-file-generators

declare -g -r __bin_path='..'

###########
# Setup
###########

setup() {
  rm -f "${testInFileOgg}"
  rm -f "${testInFileOggA}"
  rm -f "${testInFileOggB}"
  rm -f "${testInFileFlac}"
  rm -f "${testOutFileOgg}"
  rm -f "${testOutFileFlac}"
  rm -f "${invalidInFile}"
}

teardown() {
  rm -f "${testInFileOgg}"
  rm -f "${testInFileOggA}"
  rm -f "${testInFileOggB}"
  rm -f "${testInFileFlac}"
  rm -f "${testOutFileOgg}"
  rm -f "${testOutFileFlac}"
  rm -f "${invalidInFile}"
}

###########
# Tests
###########

@test "invoke aio-concat - help" {
  run ${__bin_path}/aio-concat -h
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-concat --help
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-concat help
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-concat h
  [ ${status} -eq 0 ]
}

@test "invoke aio-concat - help short" {

  run ${__bin_path}/aio-concat help short
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-concat help s
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-concat h s
  [ ${status} -eq 0 ]
}

@test "invoke aio-concat - help details" {

  run ${__bin_path}/aio-concat help details
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-concat help d
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-concat h d
  [ ${status} -eq 0 ]
}

@test "invoke aio-concat - help examples" {

  run ${__bin_path}/aio-concat help examples
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-concat help e
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-concat h e
  [ ${status} -eq 0 ]
}

@test "invoke aio-concat --version" {
  run ${__bin_path}/aio-concat --version
  [ ${status} -eq 0 ]
}

@test "invoke aio-concat - missing input files - default output file" {
  # no input, default output
  run ${__bin_path}/aio-concat
  [ ${status} -eq 1 ]
  [ "${output}" == "error: at least two input files must be specified ... stopping" ]

  # one input file, default output
  run ${__bin_path}/aio-concat dummyIn
  [ ${status} -eq 1 ]
  [ "${output}" == "error: at least two input files must be specified ... stopping" ]
}

@test "invoke aio-concat - missing input files - explicit output file" {
  # no input, explicit output
  run ${__bin_path}/aio-concat -o dummyOut
  [ ${status} -eq 1 ]
  [ "${output}" == "error: at least two input files must be specified ... stopping" ]

  # one input file, default output
  run ${__bin_path}/aio-concat -o dummyOut dummyIn
  [ ${status} -eq 1 ]
  [ "${output}" == "error: at least two input files must be specified ... stopping" ]
}

@test "invoke aio-concat - non-existsing input files - default output file" {

  run ${__bin_path}/aio-concat dummyIn1 dummyIn2

  [ ${status} -eq 1 ]

  [ "${#lines[*]}" -eq 2 ]
  [ "${lines[0]}" == 'error: input file [dummyIn1] does not exist ... skipping' ]
  [ "${lines[1]}" == 'error: input file [dummyIn2] does not exist ... skipping' ]
}

@test "invoke aio-concat - non-existsing input files - explicit output file" {

  run ${__bin_path}/aio-concat -o dummyOut dummyIn1 dummyIn2

  [ ${status} -eq 1 ]

  [ "${#lines[*]}" -eq 2 ]
  [ "${lines[0]}" == 'error: input file [dummyIn1] does not exist ... skipping' ]
  [ "${lines[1]}" == 'error: input file [dummyIn2] does not exist ... skipping' ]
}

@test "invoke aio-concat - concat - ogg" {
  local inFiles=()

  # generate a ogg file with a sine wave
  aio-generate-ogg-file-sine "${testInFileOgg}"

  inFiles=("${testInFileOgg}" "${testInFileOgg}")
  run ${__bin_path}/aio-concat "${inFiles[@]}"
  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "${testOutFileOgg}" 10
  rm -f "${testOutFileOgg}"

  inFiles=("${testInFileOgg}" "${testInFileOgg}" "${testInFileOgg}" "${testInFileOgg}" "${testInFileOgg}")
  run ${__bin_path}/aio-concat "${inFiles[@]}"
  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "${testOutFileOgg}" 25
  rm -f "${testOutFileOgg}"

  inFiles=("${testInFileOgg}" "${testInFileOgg}" "${testInFileOgg}" "${testInFileOgg}" "${testInFileOgg}" "${testInFileOgg}" "${testInFileOgg}" "${testInFileOgg}" "${testInFileOgg}" "${testInFileOgg}")
  run ${__bin_path}/aio-concat "${inFiles[@]}"
  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "${testOutFileOgg}" 50
  rm -f "${testOutFileOgg}"
}

@test "invoke aio-concat - concat - flac" {

  skip "due to problems with flac file timestamp inconsistency. files are not concatenated"

  #skip "as flac files does not seem to be concatenated"
  local inFiles=()

  # generate a ogg file with a sine wave
  aio-generate-flac-file-sine "${testInFileFlac}"

  cp "${testInFileFlac}" "keep-${testInFileFlac}"

  inFiles=("${testInFileFlac}" "${testInFileFlac}")
  run ${__bin_path}/aio-concat "${inFiles[@]}"

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "${testOutFileFlac}" 10
  rm -f "${testOutFileFlac}"

  inFiles=("${testInFileFlac}" "${testInFileFlac}" "${testInFileFlac}" "${testInFileFlac}" "${testInFileFlac}")
  run ${__bin_path}/aio-concat "${inFiles[@]}"
  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "${testOutFileFlac}" 25
  rm -f "${testOutFileFlac}"

  inFiles=("${testInFileFlac}" "${testInFileFlac}" "${testInFileFlac}" "${testInFileFlac}" "${testInFileFlac}" "${testInFileFlac}" "${testInFileFlac}" "${testInFileFlac}" "${testInFileFlac}" "${testInFileFlac}")
  run ${__bin_path}/aio-concat "${inFiles[@]}"
  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "${testOutFileFlac}" 50
  rm -f "${testOutFileFlac}"
}

@test "invoke aio-concat - concat - mixed input" {
  local inFiles=()

  # generate a ogg and flac files with a sine wave
  aio-generate-ogg-file-sine "${testInFileOgg}"
  aio-generate-flac-file-sine "${testInFileFlac}"

  inFiles=("${testInFileOgg}" "${testInFileFlac}")
  run ${__bin_path}/aio-concat "${inFiles[@]}"

  # printf '# l:%s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: input files must use the same codec but we got [flac vorbis] ... stopping' ]
}

@test "invoke aio-concat - concat invalid input files" {
  local inFiles=()

  # generate a ogg file with a sine wave
  aio-generate-ogg-file-sine "${testInFileOgg}"
  printf 'this is a text file\n' > "${invalidInFile}"

  inFiles=("${invalidInFile}" "${testInFileOgg}")
  run ${__bin_path}/aio-concat "${inFiles[@]}" 2>&1

  [ ${status} -eq 1 ]
  [ "${output}" == "error: input file [${invalidInFile}] is no audio file ... skipping" ]
}

@test "invoke aio-concat - concat - output file is one of the input files" {
  local inFiles=()

  # generate a ogg file with a sine wave
  aio-generate-ogg-file-sine "${testInFileOggA}"
  aio-generate-ogg-file-sine "${testInFileOggB}"

  inFiles=("${testInFileOggA}" "${testInFileOggB}")

  run ${__bin_path}/aio-concat -o "${testInFileOggA}" "${inFiles[@]}"

  [ ${status} -eq 1 ]

  [ "${output}" == "error: output file [${testInFileOggA}] has the same name as one of the input files ... stopping" ]

  run ${__bin_path}/aio-concat -o "${testInFileOggB}" "${inFiles[@]}"

  [ ${status} -eq 1 ]
  [ "${output}" == "error: output file [${testInFileOggB}] has the same name as one of the input files ... stopping" ]
}

@test "invoke aio-concat - concat - output file has no valid format extension" {
  local inFiles=()

  # generate a ogg file with a sine wave
  aio-generate-ogg-file-sine "${testInFileOggA}"
  aio-generate-ogg-file-sine "${testInFileOggB}"

  inFiles=("${testInFileOggA}" "${testInFileOggB}")

  # output has no format extension
  run ${__bin_path}/aio-concat -o concat-test-in "${inFiles[@]}"

  [ ${status} -eq 0 ]
  [ -f concat-test-in.ogg ]
  rm -f concat-test-in.ogg

  # output has no format extension
  run ${__bin_path}/aio-concat -o concat-test-in. "${inFiles[@]}"

  [ ${status} -eq 0 ]
  [ -f concat-test-in.ogg ]
}

@test "invoke aio-concat - concat - output file format extension does not fit input file" {
  local inFiles=()

  # generate a ogg file with a sine wave
  aio-generate-ogg-file-sine "${testInFileOggA}"
  aio-generate-ogg-file-sine "${testInFileOggB}"

  inFiles=("${testInFileOggA}" "${testInFileOggB}")

  # output has wrong format extension
  run ${__bin_path}/aio-concat -o dummyOut.flac "${inFiles[@]}"

  [ ${status} -eq 1 ]
}
