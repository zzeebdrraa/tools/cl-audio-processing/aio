#!/usr/bin/env bats

# This file is part of aio-proc
#
#     aio-proc is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     aio-proc is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with aio-proc. If not, see <http://www.gnu.org/licenses/>.

###########
# Helpers
###########

declare -g -r __bin_path='..'

###########
# Tests
###########

@test "invoke aio-metadata-generate - help" {

  run ${__bin_path}/aio-metadata-generate -h
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-metadata-generate --help
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-metadata-generate h
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-metadata-generate help
  [ ${status} -eq 0 ]
}

@test "invoke aio-metadata-generate - help short" {

  run ${__bin_path}/aio-metadata-generate help short
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-metadata-generate help s
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-metadata-generate h s
  [ ${status} -eq 0 ]
}

@test "invoke aio-metadata-generate - help details" {

  run ${__bin_path}/aio-metadata-generate help details
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-metadata-generate help d
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-metadata-generate h d
  [ ${status} -eq 0 ]
}

@test "invoke aio-metadata-generate - help examples" {

  run ${__bin_path}/aio-metadata-generate help examples
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-metadata-generate help e
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-metadata-generate h e
  [ ${status} -eq 0 ]
}

@test "invoke aio-metadata-generate --version" {
  run ${__bin_path}/aio-metadata-generate --version
  [ ${status} -eq 0 ]
}

@test "invoke aio-metadata-generate - no cmd" {
  run ${__bin_path}/aio-metadata-generate

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: you need to specify at least one of the [common] or [individual] commands ... stopping' ]
}

@test "invoke aio-metadata-generate - individual - default" {
  run ${__bin_path}/aio-metadata-generate individual 4

  [ ${status} -eq 0 ]
  [ ${#lines[*]} -eq 12 ]

  local count=0
  while read -r line; do
    [[ -z "${line}" ]] && continue
    [ ${line} == "${lines[${count}]}" ]
    count=$((count + 1))
  done <<EOF
TIT2
TPE2
TRCK

TIT2
TPE2
TRCK

TIT2
TPE2
TRCK

TIT2
TPE2
TRCK
EOF

  run ${__bin_path}/aio-metadata-generate i 4

  [ ${status} -eq 0 ]
  [ ${#lines[*]} -eq 12 ]

  local count=0
  while read -r line; do
    [[ -z "${line}" ]] && continue
    [ ${line} == "${lines[${count}]}" ]
    count=$((count + 1))
  done <<EOF
TIT2
TPE2
TRCK

TIT2
TPE2
TRCK

TIT2
TPE2
TRCK

TIT2
TPE2
TRCK
EOF
}

@test "invoke aio-metadata-generate - individual - custom tags" {
  # NOTE by verifying the command output in lines, we will lose the empty lines
  #      in between the individual paragraphs. this is due to bats way of storing
  #      command outputs. this we just count the number of filled lines
  run ${__bin_path}/aio-metadata-generate individual 4 tagA

  [ ${status} -eq 0 ]
  [ ${#lines[*]} -eq 4 ]

  local count=0
  while read -r line; do
    [[ -z "${line}" ]] && continue
    [ ${line} == "${lines[${count}]}" ]
    count=$((count+1))
  done <<EOF
tagA

tagA

tagA

tagA
EOF

  run ${__bin_path}/aio-metadata-generate individual 4 tagA tagB tagC
  [ ${status} -eq 0 ]
  [ ${#lines[*]} -eq 12 ]

  count=0
  while read -r line; do
    [[ -z "${line}" ]] && continue
    [ ${line} == "${lines[${count}]}" ]
    count=$((count+1))
  done <<EOF
tagA
tagB
tagC

tagA
tagB
tagC

tagA
tagB
tagC

tagA
tagB
tagC
EOF
}

@test "invoke aio-metadata-generate - individual - id3v2" {
  run ${__bin_path}/aio-metadata-generate individual 4 i3

  [ ${status} -eq 0 ]
  [ ${#lines[*]} -eq 12 ]

  local count=0
  while read -r line; do
    [[ -z "${line}" ]] && continue
    [ ${line} == "${lines[${count}]}" ]
    count=$((count + 1))
  done <<EOF
TIT2
TPE2
TRCK

TIT2
TPE2
TRCK

TIT2
TPE2
TRCK

TIT2
TPE2
TRCK
EOF
}

@test "invoke aio-metadata-generate - individual - vorbis comment" {
  run ${__bin_path}/aio-metadata-generate individual 4 vc

  [ ${status} -eq 0 ]
  [ ${#lines[*]} -eq 12 ]

  local count=0
  while read -r line; do
    [[ -z "${line}" ]] && continue
    [ ${line} == "${lines[${count}]}" ]
    count=$((count + 1))
  done <<EOF
TITLE
ALBUM_ARTIST
TRACKNUMBER

TITLE
ALBUM_ARTIST
TRACKNUMBER

TITLE
ALBUM_ARTIST
TRACKNUMBER

TITLE
ALBUM_ARTIST
TRACKNUMBER
EOF
}

@test "invoke aio-metadata-generate - individual - ffmpeg" {
  run ${__bin_path}/aio-metadata-generate individual 4 ff

  [ ${status} -eq 0 ]
  [ ${#lines[*]} -eq 12 ]

  local count=0
  while read -r line; do
    [[ -z "${line}" ]] && continue
    [ ${line} == "${lines[${count}]}" ]
    count=$((count + 1))
  done <<EOF
title
album_artist
track

title
album_artist
track

title
album_artist
track

title
album_artist
track
EOF
}

@test "invoke aio-metadata-generate - individual - riff" {
  run ${__bin_path}/aio-metadata-generate individual 4 ri

  [ ${status} -eq 0 ]
  [ ${#lines[*]} -eq 8 ]

  local count=0
  while read -r line; do
    [[ -z "${line}" ]] && continue
    [ ${line} == "${lines[${count}]}" ]
    count=$((count + 1))
  done <<EOF
INAM
ITRK

INAM
ITRK

INAM
ITRK

INAM
ITRK
EOF
}

@test "invoke aio-metadata-generate - individual - no number of items" {
  run ${__bin_path}/aio-metadata-generate individual
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: no item count provided for cmd [individual] ... stopping' ]

  run ${__bin_path}/aio-metadata-generate i
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: no item count provided for cmd [individual] ... stopping' ]
}

@test "invoke aio-metadata-generate - individual - invalid number of items" {
  run ${__bin_path}/aio-metadata-generate individual 0
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: item count provided for cmd [individual] must be larger 0 ... stopping' ]

  run ${__bin_path}/aio-metadata-generate individual -1
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: invalid item count [-1] provided for cmd [individual] ... stopping' ]

  run ${__bin_path}/aio-metadata-generate individual a
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: invalid item count [a] provided for cmd [individual] ... stopping' ]
}

@test "invoke aio-metadata-generate - common - default" {
  run ${__bin_path}/aio-metadata-generate common
  [ ${status} -eq 0 ]
  [ ${#lines[*]} -eq 4 ]

  local count=0
  while read -r line; do
    [ ${line} == "${lines[${count}]}" ]
    count=$((count+1))
  done <<EOF
TPE1
TALB
TYER
COMM
EOF

  run ${__bin_path}/aio-metadata-generate c
  [ ${status} -eq 0 ]
  [ ${#lines[*]} -eq 4 ]

  count=0
  while read -r line; do
    [ ${line} == "${lines[${count}]}" ]
    count=$((count+1))
  done <<EOF
TPE1
TALB
TYER
COMM
EOF
}

@test "invoke aio-metadata-generate - common - custom tags" {
  run ${__bin_path}/aio-metadata-generate common tagA
  [ ${status} -eq 0 ]
  [ ${#lines[*]} -eq 1 ]

  local count=0
  while read -r line; do
    [ ${line} == "${lines[${count}]}" ]
    count=$((count+1))
  done <<EOF
tagA
EOF

  run ${__bin_path}/aio-metadata-generate common tagA tagB tagC
  [ ${status} -eq 0 ]
  [ ${#lines[*]} -eq 3 ]

  count=0
  while read -r line; do
    [ ${line} == "${lines[${count}]}" ]
    count=$((count+1))
  done <<EOF
tagA
tagB
tagC
EOF
}

@test "invoke aio-metadata-generate - common - id3v2" {
  run ${__bin_path}/aio-metadata-generate common i3
  [ ${status} -eq 0 ]
  [ ${#lines[*]} -eq 4 ]

  local count=0
  while read -r line; do
    [ ${line} == "${lines[${count}]}" ]
    count=$((count+1))
  done <<EOF
TPE1
TALB
TYER
COMM
EOF
}

@test "invoke aio-metadata-generate - common - vorbis comment" {
  run ${__bin_path}/aio-metadata-generate common vc
  [ ${status} -eq 0 ]
  [ ${#lines[*]} -eq 4 ]

  local count=0
  while read -r line; do
    [ ${line} == "${lines[${count}]}" ]
    count=$((count+1))
  done <<EOF
ARTIST
ALBUM
DATE
DESCRIPTION
EOF
}

@test "invoke aio-metadata-generate - common - ffmpeg" {
  run ${__bin_path}/aio-metadata-generate common ff
  [ ${status} -eq 0 ]
  [ ${#lines[*]} -eq 4 ]

  local count=0
  while read -r line; do
    [ ${line} == "${lines[${count}]}" ]
    count=$((count+1))
  done <<EOF
artist
album
date
comment
EOF
}

@test "invoke aio-metadata-generate - common - riff" {
  run ${__bin_path}/aio-metadata-generate common ri
  [ ${status} -eq 0 ]
  [ ${#lines[*]} -eq 4 ]

  local count=0
  while read -r line; do
    [ ${line} == "${lines[${count}]}" ]
    count=$((count+1))
  done <<EOF
IART
IPRD
ICRD
ICMT
EOF
}
