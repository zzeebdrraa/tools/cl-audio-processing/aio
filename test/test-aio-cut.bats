#!/usr/bin/env bats

# This file is part of aio-proc
#
#     aio-proc is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     aio-proc is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with aio-proc. If not, see <http://www.gnu.org/licenses/>.

###########
# TODO
#
# add more tests:
#
#   - test output file with no extension
#   - test input files with no extension
#   - fix tests that are skipped rihgt now
#
# more:
#
#   - use bats hooks setup_file() amd teardown_file() in order to create temporary working dirs
#
###########

###########
# Test Data
###########

declare -g -r testInFileOgg="test-in.ogg"
declare -g -r invalidInFile="invalid.ogg"
declare -g -r testOutFile="test-out.ogg"

declare -g -r testOneLevelSubdirPath="test-subdir"
declare -g -r testThreeLevelSubdirPath="test/sub/dir"

declare -g -r testInFileWithWhitespacesOgg="test in 2000.ogg"
declare -g -r testInFileWithEscapedWhitespacesOgg="test\ in\ 2000.ogg"
declare -g -r testInFileWithSpecialCharsOgg="test-(in).2000.ogg"
declare -g -r testInFileWithSpecialCharsAndWhitespacesOgg="test-(in).2000 01.ogg"

declare -g -r testOutFileWithWhitespacesOgg="cut-test in 2000.ogg"
declare -g -r testOutFileWithSpecialCharsOgg="cut-test-(in).2000.ogg"
declare -g -r testOutFileWithSpecialCharsAndWhitespacesOgg="cut-test-(in).2000 01.ogg"

declare -g -r cutOutputFiles=(
  "a-cut-${testInFileOgg}"
  "b-cut-${testInFileOgg}"
  "c-cut-${testInFileOgg}"
  "cut-${testInFileWithWhitespacesOgg}"
  "cut-${testInFileWithEscapedWhitespacesOgg}"
  "cut-${testInFileWithSpecialCharsOgg}"
  "cut-${testInFileWithSpecialCharsAndWhitespacesOgg}"
  "a-cut-${testInFileWithWhitespacesOgg}"
  "a-cut-${testInFileWithEscapedWhitespacesOgg}"
  "a-cut-${testInFileWithSpecialCharsOgg}"
  "a-cut-${testInFileWithSpecialCharsAndWhitespacesOgg}"
)

###########
# Envvars
###########

export ENV_AIO_CUT_TS_TO_SEC_BIN_PATH='..'

###########
# Setup
###########

declare -g -r __bin_path='..'

load test-aio-common-asserts
load test-aio-common-file-generators

setup() {
  rm -f "${testInFileOgg}"
  rm -f "${testOutFile}"
  rm -f "${invalidInFile}"

  rm -f "${testInFileWithWhitespacesOgg}"
  rm -f "${testInFileWithEscapedWhitespacesOgg}"
  rm -f "${testInFileWithSpecialCharsOgg}"
  rm -f "${testInFileWithSpecialCharsAndWhitespacesOgg}"

  rm -f "${cutOutputFiles[@]}"

  if [[ -d "${testOneLevelSubdirPath}" ]]; then
    rm -r "${testOneLevelSubdirPath}"
  fi
  if [[ -d "${testThreeLevelSubdirPath}" ]]; then
    rm -v -f ${testThreeLevelSubdirPath}/*
    rmdir -p "${testThreeLevelSubdirPath}"
  fi
}

teardown() {
  rm -f "${testInFileOgg}"
  rm -f "${testOutFile}"
  rm -f "${invalidInFile}"

  rm -f "${testInFileWithWhitespacesOgg}"
  rm -f "${testInFileWithEscapedWhitespacesOgg}"
  rm -f "${testInFileWithSpecialCharsOgg}"
  rm -f "${testInFileWithSpecialCharsAndWhitespacesOgg}"

  rm -f "${cutOutputFiles[@]}"

  if [[ -d "${testOneLevelSubdirPath}" ]]; then
    rm -r "${testOneLevelSubdirPath}"
  fi
  if [[ -d "${testThreeLevelSubdirPath}" ]]; then
    rm -v -f ${testThreeLevelSubdirPath}/*
    rmdir -p "${testThreeLevelSubdirPath}"
  fi
}

###########
# Tests
###########


@test "invoke aio-cut - help" {
  run ${__bin_path}/aio-cut -h
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-cut --help
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-cut help
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-cut h
  [ ${status} -eq 0 ]
}

@test "invoke aio-cut - help short" {

  run ${__bin_path}/aio-cut help short
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-cut help s
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-cut h s
  [ ${status} -eq 0 ]
}

@test "invoke aio-cut - help details" {

  run ${__bin_path}/aio-cut help details
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-cut help d
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-cut h d
  [ ${status} -eq 0 ]
}

@test "invoke aio-cut - version" {
  run ${__bin_path}/aio-cut --version
  [ ${status} -eq 0 ]
}

@test "invoke aio-cut - cl - no input file" {
  run ${__bin_path}/aio-cut
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: not a file [] ... skipping' ]
}

@test "invoke aio-cut - cl - non-existing input file" {
  run ${__bin_path}/aio-cut dummyIn1

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: not a file [dummyIn1] ... skipping' ]
}

@test "invoke aio-cut - cl - input file in sub-directory - one level" {

  mkdir "${testOneLevelSubdirPath}"

  aio-generate-ogg-file-sine "${testOneLevelSubdirPath}/${testInFileOgg}"

  # NOTE outpufile will be created in this directory and not in the sub-directory
  run ${__bin_path}/aio-cut to 1.1 "${testOneLevelSubdirPath}/${testInFileOgg}" "${testOutFile}"

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "${testOutFile}" 1.1
}

@test "invoke aio-cut - cl - input file in sub-directory - more levels" {

  mkdir -p "${testThreeLevelSubdirPath}"

  aio-generate-ogg-file-sine "${testThreeLevelSubdirPath}/${testInFileOgg}"

  # NOTE outpufile will be created in this directory and not in the sub-directory
  run ${__bin_path}/aio-cut to 1.1 "${testThreeLevelSubdirPath}/${testInFileOgg}" "${testOutFile}"

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "${testOutFile}" 1.1
}

@test "invoke aio-cut - cl - input file - name with whitespaces" {
  aio-generate-ogg-file-sine "${testInFileWithWhitespacesOgg}"

  run ${__bin_path}/aio-cut from 1.1 "${testInFileWithWhitespacesOgg}"
  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "cut-${testInFileWithWhitespacesOgg}" 3.9
}

@test "invoke aio-cut - cl - input file - name with escaped whitespaces" {
  aio-generate-ogg-file-sine "${testInFileWithEscapedWhitespacesOgg}"

  run ${__bin_path}/aio-cut from 1.1 "${testInFileWithEscapedWhitespacesOgg}"
  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "cut-${testInFileWithEscapedWhitespacesOgg}" 3.9
}

@test "invoke aio-cut - cl - input file - name with special chars" {
  aio-generate-ogg-file-sine "${testInFileWithSpecialCharsOgg}"

  run ${__bin_path}/aio-cut from 1.1 "${testInFileWithSpecialCharsOgg}"
  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "cut-${testInFileWithSpecialCharsOgg}" 3.9
}

@test "invoke aio-cut - cl - input file - name with special chars and whitespaces" {
  aio-generate-ogg-file-sine "${testInFileWithSpecialCharsAndWhitespacesOgg}"

  run ${__bin_path}/aio-cut from 1.1 "${testInFileWithSpecialCharsAndWhitespacesOgg}"
  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "cut-${testInFileWithSpecialCharsAndWhitespacesOgg}" 3.9
}

@test "invoke aio-cut - cl - already existing output file - no overwrite" {

  aio-generate-ogg-file-sine "${testInFileOgg}"
  touch "${testOutFile}"

  run ${__bin_path}/aio-cut from 1.1 "${testInFileOgg}" "${testOutFile}"

  [ ${status} -eq 1 ]
  [ "${output}" == "error: output file [${testOutFile}] is already existing ... skipping" ]
}

@test "invoke aio-cut - cl - already existing output file - force overwrite" {

  aio-generate-ogg-file-sine "${testInFileOgg}"
  touch "${testOutFile}"

  run ${__bin_path}/aio-cut -f from 1.1 "${testInFileOgg}" "${testOutFile}"

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "${testOutFile}" 3.9
}

@test "invoke aio-cut - cl - select whole file" {
  touch "${testInFileOgg}"

  run ${__bin_path}/aio-cut from 0 "${testInFileOgg}" "${testOutFile}"

  [ ${status} -eq 1 ]
  [ "${output}" == "info: the whole file [${testInFileOgg}] has been selected ... nothing to do" ]
}

@test "invoke aio-cut - cl - invalid from positions" {
  touch "${testInFileOgg}"

  run ${__bin_path}/aio-cut from 00.00.0 "${testInFileOgg}" "${testOutFile}"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: not a valid from-timestamp [00.00.0] ... skipping' ]
}

@test "invoke aio-cut - cl - valid from positions" {
  aio-generate-ogg-file-sine "${testInFileOgg}"

  run ${__bin_path}/aio-cut from 1.1 "${testInFileOgg}" "${testOutFile}"
  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "${testOutFile}" 3.9
}

@test "invoke aio-cut - cl - invalid to-position" {
  touch "${testInFileOgg}"

  run ${__bin_path}/aio-cut to 01.00.0 "${testInFileOgg}" "${testOutFile}"
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: not a valid to-timestamp [01.00.0] ... skipping' ]
}

@test "invoke aio-cut - cl - valid to-position" {
  aio-generate-ogg-file-sine "${testInFileOgg}"

  run ${__bin_path}/aio-cut to 1.1 "${testInFileOgg}" "${testOutFile}"
  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "${testOutFile}" 1.1
}

# TODO add more invalid data combinations
@test "invoke aio-cut - stdin - invalid data - heredoc" {
  touch "${testInFileOgg}"

  run ${__bin_path}/aio-cut -- <<EOF
${testInFileOgg} 01.00.0
EOF

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: not a valid from-timestamp [01.00.0] ... skipping' ]

  run ${__bin_path}/aio-cut -- <<EOF
${testInFileOgg}
EOF

  [ ${status} -eq 1 ]
  [ "${output}" == "error: not enough data fields in line [${testInFileOgg}]. expected at least <file-name> <from-timestamp> ... skipping" ]
}

@test "invoke aio-cut - stdin - valid single data - heredoc" {
  aio-generate-ogg-file-sine "${testInFileOgg}"

  local -r fout="a-cut-${testInFileOgg}"
  run ${__bin_path}/aio-cut -- <<EOF
${testInFileOgg} 00:01.1
EOF

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "${fout}" 3.9
}

@test "invoke aio-cut - stdin - valid single data - herestring" {
  aio-generate-ogg-file-sine "${testInFileOgg}"

  local -r fout="a-cut-${testInFileOgg}"
  run ${__bin_path}/aio-cut -- <<<"${testInFileOgg} 00:01.1"

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "${fout}" 3.9
}

@test "invoke aio-cut - stdin - input file - name with whitespaces - herestring" {
  skip "WHITESPACES IN FILENAME NOT YET HANDLED CORRECTLY"

  aio-generate-ogg-file-sine "${testInFileWithWhitespacesOgg}"

  run ${__bin_path}/aio-cut stdin <<<"${testInFileWithWhitespacesOgg} 1.1"

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "a-cut-${testInFileWithWhitespacesOgg}" 3.9
}

@test "invoke aio-cut - stdin - input file - name with escaped whitespaces - herestring" {
  skip "WHITESPACES IN FILENAME NOT YET HANDLED CORRECTLY"

  aio-generate-ogg-file-sine "${testInFileWithEscapedWhitespacesOgg}"

  run ${__bin_path}/aio-cut stdin <<<"${testInFileWithEscapedWhitespacesOgg} 1.1"

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "a-cut-${testInFileWithEscapedWhitespacesOgg}" 3.9
}

@test "invoke aio-cut - stdin - input file - name with special chars - herestring" {
  aio-generate-ogg-file-sine "${testInFileWithSpecialCharsOgg}"

  run ${__bin_path}/aio-cut stdin <<<"${testInFileWithSpecialCharsOgg} 1.1"

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "a-cut-${testInFileWithSpecialCharsOgg}" 3.9
}

@test "invoke aio-cut - stdin - input file - name with special chars and whitespaces - herestring" {
  skip "WHITESPACES IN FILENAME NOT YET HANDLED CORRECTLY"

  aio-generate-ogg-file-sine "${testInFileWithSpecialCharsAndWhitespacesOgg}"

  run ${__bin_path}/aio-cut <<<"${testInFileWithSpecialCharsAndWhitespacesOgg} 1.1"

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "a-cut-${testInFileWithSpecialCharsAndWhitespacesOgg}" 3.9
}

@test "invoke aio-cut - stdin - with comments - heredoc" {
  aio-generate-ogg-file-sine "${testInFileOgg}"

  local -r foutA="a-cut-${testInFileOgg}"
  local -r foutB="b-cut-${testInFileOgg}"
  local -r foutC="c-cut-${testInFileOgg}"

  local -r indentWhitespaces='      '
  local -r indentTabs="$(printf '\t\t')"

  run ${__bin_path}/aio-cut -- <<EOF
# a single line comment
${testInFileOgg} 00:01.1
${testInFileOgg} 0.5
# a
${indentTabs}# multi
${indentWhitespaces}# line
${indentTabs}${indentWhitespaces}# comment
${testInFileOgg} 2.25
EOF

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "${foutA}" 3.9
  aio-assert-duration-ffmpeg "${foutB}" 4.5
  aio-assert-duration-ffmpeg "${foutC}" 2.75
}

@test "invoke aio-cut - stdin - with empty lines - heredoc" {
  aio-generate-ogg-file-sine "${testInFileOgg}"

  local -r foutA="a-cut-${testInFileOgg}"
  local -r foutB="b-cut-${testInFileOgg}"
  local -r foutC="c-cut-${testInFileOgg}"

  local -r lineWithWhitespaces='      '
  local -r lineWithTabs="$(printf '\t\t\t')"

  run ${__bin_path}/aio-cut -- <<EOF
# single empty line

${testInFileOgg} 00:01.1
# empty line containing only whitespaces
${lineWithWhitespaces}
# empty line containing only tabs
${lineWithTabs}
${testInFileOgg} 0.5
# multiple empty lines with and without whitespaces and tabs

${lineWithWhitespaces}


${lineWithTabs}
${lineWithWhitespaces}
${lineWithTabs}${lineWithWhitespaces}
${testInFileOgg} 2.25
EOF

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "${foutA}" 3.9
  aio-assert-duration-ffmpeg "${foutB}" 4.5
  aio-assert-duration-ffmpeg "${foutC}" 2.75
}

@test "invoke aio-cut - stdin - with indented data - from - heredoc" {
  aio-generate-ogg-file-sine "${testInFileOgg}"

  local -r foutA="a-cut-${testInFileOgg}"
  local -r foutB="b-cut-${testInFileOgg}"
  local -r foutC="c-cut-${testInFileOgg}"

  local -r indentWhitespaces='      '
  local -r indentTabs="$(printf '\t\t')"

  run ${__bin_path}/aio-cut -- "${testInFileOgg}" <<EOF
${indentWhitespaces}00:01.1
${indentTabs}0.5${indentWhitespaces}
${indentTabs}${indentWhitespaces}2.25${indentTabs}
EOF

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "${foutA}" 3.9
  aio-assert-duration-ffmpeg "${foutB}" 4.5
  aio-assert-duration-ffmpeg "${foutC}" 2.75
}

@test "invoke aio-cut - stdin - with indented data - file from - heredoc" {
  aio-generate-ogg-file-sine "${testInFileOgg}"

  local -r foutA="a-cut-${testInFileOgg}"
  local -r foutB="b-cut-${testInFileOgg}"
  local -r foutC="c-cut-${testInFileOgg}"

  local -r indentWhitespaces='      '
  local -r indentTabs="$(printf '\t\t')"

  run ${__bin_path}/aio-cut -- <<EOF
${indentWhitespaces}${testInFileOgg} 00:01.1
${indentTabs}${testInFileOgg}${indentWhitespaces}0.5${indentTabs}
${indentTabs}${indentWhitespaces}${testInFileOgg}${indentTabs}2.25${indentWhitespaces}
EOF

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "${foutA}" 3.9
  aio-assert-duration-ffmpeg "${foutB}" 4.5
  aio-assert-duration-ffmpeg "${foutC}" 2.75
}

@test "invoke aio-cut - stdin - valid multiline data for one file given multiple times - heredoc" {
  aio-generate-ogg-file-sine "${testInFileOgg}"

  local -r foutA="a-cut-${testInFileOgg}"
  local -r foutB="b-cut-${testInFileOgg}"
  local -r foutC="c-cut-${testInFileOgg}"

  run ${__bin_path}/aio-cut -- <<EOF
${testInFileOgg} 00:01.1
${testInFileOgg} 0.5
${testInFileOgg} 2.25
EOF

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "${foutA}" 3.9
  aio-assert-duration-ffmpeg "${foutB}" 4.5
  aio-assert-duration-ffmpeg "${foutC}" 2.75
}

@test "invoke aio-cut - stdin - valid multiline data for one file - heredoc" {
  aio-generate-ogg-file-sine "${testInFileOgg}"

  local -r foutA="a-cut-${testInFileOgg}"
  local -r foutB="b-cut-${testInFileOgg}"
  local -r foutC="c-cut-${testInFileOgg}"

  run ${__bin_path}/aio-cut -- "${testInFileOgg}" <<EOF
00:01.1
0.5
2.25
EOF

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "${foutA}" 3.9
  aio-assert-duration-ffmpeg "${foutB}" 4.5
  aio-assert-duration-ffmpeg "${foutC}" 2.75
}

@test "invoke aio-cut - stdin - already existing output files - no overwrite" {
  aio-generate-ogg-file-sine "${testInFileOgg}"

  local -r foutA="a-cut-${testInFileOgg}"
  local -r foutB="b-cut-${testInFileOgg}"
  local -r foutC="c-cut-${testInFileOgg}"

  touch "${foutA}" "${foutB}" "${foutC}"

  run ${__bin_path}/aio-cut stdin "${testInFileOgg}" <<EOF
00:01.1
0.5
2.25
EOF

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: output file [${foutA}] is already existing ... skipping" ]
  [ "${lines[1]}" == "error: output file [${foutB}] is already existing ... skipping" ]
  [ "${lines[2]}" == "error: output file [${foutC}] is already existing ... skipping" ]
}

@test "invoke aio-cut - stdin - already existing output files - force overwrite" {
  aio-generate-ogg-file-sine "${testInFileOgg}"

  local -r foutA="a-cut-${testInFileOgg}"
  local -r foutB="b-cut-${testInFileOgg}"
  local -r foutC="c-cut-${testInFileOgg}"

  touch "${foutA}" "${foutB}" "${foutC}"

  run ${__bin_path}/aio-cut -f stdin "${testInFileOgg}" <<EOF
00:01.1
0.5
2.25
EOF

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "${foutA}" 3.9
  aio-assert-duration-ffmpeg "${foutB}" 4.5
  aio-assert-duration-ffmpeg "${foutC}" 2.75
}

@test "invoke aio-cut - stdin - input file in sub-directory - one level" {

  mkdir "${testOneLevelSubdirPath}"

  aio-generate-ogg-file-sine "${testOneLevelSubdirPath}/${testInFileOgg}"
  local -r fout="a-cut-${testInFileOgg}"

  # NOTE outpufile will be created in this directory and not in the sub-directory
  run ${__bin_path}/aio-cut stdin <<<"${testOneLevelSubdirPath}/${testInFileOgg} 1.1"
  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "${fout}" 3.9
}

@test "invoke aio-cut - stdin - input file in sub-directory - more levels" {

  mkdir -p "${testThreeLevelSubdirPath}"

  aio-generate-ogg-file-sine "${testThreeLevelSubdirPath}/${testInFileOgg}"
  local -r fout="a-cut-${testInFileOgg}"

  # NOTE outpufile will be created in this directory and not in the sub-directory
  run ${__bin_path}/aio-cut stdin <<<"${testThreeLevelSubdirPath}/${testInFileOgg} 1.1"

  [ ${status} -eq 0 ]

  aio-assert-duration-ffmpeg "${fout}" 3.9
}
