#!/usr/bin/env bats

# This file is part of aio-proc
#
#     aio-proc is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     aio-proc is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with aio-proc. If not, see <http://www.gnu.org/licenses/>.

###########
# TODO
#
# add tests for:
#
#   - test --force-overwrite flag
#   - test invalid quality settings
#   - test input files with no extension
#   - test loudness normalization
#
# more:
#
#   - use bats hooks setup_file() amd teardown_file() in order to create temporary working dirs
#
###########

###########
# Test Data
###########

declare -g -r testOutFileA="norm-peak-0Db-test-in-a.ogg"
declare -g -r testOutFileB="norm-peak-0Db-test-in-b.ogg"
declare -g -r testOutFileA1="norm-peak--20db-test-in-a.ogg"
declare -g -r testOutFileB1="norm-peak--20db-test-in-b.ogg"
declare -g -r testOutFileA4="norm-peak-20db-test-in-a.ogg"
declare -g -r testOutFileB4="norm-peak-20db-test-in-b.ogg"
declare -g -r testOutFileA2="norm-peak-50-test-in-a.ogg"
declare -g -r testOutFileB2="norm-peak-50-test-in-b.ogg"
declare -g -r testOutFileA3="norm-peak-200-test-in-a.ogg"
declare -g -r testOutFileB3="norm-peak-200-test-in-b.ogg"

declare -g -r testOutFileAMp3="norm-peak-0Db-test-in-a.mp3"
declare -g -r testOutFileAFlac="norm-peak-0Db-test-in-a.flac"

declare -g -r testInFileA="test-in-a.ogg"
declare -g -r testInFileB="test-in-b.ogg"
declare -g -r testInFileAMp3="test-in-a.mp3"
declare -g -r testInFileAFlac="test-in-a.flac"

declare -g -r invalidPeakLevels=(
  ''
  '--50'
  '..50'
  '.5.'
  '.5.50'
  '-50'
  '-50-5'
  '-50.5'
  '10i0'
  'db50'
  '50d'
  '50B'
  '--50db'
  '-50.9.db'
  '-50.9.0db'
  '50.9.0db'
)

###########
# Helpers
###########

declare -g -r __bin_path='..'

load test-aio-common-file-generators
load test-aio-common-asserts

###########
# Setup
###########

setup() {
  rm -f "${testOutFileAMp3}"
  rm -f "${testOutFileAFlac}"
  rm -f "${testOutFileA}"
  rm -f "${testOutFileB}"
  rm -f "${testOutFileA1}"
  rm -f "${testOutFileB1}"
  rm -f "${testOutFileA2}"
  rm -f "${testOutFileB2}"
  rm -f "${testOutFileA3}"
  rm -f "${testOutFileB3}"
  rm -f "${testOutFileA4}"
  rm -f "${testOutFileB4}"
  rm -f "${testInFileA}"
  rm -f "${testInFileB}"
  rm -f "${testInFileAMp3}"
  rm -f "${testInFileAFlac}"
}

teardown() {
  rm -f "${testOutFileAMp3}"
  rm -f "${testOutFileAFlac}"
  rm -f "${testOutFileA}"
  rm -f "${testOutFileB}"
  rm -f "${testOutFileA1}"
  rm -f "${testOutFileB1}"
  rm -f "${testOutFileA2}"
  rm -f "${testOutFileB2}"
  rm -f "${testOutFileA3}"
  rm -f "${testOutFileB3}"
  rm -f "${testOutFileA4}"
  rm -f "${testOutFileB4}"
  rm -f "${testInFileA}"
  rm -f "${testInFileB}"
  rm -f "${testInFileAMp3}"
  rm -f "${testInFileAFlac}"
}

###########
# Asserts
###########

# @param $1 a invalid peak level
# @param $2 ... input files
assert-invalid-peak-level() {
  local -r invalidPeakLevel="$1"
  shift

  run ${__bin_path}/aio-normalize peak -p "${invalidPeakLevel}" "$@"
  [ ${status} -eq 1 ]
  [ "${output}" == "error: invalid peak level [${invalidPeakLevel}] ... stopping" ]
}

###########
# Tests
###########

@test "invoke aio-normalize - help" {

  run ${__bin_path}/aio-normalize -h
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-normalize --help
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-normalize h
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-normalize help
  [ ${status} -eq 0 ]
}

@test "invoke aio-normalize - help short" {

  run ${__bin_path}/aio-normalize help short
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-normalize help s
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-normalize h s
  [ ${status} -eq 0 ]
}

@test "invoke aio-normalize - help details" {

  run ${__bin_path}/aio-normalize help details
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-normalize help d
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-normalize h d
  [ ${status} -eq 0 ]
}

@test "invoke aio-normalize - help examples" {

  run ${__bin_path}/aio-normalize help examples
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-normalize help e
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-normalize h e
  [ ${status} -eq 0 ]
}

@test "invoke aio-normalize --version" {
  run ${__bin_path}/aio-normalize --version
  [ ${status} -eq 0 ]
}

@test "invoke aio-normalize - settings" {
  run ${__bin_path}/aio-normalize settings
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-normalize s
  [ ${status} -eq 0 ]
}

@test "invoke aio-normalize - no cmd" {
  run ${__bin_path}/aio-normalize

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: unknown command [none] ... stopping' ]

  run ${__bin_path}/aio-normalize dummyIn1

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: unknown command [none] ... stopping' ]
}

@test "invoke aio-normalize - volume" {
  aio-generate-ogg-file-sine "${testInFileA}" 2

  run ${__bin_path}/aio-normalize volume "${testInFileA}"

  [ ${status} -eq 0 ]
}

@test "invoke aio-normalize - peak - no input file" {
  run ${__bin_path}/aio-normalize peak

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: no input files provided .. stopping' ]
}

@test "invoke aio-normalize - peak - non-existing input file" {
  run ${__bin_path}/aio-normalize peak dummyIn1 dummyIn2

  [ ${status} -eq 1 ]

  [ "${#lines[*]}" -eq 2 ]
  [ "${lines[0]}" == 'error: file [dummyIn1] does not exist ... skipping' ]
  [ "${lines[1]}" == 'error: file [dummyIn2] does not exist ... skipping' ]
}

@test "invoke aio-normalize - peak - 0db" {
  aio-generate-ogg-file-sine "${testInFileA}" 2
  aio-generate-ogg-file-sine "${testInFileB}" 2

  run ${__bin_path}/aio-normalize peak "${testInFileA}" "${testInFileB}"

  [ ${status} -eq 0 ]

  [ -f "${testOutFileA}" ]
  [ -f "${testOutFileB}" ]

  aio-assert-max-volume-ffmpeg "${testOutFileA}" 0.0 0.0
  aio-assert-max-volume-ffmpeg "${testOutFileB}" 0.0 0.0
}

@test "invoke aio-normalize - peak - -20db" {
  aio-generate-ogg-file-sine "${testInFileA}" 2
  aio-generate-ogg-file-sine "${testInFileB}" 2

  run ${__bin_path}/aio-normalize peak -p -20db "${testInFileA}" "${testInFileB}"

  [ ${status} -eq 0 ]

  [ -f "${testOutFileA1}" ]
  [ -f "${testOutFileB1}" ]

  aio-assert-max-volume-ffmpeg "${testOutFileA1}" -20.2 -19.8
  aio-assert-max-volume-ffmpeg "${testOutFileB1}" -20.2 -19.8
}

@test "invoke aio-normalize - peak - 20db" {
  aio-generate-ogg-file-sine "${testInFileA}" 2
  aio-generate-ogg-file-sine "${testInFileB}" 2

  run ${__bin_path}/aio-normalize peak -p 20db "${testInFileA}" "${testInFileB}"

  [ ${status} -eq 0 ]

  [ -f "${testOutFileA4}" ]
  [ -f "${testOutFileB4}" ]

  aio-assert-max-volume-ffmpeg "${testOutFileA4}" 0.0 0.0
  aio-assert-max-volume-ffmpeg "${testOutFileB4}" 0.0 0.0
}

@test "invoke aio-normalize - peak - half" {
  aio-generate-ogg-file-sine "${testInFileA}" 2
  aio-generate-ogg-file-sine "${testInFileB}" 2

  run ${__bin_path}/aio-normalize peak -p 50 "${testInFileA}" "${testInFileB}"
  [ ${status} -eq 0 ]

  [ -f "${testOutFileA2}" ]
  [ -f "${testOutFileB2}" ]

  aio-assert-max-volume-ffmpeg "${testOutFileA2}" -24.3 -23.3
  aio-assert-max-volume-ffmpeg "${testOutFileB2}" -24.3 -23.3
}

@test "invoke aio-normalize - peak - double" {
  aio-generate-ogg-file-sine "${testInFileA}" 2
  aio-generate-ogg-file-sine "${testInFileB}" 2

  run ${__bin_path}/aio-normalize peak -p 200 "${testInFileA}" "${testInFileB}"

  [ ${status} -eq 0 ]

  [ -f "${testOutFileA3}" ]
  [ -f "${testOutFileB3}" ]

  aio-assert-max-volume-ffmpeg "${testOutFileA3}" -12.3 -11.3
  aio-assert-max-volume-ffmpeg "${testOutFileB3}" -12.3 -11.3
}

@test "invoke aio-normalize - peak - invalid" {
  aio-generate-ogg-file-sine "${testInFileA}" 2
  aio-generate-ogg-file-sine "${testInFileB}" 2

  for invalidPeakLevel in "${invalidPeakLevels[@]}"; do
    assert-invalid-peak-level "${invalidPeakLevel}" "${testInFileA}" "${testInFileB}"
  done
}

@test "invoke aio-normalize - peak - default quality - ogg" {
  aio-generate-ogg-file-sine "${testInFileA}" 2

  run ${__bin_path}/aio-normalize peak "${testInFileA}"

  [ ${status} -eq 0 ]

  [ -f "${testOutFileA}" ]

  aio-assert-bitrate "${testOutFileA}" 120000 VBR
}

@test "invoke aio-normalize - peak - custom quality - ogg" {
  aio-generate-ogg-file-sine "${testInFileA}" 2

  run ${__bin_path}/aio-normalize -q 2 peak "${testInFileA}"

  [ ${status} -eq 0 ]

  [ -f "${testOutFileA}" ]

  aio-assert-bitrate "${testOutFileA}" 70000 VBR
}

@test "invoke aio-normalize - peak - default quality - mp3" {
  aio-generate-mp3-file-sine "${testInFileAMp3}" 2

  run ${__bin_path}/aio-normalize peak "${testInFileAMp3}"

  [ ${status} -eq 0 ]
  [ -f "${testOutFileAMp3}" ]

  aio-assert-bitrate "${testOutFileAMp3}" 51764 VBR
}

@test "invoke aio-normalize - peak - custom quality - mp3" {
  aio-generate-mp3-file-sine "${testInFileAMp3}" 2

  run ${__bin_path}/aio-normalize -q 5 peak "${testInFileAMp3}"

  [ ${status} -eq 0 ]
  [ -f "${testOutFileAMp3}" ]

  aio-assert-bitrate "${testOutFileAMp3}" 34716 VBR
}

@test "invoke aio-normalize - peak - default quality - flac" {
  aio-generate-flac-file-sine "${testInFileAFlac}" 2

  run ${__bin_path}/aio-normalize peak "${testInFileAFlac}"

  [ ${status} -eq 0 ]
  [ -f "${testOutFileAFlac}" ]

  aio-assert-bitrate "${testOutFileAFlac}" 508172 VBR
}

@test "invoke aio-normalize - peak - custom quality - flac" {
  aio-generate-flac-file-sine "${testInFileAFlac}" 2

  # quality setting should not have any effect for lossless codecs
  run ${__bin_path}/aio-normalize -q 5 peak "${testInFileAFlac}"

  [ ${status} -eq 0 ]
  [ -f "${testOutFileAFlac}" ]

  aio-assert-bitrate "${testOutFileAFlac}" 508172 VBR
}
