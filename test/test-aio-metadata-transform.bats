#!/usr/bin/env bats

# This file is part of aio-proc
#
#     aio-proc is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     aio-proc is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with aio-proc. If not, see <http://www.gnu.org/licenses/>.

###########
# TODO
#
# - use bats hooks setup_file() amd teardown_file() in order to create temporary working dirs
#

###########
# Helpers
###########

declare -g -r __bin_path='..'

###########
# Setup
###########

setup() {
  rm -f meta.txt
  rm -f tags.txt
}

teardown() {
  rm -f meta.txt
  rm -f tags.txt
}

###########
# Tests
###########

@test "invoke aio-metadata-transform - help" {

  run ${__bin_path}/aio-metadata-transform -h
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-metadata-transform --help
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-metadata-transform h
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-metadata-transform help
  [ ${status} -eq 0 ]
}

@test "invoke aio-metadata-transform - help short" {

  run ${__bin_path}/aio-metadata-transform help short
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-metadata-transform help s
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-metadata-transform h s
  [ ${status} -eq 0 ]
}

@test "invoke aio-metadata-transform - help details" {

  run ${__bin_path}/aio-metadata-transform help details
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-metadata-transform help d
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-metadata-transform h d
  [ ${status} -eq 0 ]
}

@test "invoke aio-metadata-transform - help examples" {

  run ${__bin_path}/aio-metadata-transform help examples
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-metadata-transform help e
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-metadata-transform h e
  [ ${status} -eq 0 ]
}

@test "invoke aio-metadata-transform --version" {
  run ${__bin_path}/aio-metadata-transform --version
  [ ${status} -eq 0 ]
}

@test "invoke aio-metadata-transform - no cmd" {
  run ${__bin_path}/aio-metadata-transform

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: no command provided ... stopping' ]
}

@test "invoke aio-metadata-transform - number - full sequence" {
  run ${__bin_path}/aio-metadata-transform number T <<EOF
T
T
T
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'T 1' ]
  [ "${lines[1]}" == 'T 2' ]
  [ "${lines[2]}" == 'T 3' ]

  run ${__bin_path}/aio-metadata-transform number T 5 <<EOF
T
T
T
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'T 5' ]
  [ "${lines[1]}" == 'T 6' ]
  [ "${lines[2]}" == 'T 7' ]
}

@test "invoke aio-metadata-transform - number - limited sequence" {

  run ${__bin_path}/aio-metadata-transform number T 5 6 <<EOF
T
T
T
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'T 5' ]
  [ "${lines[1]}" == 'T 6' ]
  [ "${lines[2]}" == 'T' ]
}

@test "invoke aio-metadata-transform - number - full sequence - selected line" {

  skip "select option not yet implemented"

  run ${__bin_path}/aio-metadata-transform number T 5 6 select 2 <<EOF
T
T
T
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'T' ]
  [ "${lines[1]}" == 'T 5' ]
  [ "${lines[2]}" == 'T 6' ]
}

@test "invoke aio-metadata-transform - number - limited sequence - selected line" {

  skip "select option not yet implemented"

  run ${__bin_path}/aio-metadata-transform number T 5 6 select 2 <<EOF
T
T
T
T
EOF

  printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'T' ]
  [ "${lines[1]}" == 'T 5' ]
  [ "${lines[2]}" == 'T 6' ]
  [ "${lines[3]}" == 'T' ]
}


@test "invoke aio-metadata-transform - number - full sequence - dublicate" {

  run ${__bin_path}/aio-metadata-transform number T 5 ::: number T <<EOF
T
T
T
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'T 1' ]
  [ "${lines[1]}" == 'T 2' ]
  [ "${lines[2]}" == 'T 3' ]
}

@test "invoke aio-metadata-transform - number - limited sequence - dublicate" {

  run ${__bin_path}/aio-metadata-transform number T 5 ::: number T 1 2 <<EOF
T
T
T
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'T 1' ]
  [ "${lines[1]}" == 'T 2' ]
  [ "${lines[2]}" == 'T' ]
}

@test "invoke aio-metadata-transform - kv - single - no selector" {

  printf '%s' '
T aaa
A xxx

T bbb
B yyy

T ccc
C zzz
' > meta.txt

  run ${__bin_path}/aio-metadata-transform kv T as TITLE meta.txt <<EOF
TITLE x
TITLE y
TITLE z
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'TITLE aaa' ]
  [ "${lines[1]}" == 'TITLE bbb' ]
  [ "${lines[2]}" == 'TITLE ccc' ]
}

@test "invoke aio-metadata-transform - kv - single - no selector - spaces" {

  printf '%s' '
 T aaa
A xxx

 T bbb
B yyy

 T ccc
C zzz
' > meta.txt

  run ${__bin_path}/aio-metadata-transform kv T as TITLE meta.txt <<EOF
TITLE x
TITLE y
TITLE z
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'TITLE aaa' ]
  [ "${lines[1]}" == 'TITLE bbb' ]
  [ "${lines[2]}" == 'TITLE ccc' ]

  printf '%s' '
T aaa
A xxx

T bbb
B yyy

T ccc
C zzz
' > meta.txt

  run ${__bin_path}/aio-metadata-transform kv T as TITLE meta.txt <<EOF
 TITLE y
 TITLE x
 TITLE z
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'TITLE aaa' ]
  [ "${lines[1]}" == 'TITLE bbb' ]
  [ "${lines[2]}" == 'TITLE ccc' ]
}

@test "invoke aio-metadata-transform - kv - single - no selector - warnings" {

  printf '%s' '
T aaa
A xxx

T bbb
B yyy

T ccc
C zzz
' > meta.txt

  run ${__bin_path}/aio-metadata-transform kv T as TITLE meta.txt <<EOF
TITLE x
TITLE y
TITLE z
TITLE a
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'warning: number of entries for metadata tag [TITLE] is larger than entries [3] for custom tag [T] ... skipping' ]
  [ "${lines[1]}" == 'TITLE aaa' ]
  [ "${lines[2]}" == 'TITLE bbb' ]
  [ "${lines[3]}" == 'TITLE ccc' ]
  [ "${lines[4]}" == 'TITLE a' ]

  printf '%s' '
T aaa
A xxx

T bbb
B yyy

T ccc
C zzz
' > meta.txt

  run ${__bin_path}/aio-metadata-transform kv T as TITLE meta.txt <<EOF
TITLE x
TITLE y
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'warning: number of entries for metadata tag [TITLE] is less than entries [3] for custom tag [T] ... skipping' ]
  [ "${lines[1]}" == 'TITLE aaa' ]
  [ "${lines[2]}" == 'TITLE bbb' ]
}

@test "invoke aio-metadata-transform - kv - single - selector" {

  printf '%s' '
# T aaa
T xxx

# T bbb
T yyy

# T ccc
T zzz
' > meta.txt

  run ${__bin_path}/aio-metadata-transform kv T as TITLE meta.txt selector '#' <<EOF
TITLE x
TITLE y
TITLE z
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'TITLE aaa' ]
  [ "${lines[1]}" == 'TITLE bbb' ]
  [ "${lines[2]}" == 'TITLE ccc' ]

  printf '%s' '
# T aaa
! ! T xxx

# T bbb
! ! T yyy

# T ccc
! ! T zzz
' > meta.txt

  run ${__bin_path}/aio-metadata-transform kv T as TITLE meta.txt selector '! !' <<EOF
TITLE x
TITLE y
TITLE z
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'TITLE xxx' ]
  [ "${lines[1]}" == 'TITLE yyy' ]
  [ "${lines[2]}" == 'TITLE zzz' ]
}

@test "invoke aio-metadata-transform - kv - single - selector - spaces" {

  printf '%s' '
 # T aaa
T xxx

 # T bbb
T yyy

 # T ccc
T zzz
' > meta.txt

  run ${__bin_path}/aio-metadata-transform kv T as TITLE meta.txt selector '#' <<EOF
TITLE x
TITLE y
TITLE z
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'TITLE aaa' ]
  [ "${lines[1]}" == 'TITLE bbb' ]
  [ "${lines[2]}" == 'TITLE ccc' ]

  printf '%s' '
# T aaa
! ! T xxx

# T bbb
! ! T yyy

# T ccc
! ! T zzz
' > meta.txt

  run ${__bin_path}/aio-metadata-transform kv T as TITLE meta.txt selector '! !' <<EOF
  TITLE x
  TITLE z
  TITLE y
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'TITLE xxx' ]
  [ "${lines[1]}" == 'TITLE yyy' ]
  [ "${lines[2]}" == 'TITLE zzz' ]
}

@test "invoke aio-metadata-transform - kv - single - selector - warnings" {

  printf '%s' '
# T aaa
T xxx

# T bbb
T yyy

# T ccc
T zzz
' > meta.txt

  run ${__bin_path}/aio-metadata-transform kv T as TITLE meta.txt selector '#' <<EOF
TITLE x
TITLE y
TITLE z
TITLE a
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'warning: number of entries for metadata tag [TITLE] is larger than entries [3] for custom tag [T] ... skipping' ]
  [ "${lines[1]}" == 'TITLE aaa' ]
  [ "${lines[2]}" == 'TITLE bbb' ]
  [ "${lines[3]}" == 'TITLE ccc' ]
  [ "${lines[4]}" == 'TITLE a' ]

  printf '%s' '
# T aaa
T xxx

# T bbb
T yyy

# T ccc
T zzz
' > meta.txt

  run ${__bin_path}/aio-metadata-transform kv T as TITLE meta.txt selector '#' <<EOF
TITLE x
TITLE y
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'warning: number of entries for metadata tag [TITLE] is less than entries [3] for custom tag [T] ... skipping' ]
  [ "${lines[1]}" == 'TITLE aaa' ]
  [ "${lines[2]}" == 'TITLE bbb' ]
}

@test "invoke aio-metadata-transform - kv - multiple - no selector" {

  printf '%s' '
T aaa
A xxx

T bbb
A yyy

T ccc
A zzz
' > meta.txt

  run ${__bin_path}/aio-metadata-transform kv T as TITLE meta.txt \
                                       ::: kv A as ARTIST meta.txt <<EOF
TITLE x
ARTIST 1
TITLE y
ARTIST 2
TITLE z
ARTIST 3
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'TITLE aaa' ]
  [ "${lines[1]}" == 'ARTIST xxx' ]
  [ "${lines[2]}" == 'TITLE bbb' ]
  [ "${lines[3]}" == 'ARTIST yyy' ]
  [ "${lines[4]}" == 'TITLE ccc' ]
  [ "${lines[5]}" == 'ARTIST zzz' ]

  printf '%s' '
X xxx
X yyy
X zzz
' > meta.txt

  run ${__bin_path}/aio-metadata-transform kv X as TITLE meta.txt \
                                       ::: kv X as ARTIST meta.txt <<EOF
TITLE x
ARTIST 1
TITLE y
ARTIST 2
TITLE z
ARTIST 3
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'TITLE xxx' ]
  [ "${lines[1]}" == 'ARTIST xxx' ]
  [ "${lines[2]}" == 'TITLE yyy' ]
  [ "${lines[3]}" == 'ARTIST yyy' ]
  [ "${lines[4]}" == 'TITLE zzz' ]
  [ "${lines[5]}" == 'ARTIST zzz' ]
}

@test "invoke aio-metadata-transform - kv - multiple - no selector - warnings" {

  printf '%s' '
T aaa
A xxx

T bbb
A yyy

T ccc
A zzz
' > meta.txt

  run ${__bin_path}/aio-metadata-transform kv T as TITLE meta.txt \
                                       ::: kv A as ARTIST meta.txt <<EOF
TITLE x
ARTIST 1
TITLE y
ARTIST 2
TITLE z
ARTIST 3
TITLE a
ARTIST 4
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'warning: number of entries for metadata tag [TITLE] is larger than entries [3] for custom tag [T] ... skipping' ]
  [ "${lines[1]}" == 'warning: number of entries for metadata tag [ARTIST] is larger than entries [3] for custom tag [A] ... skipping' ]
  [ "${lines[2]}" == 'TITLE aaa' ]
  [ "${lines[3]}" == 'ARTIST xxx' ]
  [ "${lines[4]}" == 'TITLE bbb' ]
  [ "${lines[5]}" == 'ARTIST yyy' ]
  [ "${lines[6]}" == 'TITLE ccc' ]
  [ "${lines[7]}" == 'ARTIST zzz' ]
  [ "${lines[8]}" == 'TITLE a' ]
  [ "${lines[9]}" == 'ARTIST 4' ]

  printf '%s' '
X xxx
X yyy
X zzz
' > meta.txt

  run ${__bin_path}/aio-metadata-transform kv X as TITLE meta.txt \
                                       ::: kv X as ARTIST meta.txt <<EOF
TITLE x
ARTIST 1
TITLE y
ARTIST 2
TITLE z
ARTIST 3
TITLE a
ARTIST 4
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'warning: number of entries for metadata tag [TITLE] is larger than entries [3] for custom tag [X] ... skipping' ]
  [ "${lines[1]}" == 'warning: number of entries for metadata tag [ARTIST] is larger than entries [3] for custom tag [X] ... skipping' ]
  [ "${lines[2]}" == 'TITLE xxx' ]
  [ "${lines[3]}" == 'ARTIST xxx' ]
  [ "${lines[4]}" == 'TITLE yyy' ]
  [ "${lines[5]}" == 'ARTIST yyy' ]
  [ "${lines[6]}" == 'TITLE zzz' ]
  [ "${lines[7]}" == 'ARTIST zzz' ]
  [ "${lines[8]}" == 'TITLE a' ]
  [ "${lines[9]}" == 'ARTIST 4' ]

  printf '%s' '
X xxx
X yyy
X zzz
' > meta.txt

  run ${__bin_path}/aio-metadata-transform kv X as TITLE meta.txt \
                                       ::: kv X as ARTIST meta.txt <<EOF
TITLE x
ARTIST 1
TITLE y
ARTIST 2
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'warning: number of entries for metadata tag [TITLE] is less than entries [3] for custom tag [X] ... skipping' ]
  [ "${lines[1]}" == 'warning: number of entries for metadata tag [ARTIST] is less than entries [3] for custom tag [X] ... skipping' ]
  [ "${lines[2]}" == 'TITLE xxx' ]
  [ "${lines[3]}" == 'ARTIST xxx' ]
  [ "${lines[4]}" == 'TITLE yyy' ]
  [ "${lines[5]}" == 'ARTIST yyy' ]
}

@test "invoke aio-metadata-transform - kv - multiple - no selector - errors" {

  printf '%s' '
T aaa
A xxx

T bbb
A yyy

T ccc
' > meta.txt

  run ${__bin_path}/aio-metadata-transform kv T as TITLE meta.txt \
                                       ::: kv A as ARTIST meta.txt <<EOF
TITLE x
ARTIST 1
TITLE y
ARTIST 2
TITLE z
ARTIST 3
TITLE a
ARTIST 4
EOF

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: custom tag [A] only occured [2] times in [meta.txt] ... stopping' ]
  [ "${lines[1]}" == 'error: all custom tag names need to occure the same number of times [3]' ]
}

@test "invoke aio-metadata-transform - kv - multiple - selector" {

  printf '%s' '
# T aaa
# A xxx

# T bbb
# A yyy

# T ccc
# A zzz
' > meta.txt

  run ${__bin_path}/aio-metadata-transform kv T as TITLE meta.txt selector '#' \
                                       ::: kv A as ARTIST meta.txt selector '#' <<EOF
TITLE x
ARTIST 1
TITLE y
ARTIST 2
TITLE z
ARTIST 3
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'TITLE aaa' ]
  [ "${lines[1]}" == 'ARTIST xxx' ]
  [ "${lines[2]}" == 'TITLE bbb' ]
  [ "${lines[3]}" == 'ARTIST yyy' ]
  [ "${lines[4]}" == 'TITLE ccc' ]
  [ "${lines[5]}" == 'ARTIST zzz' ]

  printf '%s' '
# T aaa
A xxx

# T bbb
A yyy

# T ccc
A zzz
' > meta.txt

  run ${__bin_path}/aio-metadata-transform kv T as TITLE meta.txt selector '#' \
                                       ::: kv A as ARTIST meta.txt <<EOF
TITLE x
ARTIST 1
TITLE y
ARTIST 2
TITLE z
ARTIST 3
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'TITLE aaa' ]
  [ "${lines[1]}" == 'ARTIST xxx' ]
  [ "${lines[2]}" == 'TITLE bbb' ]
  [ "${lines[3]}" == 'ARTIST yyy' ]
  [ "${lines[4]}" == 'TITLE ccc' ]
  [ "${lines[5]}" == 'ARTIST zzz' ]

  printf '%s' '
# T aaa
T xxx

# T bbb
T yyy

# T ccc
T zzz
' > meta.txt

  run ${__bin_path}/aio-metadata-transform kv T as TITLE meta.txt selector '#' \
                                       ::: kv T as ARTIST meta.txt <<EOF
TITLE x
ARTIST 1
TITLE y
ARTIST 2
TITLE z
ARTIST 3
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'TITLE aaa' ]
  [ "${lines[1]}" == 'ARTIST xxx' ]
  [ "${lines[2]}" == 'TITLE bbb' ]
  [ "${lines[3]}" == 'ARTIST yyy' ]
  [ "${lines[4]}" == 'TITLE ccc' ]
  [ "${lines[5]}" == 'ARTIST zzz' ]
}

@test "invoke aio-metadata-transform - kv - multiple - selector - warnings" {

  printf '%s' '
# T aaa
// A xxx

# T bbb
// A yyy

# T ccc
// A zzz
' > meta.txt

  run ${__bin_path}/aio-metadata-transform kv T as TITLE meta.txt selector '#' \
                                       ::: kv A as ARTIST meta.txt selector '//' <<EOF
TITLE x
ARTIST 1
TITLE y
ARTIST 2
TITLE z
ARTIST 3
TITLE a
ARTIST 4
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'warning: number of entries for metadata tag [TITLE] is larger than entries [3] for custom tag [T] ... skipping' ]
  [ "${lines[1]}" == 'warning: number of entries for metadata tag [ARTIST] is larger than entries [3] for custom tag [A] ... skipping' ]
  [ "${lines[2]}" == 'TITLE aaa' ]
  [ "${lines[3]}" == 'ARTIST xxx' ]
  [ "${lines[4]}" == 'TITLE bbb' ]
  [ "${lines[5]}" == 'ARTIST yyy' ]
  [ "${lines[6]}" == 'TITLE ccc' ]
  [ "${lines[7]}" == 'ARTIST zzz' ]
  [ "${lines[8]}" == 'TITLE a' ]
  [ "${lines[9]}" == 'ARTIST 4' ]

  printf '%s' '
# X xxx
# X yyy
# X zzz
' > meta.txt

  run ${__bin_path}/aio-metadata-transform kv X as TITLE meta.txt selector '#' \
                                       ::: kv X as ARTIST meta.txt selector '#' <<EOF
TITLE x
ARTIST 1
TITLE y
ARTIST 2
TITLE z
ARTIST 3
TITLE a
ARTIST 4
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'warning: number of entries for metadata tag [TITLE] is larger than entries [3] for custom tag [X] ... skipping' ]
  [ "${lines[1]}" == 'warning: number of entries for metadata tag [ARTIST] is larger than entries [3] for custom tag [X] ... skipping' ]
  [ "${lines[2]}" == 'TITLE xxx' ]
  [ "${lines[3]}" == 'ARTIST xxx' ]
  [ "${lines[4]}" == 'TITLE yyy' ]
  [ "${lines[5]}" == 'ARTIST yyy' ]
  [ "${lines[6]}" == 'TITLE zzz' ]
  [ "${lines[7]}" == 'ARTIST zzz' ]
  [ "${lines[8]}" == 'TITLE a' ]
  [ "${lines[9]}" == 'ARTIST 4' ]

  printf '%s' '
# X xxx
# X yyy
# X zzz
' > meta.txt

  run ${__bin_path}/aio-metadata-transform kv X as TITLE meta.txt selector '#' \
                                       ::: kv X as ARTIST meta.txt selector '#' <<EOF
TITLE x
ARTIST 1
TITLE y
ARTIST 2
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'warning: number of entries for metadata tag [TITLE] is less than entries [3] for custom tag [X] ... skipping' ]
  [ "${lines[1]}" == 'warning: number of entries for metadata tag [ARTIST] is less than entries [3] for custom tag [X] ... skipping' ]
  [ "${lines[2]}" == 'TITLE xxx' ]
  [ "${lines[3]}" == 'ARTIST xxx' ]
  [ "${lines[4]}" == 'TITLE yyy' ]
  [ "${lines[5]}" == 'ARTIST yyy' ]
}

@test "invoke aio-metadata-transform - kv - multiple - selector - errors" {

  printf '%s' '
# T aaa
// A xxx

# T bbb
// A yyy

# T ccc
' > meta.txt

  run ${__bin_path}/aio-metadata-transform kv T as TITLE meta.txt selector '#' \
                                       ::: kv A as ARTIST meta.txt selector '//' <<EOF
TITLE x
ARTIST 1
TITLE y
ARTIST 2
TITLE z
ARTIST 3
TITLE a
ARTIST 4
EOF

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: custom tag [A] only occured [2] times in [meta.txt] ... stopping' ]
  [ "${lines[1]}" == 'error: all custom tag names need to occure the same number of times [3]' ]
}

@test "invoke aio-metadata-transform - column - single - default delimiter" {

  printf '%s' '
A aaa
B bbb
C ccc
' > meta.txt

  run ${__bin_path}/aio-metadata-transform column 2 as TITLE meta.txt <<EOF
TITLE x
TITLE y
TITLE z
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'TITLE aaa' ]
  [ "${lines[1]}" == 'TITLE bbb' ]
  [ "${lines[2]}" == 'TITLE ccc' ]
}

@test "invoke aio-metadata-transform - column - single - default delimiter - warnings" {

  printf '%s' '
A aaa
B bbb
C ccc
' > meta.txt

  run ${__bin_path}/aio-metadata-transform column 2 as TITLE meta.txt <<EOF
TITLE x
TITLE y
TITLE z
TITLE z
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'warning: metadata tag [TITLE] occurs more often than available columns [3] in [meta.txt] ... skipping' ]
  [ "${lines[1]}" == 'TITLE aaa' ]
  [ "${lines[2]}" == 'TITLE bbb' ]
  [ "${lines[3]}" == 'TITLE ccc' ]
  [ "${lines[4]}" == 'TITLE z' ]

  printf '%s' '
A aaa
B bbb
C ccc
' > meta.txt

  run ${__bin_path}/aio-metadata-transform column 2 as TITLE meta.txt <<EOF
TITLE x
TITLE y
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'warning: metadata tag [TITLE] occurs less often than available columns [3] in [meta.txt] ... skipping' ]
  [ "${lines[1]}" == 'TITLE aaa' ]
  [ "${lines[2]}" == 'TITLE bbb' ]
}

@test "invoke aio-metadata-transform - column - single - custom delimiter" {

  printf '%s' '
A,aaa
B,bbb
C,ccc
' > meta.txt

  run ${__bin_path}/aio-metadata-transform column 2 as TITLE meta.txt delimiter ',' <<EOF
TITLE x
TITLE y
TITLE z
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'TITLE aaa' ]
  [ "${lines[1]}" == 'TITLE bbb' ]
  [ "${lines[2]}" == 'TITLE ccc' ]

  printf '%s' '
A = x = aaa
B = y = bbb
C = z = ccc
' > meta.txt

  run ${__bin_path}/aio-metadata-transform column 3 as TITLE meta.txt delimiter '=' <<EOF
TITLE x
TITLE y
TITLE z
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'TITLE aaa' ]
  [ "${lines[1]}" == 'TITLE bbb' ]
  [ "${lines[2]}" == 'TITLE ccc' ]
}

@test "invoke aio-metadata-transform - column - single - custom delimiter - warnings" {

  printf '%s' '
A,aaa
B,bbb
C,ccc
' > meta.txt

  run ${__bin_path}/aio-metadata-transform column 2 as TITLE meta.txt delimiter ',' <<EOF
TITLE x
TITLE y
TITLE z
TITLE z
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'warning: metadata tag [TITLE] occurs more often than available columns [3] in [meta.txt] ... skipping' ]
  [ "${lines[1]}" == 'TITLE aaa' ]
  [ "${lines[2]}" == 'TITLE bbb' ]
  [ "${lines[3]}" == 'TITLE ccc' ]
  [ "${lines[4]}" == 'TITLE z' ]

  printf '%s' '
A,aaa
B,bbb
C,ccc
' > meta.txt

  run ${__bin_path}/aio-metadata-transform column 2 as TITLE meta.txt delimiter ',' <<EOF
TITLE x
TITLE y
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'warning: metadata tag [TITLE] occurs less often than available columns [3] in [meta.txt] ... skipping' ]
  [ "${lines[1]}" == 'TITLE aaa' ]
  [ "${lines[2]}" == 'TITLE bbb' ]
}

@test "invoke aio-metadata-transform - column - multiple - default delimiter" {

  printf '%s' '
A aaa hello
B bbb world
C ccc !
' > meta.txt

  # select data from different columns for different tags
  run ${__bin_path}/aio-metadata-transform column 2 as TITLE meta.txt \
                                       ::: column 3 as ARTIST meta.txt <<EOF
TITLE x
ARTIST 1
TITLE y
ARTIST 2
TITLE z
ARTIST 3
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'TITLE aaa' ]
  [ "${lines[1]}" == 'ARTIST hello' ]
  [ "${lines[2]}" == 'TITLE bbb' ]
  [ "${lines[3]}" == 'ARTIST world' ]
  [ "${lines[4]}" == 'TITLE ccc' ]
  [ "${lines[5]}" == 'ARTIST !' ]

  printf '%s' '
A x aaa
B y bbb
C z ccc
' > meta.txt

  # select data from the same column for different tags
  run ${__bin_path}/aio-metadata-transform column 2 as TITLE meta.txt \
                                       ::: column 2 as ARTIST meta.txt <<EOF
TITLE x
ARTIST 1
TITLE y
ARTIST 2
TITLE z
ARTIST 3
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'TITLE x' ]
  [ "${lines[1]}" == 'ARTIST x' ]
  [ "${lines[2]}" == 'TITLE y' ]
  [ "${lines[3]}" == 'ARTIST y' ]
  [ "${lines[4]}" == 'TITLE z' ]
  [ "${lines[5]}" == 'ARTIST z' ]
}

@test "invoke aio-metadata-transform - column - multiple - default delimiter - empty lines" {

  printf '%s' '
A aaa hello
B bbb world
C ccc !
' > meta.txt

  # select data from different columns for different tags
  run ${__bin_path}/aio-metadata-transform column 2 as TITLE meta.txt \
                                       ::: column 3 as ARTIST meta.txt <<EOF
TITLE x
ARTIST 1
# comment
# comment
TITLE y
ARTIST 2
# comment
# comment
TITLE z
ARTIST 3
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'TITLE aaa' ]
  [ "${lines[1]}" == 'ARTIST hello' ]
  [ "${lines[2]}" == '# comment' ]
  [ "${lines[3]}" == '# comment' ]
  [ "${lines[4]}" == 'TITLE bbb' ]
  [ "${lines[5]}" == 'ARTIST world' ]
  [ "${lines[6]}" == '# comment' ]
  [ "${lines[7]}" == '# comment' ]
  [ "${lines[8]}" == 'TITLE ccc' ]
  [ "${lines[9]}" == 'ARTIST !' ]
}

@test "invoke aio-metadata-transform - column - multiple - default delimiter - errors" {

  # this would lead to an error because '# comment' is considered as two columns
  # 2 columns do occure 5 times, while 3 columns only occure 3 times
  printf '%s' '
A aaa hello
# comment
B bbb world
# comment
C ccc !
' > meta.txt

  # select data from different columns for different tags
  run ${__bin_path}/aio-metadata-transform column 2 as TITLE meta.txt \
                                       ::: column 3 as ARTIST meta.txt <<EOF
TITLE x
ARTIST 1
# comment
# comment
TITLE y
ARTIST 2
# comment
# comment
TITLE z
ARTIST 3
EOF

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: all columns need to occure the same number of times [5]' ]
  [ "${lines[1]}" == 'error: column [3] separated by [ ] occured [3] times in [meta.txt] ... stopping' ]

}

@test "invoke aio-metadata-transform - column - multiple - default delimiter - warnings" {

  printf '%s' '
A aaa hello
B bbb world
C ccc !
' > meta.txt

  # select data from different columns for different tags
  run ${__bin_path}/aio-metadata-transform column 2 as TITLE meta.txt \
                                       ::: column 3 as ARTIST meta.txt <<EOF
TITLE x
ARTIST 1

ARTIST 2

ARTIST 3

ARTIST 4
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'warning: metadata tag [TITLE] occurs less often than available columns [3] in [meta.txt] ... skipping' ]
  [ "${lines[1]}" == 'warning: metadata tag [ARTIST] occurs more often than available columns [3] in [meta.txt] ... skipping' ]
  [ "${lines[2]}" == 'TITLE aaa' ]
  [ "${lines[3]}" == 'ARTIST hello' ]
  [ "${lines[4]}" == 'ARTIST world' ]
  [ "${lines[5]}" == 'ARTIST !' ]
  [ "${lines[6]}" == 'ARTIST 4' ]
}

@test "invoke aio-metadata-transform - column - multiple - custom delimiter" {

  printf '%s' '
A,aaa,hello
B,bbb,world
C,ccc,!
' > meta.txt

  # select data from different columns for different tags
  run ${__bin_path}/aio-metadata-transform column 2 as TITLE meta.txt delimiter ',' \
                                       ::: column 3 as ARTIST meta.txt delimiter ',' <<EOF
TITLE x
ARTIST 1
TITLE y
ARTIST 2
TITLE z
ARTIST 3
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'TITLE aaa' ]
  [ "${lines[1]}" == 'ARTIST hello' ]
  [ "${lines[2]}" == 'TITLE bbb' ]
  [ "${lines[3]}" == 'ARTIST world' ]
  [ "${lines[4]}" == 'TITLE ccc' ]
  [ "${lines[5]}" == 'ARTIST !' ]

  printf '%s' '
A = x = aaa
B = y = bbb
C = z = ccc
' > meta.txt

  # select data from the same column for different tags
  run ${__bin_path}/aio-metadata-transform column 2 as TITLE meta.txt delimiter '=' \
                                       ::: column 2 as ARTIST meta.txt delimiter '=' <<EOF
TITLE x
ARTIST 1
TITLE y
ARTIST 2
TITLE z
ARTIST 3
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'TITLE x' ]
  [ "${lines[1]}" == 'ARTIST x' ]
  [ "${lines[2]}" == 'TITLE y' ]
  [ "${lines[3]}" == 'ARTIST y' ]
  [ "${lines[4]}" == 'TITLE z' ]
  [ "${lines[5]}" == 'ARTIST z' ]

  printf '%s' '
A,aaa,hello
A AAA HELLO
B,bbb,world
B BBB WORLD
C,ccc,!
C CCC !!
' > meta.txt

  # select data from the same column for different tags
  # columns are selected by different separators
  run ${__bin_path}/aio-metadata-transform column 2 as TITLE meta.txt delimiter ',' \
                                       ::: column 2 as ARTIST meta.txt <<EOF
TITLE x
ARTIST 1
TITLE y
ARTIST 2
TITLE z
ARTIST 3
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'TITLE aaa' ]
  [ "${lines[1]}" == 'ARTIST AAA' ]
  [ "${lines[2]}" == 'TITLE bbb' ]
  [ "${lines[3]}" == 'ARTIST BBB' ]
  [ "${lines[4]}" == 'TITLE ccc' ]
  [ "${lines[5]}" == 'ARTIST CCC' ]

  printf '%s' '
a,aaa,hello
A AAA HELLO
b,bbb,world
B BBB WORLD
c,ccc,!
C CCC !!
' > meta.txt

  # select data from the same column for different tags
  # columns are selected by different separators
  run ${__bin_path}/aio-metadata-transform column 1 as TITLE meta.txt delimiter ',' \
                                       ::: column 1 as ARTIST meta.txt <<EOF
TITLE x
ARTIST 1
TITLE y
ARTIST 2
TITLE z
ARTIST 3
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'TITLE a' ]
  [ "${lines[1]}" == 'ARTIST A' ]
  [ "${lines[2]}" == 'TITLE b' ]
  [ "${lines[3]}" == 'ARTIST B' ]
  [ "${lines[4]}" == 'TITLE c' ]
  [ "${lines[5]}" == 'ARTIST C' ]

  printf '%s' '
A=x=aaa
B=y=bbb
C=z=ccc
D d ddd
E e eee
F f fff
' > meta.txt

  # select data from different columns for different tags
  run ${__bin_path}/aio-metadata-transform column 2 as TITLE meta.txt delimiter '=' \
                                       ::: column 3 as ARTIST meta.txt <<EOF
TITLE x
ARTIST 1

TITLE y
ARTIST 2

TITLE z
ARTIST 3
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'TITLE x' ]
  [ "${lines[1]}" == 'ARTIST ddd' ]
  [ "${lines[2]}" == 'TITLE y' ]
  [ "${lines[3]}" == 'ARTIST eee' ]
  [ "${lines[4]}" == 'TITLE z' ]
  [ "${lines[5]}" == 'ARTIST fff' ]
}

@test "invoke aio-metadata-transform - column - multiple - custom delimiter - errors" {

  # this would lead to an error because 'd = d =' is considered as two columns
  # 2 columns do occure 4 times, while 3 columns only occure 3 times
  printf '%s' '
A = x = aaa
B = y = bbb
C = z = ccc
D = d =
' > meta.txt

  # select data from different columns for different tags
  run ${__bin_path}/aio-metadata-transform column 2 as TITLE meta.txt delimiter '=' \
                                       ::: column 3 as ARTIST meta.txt delimiter '=' <<EOF
TITLE x
ARTIST 1

TITLE y
ARTIST 2

TITLE z
ARTIST 3
EOF

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: all columns need to occure the same number of times [4]' ]
  [ "${lines[1]}" == 'error: column [3] separated by [=] occured [3] times in [meta.txt] ... stopping' ]

  # this would lead to an error because 'a = x = aaa' is is also considered
  # by default whitespace delimiter leading to 6 occurences of column 3 while
  # columns separated by = do only occure 3 times
  printf '%s' '
A = x = aaa
B = y = bbb
C = z = ccc
D d ddd
E e eee
F f fff
' > meta.txt

  # select data from different columns for different tags
  run ${__bin_path}/aio-metadata-transform column 2 as TITLE meta.txt delimiter '=' \
                                       ::: column 3 as ARTIST meta.txt <<EOF
TITLE x
ARTIST 1

TITLE y
ARTIST 2

TITLE z
ARTIST 3
EOF

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: all columns need to occure the same number of times [3]' ]
  [ "${lines[1]}" == 'error: column [3] separated by [ ] occured [6] times in [meta.txt] ... stopping' ]

  # this would lead to an error because 'D d' is is also considered
  # as two columns leading to column 3 separated by whitespace to occure
  # only 2 times while column 2 separated by = do occure 3 times
  printf '%s' '
A=x=aaa
B=y=bbb
C=z=ccc
D d
E e eee
F f fff
' > meta.txt

  # select data from different columns for different tags
  run ${__bin_path}/aio-metadata-transform column 2 as TITLE meta.txt delimiter '=' \
                                       ::: column 3 as ARTIST meta.txt <<EOF
TITLE x
ARTIST 1

TITLE y
ARTIST 2

TITLE z
ARTIST 3
EOF

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: all columns need to occure the same number of times [3]' ]
  [ "${lines[1]}" == 'error: column [3] separated by [ ] occured [2] times in [meta.txt] ... stopping' ]
}

@test "invoke aio-metadata-transform - column - multiple - custom delimiter - warnings" {

  printf '%s' '
A = x = aaa
B = y = bbb
C = z = ccc
' > meta.txt

  # select data from different columns for different tags
  run ${__bin_path}/aio-metadata-transform column 2 as TITLE meta.txt delimiter '=' \
                                       ::: column 3 as ARTIST meta.txt delimiter '=' <<EOF
TITLE x
ARTIST 1

ARTIST 2

ARTIST 3

ARTIST 4
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'warning: metadata tag [TITLE] occurs less often than available columns [3] in [meta.txt] ... skipping' ]
  [ "${lines[1]}" == 'warning: metadata tag [ARTIST] occurs more often than available columns [3] in [meta.txt] ... skipping' ]
  [ "${lines[2]}" == 'TITLE x' ]
  [ "${lines[3]}" == 'ARTIST aaa' ]
  [ "${lines[4]}" == 'ARTIST bbb' ]
  [ "${lines[5]}" == 'ARTIST ccc' ]
  [ "${lines[6]}" == 'ARTIST 4' ]
}

@test "invoke aio-metadata-transform - value - single - all tags" {

  # select data from different columns for different tags
  run ${__bin_path}/aio-metadata-transform value 'new artist' as ARTIST <<EOF
TITLE a
ARTIST 1

TITLE b
ARTIST 2

TITLE c
ARTIST 3

TITLE d
ARTIST 4
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'TITLE a' ]
  [ "${lines[1]}" == 'ARTIST new artist' ]
  [ "${lines[2]}" == 'TITLE b' ]
  [ "${lines[3]}" == 'ARTIST new artist' ]
  [ "${lines[4]}" == 'TITLE c' ]
  [ "${lines[5]}" == 'ARTIST new artist' ]
  [ "${lines[6]}" == 'TITLE d' ]
  [ "${lines[7]}" == 'ARTIST new artist' ]
}

@test "invoke aio-metadata-transform - value - single - one tag" {

  # select data from different columns for different tags
  run ${__bin_path}/aio-metadata-transform value 'new artist' as ARTIST select 2 <<EOF
TITLE a
ARTIST 1

TITLE b
ARTIST 2

TITLE c
ARTIST 3

TITLE d
ARTIST 4
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'TITLE a' ]
  [ "${lines[1]}" == 'ARTIST 1' ]
  [ "${lines[2]}" == 'TITLE b' ]
  [ "${lines[3]}" == 'ARTIST new artist' ]
  [ "${lines[4]}" == 'TITLE c' ]
  [ "${lines[5]}" == 'ARTIST 3' ]
  [ "${lines[6]}" == 'TITLE d' ]
  [ "${lines[7]}" == 'ARTIST 4' ]
}

@test "invoke aio-metadata-transform - value - single - multiple tags" {

  # select data from different columns for different tags
  run ${__bin_path}/aio-metadata-transform value 'new artist' as ARTIST select 2 4 <<EOF
TITLE a
ARTIST 1

TITLE b
ARTIST 2

TITLE c
ARTIST 3

TITLE d
ARTIST 4
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'TITLE a' ]
  [ "${lines[1]}" == 'ARTIST 1' ]
  [ "${lines[2]}" == 'TITLE b' ]
  [ "${lines[3]}" == 'ARTIST new artist' ]
  [ "${lines[4]}" == 'TITLE c' ]
  [ "${lines[5]}" == 'ARTIST 3' ]
  [ "${lines[6]}" == 'TITLE d' ]
  [ "${lines[7]}" == 'ARTIST new artist' ]
}

@test "invoke aio-metadata-transform - value - single - tags from range" {

  # select data from different columns for different tags
  run ${__bin_path}/aio-metadata-transform value 'new artist' as ARTIST select 2- <<EOF
TITLE a
ARTIST 1

TITLE b
ARTIST 2

TITLE c
ARTIST 3

TITLE d
ARTIST 4
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'TITLE a' ]
  [ "${lines[1]}" == 'ARTIST 1' ]
  [ "${lines[2]}" == 'TITLE b' ]
  [ "${lines[3]}" == 'ARTIST new artist' ]
  [ "${lines[4]}" == 'TITLE c' ]
  [ "${lines[5]}" == 'ARTIST new artist' ]
  [ "${lines[6]}" == 'TITLE d' ]
  [ "${lines[7]}" == 'ARTIST new artist' ]
}

@test "invoke aio-metadata-transform - value - single - tags within range" {

  # select data from different columns for different tags
  run ${__bin_path}/aio-metadata-transform value 'new artist' as ARTIST select 1-3 <<EOF
TITLE a
ARTIST 1

TITLE b
ARTIST 2

TITLE c
ARTIST 3

TITLE d
ARTIST 4
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'TITLE a' ]
  [ "${lines[1]}" == 'ARTIST new artist' ]
  [ "${lines[2]}" == 'TITLE b' ]
  [ "${lines[3]}" == 'ARTIST new artist' ]
  [ "${lines[4]}" == 'TITLE c' ]
  [ "${lines[5]}" == 'ARTIST new artist' ]
  [ "${lines[6]}" == 'TITLE d' ]
  [ "${lines[7]}" == 'ARTIST 4' ]
}

@test "invoke aio-metadata-transform - value - multiple - all tags" {

  # select data from different columns for different tags
  run ${__bin_path}/aio-metadata-transform value 'new artist' as ARTIST \
                                       ::: value 'new album' as ALBUM <<EOF
TITLE a
ARTIST 1
ALBUM 11

TITLE b
ARTIST 2
ALBUM 22

TITLE c
ARTIST 3
ALBUM 33

TITLE d
ARTIST 4
ALBUM 44
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'TITLE a' ]
  [ "${lines[1]}" == 'ARTIST new artist' ]
  [ "${lines[2]}" == 'ALBUM new album' ]
  [ "${lines[3]}" == 'TITLE b' ]
  [ "${lines[4]}" == 'ARTIST new artist' ]
  [ "${lines[5]}" == 'ALBUM new album' ]
  [ "${lines[6]}" == 'TITLE c' ]
  [ "${lines[7]}" == 'ARTIST new artist' ]
  [ "${lines[8]}" == 'ALBUM new album' ]
  [ "${lines[9]}" == 'TITLE d' ]
  [ "${lines[10]}" == 'ARTIST new artist' ]
  [ "${lines[11]}" == 'ALBUM new album' ]
}

@test "invoke aio-metadata-transform - value - multiple - selected tags" {

  # assign data to selected occurences of tags
  run ${__bin_path}/aio-metadata-transform value 'new artist' as ARTIST select 1 3-4 \
                                       ::: value 'new album' as ALBUM select 2- <<EOF
TITLE a
ARTIST 1
ALBUM 11

TITLE b
ARTIST 2
ALBUM 22

TITLE c
ARTIST 3
ALBUM 33

TITLE d
ARTIST 4
ALBUM 44
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'TITLE a' ]
  [ "${lines[1]}" == 'ARTIST new artist' ]
  [ "${lines[2]}" == 'ALBUM 11' ]
  [ "${lines[3]}" == 'TITLE b' ]
  [ "${lines[4]}" == 'ARTIST 2' ]
  [ "${lines[5]}" == 'ALBUM new album' ]
  [ "${lines[6]}" == 'TITLE c' ]
  [ "${lines[7]}" == 'ARTIST new artist' ]
  [ "${lines[8]}" == 'ALBUM new album' ]
  [ "${lines[9]}" == 'TITLE d' ]
  [ "${lines[10]}" == 'ARTIST new artist' ]
  [ "${lines[11]}" == 'ALBUM new album' ]

  # assign data to all and selected occurences of tags
  run ${__bin_path}/aio-metadata-transform value 'new artist' as ARTIST select 1 3-4 \
                                       ::: value 'new album' as ALBUM <<EOF
TITLE a
ARTIST 1
ALBUM 11

TITLE b
ARTIST 2
ALBUM 22

TITLE c
ARTIST 3
ALBUM 33

TITLE d
ARTIST 4
ALBUM 44
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'TITLE a' ]
  [ "${lines[1]}" == 'ARTIST new artist' ]
  [ "${lines[2]}" == 'ALBUM new album' ]
  [ "${lines[3]}" == 'TITLE b' ]
  [ "${lines[4]}" == 'ARTIST 2' ]
  [ "${lines[5]}" == 'ALBUM new album' ]
  [ "${lines[6]}" == 'TITLE c' ]
  [ "${lines[7]}" == 'ARTIST new artist' ]
  [ "${lines[8]}" == 'ALBUM new album' ]
  [ "${lines[9]}" == 'TITLE d' ]
  [ "${lines[10]}" == 'ARTIST new artist' ]
  [ "${lines[11]}" == 'ALBUM new album' ]
}

assert-kv-number-pipeline() {
  ${__bin_path}/aio-metadata-transform kv T as TITLE meta.txt ::: kv A as ARTIST meta.txt <tags.txt \
| ${__bin_path}/aio-metadata-transform n TRACK
}

assert-column-number-pipeline() {
  ${__bin_path}/aio-metadata-transform c 1 as TITLE meta.txt ::: c 2 as ARTIST meta.txt <tags.txt \
| ${__bin_path}/aio-metadata-transform n TRACK
}

assert-number-column-pipeline() {
  ${__bin_path}/aio-metadata-transform n TRACK  <tags.txt \
| ${__bin_path}/aio-metadata-transform c 1 as TITLE meta.txt ::: c 2 as ARTIST meta.txt
}

@test "invoke aio-metadata-transform - pipeline" {

  printf '%s' '
TITLE x
ARTIST 1
TRACK

TITLE y
ARTIST 2
TRACK

TITLE z
ARTIST 3
TRACK
' > tags.txt

  printf '%s' '
T aaa
A xxx

T bbb
A yyy

T ccc
A zzz
' > meta.txt

  run assert-kv-number-pipeline

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'TITLE aaa' ]
  [ "${lines[1]}" == 'ARTIST xxx' ]
  [ "${lines[2]}" == 'TRACK 1' ]
  [ "${lines[3]}" == 'TITLE bbb' ]
  [ "${lines[4]}" == 'ARTIST yyy' ]
  [ "${lines[5]}" == 'TRACK 2' ]
  [ "${lines[6]}" == 'TITLE ccc' ]
  [ "${lines[7]}" == 'ARTIST zzz' ]
  [ "${lines[8]}" == 'TRACK 3' ]

  printf '%s' '
aaa xxx
bbb yyy
ccc zzz
' > meta.txt

  run assert-column-number-pipeline

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'TITLE aaa' ]
  [ "${lines[1]}" == 'ARTIST xxx' ]
  [ "${lines[2]}" == 'TRACK 1' ]
  [ "${lines[3]}" == 'TITLE bbb' ]
  [ "${lines[4]}" == 'ARTIST yyy' ]
  [ "${lines[5]}" == 'TRACK 2' ]
  [ "${lines[6]}" == 'TITLE ccc' ]
  [ "${lines[7]}" == 'ARTIST zzz' ]
  [ "${lines[8]}" == 'TRACK 3' ]

  run assert-number-column-pipeline

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'TITLE aaa' ]
  [ "${lines[1]}" == 'ARTIST xxx' ]
  [ "${lines[2]}" == 'TRACK 1' ]
  [ "${lines[3]}" == 'TITLE bbb' ]
  [ "${lines[4]}" == 'ARTIST yyy' ]
  [ "${lines[5]}" == 'TRACK 2' ]
  [ "${lines[6]}" == 'TITLE ccc' ]
  [ "${lines[7]}" == 'ARTIST zzz' ]
  [ "${lines[8]}" == 'TRACK 3' ]

}
