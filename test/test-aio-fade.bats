#!/usr/bin/env bats

# This file is part of aio-proc
#
#     aio-proc is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     aio-proc is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with aio-proc. If not, see <http://www.gnu.org/licenses/>.

###########
# TODO
#
# add tests for:
#
#   - test --force-overwrite flag
#   - test input files with no extension
#
# some test fail from time to time, probably due to too narrow time frame
#
#   - invoke aio-fade - fade-out - default duration
#
# more:
#
#   - use bats hooks setup_file() amd teardown_file() in order to create temporary working dirs
#
###########

###########
# Test Data
###########

declare -g -r testInFileA="test-in-a.ogg"
declare -g -r testInFileB="test-in-b.ogg"

declare -g -r testOutFileInA="fade-in-test-in-a.ogg"
declare -g -r testOutFileInB="fade-in-test-in-b.ogg"
declare -g -r testOutFileOutA="fade-out-test-in-a.ogg"
declare -g -r testOutFileOutB="fade-out-test-in-b.ogg"
declare -g -r testOutFileInOutA="fade-inout-test-in-a.ogg"
declare -g -r testOutFileInOutB="fade-inout-test-in-b.ogg"

declare -g -r testInFileAMp3="test-in-a.mp3"
declare -g -r testInFileAFlac="test-in-a.flac"

declare -g -r testOutFileOutAMp3="fade-out-test-in-a.mp3"
declare -g -r testOutFileOutAFlac="fade-out-test-in-a.flac"

declare -g -r -a __invalid_fade_times=(
  '0000'
  '0'
  '0.00'
  '0000.'
  '--50'
  '..50'
  '.5.'
  '.5.50'
  '-50'
  '-50-5'
  '-50.5'
  '10i0'
  'sec50'
  '50s'
  '50S'
  '--50s'
  '-50.9.0'
  '-50.9,0'
  '50.9.0'
)

declare -g -r -a __valid_fade_times=(
  '1'
  '1.0'
  '.5'
  '0.5'
)

declare -g -r __invalid_quality_settings=(
  '1.5'
  '0.5'
  '1.'
  '.5'
  '-1'
  '-1.5'
  '-1.a'
  '-a'
  'aa'
  '1a'
  'a1'
)

###########
# Helpers
###########

declare -g -r __bin_path='..'

load test-aio-common-asserts
load test-aio-common-file-generators

###########
# Asserts
###########

assert-invalid-in-fade-time() {
  local -r durationSec="$1"
  shift

  run ${__bin_path}/aio-fade in --di "${durationSec}" "$@"
  [ ${status} -eq 1 ]
  [ "${output}" == "error: invalid fade-in duration time [${durationSec}] ... stopping" ]
}

assert-invalid-out-fade-time() {
  local -r durationSec="$1"
  shift

  run ${__bin_path}/aio-fade out --do "${durationSec}" "$@"
  [ ${status} -eq 1 ]
  [ "${output}" == "error: invalid fade-out duration time [${durationSec}] ... stopping" ]
}

assert-dry-run-output() {
  declare -r -n outListRef="$1"
  
  local line
  local expectedOutList=()
  
  while read -r line; do
    linePartList=(${line})
    expectedOutList+=("${linePartList[*]}")
  done
  
  [ "${#outListRef[*]}" -eq "${#expectedOutList[*]}" ]
  
  local index
  local linePartList=()

  for index in "${!outListRef[@]}"; do
  
    # dont qoute as we want to remove all trailing and leading whitespaces in a line
    linePartList=(${outListRef[${index}]})

    [ "${linePartList[*]}" == "${expectedOutList[${index}]}" ]
  done
}

###########
# Setup
###########

setup() {
  rm -f "${testInFileA}"
  rm -f "${testInFileB}"

  rm -f "${testOutFileInA}"
  rm -f "${testOutFileInB}"
  rm -f "${testOutFileOutA}"
  rm -f "${testOutFileOutB}"
  rm -f "${testOutFileInOutA}"
  rm -f "${testOutFileInOutB}"

  rm -f "${testInFileAMp3}"
  rm -f "${testInFileAFlac}"

  rm -f "${testOutFileOutAMp3}"
  rm -f "${testOutFileOutAFlac}"

  rm -f noaudioIn1
  rm -f noaudioIn2
}

teardown() {
  rm -f "${testInFileA}"
  rm -f "${testInFileB}"

  rm -f "${testOutFileInA}"
  rm -f "${testOutFileInB}"
  rm -f "${testOutFileOutA}"
  rm -f "${testOutFileOutB}"
  rm -f "${testOutFileInOutA}"
  rm -f "${testOutFileInOutB}"

  rm -f "${testInFileAMp3}"
  rm -f "${testInFileAFlac}"

  rm -f "${testOutFileOutAMp3}"
  rm -f "${testOutFileOutAFlac}"

  rm -f noaudioIn1
  rm -f noaudioIn2
}

###########
# Tests
###########

@test "invoke aio-fade - help" {

  run ${__bin_path}/aio-fade -h
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-fade --help
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-fade h
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-fade help
  [ ${status} -eq 0 ]
}

@test "invoke aio-fade - help short" {

  run ${__bin_path}/aio-fade help short
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-fade help s
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-fade h s
  [ ${status} -eq 0 ]
}

@test "invoke aio-fade - help details" {

  run ${__bin_path}/aio-fade help details
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-fade help d
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-fade h d
  [ ${status} -eq 0 ]
}

@test "invoke aio-fade - help examples" {

  run ${__bin_path}/aio-fade help examples
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-fade help e
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-fade h e
  [ ${status} -eq 0 ]
}

@test "invoke aio-fade --version" {
  run ${__bin_path}/aio-fade --version
  [ ${status} -eq 0 ]
}

@test "invoke aio-fade - settings" {

  run ${__bin_path}/aio-fade settings
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-fade s
  [ ${status} -eq 0 ]
}

@test "invoke aio-fade - no cmd" {
  run ${__bin_path}/aio-fade

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: unknown command [none] ... stopping' ]

  run ${__bin_path}/aio-fade dummyFileA dummyFileB

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: unknown command [none] ... stopping' ]

  run ${__bin_path}/aio-fade infade dummyFileA dummyFileB

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: unknown command [none] ... stopping' ]
}

@test "invoke aio-fade - no input file" {
  run ${__bin_path}/aio-fade in

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: no input files provided ... stopping' ]

  run ${__bin_path}/aio-fade out

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: no input files provided ... stopping' ]

  run ${__bin_path}/aio-fade inout

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: no input files provided ... stopping' ]
}

@test "invoke aio-fade - non-existing input file" {
  run ${__bin_path}/aio-fade in dummyIn1 dummyIn2

  [ ${status} -eq 1 ]

  [ "${#lines[*]}" -eq 2 ]
  [ "${lines[0]}" == 'error: file [dummyIn1] does not exist ... skipping' ]
  [ "${lines[1]}" == 'error: file [dummyIn2] does not exist ... skipping' ]

  run ${__bin_path}/aio-fade out dummyIn1 dummyIn2

  [ ${status} -eq 1 ]

  [ "${#lines[*]}" -eq 2 ]
  [ "${lines[0]}" == 'error: file [dummyIn1] does not exist ... skipping' ]
  [ "${lines[1]}" == 'error: file [dummyIn2] does not exist ... skipping' ]

  run ${__bin_path}/aio-fade inout dummyIn1 dummyIn2

  [ ${status} -eq 1 ]

  [ "${#lines[*]}" -eq 2 ]
  [ "${lines[0]}" == 'error: file [dummyIn1] does not exist ... skipping' ]
  [ "${lines[1]}" == 'error: file [dummyIn2] does not exist ... skipping' ]
}

@test "invoke aio-fade - non-audio input file" {
  rm -f noaudioIn1 noaudioIn2
  touch noaudioIn1 noaudioIn2
  echo "text" > noaudioIn2

  run ${__bin_path}/aio-fade in noaudioIn1 noaudioIn2

  [ ${status} -eq 1 ]

  [ "${#lines[*]}" -eq 2 ]
  [ "${lines[0]}" == 'error: input file [noaudioIn1] is no audio file ... skipping' ]
  [ "${lines[1]}" == 'error: input file [noaudioIn2] is no audio file ... skipping' ]

  run ${__bin_path}/aio-fade out noaudioIn1 noaudioIn2

  [ ${status} -eq 1 ]

  [ "${#lines[*]}" -eq 2 ]
  [ "${lines[0]}" == 'error: input file [noaudioIn1] is no audio file ... skipping' ]
  [ "${lines[1]}" == 'error: input file [noaudioIn2] is no audio file ... skipping' ]

  run ${__bin_path}/aio-fade inout noaudioIn1 noaudioIn2

  [ ${status} -eq 1 ]

  [ "${#lines[*]}" -eq 2 ]
  [ "${lines[0]}" == 'error: input file [noaudioIn1] is no audio file ... skipping' ]
  [ "${lines[1]}" == 'error: input file [noaudioIn2] is no audio file ... skipping' ]

  rm -f noaudioIn1 noaudioIn2
}

@test "invoke aio-fade - fade-in-out - default duration" {
  aio-generate-ogg-file-noise "${testInFileA}"
  cp "${testInFileA}" "${testInFileB}"

  run ${__bin_path}/aio-fade inout "${testInFileA}" "${testInFileB}"

  [ ${status} -eq 0 ]
  [ -f "${testOutFileInOutA}" ]
  [ -f "${testOutFileInOutB}" ]

  aio-assert-volume-begin-ffmpeg "${testOutFileInOutA}" 0.01 0 -1.5 -9 -11.5
  aio-assert-volume-begin-ffmpeg "${testOutFileInOutB}" 0.01 0 -1.5 -9 -11.5
  aio-assert-volume-end-ffmpeg "${testOutFileInOutA}" 4.99 0 0 -7.5 -11
  aio-assert-volume-end-ffmpeg "${testOutFileInOutB}" 4.99 0 0 -7.5 -11
}

@test "invoke aio-fade - fade-in - default duration" {
  aio-generate-ogg-file-noise "${testInFileA}"
  cp "${testInFileA}" "${testInFileB}"

  run ${__bin_path}/aio-fade in "${testInFileA}" "${testInFileB}"

  [ ${status} -eq 0 ]
  [ -f "${testOutFileInA}" ]
  [ -f "${testOutFileInB}" ]

  aio-assert-volume-begin-ffmpeg "${testOutFileInA}" 0.01 0 -1.5 -9 -11.5
  aio-assert-volume-begin-ffmpeg "${testOutFileInB}" 0.01 0 -1.5 -9 -11.5
}

@test "invoke aio-fade - fade-out - default duration" {
  aio-generate-ogg-file-noise "${testInFileA}"
  cp "${testInFileA}" "${testInFileB}"

  run ${__bin_path}/aio-fade out "${testInFileA}" "${testInFileB}"

  [ ${status} -eq 0 ]
  [ -f "${testOutFileOutA}" ]
  [ -f "${testOutFileOutB}" ]

  aio-assert-volume-end-ffmpeg "${testOutFileOutA}" 4.99 0 -1.5 -8 -11
  aio-assert-volume-end-ffmpeg "${testOutFileOutB}" 4.99 0 -1.5 -8 -11
}

@test "invoke aio-fade - fade-in - custom duration" {
  aio-generate-ogg-file-noise "${testInFileA}"
  cp "${testInFileA}" "${testInFileB}"

  for validFadeTime in "${__valid_fade_times[@]}"; do

    run ${__bin_path}/aio-fade in --di "${validFadeTime}" "${testInFileA}" "${testInFileB}"

    [ ${status} -eq 0 ]
    [ -f "${testOutFileInA}" ]
    [ -f "${testOutFileInB}" ]

    aio-assert-volume-begin-ffmpeg "${testOutFileInA}" "${validFadeTime}" 0 -1.5 -9 -11.5
    aio-assert-volume-begin-ffmpeg "${testOutFileInB}" "${validFadeTime}" 0 -1.5 -9 -11.5

    rm -f "${testOutFileInA}"
    rm -f "${testOutFileInB}"
  done
}

@test "invoke aio-fade - fade-out - custom duration" {
  aio-generate-ogg-file-noise "${testInFileA}"
  cp "${testInFileA}" "${testInFileB}"

  for validFadeTime in "${__valid_fade_times[@]}"; do
    run ${__bin_path}/aio-fade out --do "${validFadeTime}" "${testInFileA}" "${testInFileB}"

    [ ${status} -eq 0 ]
    [ -f "${testOutFileOutA}" ]
    [ -f "${testOutFileOutB}" ]

    aio-assert-volume-end-ffmpeg "${testOutFileOutA}" $(bc -l <<<"5 - ${validFadeTime}") 0 -1.5 -9 -11
    aio-assert-volume-end-ffmpeg "${testOutFileOutB}" $(bc -l <<<"5 - ${validFadeTime}") 0 -1.5 -9 -11

    rm -f "${testOutFileOutA}"
    rm -f "${testOutFileOutB}"
  done
}

@test "invoke aio-fade - fade-in-out - custom duration" {
  aio-generate-ogg-file-noise "${testInFileA}"
  cp "${testInFileA}" "${testInFileB}"

  for validFadeTime in "${__valid_fade_times[@]}"; do
    run ${__bin_path}/aio-fade inout --di "${validFadeTime}" --do "${validFadeTime}" "${testInFileA}" "${testInFileB}"

    [ ${status} -eq 0 ]
    [ -f "${testOutFileInOutA}" ]
    [ -f "${testOutFileInOutB}" ]

    aio-assert-volume-begin-ffmpeg "${testOutFileInOutA}" "${validFadeTime}" 0 0 -9 -11.5
    aio-assert-volume-begin-ffmpeg "${testOutFileInOutB}" "${validFadeTime}" 0 0 -9 -11.5
    aio-assert-volume-end-ffmpeg "${testOutFileInOutA}" $(bc -l <<<"5 - ${validFadeTime}") 0 -1.5 -9 -11
    aio-assert-volume-end-ffmpeg "${testOutFileInOutB}" $(bc -l <<<"5 - ${validFadeTime}") 0 -1.5 -9 -11

    rm -f "${testOutFileInOutA}"
    rm -f "${testOutFileInOutB}"
  done
}

@test "invoke aio-fade - default quality - ogg" {
  aio-generate-ogg-file-noise "${testInFileA}"

  run ${__bin_path}/aio-fade out "${testInFileA}"

  [ ${status} -eq 0 ]
  [ -f "${testOutFileOutA}" ]

  aio-assert-bitrate "${testOutFileOutA}" 120000 VBR
}

@test "invoke aio-fade - default quality - mp3" {
  aio-generate-mp3-file-noise "${testInFileAMp3}"

  run ${__bin_path}/aio-fade out "${testInFileAMp3}"

  [ ${status} -eq 0 ]
  [ -f "${testOutFileOutAMp3}" ]

  aio-assert-bitrate "${testOutFileOutAMp3}" 99242 VBR
}

@test "invoke aio-fade - default quality - flac" {
  aio-generate-flac-file-noise "${testInFileAFlac}"

  run ${__bin_path}/aio-fade out "${testInFileAFlac}"

  [ ${status} -eq 0 ]
  [ -f "${testOutFileOutAFlac}" ]

  aio-assert-bitrate "${testOutFileOutAFlac}" 1059094 VBR
}

@test "invoke aio-fade - custom quality - ogg" {
  aio-generate-ogg-file-noise "${testInFileA}"

  # low quality
  run ${__bin_path}/aio-fade out -q 2 "${testInFileA}"

  [ ${status} -eq 0 ]
  [ -f "${testOutFileOutA}" ]

  aio-assert-bitrate "${testOutFileOutA}" 70000 VBR
}

@test "invoke aio-fade - custom quality - mp3" {
  aio-generate-mp3-file-noise "${testInFileAMp3}"

  # low quality
  run ${__bin_path}/aio-fade out -q 5 "${testInFileAMp3}"

  [ ${status} -eq 0 ]
  [ -f "${testOutFileOutAMp3}" ]

  aio-assert-bitrate "${testOutFileOutAMp3}" 64000 VBR
}

@test "invoke aio-fade - custom quality - flac" {
  aio-generate-flac-file-noise "${testInFileAFlac}"

  # quality setting should not have any effect for lossless codecs
  run ${__bin_path}/aio-fade out -q 5 "${testInFileAFlac}"

  [ ${status} -eq 0 ]
  [ -f "${testOutFileOutAFlac}" ]

  aio-assert-bitrate "${testOutFileOutAFlac}" 1059094 VBR
}

@test "invoke aio-fade - invalid quality" {
  aio-generate-flac-file-noise "${testInFileAFlac}"

  local invalidQuality
  for invalidQuality in "${__invalid_quality_settings[@]}"; do
    run ${__bin_path}/aio-fade out -q "${invalidQuality}" "${testInFileAFlac}"
    
    [ ${status} -eq 1 ]
    [ "${output}" == "error: value of codec-quality is invalid [${invalidQuality}] ... stopping" ]
  done
}

@test "invoke aio-fade - invalid fade-in duration" {
  aio-generate-ogg-file-noise "${testInFileA}"
  cp "${testInFileA}" "${testInFileB}"

  for invalidFadeTime in "${__invalid_fade_times[@]}"; do
    assert-invalid-in-fade-time "${invalidFadeTime}" "${testInFileA}" "${testInFileB}"
  done
}

@test "invoke aio-fade - invalid empty fade-in duration" {
  aio-generate-ogg-file-noise "${testInFileA}"
  cp "${testInFileA}" "${testInFileB}"

  run ${__bin_path}/aio-fade in --di "" "$@"
  [ ${status} -eq 1 ]
  [ "${output}" == "error: invalid fade-in duration time [null] ... stopping" ]
}

@test "invoke aio-fade - invalid fade-out duration" {
  aio-generate-ogg-file-noise "${testInFileA}"
  cp "${testInFileA}" "${testInFileB}"

  for invalidFadeTime in "${__invalid_fade_times[@]}"; do
    assert-invalid-out-fade-time "${invalidFadeTime}" "${testInFileA}" "${testInFileB}"
  done
}

@test "invoke aio-fade - invalid empty fade-out duration" {
  aio-generate-ogg-file-noise "${testInFileA}"

  run ${__bin_path}/aio-fade out --do "" "${testInFileA}"
  [ ${status} -eq 1 ]
  [ "${output}" == "error: invalid fade-out duration time [null] ... stopping" ]
}

@test "invoke aio-fade - dry-run" {
  aio-generate-ogg-file-noise "${testInFileA}"
  cp "${testInFileA}" "${testInFileB}"

  run ${__bin_path}/aio-fade --dry-run out "${testInFileA}" "${testInFileB}"

  [ ${status} -eq 0 ]
  assert-dry-run-output lines <<EOF
ffmpeg-fade-out
test-in-a.ogg
fade-out-test-in-a.ogg
curve-out : tri
dry-run : 1
duration-audio : 5.000000
duration-out : 0.01
force-overwrite : 0
prefix : fade-out
quality : 7
ffmpeg-fade-out
test-in-b.ogg
fade-out-test-in-b.ogg
curve-out : tri
dry-run : 1
duration-audio : 5.000000
duration-out : 0.01
force-overwrite : 0
prefix : fade-out
quality : 7
EOF

  run ${__bin_path}/aio-fade --dry-run in "${testInFileA}" "${testInFileB}"

  [ ${status} -eq 0 ]
  assert-dry-run-output lines <<EOF
ffmpeg-fade-in
test-in-a.ogg
fade-in-test-in-a.ogg
curve-in : tri
dry-run : 1
duration-audio : 5.000000
duration-in : 0.01
force-overwrite : 0
prefix : fade-in
quality : 7
ffmpeg-fade-in
test-in-b.ogg
fade-in-test-in-b.ogg
curve-in : tri
dry-run : 1
duration-audio : 5.000000
duration-in : 0.01
force-overwrite : 0
prefix : fade-in
quality : 7
EOF

  run ${__bin_path}/aio-fade --dry-run inout "${testInFileA}" "${testInFileB}"

  [ ${status} -eq 0 ]
  assert-dry-run-output lines <<EOF
ffmpeg-fade-inout
test-in-a.ogg
fade-inout-test-in-a.ogg
curve-in : tri
curve-out : tri
dry-run : 1
duration-audio : 5.000000
duration-in : 0.01
duration-out : 0.01
force-overwrite : 0
prefix : fade-inout
quality : 7
ffmpeg-fade-inout
test-in-b.ogg
fade-inout-test-in-b.ogg
curve-in : tri
curve-out : tri
dry-run : 1
duration-audio : 5.000000
duration-in : 0.01
duration-out : 0.01
force-overwrite : 0
prefix : fade-inout
quality : 7
EOF

}
