#!/usr/bin/env bats

# This file is part of aio-proc
#
#     aio-proc is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     aio-proc is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with aio-proc. If not, see <http://www.gnu.org/licenses/>.

###########
# TODO
#
# - use bats hooks setup_file() amd teardown_file() in order to create temporary working dirs
#

###########
# Test Data
###########

declare -g -r __ENABLE_DBG=0

declare -g -r fileTxtA="testA.txt"
declare -g -r fileTxtB="testB.txt"

declare -g -r fileA="testA.ogg"
declare -g -r fileB="testB.ogg"

declare -g -r fileAMp3="testA.mp3"
declare -g -r fileBMp3="testB.mp3"

declare -g -r fileAFlac="testA.flac"
declare -g -r fileBFlac="testB.flac"

declare -g -r fileAOpus="testA.opus"
declare -g -r fileBOpus="testB.opus"

declare -g -r fileAWav="testA.wav"
declare -g -r fileBWav="testB.wav"

# used for generic ffmpeg tags in wav ogg, opus, flac, mp3 files
declare -r -g __expected_tags_ffmpeg_generic_ffprobe_all=(
  'album bar'
  'album_artist foo-1'
  'artist foo'
  'comment this is a comment'
  'composer foo-2'
  'copyrigh copyleft'
  'creation_time 2020'
  'date 2021'
  'disc 1'
  'encoded_by who knows'
  'filename file.orig'
  'genre unittest'
  'performer bats'
  'language en'
  'publisher world'
  'service_name none'
  'service_provider none'
  'title a foo bar'
  'track 1'
  'variant_bitrate none'
)

# used for generic ffmpeg tags in wav files
declare -r -g __expected_tags_ffmpeg_generic_ffprobe_subset=(
  'album bar'
  'artist foo'
  'comment this is a comment'
  'date 2021'
  'encoded_by who knows'
  'genre unittest'
  'language en'
  'title a foo bar'
  'track 1'
)

# ffprobe changes tagnames:
# => artist
# => comment
# => encoded_by
# => track
# => genre
# => title
# => album
# => copyright
# ffmpeg does not write the following tags:
# ISFT	The software used to create the file.
declare -r -g __expected_tags_riff_ffprobe_subset=(
  'artist The artist who created this.'
  'comment A text comment.'
  'encoded_by The technician.'
  'ISRC Source: the name of the person or organization that supplied the original subject of the file.'
  'ISBJ The subject.'
  'date The creation date in YYYY-MM-DD format, however you should expect to see just YYYY in this field.'
  'track Track number.'
  'IKEY The keywords for the project or file.'
  'IENG The name(s) of the engineer. Multiple names separated by a semicolon and a blank.'
  'genre Genre of content.'
  'title Track title.'
  'album The album title.'
  'copyright The copyright information.'
)

# ffprobe changes tagnames
# - TRACKNUMBER => track
# - DESCRIPTION => comment
declare -g -r __expected_tags_vorbis_comment_ffprobe_all=(
  'TITLE a foo bar'
  'VERSION remix'
  'ALBUM bar'
  'track 1'
  'ARTIST foo'
  'PERFORMER foo-2'
  'COPYRIGHT copyleft'
  'LICENSE copyleft'
  'ORGANIZATION none'
  'comment this is a comment'
  'GENRE unittest'
  'DATE 2020'
  'LOCATION none'
  'CONTACT none'
  'ISRC 666'
)

# vorbiscomment does not change tag names
declare -g -r __expected_tags_vorbis_comment_vorbis_comment_all=(
  'TITLE a foo bar'
  'VERSION remix'
  'ALBUM bar'
  'TRACKNUMBER 1'
  'ARTIST foo'
  'PERFORMER foo-2'
  'COPYRIGHT copyleft'
  'LICENSE copyleft'
  'ORGANIZATION none'
  'DESCRIPTION this is a comment'
  'GENRE unittest'
  'DATE 2020'
  'LOCATION none'
  'CONTACT none'
  'ISRC 666'
)

# in contrast to id3v2, those tags are set by ffmpeg:
# ENCR GEOB GRID IPLS LINK MCDI MLLT OWNE PRIV
# PCNT POPM POSS RBUF RVAD RVRB SYLT SYTC UFID WXXX
#
# not set by ffmpeg:
# TLAN    [#TLAN Language(s)]
# TSSE    [#TSEE Software/Hardware and settings used for encoding]
#
# changed by ffrope show
# TALB => album
# TCOM => composer
# TCON => genre
# TCOP => copyright
# TENC => encoded_by
# TIT2 => title
# TPE1 => artist
# TPE2 => album_artist
# TPE3 => performer
# TPOS => disc
# TPUB => publisher
# TRCK => track
# USLT => lyrics
declare -r -g __expected_tags_id3v2_ffprobe_subset_ffmpeg=(
  'ENCR    [#sec4.26 Encryption method registration]'
  'GEOB    [#sec4.16 General encapsulated object]'
  'GRID    [#sec4.27 Group identification registration]'
  'IPLS    [#sec4.4 Involved people list]'
  'LINK    [#sec4.21 Linked information]'
  'MCDI    [#sec4.5 Music CD identifier]'
  'MLLT    [#sec4.7 MPEG location lookup table]'
  'OWNE    [#sec4.24 Ownership frame]'
  'PRIV    [#sec4.28 Private frame]'
  'PCNT    [#sec4.17 Play counter]'
  'POPM    [#sec4.18 Popularimeter]'
  'POSS    [#sec4.22 Position synchronisation frame]'
  'RBUF    [#sec4.19 Recommended buffer size]'
  'RVAD    [#sec4.12 Relative volume adjustment]'
  'RVRB    [#sec4.14 Reverb]'
  'SYLT    [#sec4.10 Synchronized lyric/text]'
  'SYTC    [#sec4.8 Synchronized tempo codes]'
  'UFID    [#sec4.1 Unique file identifier]'
  'WXXX    [#WXXX User defined URL link frame]'
  'COMM    [#sec4.11 Comments]'
  'album    [#TALB Album/Movie/Show title]'
  'TBPM    [#TBPM BPM (beats per minute)]'
  'composer    [#TCOM Composer]'
  'genre    [#TCON Content type]'
  'copyright    [#TCOP Copyright message]'
  'TDAT    [#TDAT Date]'
  'TDLY    [#TDLY Playlist delay]'
  'encoded_by    [#TENC Encoded by]'
  'TEXT    [#TEXT Lyricist/Text writer]'
  'TFLT    [#TFLT File type]'
  'TIME    [#TIME Time]'
  'TIT1    [#TIT1 Content group description]'
  'title    [#TIT2 Title/songname/content description]'
  'TIT3    [#TIT3 Subtitle/Description refinement]'
  'TKEY    [#TKEY Initial key]'
  'TLEN    [#TLEN Length]'
  'TMED    [#TMED Media type]'
  'TOAL    [#TOAL Original album/movie/show title]'
  'TOFN    [#TOFN Original filename]'
  'TOLY    [#TOLY Original lyricist(s)/text writer(s)]'
  'TOPE    [#TOPE Original artist(s)/performer(s)]'
  'TORY    [#TORY Original release year]'
  'TOWN    [#TOWN File owner/licensee]'
  'artist    [#TPE1 Lead performer(s)/Soloist(s)]'
  'album_artist    [#TPE2 Band/orchestra/accompaniment]'
  'performer    [#TPE3 Conductor/performer refinement]'
  'TPE4    [#TPE4 Interpreted, remixed, or otherwise modified by]'
  'disc    [#TPOS Part of a set]'
  'publisher    [#TPUB Publisher]'
  'track    [#TRCK Track number/Position in set]'
  'TRDA    [#TRDA Recording dates]'
  'TRSN    [#TRSN Internet radio station name]'
  'TRSO    [#TRSO Internet radio station owner]'
  'TSIZ    [#TSIZ Size]'
  'TSRC    [#TSRC ISRC (international standard recording code)]'
  'TYER    [#TYER Year]'
  'TXXX    [#TXXX User defined text information frame]'
  'USER    [#sec4.23 Terms of use]'
  'lyrics    [#sec4.9 Unsychronized lyric/text transcription]'
  'WCOM    [#WCOM Commercial information]'
  'WCOP    [#WCOP Copyright/Legal information]'
  'WOAF    [#WOAF Official audio file webpage]'
  'WOAR    [#WOAR Official artist/performer webpage]'
  'WOAS    [#WOAS Official audio source webpage]'
  'WORS    [#WORS Official internet radio station homepage]'
  'WPAY    [#WPAY Payment]'
  'WPUB    [#WPUB Publishers official webpage]'
)

# not set by id3v2
# ENCR    [#sec4.26 Encryption method registration]
# GEOB    [#sec4.16 General encapsulated object]
# GRID    [#sec4.27 Group identification registration]
# IPLS    [#sec4.4 Involved people list]
# LINK    [#sec4.21 Linked information]
# MCDI    [#sec4.5 Music CD identifier]
# MLLT    [#sec4.7 MPEG location lookup table]
# OWNE    [#sec4.24 Ownership frame]
# PRIV    [#sec4.28 Private frame]
# PCNT    [#sec4.17 Play counter]
# POPM    [#sec4.18 Popularimeter]
# POSS    [#sec4.22 Position synchronisation frame]
# RBUF    [#sec4.19 Recommended buffer size]
# RVAD    [#sec4.12 Relative volume adjustment]
# RVRB    [#sec4.14 Reverb]
# SYLT    [#sec4.10 Synchronized lyric/text]
# SYTC    [#sec4.8 Synchronized tempo codes]
# UFID    [#sec4.1 Unique file identifier]
# WXXX    [#WXXX User defined URL link frame]
declare -r -g __expected_tags_id3v2_subset_id3v2=(
  'COMM    [#sec4.11 Comments]'
  'TALB    [#TALB Album/Movie/Show title]'
  'TBPM    [#TBPM BPM (beats per minute)]'
  'TCOM    [#TCOM Composer]'
  'TCON    [#TCON Content type] (255)'
  'TCOP    [#TCOP Copyright message]'
  'TDAT    [#TDAT Date]'
  'TDLY    [#TDLY Playlist delay]'
  'TENC    [#TENC Encoded by]'
  'TEXT    [#TEXT Lyricist/Text writer]'
  'TFLT    [#TFLT File type]'
  'TIME    [#TIME Time]'
  'TIT1    [#TIT1 Content group description]'
  'TIT2    [#TIT2 Title/songname/content description]'
  'TIT3    [#TIT3 Subtitle/Description refinement]'
  'TKEY    [#TKEY Initial key]'
  'TLEN    [#TLEN Length]'
  'TMED    [#TMED Media type]'
  'TOAL    [#TOAL Original album/movie/show title]'
  'TOFN    [#TOFN Original filename]'
  'TOLY    [#TOLY Original lyricist(s)/text writer(s)]'
  'TOPE    [#TOPE Original artist(s)/performer(s)]'
  'TORY    [#TORY Original release year]'
  'TOWN    [#TOWN File owner/licensee]'
  'TPE1    [#TPE1 Lead performer(s)/Soloist(s)]'
  'TPE2    [#TPE2 Band/orchestra/accompaniment]'
  'TPE3    [#TPE3 Conductor/performer refinement]'
  'TPE4    [#TPE4 Interpreted, remixed, or otherwise modified by]'
  'TPOS    [#TPOS Part of a set]'
  'TPUB    [#TPUB Publisher]'
  'TRCK    [#TRCK Track number/Position in set]'
  'TRDA    [#TRDA Recording dates]'
  'TRSN    [#TRSN Internet radio station name]'
  'TRSO    [#TRSO Internet radio station owner]'
  'TSIZ    [#TSIZ Size]'
  'TSRC    [#TSRC ISRC (international standard recording code)]'
  'TYER    [#TYER Year]'
  'TXXX    [#TXXX User defined text information frame]'
  'USER    [#sec4.23 Terms of use]'
  'USLT    [#sec4.9 Unsychronized lyric/text transcription]'
  'WCOM    [#WCOM Commercial information]'
  'WCOP    [#WCOP Copyright/Legal information]'
  'WOAF    [#WOAF Official audio file webpage]'
  'WOAR    [#WOAR Official artist/performer webpage]'
  'WOAS    [#WOAS Official audio source webpage]'
  'WORS    [#WORS Official internet radio station homepage]'
  'WPAY    [#WPAY Payment]'
  'WPUB    [#WPUB Publishers official webpage]'
)

# for showing metadata via id3v2, only contains those tags
# that can be set via generic id3v2 metadata arguments
declare -r -g __expected_tags_id3v2_generic_subset_id3v2_a=(
  'TPE1 foo'
  'TALB bar'
  'TIT2 a foo bar'
  'COMM this is a comment'
  'TCON unittest (255)'
  'TYER 2020'
  'TRCK 1'
)

# for showing a subset of metadata via id3v2
declare -r -g __expected_tags_id3v2_generic_subset_id3v2_b=(
  'TALB bar'
  'COMM this is a comment'
  'TYER 1977'
)

# for showing metadata via metaflac, opusinfo, ogginfo, vorbiscomment
# when invalid vorbid-comment tags are used
declare -r -g __expected_tags_vorbis_comment_invalid_a=(
  'AALBUM bar'
  'THIS this is a comment'
  'WHAT 1977'
)

# for showing metadata via metaflac, opusinfo, ogginfo, vorbiscomment
# when invalid vorbid-comment tags are used
declare -r -g __expected_tags_vorbis_comment_invalid_b=(
  'AALBUM foo'
  'THIS this is another comment'
  'WHAT 1978'
)

# for showing metadata via metaflac, opusinfo, ogginfo, vorbiscomment
# when a valid subset of vorbid-comment tags is used
declare -r -g __expected_tags_vorbis_comment_valid_subset_a=(
  'ARTIST foo'
  'TRACKNUMBER 1'
  'ALBUM bar'
)

# for showing metadata via metaflac, opusinfo, ogginfo, vorbiscomment
# when a valid subset of vorbid-comment tags is used
declare -r -g __expected_tags_vorbis_comment_valid_subset_b=(
  'ARTIST fee'
  'TRACKNUMBER 2'
  'ALBUM ber'
)

# for showing metadata via metaflac, opusinfo, ogginfo, vorbiscomment
# when a valid subset of vorbid-comment tags is used
declare -r -g __expected_tags_vorbis_comment_valid_subset_x=(
  'DATE 1977'
  'DESCRIPTION this is a comment'
  'ALBUM bar'
)

# for showing metadata via id3v2
# when a valid subset of id3v2 tags is used
declare -r -g __expected_tags_id3v2_valid_subset_a=(
  'TPE1 foo'
  'TRCK 1'
  'TALB bar'
)

# for showing metadata via metaflac, opusinfo, ogginfo, vorbiscomment
# when a valid subset of vorbid-comment tags is used
declare -r -g __expected_tags_id3v2_valid_subset_b=(
  'TPE1 fee'
  'TRCK 2'
  'TALB ber'
)

# for showing metadata via ffprobe
# when a valid subset of generic metadata tags is used
declare -r -g __expected_tags_generic_ffmpeg_valid_subset_a=(
  'artist foo'
  'track 1'
  'album bar'
)

# for showing metadata via ffprobe
# when a valid subset of generic metadata tags is used
declare -r -g __expected_tags_generic_ffmpeg_valid_subset_b=(
  'artist fee'
  'track 2'
  'album ber'
)

###########
# Debug Tools
###########

dbg-print-metadata-tags() {
  [[ ${__ENABLE_DBG} -eq  0 ]] && return 0
  local -n tagsRef="$1"

  printf '# \n' >&3
  while read -r key; do
    printf '# tags %s %s\n' "${key}" "${tagsRef[${key}]}" >&3
  done < <(printf '%s\n' "${!tagsRef[@]}")
  return 0
}

dbg-print-metadata-ffprobe() {
  [[ ${__ENABLE_DBG} -eq  0 ]] && return 0
  printf '# \n' >&3
  while read -r line; do
    printf '# show %s\n' "${line}" >&3
  done < <(ffprobe-show-format-tags "$1")
  return 0
}

dbg-print-metadata-ogginfo() {
  [[ ${__ENABLE_DBG} -eq  0 ]] && return 0
  printf '# \n' >&3
  while read -r line; do
    printf '# ogginfo %s\n' "${line}" >&3
  done < <(ogginfo "$1")
  return 0
}

dbg-print-metadata-opusinfo() {
  [[ ${__ENABLE_DBG} -eq  0 ]] && return 0
  printf '# \n' >&3
  while read -r line; do
    printf '# opusinfo %s\n' "${line}" >&3
  done < <(opusinfo "$1")
  return 0
}

dbg-print-file-header-hex() {
  [[ ${__ENABLE_DBG} -eq  0 ]] && return 0
  printf '# \n' >&3
  while read -r line; do
    printf '# hex %s\n' "${line}" >&3
  done < <(hexdump -C "$1" | head -50)
  return 0
}

dbg-print-metadata-eyed3() {
  [[ ${__ENABLE_DBG} -eq  0 ]] && return 0
  printf '# \n' >&3
  while read -r line; do
    printf '# eyeD3 v2 %s\n' "${line}" >&3
  done < <(eyeD3 -v2 "$1")
  return 0
}

dbg-print-metadata-metaflac() {
  [[ ${__ENABLE_DBG} -eq  0 ]] && return 0
  printf '# \n' >&3
  while read -r line; do
    printf '# metaflac %s\n' "${line}" >&3
  done < <(metaflac-show-comments "$1")
  return 0
}

###########
# Helpers
###########

declare -g -r __bin_path='..'

load test-aio-common-file-generators

###########
# Tests Setup
###########

setup() {
  rm -f "${fileTxtA}"
  rm -f "${fileTxtB}"
  rm -f "${fileA}"
  rm -f "${fileB}"
  rm -f "${fileAMp3}"
  rm -f "${fileBMp3}"
  rm -f "${fileAFlac}"
  rm -f "${fileBFlac}"
  rm -f "${fileAOpus}"
  rm -f "${fileBOpus}"
  rm -f "${fileAWav}"
  rm -f "${fileBWav}"
}

teardown() {
  rm -f "${fileTxtA}"
  rm -f "${fileTxtB}"
  rm -f "${fileA}"
  rm -f "${fileB}"
  rm -f "${fileAMp3}"
  rm -f "${fileBMp3}"
  rm -f "${fileAFlac}"
  rm -f "${fileBFlac}"
  rm -f "${fileAOpus}"
  rm -f "${fileBOpus}"
  rm -f "${fileAWav}"
  rm -f "${fileBWav}"
}

###########
# Tests Tools
###########

ffprobe-show-format-tags() {
  ffprobe -show_entries format_tags "$1" | grep -E '^TAG:.*'
}

ffprobe-show-stream-tags() {
  ffprobe -show_entries stream_tags "$1" | grep -E '^TAG:.*'
}

metaflac-show-comments() {
  metaflac --list --block-type=VORBIS_COMMENT "$1" | grep -E 'comment\[[0-9]+\]'
}

metaflac-show-num-comments() {
  local c value

  c=$(metaflac --list --block-type=VORBIS_COMMENT "$1" | grep -E 'comments:[ 0-9]+')
  IFS=':' read -r _ value <<<"${c}"
  printf '%s' "${value// /}"
}

opusinfo-show-metadata() {
  local -r fin="$1"
  shift

  local -r tags="$*"
  local -r tagsRegex="(${tags// /|})"
  # regex => (TAG1|TAG1|...|TAGX)=.*
  opusinfo "${fin}" | grep -o -E "${tagsRegex}"'=.*'
}

id3v2-show-metadata() {
  id3v2 -l "$1" | tail -n +2
}

# stdin lines => key (description text): value
read-metadata-id3v2() {
  local -n tagsRef="$1"

  local line
  local key
  local value

  while read -r -t 10 line; do
    key="${line%% *}"
    value="${line##*: }"
    # printf '# kv %s %s\n' "${key}" "${value}" >&3
    tagsRef["${key}"]="${value}"
  done

  # printf '# n %s\n' "${#tagsRef[@]}"  >&3
}

# stdin lines => TAG:key=value
read-metadata-ffprobe() {
  local -n tagsRef="$1"

  local line
  local data
  local key
  local value

  while read -r -t 10 line; do
    data="${line#*:}"
    IFS='=' read -r key value <<<"${data}"
    # printf '# kv %s %s\n' "${key}" "${value}" >&3
    tags["${key}"]="${value}"
  done

  # printf '# n %s\n' "${#tagsRef[@]}"  >&3
}

# stdin lines => comment[n]: key=value
read-metadata-metaflac() {
  local -n tagsRef="$1"

  local line
  local data
  local key
  local value

  while read -r -t 10 line; do
    data="${line##*:}"
    IFS='=' read -r key value <<<"${data}"
    # printf '# kv %s %s\n' "${key}" "${value}" >&3
    tagsRef["${key// /}"]="${value}"
  done

  # printf '# n %s\n' "${#tagsRef[@]}"  >&3
}

# stdin lines => key=value
read-metadata-vorbis-comment() {
  local -n tagsRef="$1"

  local line
  local key
  local value

  while read -r -t 10 line; do
    IFS='=' read -r key value <<<"${line}"
    # printf '# kv %s %s\n' "${key}" "${value}" >&3
    tagsRef["${key// /}"]="${value}"
  done

  # printf '# t %s\n' "${!tagsRef[@]}"  >&3
}

assert-metadata() {
  local -n -r tagsRef="$1"

  local line
  local keyExpected
  local valueExpected

  while read -r -t 10 line; do
    [[ -z "${line// /}" ]] && continue
    [[ "${line}" =~ ^# ]] && continue
    IFS=' ' read -r keyExpected valueExpected <<<"${line}"
    [ "${tagsRef[${keyExpected}]}" == "${valueExpected}" ]
    # printf '# kve <%s> <%s>=<%s>\n' "${keyExpected}" "${valueExpected}" "${tagsRef[${keyExpected}]}" >&3
  done
}

###########
# Tests
###########


@test "invoke aio-metadata-generate - help" {

  run ${__bin_path}/aio-metadata-set -h
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-metadata-set --help
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-metadata-set h
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-metadata-set help
  [ ${status} -eq 0 ]
}

@test "invoke aio-metadata-set - help short" {

  run ${__bin_path}/aio-metadata-set help short
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-metadata-set help s
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-metadata-set h s
  [ ${status} -eq 0 ]
}

@test "invoke aio-metadata-set - help details" {

  run ${__bin_path}/aio-metadata-set help details
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-metadata-set help d
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-metadata-set h d
  [ ${status} -eq 0 ]
}

@test "invoke aio-metadata-set - help examples" {

  run ${__bin_path}/aio-metadata-set help examples
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-metadata-set help e
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-metadata-set h e
  [ ${status} -eq 0 ]
}

@test "invoke aio-metadata-set --version" {
  run ${__bin_path}/aio-metadata-set --version
  [ ${status} -eq 0 ]
}

@test "invoke aio-metadata-set - no cmd" {
  run ${__bin_path}/aio-metadata-set

  [ ${status} -eq 1 ]
  [ "${output}" == "error: no command provided ... stopping" ]

  run ${__bin_path}/aio-metadata-set <<EOF
  artist foo
  track 1
  album bar
EOF

  [ ${status} -eq 1 ]
  [ "${output}" == "error: no command provided ... stopping" ]
}

@test "invoke aio-metadata-set - status" {
  run ${__bin_path}/aio-metadata-set status

  [ ${status} -eq 0 ]
}

@test "invoke aio-metadata-set - count" {
  aio-generate-ogg-file-silence "${fileA}" 1
  cp "${fileA}" "${fileB}"

  run ${__bin_path}/aio-metadata-set cn test*.ogg

  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${output}" == "2" ]
}

@test "invoke aio-metadata-set - list" {
  aio-generate-ogg-file-silence "${fileA}" 1
  cp "${fileA}" "${fileB}"

  run ${__bin_path}/aio-metadata-set ls test*.ogg

  [ ${status} -eq 0 ]
  [ ${#lines[@]} == 2 ]
  [ "${lines[0]}" == "${fileA}" ]
  [ "${lines[1]}" == "${fileB}" ]
}

@test "invoke aio-metadata-set - individual - one file - ogg" {

  aio-generate-ogg-file-silence "${fileA}" 1

  run ${__bin_path}/aio-metadata-set individual "${fileA}" <<EOF
ARTIST foo
TRACKNUMBER 1
ALBUM bar
EOF

  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-vorbis-comment tags < <(vorbiscomment -l "${fileA}")

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_valid_subset_a[@]}")
}

@test "invoke aio-metadata-set - individual - one file - flac" {

  aio-generate-flac-file-sine "${fileAFlac}" 1

  run ${__bin_path}/aio-metadata-set individual "${fileAFlac}" <<EOF
ARTIST foo
TRACKNUMBER 1
ALBUM bar
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-metaflac tags < <( metaflac-show-comments "${fileAFlac}" )

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_valid_subset_a[@]}")
}

@test "invoke aio-metadata-set - individual - one file - opus" {

  aio-generate-opus-file-sine "${fileAOpus}" 1

  run ${__bin_path}/aio-metadata-set individual "${fileAOpus}" <<EOF
ARTIST foo
TRACKNUMBER 1
ALBUM bar
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-vorbis-comment tags < <(opusinfo-show-metadata "${fileAOpus}" ARTIST TRACKNUMBER ALBUM)

  [ ${#tags[*]} -eq 3 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_valid_subset_a[@]}")
}

@test "invoke aio-metadata-set - individual - one file - mp3" {

  aio-generate-mp3-file-sine "${fileAMp3}" 1

  run ${__bin_path}/aio-metadata-set individual "${fileAMp3}" <<EOF
artist foo
track 1
album bar
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-id3v2 tags < <(id3v2-show-metadata "${fileAMp3}")

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_id3v2_valid_subset_a[@]}")
}

@test "invoke aio-metadata-set - individual - one file - wav" {

  aio-generate-wav-file-sine "${fileAWav}" 1

  run ${__bin_path}/aio-metadata-set individual "${fileAWav}" <<EOF
artist foo
track 1
album bar
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-ffprobe tags < <( ffprobe-show-format-tags "${fileAWav}" )

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_generic_ffmpeg_valid_subset_a[@]}")
}

@test "invoke aio-metadata-set - individual - more files - ogg" {

  aio-generate-ogg-file-silence "${fileA}" 1
  cp "${fileA}" "${fileB}"

  run ${__bin_path}/aio-metadata-set individual "${fileA}" "${fileB}" <<EOF
ARTIST foo
TRACKNUMBER 1
ALBUM bar

ARTIST fee
TRACKNUMBER 2
ALBUM ber
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-vorbis-comment tags < <(vorbiscomment -l "${fileA}")

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_valid_subset_a[@]}")

  tags=()
  read-metadata-vorbis-comment tags < <(vorbiscomment -l "${fileB}")

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_valid_subset_b[@]}")
}

@test "invoke aio-metadata-set - individual - more files - opus" {

  aio-generate-opus-file-sine "${fileAOpus}" 1
  cp "${fileAOpus}" "${fileBOpus}"

  run ${__bin_path}/aio-metadata-set individual "${fileAOpus}" "${fileBOpus}" <<EOF
ARTIST foo
TRACKNUMBER 1
ALBUM bar

ARTIST fee
TRACKNUMBER 2
ALBUM ber
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-vorbis-comment tags < <(opusinfo-show-metadata "${fileAOpus}" ARTIST TRACKNUMBER ALBUM)

  [ ${#tags[*]} -eq 3 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_valid_subset_a[@]}")

  tags=()
  read-metadata-vorbis-comment tags < <(opusinfo-show-metadata "${fileBOpus}" ARTIST TRACKNUMBER ALBUM)

  [ ${#tags[*]} -eq 3 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_valid_subset_b[@]}")
}

@test "invoke aio-metadata-set - individual - more files - flac" {

  aio-generate-flac-file-sine "${fileAFlac}" 1
  cp "${fileAFlac}" "${fileBFlac}"

  run ${__bin_path}/aio-metadata-set individual "${fileAFlac}" "${fileBFlac}" <<EOF
ARTIST foo
TRACKNUMBER 1
ALBUM bar

ARTIST fee
TRACKNUMBER 2
ALBUM ber
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-metaflac tags < <( metaflac-show-comments "${fileAFlac}" )

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_valid_subset_a[@]}")

  tags=()
  read-metadata-metaflac tags < <( metaflac-show-comments "${fileBFlac}" )

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_valid_subset_b[@]}")
}

@test "invoke aio-metadata-set - individual - more files - mp3" {

  aio-generate-mp3-file-sine "${fileAMp3}" 1
  cp "${fileAMp3}" "${fileBMp3}"

  run ${__bin_path}/aio-metadata-set individual "${fileAMp3}" "${fileBMp3}" <<EOF
artist foo
track 1
album bar

artist fee
track 2
album ber
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-id3v2 tags < <(id3v2-show-metadata "${fileAMp3}")

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_id3v2_valid_subset_a[@]}")

  tags=()
  read-metadata-id3v2 tags < <(id3v2-show-metadata "${fileBMp3}")

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_id3v2_valid_subset_b[@]}")
}

@test "invoke aio-metadata-set - individual - more files - wav" {

  aio-generate-mp3-file-sine "${fileAWav}" 1
  cp "${fileAWav}" "${fileBWav}"

  run ${__bin_path}/aio-metadata-set individual "${fileAWav}" "${fileBWav}" <<EOF
artist foo
track 1
album bar

artist fee
track 2
album ber
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-ffprobe tags < <( ffprobe-show-format-tags "${fileAWav}" )

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_generic_ffmpeg_valid_subset_a[@]}")

  tags=()
  read-metadata-ffprobe tags < <( ffprobe-show-format-tags "${fileBWav}" )

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_generic_ffmpeg_valid_subset_b[@]}")
}

@test "invoke aio-metadata-set - individual - more files - with comments" {

  aio-generate-ogg-file-silence "${fileA}" 1
  cp "${fileA}" "${fileB}"

  local -r indentWhitespaces='      '
  local -r indentTabs="$(printf '\t\t')"

  run ${__bin_path}/aio-metadata-set individual "${fileA}" "${fileB}" <<EOF
# a single line comment
ARTIST foo
TRACKNUMBER 1
# a
${indentTabs}# multi
ALBUM bar

${indentWhitespaces}# line
${indentTabs}${indentWhitespaces}# comment
ARTIST fee
TRACKNUMBER 2
ALBUM ber
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-vorbis-comment tags < <(vorbiscomment -l "${fileA}")

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_valid_subset_a[@]}")

  tags=()
  read-metadata-vorbis-comment tags < <(vorbiscomment -l "${fileB}")

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_valid_subset_b[@]}")
}

@test "invoke aio-metadata-set - individual - more files - with empty lines" {

  aio-generate-ogg-file-silence "${fileA}" 1
  cp "${fileA}" "${fileB}"

  local -r lineWithWhitespaces='      '
  local -r lineWithTabs="$(printf '\t\t\t')"

  run ${__bin_path}/aio-metadata-set individual "${fileA}" "${fileB}" <<EOF
# single empty line

ARTIST foo
TRACKNUMBER 1
ALBUM bar
# multiple empty lines with and without whitespaces and tabs

${lineWithWhitespaces}


${lineWithTabs}
${lineWithWhitespaces}
${lineWithTabs}${lineWithWhitespaces}
ARTIST fee
TRACKNUMBER 2
ALBUM ber
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-vorbis-comment tags < <(vorbiscomment -l "${fileA}")

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_valid_subset_a[@]}")

  tags=()
  read-metadata-vorbis-comment tags < <(vorbiscomment -l "${fileB}")

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_valid_subset_b[@]}")
}

@test "invoke aio-metadata-set - individual - more files - with indented data" {

  aio-generate-ogg-file-silence "${fileA}" 1
  cp "${fileA}" "${fileB}"

  local -r indentWhitespaces='      '
  local -r indentTabs="$(printf '\t\t')"

  run ${__bin_path}/aio-metadata-set individual "${fileA}" "${fileB}" <<EOF
${indentWhitespaces}ARTIST foo
${indentTabs}TRACKNUMBER${indentWhitespaces}1
${indentWhitespaces}${indentTabs}ALBUM${indentTabs}bar

${indentTabs}ARTIST fee
${indentWhitespaces}TRACKNUMBER${indentWhitespaces}2
${indentWhitespaces}${indentTabs}ALBUM${indentTabs}ber${indentWhitespaces}
EOF

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-vorbis-comment tags < <(vorbiscomment -l "${fileA}")

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_valid_subset_a[@]}")

  tags=()
  read-metadata-vorbis-comment tags < <(vorbiscomment -l "${fileB}")

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_valid_subset_b[@]}")
}

@test "invoke aio-metadata-set - individual - no tags - ogg" {

  aio-generate-ogg-file-silence "${fileA}" 1
  cp "${fileA}" "${fileB}"

  run ${__bin_path}/aio-metadata-set individual "${fileA}" "${fileB}"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: no tags provided via stdin ... stopping' ]

  # no tags are supposed to be set on input files.
  # vorbiscomment just writes one line with version infos
  # if no metadata is available
  [ $(vorbiscomment -l "${fileA}" | wc -l) -eq 1 ]
  [ $(vorbiscomment -l "${fileB}" | wc -l) -eq 1 ]
}

@test "invoke aio-metadata-set - individual - no tags - flac" {

  aio-generate-flac-file-sine "${fileAFlac}" 1
  cp "${fileAFlac}" "${fileBFlac}"

  run ${__bin_path}/aio-metadata-set individual "${fileAFlac}" "${fileBFlac}"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: no tags provided via stdin ... stopping' ]

  # no tags are supposed to be set on input files.
  # ffmpeg just writes one tag entry with encoder infos on file creation
  [ "$(metaflac-show-num-comments ${fileAFlac})" == '1' ]
  [ "$(metaflac-show-num-comments ${fileBFlac})" == '1' ]
}

@test "invoke aio-metadata-set - individual - no tags - opus" {

  aio-generate-opus-file-sine "${fileAOpus}" 1
  cp "${fileAOpus}" "${fileBOpus}"

  run ${__bin_path}/aio-metadata-set individual "${fileAOpus}" "${fileBOpus}"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: no tags provided via stdin ... stopping' ]

  local count

  # no tags are supposed to be set on input files.
  # ffmpeg just writes one tag entry with encoder infos on file creation
  count=$( ffprobe-show-stream-tags "${fileAOpus}" | wc -l )
  [ "${count}" -eq 1 ]

  count=$( ffprobe-show-stream-tags "${fileBOpus}" | wc -l )
  [ "${count}" -eq 1 ]
}

@test "invoke aio-metadata-set - individual - no tags - mp3" {

  aio-generate-mp3-file-sine "${fileAMp3}" 1
  cp "${fileAMp3}" "${fileBMp3}"

  run ${__bin_path}/aio-metadata-set individual "${fileAMp3}" "${fileBMp3}"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: no tags provided via stdin ... stopping' ]

  [ "$(id3v2 -l "${fileAMp3}")" == "${fileAMp3}: No ID3 tag" ]
  [ "$(id3v2 -l "${fileBMp3}")" == "${fileBMp3}: No ID3 tag" ]
}

@test "invoke aio-metadata-set - individual - no tags - wav" {

  aio-generate-wav-file-sine "${fileAWav}" 1
  cp "${fileAWav}" "${fileBWav}"

  run ${__bin_path}/aio-metadata-set individual "${fileAWav}" "${fileBWav}"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: no tags provided via stdin ... stopping' ]

  local count

  # no tags are supposed to be set on input files.
  # ffmpeg just writes one tag entry with encoder infos on file creation
  count=$( ffprobe-show-format-tags "${fileAWav}" | wc -l )
  [ "${count}" -eq 1 ]

  count=$( ffprobe-show-format-tags "${fileBWav}" | wc -l )
  [ "${count}" -eq 1 ]
}

@test "invoke aio-metadata-set - individual - no tags - read timeout" {

  aio-generate-ogg-file-silence "${fileA}" 1
  cp "${fileA}" "${fileB}"

  run ${__bin_path}/aio-metadata-set individual "${fileA}" "${fileB}" < <(sleep 2; printf 'TRACKNUMBER 01\n\nTRACKNUMBER 02')

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: no tags provided via stdin ... stopping' ]

  # no tags are supposed to be set on input files.
  # vorbiscomment just writes one line with version infos
  # if no metadata is available
  [ $(vorbiscomment -l "${fileA}" | wc -l) -eq 1 ]
  [ $(vorbiscomment -l "${fileB}" | wc -l) -eq 1 ]
}

@test "invoke aio-metadata-set - individual - no tags - empty lines only" {

  aio-generate-ogg-file-silence "${fileA}" 1
  cp "${fileA}" "${fileB}"

  local -r lineWithWhitespaces='      '
  local -r lineWithTabs="$(printf '\t\t\t')"

  run ${__bin_path}/aio-metadata-set individual "${fileA}" "${fileB}" <<EOF

${lineWithWhitespaces}
${lineWithTabs}
${lineWithWhitespaces}${lineWithTabs}
EOF

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: no tags provided via stdin ... stopping' ]

  # no tags are supposed to be set on input files.
  # vorbiscomment just writes one line with version infos
  # if no metadata is available
  [ $(vorbiscomment -l "${fileA}" | wc -l) -eq 1 ]
  [ $(vorbiscomment -l "${fileB}" | wc -l) -eq 1 ]
}

@test "invoke aio-metadata-set - individual - invalid tags - ogg" {

  aio-generate-ogg-file-silence "${fileA}" 1
  cp "${fileA}" "${fileB}"

  run ${__bin_path}/aio-metadata-set individual "${fileA}" "${fileB}" <<EOF
THIS this is a comment
WHAT 1977
AALBUM bar

AALBUM foo
THIS this is another comment
WHAT 1978
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-vorbis-comment tags < <(vorbiscomment -l "${fileA}")

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_invalid_a[@]}")

  tags=()
  read-metadata-vorbis-comment tags < <(vorbiscomment -l "${fileB}")

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_invalid_b[@]}")
}

@test "invoke aio-metadata-set - individual - invalid tags - flac" {
  aio-generate-flac-file-sine "${fileAFlac}" 1
  cp "${fileAFlac}" "${fileBFlac}"

  # NOTE invalid tagname wont produce errors when using flac files(metaflac)
  #  and will be written to file
  run ${__bin_path}/aio-metadata-set individual "${fileAFlac}" "${fileBFlac}" <<EOF
  AALBUM bar
  THIS this is a comment
  WHAT 1977

  AALBUM foo
  THIS this is another comment
  WHAT 1978
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-metaflac tags < <( metaflac-show-comments "${fileAFlac}" )

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_invalid_a[@]}")

  tags=()
  read-metadata-metaflac tags < <( metaflac-show-comments "${fileBFlac}" )

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_invalid_b[@]}")
}

@test "invoke aio-metadata-set - individual - invalid tags - opus" {

  aio-generate-opus-file-sine "${fileAOpus}" 1
  cp "${fileAOpus}" "${fileBOpus}"

  # NOTE invalid tagname wont produce errors when using opus files(ffmpeg)
  #  and will be written to file
  run ${__bin_path}/aio-metadata-set individual "${fileAOpus}" "${fileBOpus}" <<EOF
  AALBUM bar
  THIS this is a comment
  WHAT 1977

  AALBUM bar
  THIS this is a comment
  WHAT 1977
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-vorbis-comment tags < <(opusinfo-show-metadata "${fileAOpus}" AALBUM THIS WHAT)

  [ ${#tags[*]} -eq 3 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_invalid_a[@]}")

  tags=()
  read-metadata-vorbis-comment tags < <(opusinfo-show-metadata "${fileBOpus}" AALBUM THIS WHAT)

  [ ${#tags[*]} -eq 3 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_invalid_a[@]}")
}

@test "invoke aio-metadata-set - individual - invalid tags - mp3" {

  aio-generate-mp3-file-sine "${fileAMp3}" 1
  cp "${fileAMp3}" "${fileBMp3}"

  run ${__bin_path}/aio-metadata-set common "${fileAMp3}" "${fileBMp3}" <<EOF
aalbum bar
comment this is a comment
year 1977

album ber
this this is a comment
year 1977
EOF

  [ ${status} -eq 1 ]
  [ ${#lines[*]} -eq 2 ]

  [ "${lines[0]}" == "error: could no set tags on file [${fileAMp3}] ... skipping" ]
  [ "${lines[1]}" == "error: could no set tags on file [${fileBMp3}] ... skipping" ]

  # no tags are supposed to be set on valid file
  [ "$(id3v2 -l "${fileAMp3}")" == "${fileAMp3}: No ID3 tag" ]
  [ "$(id3v2 -l "${fileBMp3}")" == "${fileBMp3}: No ID3 tag" ]
}

@test "invoke aio-metadata-set - individual - invalid tags - wav" {

  aio-generate-wav-file-sine "${fileAWav}" 1
  cp "${fileAWav}" "${fileBWav}"

  # invalid tags do not cause failure but is also not set
  run ${__bin_path}/aio-metadata-set individual "${fileAWav}" "${fileBWav}" <<EOF
IINAM	Track title.
IIPRD	The album title.
IIART	The artist who created this.

IINAM	Track title.
IIPRD	The album title.
IIART	The artist who created this.
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-ffprobe tags < <( ffprobe-show-format-tags "${fileAWav}" )

  [ ${#tags[*]} -eq 1 ]

  tags=()
  read-metadata-ffprobe tags < <( ffprobe-show-format-tags "${fileBWav}" )

  [ ${#tags[*]} -eq 1 ]
}

@test "invoke aio-metadata-set - individual - no files" {
  run ${__bin_path}/aio-metadata-set individual <<EOF
ARTIST foo
TRACKNUMBER 1
ALBUM bar

ARTIST fee
TRACKNUMBER 2
ALBUM ber
EOF

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: no files provided ... stopping' ]
}

@test "invoke aio-metadata-set - individual - unsupported files" {
  touch "${fileTxtA}"
  touch "${fileTxtB}"

  run ${__bin_path}/aio-metadata-set individual "${fileTxtA}" "${fileTxtB}" <<EOF
ARTIST foo
TRACKNUMBER 1
ALBUM bar

ARTIST fee
TRACKNUMBER 2
ALBUM ber
EOF

  [ ${status} -eq 1 ]

  [ "${lines[0]}" == "error: file [${fileTxtA}] format [txt] not supported ... skipping" ]
  [ "${lines[1]}" == "error: file [${fileTxtB}] format [txt] not supported ... skipping" ]
}

@test "invoke aio-metadata-set - individual - some files missing" {
  touch "${fileA}"

  run ${__bin_path}/aio-metadata-set individual "${fileA}" dummyFileB <<EOF
ARTIST foo
TRACKNUMBER 1
ALBUM bar

ARTIST fee
TRACKNUMBER 2
ALBUM ber
EOF

  [ ${status} -eq 1 ]
  [ ${#lines[*]} -eq 1 ]

  [ "${lines[0]}" == 'error: file [dummyFileB] does not exist ... stopping' ]
}

@test "invoke aio-metadata-set - individual - all files missing" {
  run ${__bin_path}/aio-metadata-set individual dummyFileA dummyFileB <<EOF
ARTIST foo
TRACKNUMBER 1
ALBUM bar

ARTIST fee
TRACKNUMBER 2
ALBUM ber
EOF

  [ ${status} -eq 1 ]
  [ ${#lines[*]} -eq 2 ]

  [ "${lines[0]}" == 'error: file [dummyFileA] does not exist ... stopping' ]
  [ "${lines[1]}" == 'error: file [dummyFileB] does not exist ... stopping' ]
}

@test "invoke aio-metadata-set - common - one file - ogg" {

  aio-generate-ogg-file-silence "${fileA}" 1

  run ${__bin_path}/aio-metadata-set common "${fileA}" <<EOF
ALBUM bar
DESCRIPTION this is a comment
DATE 1977
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-vorbis-comment tags < <(vorbiscomment -l "${fileA}")

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_valid_subset_x[@]}")
}

@test "invoke aio-metadata-set - common - one file - opus" {

  aio-generate-opus-file-sine "${fileAOpus}" 1

  run ${__bin_path}/aio-metadata-set common "${fileAOpus}" <<EOF
ALBUM bar
DESCRIPTION this is a comment
DATE 1977
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-vorbis-comment tags < <(opusinfo-show-metadata "${fileAOpus}" ALBUM DESCRIPTION DATE)

  [ ${#tags[*]} -eq 3 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_valid_subset_x[@]}")
}

@test "invoke aio-metadata-set - common - one file - flac" {

  aio-generate-flac-file-sine "${fileAFlac}" 1

  run ${__bin_path}/aio-metadata-set common "${fileAFlac}" <<EOF
ALBUM bar
DESCRIPTION this is a comment
DATE 1977
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-metaflac tags < <( metaflac-show-comments "${fileAFlac}" )

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_valid_subset_x[@]}")
}

@test "invoke aio-metadata-set - common - one file - mp3" {

  aio-generate-mp3-file-sine "${fileAMp3}" 1

  run ${__bin_path}/aio-metadata-set common "${fileAMp3}" <<EOF
album bar
comment this is a comment
year 1977
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-id3v2 tags < <(id3v2-show-metadata "${fileAMp3}")

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_id3v2_generic_subset_id3v2_b[@]}")
}

@test "invoke aio-metadata-set - common - more files - ogg" {

  aio-generate-ogg-file-silence "${fileA}" 1
  cp "${fileA}" "${fileB}"

  run ${__bin_path}/aio-metadata-set common "${fileA}" "${fileB}" <<EOF
ALBUM bar
DESCRIPTION this is a comment
DATE 1977
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-vorbis-comment tags < <(vorbiscomment -l "${fileA}")

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_valid_subset_x[@]}")

  tags=()
  read-metadata-vorbis-comment tags < <(vorbiscomment -l "${fileB}")

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_valid_subset_x[@]}")
}

@test "invoke aio-metadata-set - common - more files - opus" {

  aio-generate-opus-file-sine "${fileAOpus}" 1
  cp "${fileAOpus}" "${fileBOpus}"

  run ${__bin_path}/aio-metadata-set common "${fileAOpus}" "${fileBOpus}" <<EOF
ALBUM bar
DESCRIPTION this is a comment
DATE 1977
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-vorbis-comment tags < <(opusinfo-show-metadata "${fileAOpus}" ALBUM DESCRIPTION DATE)

  [ ${#tags[*]} -eq 3 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_valid_subset_x[@]}")

  tags=()
  read-metadata-vorbis-comment tags < <(opusinfo-show-metadata "${fileBOpus}" ALBUM DESCRIPTION DATE)

  [ ${#tags[*]} -eq 3 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_valid_subset_x[@]}")
}

@test "invoke aio-metadata-set - common - more files - flac" {

  aio-generate-flac-file-sine "${fileAFlac}" 1
  cp "${fileAFlac}" "${fileBFlac}"

  run ${__bin_path}/aio-metadata-set common "${fileAFlac}" "${fileBFlac}" <<EOF
ALBUM bar
DESCRIPTION this is a comment
DATE 1977
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-metaflac tags < <( metaflac-show-comments "${fileAFlac}" )

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_valid_subset_x[@]}")

  tags=()
  read-metadata-metaflac tags < <( metaflac-show-comments "${fileBFlac}" )

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_valid_subset_x[@]}")
}

@test "invoke aio-metadata-set - common - more files - mp3" {

  aio-generate-mp3-file-sine "${fileAMp3}" 1
  cp "${fileAMp3}" "${fileBMp3}"

  run ${__bin_path}/aio-metadata-set common "${fileAMp3}" "${fileBMp3}" <<EOF
album bar
comment this is a comment
year 1977
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-id3v2 tags < <(id3v2-show-metadata "${fileAMp3}")

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_id3v2_generic_subset_id3v2_b[@]}")

  tags=()
  read-metadata-id3v2 tags < <(id3v2-show-metadata "${fileBMp3}")

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_id3v2_generic_subset_id3v2_b[@]}")
}

@test "invoke aio-metadata-set - common - more files - with comments" {

  aio-generate-ogg-file-silence "${fileA}" 1
  cp "${fileA}" "${fileB}"

  local -r indentWhitespaces='      '
  local -r indentTabs="$(printf '\t\t')"

  run ${__bin_path}/aio-metadata-set common "${fileA}" "${fileB}" <<EOF
# a single line comment
ALBUM bar
# a
${indentTabs}# multi
DESCRIPTION this is a comment
${indentWhitespaces}# line
${indentTabs}${indentWhitespaces}# comment
DATE 1977
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-vorbis-comment tags < <(vorbiscomment -l "${fileA}")

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_valid_subset_x[@]}")

  tags=()
  read-metadata-vorbis-comment tags < <(vorbiscomment -l "${fileB}")

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_valid_subset_x[@]}")
}

@test "invoke aio-metadata-set - common - more files - with empty lines" {

  aio-generate-ogg-file-silence "${fileA}" 1
  cp "${fileA}" "${fileB}"

  local -r lineWithWhitespaces='      '
  local -r lineWithTabs="$(printf '\t\t\t')"

  run ${__bin_path}/aio-metadata-set common "${fileA}" "${fileB}" <<EOF
# single empty line

ALBUM bar
# multiple empty lines with and without whitespaces and tabs

${lineWithWhitespaces}


${lineWithTabs}
${lineWithWhitespaces}
${lineWithTabs}${lineWithWhitespaces}

DESCRIPTION this is a comment
DATE 1977
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-vorbis-comment tags < <(vorbiscomment -l "${fileA}")

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_valid_subset_x[@]}")

  tags=()
  read-metadata-vorbis-comment tags < <(vorbiscomment -l "${fileB}")

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_valid_subset_x[@]}")
}

@test "invoke aio-metadata-set - common - more files - with indented data" {

  aio-generate-ogg-file-silence "${fileA}" 1
  cp "${fileA}" "${fileB}"

  local -r indentWhitespaces='      '
  local -r indentTabs="$(printf '\t\t')"

  run ${__bin_path}/aio-metadata-set common "${fileA}" "${fileB}" <<EOF
${indentWhitespaces}ALBUM bar
${indentTabs}DESCRIPTION${indentWhitespaces}this is a comment
${indentWhitespaces}${indentTabs}DATE${indentTabs}1977
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-vorbis-comment tags < <(vorbiscomment -l "${fileA}")

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_valid_subset_x[@]}")

  tags=()
  read-metadata-vorbis-comment tags < <(vorbiscomment -l "${fileB}")

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_valid_subset_x[@]}")
}

@test "invoke aio-metadata-set - common - no tags - ogg" {

  aio-generate-ogg-file-silence "${fileA}" 1
  cp "${fileA}" "${fileB}"

  run ${__bin_path}/aio-metadata-set common "${fileA}" "${fileB}"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: no tags provided via stdin ... stopping' ]

  # no tags are supposed to be set on input files.
  # vorbiscomment just writes one line with version infos
  # if no metadata is available
  [ $(vorbiscomment -l "${fileA}" | wc -l) -eq 1 ]
  [ $(vorbiscomment -l "${fileB}" | wc -l) -eq 1 ]
}

@test "invoke aio-metadata-set - common - no tags - flac" {

  aio-generate-flac-file-sine "${fileAFlac}" 1
  cp "${fileAFlac}" "${fileBFlac}"

  run ${__bin_path}/aio-metadata-set common "${fileAFlac}" "${fileBFlac}"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: no tags provided via stdin ... stopping' ]

  # no tags are supposed to be set on input files.
  # ffmpeg just writes one tag entry with encoder infos on file creation
  [ "$(metaflac-show-num-comments ${fileAFlac})" == '1' ]
  [ "$(metaflac-show-num-comments ${fileBFlac})" == '1' ]
}

@test "invoke aio-metadata-set - common - no tags - opus" {

  aio-generate-opus-file-sine "${fileAOpus}" 1
  cp "${fileAOpus}" "${fileBOpus}"

  run ${__bin_path}/aio-metadata-set common "${fileAOpus}" "${fileBOpus}"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: no tags provided via stdin ... stopping' ]

  local count

  # no tags are supposed to be set on input files.
  # ffmpeg just writes one tag entry with encoder infos on file creation
  count=$( ffprobe-show-stream-tags "${fileAOpus}" | wc -l )
  [ "${count}" -eq 1 ]

  count=$( ffprobe-show-stream-tags "${fileBOpus}" | wc -l )
  [ "${count}" -eq 1 ]
}

@test "invoke aio-metadata-set - common - no tags - mp3" {

  aio-generate-mp3-file-sine "${fileAMp3}" 1
  cp "${fileAMp3}" "${fileBMp3}"

  run ${__bin_path}/aio-metadata-set common "${fileAMp3}" "${fileBMp3}"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: no tags provided via stdin ... stopping' ]

  # no tags are supposed to be set on files
  [ "$(id3v2 -l "${fileAMp3}")" == "${fileAMp3}: No ID3 tag" ]
  [ "$(id3v2 -l "${fileBMp3}")" == "${fileBMp3}: No ID3 tag" ]
}

@test "invoke aio-metadata-set - common - no tags - read timeout" {

  aio-generate-ogg-file-silence "${fileA}" 1
  cp "${fileA}" "${fileB}"

  run ${__bin_path}/aio-metadata-set common "${fileA}" "${fileB}" < <(sleep 1; printf 'ALBUM foobar')

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: no tags provided via stdin ... stopping' ]

  # no tags are supposed to be set on input files.
  # vorbiscomment just writes one line with version infos
  # if no metadata is available
  [ $(vorbiscomment -l "${fileA}" | wc -l) -eq 1 ]
  [ $(vorbiscomment -l "${fileB}" | wc -l) -eq 1 ]
}

@test "invoke aio-metadata-set - common - no tags - empty lines only" {

  aio-generate-ogg-file-silence "${fileA}" 1
  cp "${fileA}" "${fileB}"

  local -r lineWithWhitespaces='      '
  local -r lineWithTabs="$(printf '\t\t\t')"

  run ${__bin_path}/aio-metadata-set common "${fileA}" "${fileB}" <<EOF

${lineWithWhitespaces}
${lineWithTabs}
${lineWithWhitespaces}${lineWithTabs}
EOF

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: no tags provided via stdin ... stopping' ]

  # no tags are supposed to be set on input files.
  # vorbiscomment just writes one line with version infos
  # if no metadata is available
  [ $(vorbiscomment -l "${fileA}" | wc -l) -eq 1 ]
  [ $(vorbiscomment -l "${fileB}" | wc -l) -eq 1 ]
}

@test "invoke aio-metadata-set - common - invalid tags - ogg" {
  aio-generate-ogg-file-silence "${fileA}" 1
  cp "${fileA}" "${fileB}"

  # NOTE invalid tagname wont produce errors when using ogg files(vorbiscomment)
  #  and will be written to file
  run ${__bin_path}/aio-metadata-set common "${fileA}" "${fileB}" <<EOF
  AALBUM bar
  THIS this is a comment
  WHAT 1977
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-vorbis-comment tags < <(vorbiscomment -l "${fileA}")

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_invalid_a[@]}")

  tags=()
  read-metadata-vorbis-comment tags < <(vorbiscomment -l "${fileB}")

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_invalid_a[@]}")
}

@test "invoke aio-metadata-set - common - invalid tags - flac" {
  aio-generate-flac-file-sine "${fileAFlac}" 1
  cp "${fileAFlac}" "${fileBFlac}"

  # NOTE invalid tagname wont produce errors when using flac files(metaflac)
  #  and will be written to file
  run ${__bin_path}/aio-metadata-set common "${fileAFlac}" "${fileBFlac}" <<EOF
  AALBUM bar
  THIS this is a comment
  WHAT 1977
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-metaflac tags < <( metaflac-show-comments "${fileAFlac}" )

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_invalid_a[@]}")

  tags=()
  read-metadata-metaflac tags < <( metaflac-show-comments "${fileBFlac}" )

  [ ${#tags[*]} -eq 4 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_invalid_a[@]}")
}

@test "invoke aio-metadata-set - common - invalid tags - opus" {

  aio-generate-opus-file-sine "${fileAOpus}" 1
  cp "${fileAOpus}" "${fileBOpus}"

  # NOTE invalid tagname wont produce errors when using opus files(ffmpeg)
  #  and will be written to file
  run ${__bin_path}/aio-metadata-set common "${fileAOpus}" "${fileBOpus}" <<EOF
  AALBUM bar
  THIS this is a comment
  WHAT 1977
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-vorbis-comment tags < <(opusinfo-show-metadata "${fileAOpus}" AALBUM THIS WHAT)

  [ ${#tags[*]} -eq 3 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_invalid_a[@]}")

  tags=()
  read-metadata-vorbis-comment tags < <(opusinfo-show-metadata "${fileAOpus}" AALBUM THIS WHAT)

  [ ${#tags[*]} -eq 3 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_invalid_a[@]}")
}

@test "invoke aio-metadata-set - common - invalid tags - mp3" {
  aio-generate-mp3-file-sine "${fileAMp3}" 1
  cp "${fileAMp3}" "${fileBMp3}"

  run ${__bin_path}/aio-metadata-set common "${fileAMp3}" "${fileBMp3}" <<EOF
  aalbum bar
  comment this is a comment
  year 1977
EOF

  [ ${status} -eq 1 ]
  [ ${#lines[*]} -eq 2 ]

  [ "$(cut -d ':' -f1-2 <<<"${lines[0]}")" == "error: could no set tags on file [${fileAMp3}] ... skipping" ]
  [ "$(cut -d ':' -f1-2 <<<"${lines[1]}")" == "error: could no set tags on file [${fileBMp3}] ... skipping" ]

  # no tags are supposed to be set on valid file
  [ "$(id3v2 -l "${fileAMp3}")" == "${fileAMp3}: No ID3 tag" ]
  [ "$(id3v2 -l "${fileBMp3}")" == "${fileBMp3}: No ID3 tag" ]
}

@test "invoke aio-metadata-set - common - invalid tags - wav" {
  aio-generate-wav-file-sine "${fileAWav}" 1
  cp "${fileAWav}" "${fileBWav}"

  # invalid tags do not cause failure but is also not set
  run ${__bin_path}/aio-metadata-set individual "${fileAWav}" "${fileBWav}" <<EOF
IINAM	Track title.
IIPRD	The album title.
IIART	The artist who created this.
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-ffprobe tags < <( ffprobe-show-format-tags "${fileAWav}" )

  [ ${#tags[*]} -eq 1 ]

  tags=()
  read-metadata-ffprobe tags < <( ffprobe-show-format-tags "${fileBWav}" )

  [ ${#tags[*]} -eq 1 ]
}

@test "invoke aio-metadata-set - common - no files" {
  run ${__bin_path}/aio-metadata-set common <<EOF
  album bar
  comment this is a comment
  year 1977
EOF

  [ ${status} -eq 1 ]

  [ "${output}" == 'error: no files provided ... stopping' ]
}

@test "invoke aio-metadata-set - common - unsupported files" {
  touch "${fileTxtA}"
  touch "${fileTxtB}"

  run ${__bin_path}/aio-metadata-set common "${fileTxtA}" "${fileTxtB}" <<EOF
  album bar
  comment this is a comment
  year 1977
EOF

  [ ${status} -eq 1 ]

  [ "${lines[0]}" == "error: file [${fileTxtA}] format [txt] not supported ... skipping" ]
  [ "${lines[1]}" == "error: file [${fileTxtB}] format [txt] not supported ... skipping" ]
}

@test "invoke aio-metadata-set - common - some files missing" {
  touch "${fileA}"

  run ${__bin_path}/aio-metadata-set common "${fileA}" dummyFileB <<EOF
  ALBUM bar
  DESCRIPTION this is a comment
  DATE 1977
EOF

  [ ${status} -eq 1 ]
  [ ${#lines[*]} -eq 1 ]

  [ "${lines[0]}" == 'error: file [dummyFileB] does not exist ... stopping' ]
}

@test "invoke aio-metadata-set - common - all files missing" {
  run ${__bin_path}/aio-metadata-set common dummyFileA dummyFileB <<EOF
  ALBUM bar
  DESCRIPTION this is a comment
  DATE 1977
EOF

  [ ${status} -eq 1 ]
  [ ${#lines[*]} -eq 2 ]

  [ "${lines[0]}" == 'error: file [dummyFileA] does not exist ... stopping' ]
  [ "${lines[1]}" == 'error: file [dummyFileB] does not exist ... stopping' ]
}

@test "invoke aio-metadata-set - generic tags - ffmpeg - wav" {

  aio-generate-wav-file-sine "${fileAWav}" 1

  # all generic tag names are taken from ffmpeg/avformat.h
  # NOTE not all of them are set in wav files
  run ${__bin_path}/aio-metadata-set individual "${fileAWav}" <<EOF
album bar
album_artist foo-1
artist foo
comment this is a comment
composer foo-2
copyrigh copyleft
creation_time 2020
date 2021
disc 1
encoder none
encoded_by who knows
filename file.orig
genre unittest
language en
performer bats
publisher world
service_name none
service_provider none
title a foo bar
track 1
variant_bitrate none
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-ffprobe tags < <( ffprobe-show-format-tags "${fileAWav}" )

  [ ${#tags[*]} -eq 10 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_ffmpeg_generic_ffprobe_subset[@]}")

  dbg-print-metadata-tags tags
  dbg-print-file-header-hex "${fileAWav}"
}

@test "invoke aio-metadata-set - generic tags - ffmpeg - opus" {

  aio-generate-opus-file-sine "${fileAOpus}" 1

  # all generic tag names are taken from ffmpeg/avformat.h
  # NOTE all of them are set in opus files
  run ${__bin_path}/aio-metadata-set individual "${fileAOpus}" <<EOF
album bar
album_artist foo-1
artist foo
comment this is a comment
composer foo-2
copyrigh copyleft
creation_time 2020
date 2021
disc 1
encoder none
encoded_by who knows
filename file.orig
genre unittest
language en
performer bats
publisher world
service_name none
service_provider none
title a foo bar
track 1
variant_bitrate none
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-ffprobe tags < <( ffprobe-show-stream-tags "${fileAOpus}" )

  [ ${#tags[*]} -eq 21 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_ffmpeg_generic_ffprobe_all[@]}")

  dbg-print-metadata-tags tags
  dbg-print-metadata-opusinfo "${fileAOpus}"
  dbg-print-file-header-hex "${fileAOpus}"
}

@test "invoke aio-metadata-set - generic tags - ffmpeg - ogg" {

  aio-generate-ogg-file-sine "${fileA}" 1

  # all generic tag names are taken from ffmpeg/avformat.h
  # NOTE all of them are set in ogg files
  run ${__bin_path}/aio-metadata-set --fff individual "${fileA}" <<EOF
album bar
album_artist foo-1
artist foo
comment this is a comment
composer foo-2
copyrigh copyleft
creation_time 2020
date 2021
disc 1
encoder none
encoded_by who knows
filename file.orig
genre unittest
language en
performer bats
publisher world
service_name none
service_provider none
title a foo bar
track 1
variant_bitrate none
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-ffprobe tags < <( ffprobe-show-stream-tags "${fileA}" )

  [ ${#tags[*]} -eq 21 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_ffmpeg_generic_ffprobe_all[@]}")

  dbg-print-metadata-tags tags
  dbg-print-metadata-ogginfo "${fileA}"
  dbg-print-file-header-hex "${fileA}"
}

@test "invoke aio-metadata-set - generic tags - ffmpeg - flac" {

  aio-generate-flac-file-sine "${fileAFlac}" 1

  # all generic tag names are taken from ffmpeg/avformat.h
  # NOTE all of them are set in ogg files
  run ${__bin_path}/aio-metadata-set --fff individual "${fileAFlac}" <<EOF
album bar
album_artist foo-1
artist foo
comment this is a comment
composer foo-2
copyrigh copyleft
creation_time 2020
date 2021
disc 1
encoder none
encoded_by who knows
filename file.orig
genre unittest
language en
performer bats
publisher world
service_name none
service_provider none
title a foo bar
track 1
variant_bitrate none
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-ffprobe tags < <( ffprobe-show-format-tags "${fileAFlac}" )

  [ ${#tags[*]} -eq 21 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_ffmpeg_generic_ffprobe_all[@]}")

  dbg-print-metadata-tags tags
  dbg-print-metadata-metaflac "${fileAFlac}"
  dbg-print-file-header-hex "${fileAFlac}"
}

@test "invoke aio-metadata-set - generic tags - ffmpeg - mp3" {

  aio-generate-mp3-file-sine "${fileAMp3}" 1

  # all generic tag names are taken from ffmpeg/avformat.h
  # NOTE all of them are set in ogg files
  run ${__bin_path}/aio-metadata-set --fff individual "${fileAMp3}" <<EOF
album bar
album_artist foo-1
artist foo
comment this is a comment
composer foo-2
copyrigh copyleft
creation_time 2020
date 2021
disc 1
encoder none
encoded_by who knows
filename file.orig
genre unittest
language en
performer bats
publisher world
service_name none
service_provider none
title a foo bar
track 1
variant_bitrate none
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-ffprobe tags < <( ffprobe-show-format-tags "${fileAMp3}" )

  [ ${#tags[*]} -eq 21 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_ffmpeg_generic_ffprobe_all[@]}")

  dbg-print-metadata-tags tags
  dbg-print-metadata-eyed3 "${fileAMp3}"
  dbg-print-file-header-hex "${fileAMp3}"
}

@test "invoke aio-metadata-set - generic tags - id3v2 - mp3" {

  aio-generate-mp3-file-sine "${fileAMp3}" 1

  # all generic tag names are taken from id3v2 tool
  run ${__bin_path}/aio-metadata-set individual "${fileAMp3}" <<EOF
artist foo
album bar
song a foo bar
comment this is a comment
genre unittest
year 2020
track 1
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-id3v2 tags < <(id3v2-show-metadata "${fileAMp3}")

  [ ${#tags[*]} -eq 8 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_id3v2_generic_subset_id3v2_a[@]}")
}

@test "invoke aio-metadata-set - vorbis tags - ffmpeg - opus" {

  aio-generate-opus-file-sine "${fileAOpus}" 1

  # all vorbis comment tags are taken from https://www.xiph.org/vorbis/doc/v-comment.html
  run ${__bin_path}/aio-metadata-set individual "${fileAOpus}" <<EOF
TITLE a foo bar
VERSION remix
ALBUM bar
TRACKNUMBER 1
ARTIST foo
PERFORMER foo-2
COPYRIGHT copyleft
LICENSE copyleft
ORGANIZATION none
DESCRIPTION this is a comment
GENRE unittest
DATE 2020
LOCATION none
CONTACT none
ISRC 666
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-ffprobe tags < <( ffprobe-show-stream-tags "${fileAOpus}" )

  # NOTE ffmpeg puts an additional 'encoder' tag on file generation
  [ ${#tags[*]} -eq 16 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_ffprobe_all[@]}")

  dbg-print-metadata-tags tags
  dbg-print-metadata-opusinfo "${fileAOpus}"
  dbg-print-file-header-hex "${fileAOpus}"
}

@test "invoke aio-metadata-set - vorbis tags - metaflac - flac" {

  aio-generate-flac-file-sine "${fileAFlac}" 1

  # all vorbis comment tags are taken from https://www.xiph.org/vorbis/doc/v-comment.html
  run ${__bin_path}/aio-metadata-set individual "${fileAFlac}" <<EOF
TITLE a foo bar
VERSION remix
ALBUM bar
TRACKNUMBER 1
ARTIST foo
PERFORMER foo-2
COPYRIGHT copyleft
LICENSE copyleft
ORGANIZATION none
DESCRIPTION this is a comment
GENRE unittest
DATE 2020
LOCATION none
CONTACT none
ISRC 666
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-metaflac tags < <( metaflac-show-comments "${fileAFlac}" )

  # NOTE ffmpeg puts an additional 'encoder' tag on file creation
  [ ${#tags[*]} -eq 16 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_vorbis_comment_all[@]}")

  dbg-print-metadata-tags tags
  dbg-print-file-header-hex "${fileAFlac}"
}

@test "invoke aio-metadata-set - vorbis tags - vorbiscomment - ogg" {

  aio-generate-ogg-file-sine "${fileA}" 1

  # all vorbis comment tags are taken from https://www.xiph.org/vorbis/doc/v-comment.html
  run ${__bin_path}/aio-metadata-set individual "${fileA}" <<EOF
TITLE a foo bar
VERSION remix
ALBUM bar
TRACKNUMBER 1
ARTIST foo
PERFORMER foo-2
COPYRIGHT copyleft
LICENSE copyleft
ORGANIZATION none
DESCRIPTION this is a comment
GENRE unittest
DATE 2020
LOCATION none
CONTACT none
ISRC 666
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-vorbis-comment tags < <( vorbiscomment -l "${fileA}" )

  # NOTE ffmpeg puts an additional 'encoder' tag on file creation
  [ ${#tags[*]} -eq 16 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_vorbis_comment_all[@]}")

  dbg-print-metadata-tags tags
  dbg-print-metadata-ogginfo "${fileA}"
  dbg-print-file-header-hex "${fileA}"
}

@test "invoke aio-metadata-set - id3v2 tags - id3v2 - mp3" {

  aio-generate-mp3-file-sine "${fileAMp3}" 1

  # all id3v2 tags are taken from https://id3.org/id3v2.3.0#Declared_ID3v2_frames
  run ${__bin_path}/aio-metadata-set individual "${fileAMp3}" <<EOF
# accepted by id3v2 but not implemented
# AENC    [[#sec4.20|Audio encryption]]
# COMR    [#sec4.25 Commercial frame]
# EQUA    [#sec4.13 Equalization]
# ETCO    [#sec4.6 Event timing codes]

# accepted by id3v2 but wrong value
# APIC    [#sec4.15 Attached picture]

# accepted by id3v2
COMM    [#sec4.11 Comments]
ENCR    [#sec4.26 Encryption method registration]
GEOB    [#sec4.16 General encapsulated object]
GRID    [#sec4.27 Group identification registration]
IPLS    [#sec4.4 Involved people list]
LINK    [#sec4.21 Linked information]
MCDI    [#sec4.5 Music CD identifier]
MLLT    [#sec4.7 MPEG location lookup table]
OWNE    [#sec4.24 Ownership frame]
PRIV    [#sec4.28 Private frame]
PCNT    [#sec4.17 Play counter]
POPM    [#sec4.18 Popularimeter]
POSS    [#sec4.22 Position synchronisation frame]
RBUF    [#sec4.19 Recommended buffer size]
RVAD    [#sec4.12 Relative volume adjustment]
RVRB    [#sec4.14 Reverb]
SYLT    [#sec4.10 Synchronized lyric/text]
SYTC    [#sec4.8 Synchronized tempo codes]
TALB    [#TALB Album/Movie/Show title]
TBPM    [#TBPM BPM (beats per minute)]
TCOM    [#TCOM Composer]
TCON    [#TCON Content type]
TCOP    [#TCOP Copyright message]
TDAT    [#TDAT Date]
TDLY    [#TDLY Playlist delay]
TENC    [#TENC Encoded by]
TEXT    [#TEXT Lyricist/Text writer]
TFLT    [#TFLT File type]
TIME    [#TIME Time]
TIT1    [#TIT1 Content group description]
TIT2    [#TIT2 Title/songname/content description]
TIT3    [#TIT3 Subtitle/Description refinement]
TKEY    [#TKEY Initial key]
TLAN    [#TLAN Language(s)]
TLEN    [#TLEN Length]
TMED    [#TMED Media type]
TOAL    [#TOAL Original album/movie/show title]
TOFN    [#TOFN Original filename]
TOLY    [#TOLY Original lyricist(s)/text writer(s)]
TOPE    [#TOPE Original artist(s)/performer(s)]
TORY    [#TORY Original release year]
TOWN    [#TOWN File owner/licensee]
TPE1    [#TPE1 Lead performer(s)/Soloist(s)]
TPE2    [#TPE2 Band/orchestra/accompaniment]
TPE3    [#TPE3 Conductor/performer refinement]
TPE4    [#TPE4 Interpreted, remixed, or otherwise modified by]
TPOS    [#TPOS Part of a set]
TPUB    [#TPUB Publisher]
TRCK    [#TRCK Track number/Position in set]
TRDA    [#TRDA Recording dates]
TRSN    [#TRSN Internet radio station name]
TRSO    [#TRSO Internet radio station owner]
TSIZ    [#TSIZ Size]
TSRC    [#TSRC ISRC (international standard recording code)]
TSSE    [#TSEE Software/Hardware and settings used for encoding]
TYER    [#TYER Year]
TXXX    [#TXXX User defined text information frame]
UFID    [#sec4.1 Unique file identifier]
USER    [#sec4.23 Terms of use]
USLT    [#sec4.9 Unsychronized lyric/text transcription]
WCOM    [#WCOM Commercial information]
WCOP    [#WCOP Copyright/Legal information]
WOAF    [#WOAF Official audio file webpage]
WOAR    [#WOAR Official artist/performer webpage]
WOAS    [#WOAS Official audio source webpage]
WORS    [#WORS Official internet radio station homepage]
WPAY    [#WPAY Payment]
WPUB    [#WPUB Publishers official webpage]
WXXX    [#WXXX User defined URL link frame]
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-id3v2 tags < <(id3v2-show-metadata "${fileAMp3}")

  [ ${#tags[*]} -eq 51 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_id3v2_subset_id3v2[@]}")
}

@test "invoke aio-metadata-set - vorbis tags - ffmpeg - flac" {

  aio-generate-flac-file-sine "${fileAFlac}" 1

  # all vorbis comment tags are taken from https://www.xiph.org/vorbis/doc/v-comment.html
  run ${__bin_path}/aio-metadata-set --fff individual "${fileAFlac}" <<EOF
TITLE a foo bar
VERSION remix
ALBUM bar
TRACKNUMBER 1
ARTIST foo
PERFORMER foo-2
COPYRIGHT copyleft
LICENSE copyleft
ORGANIZATION none
DESCRIPTION this is a comment
GENRE unittest
DATE 2020
LOCATION none
CONTACT none
ISRC 666
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-ffprobe tags < <( ffprobe-show-format-tags "${fileAFlac}" )

  # NOTE ffmpeg puts an additional 'encoder' tag on file creation
  [ ${#tags[*]} -eq 16 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_ffprobe_all[@]}")

  dbg-print-metadata-tags tags
  dbg-print-file-header-hex "${fileAFlac}"
}

@test "invoke aio-metadata-set - vorbis tags - ffmpeg - ogg" {

  aio-generate-ogg-file-sine "${fileA}" 1

  # all vorbis comment tags are taken from https://www.xiph.org/vorbis/doc/v-comment.html
  run ${__bin_path}/aio-metadata-set --fff individual "${fileA}" <<EOF
TITLE a foo bar
VERSION remix
ALBUM bar
TRACKNUMBER 1
ARTIST foo
PERFORMER foo-2
COPYRIGHT copyleft
LICENSE copyleft
ORGANIZATION none
DESCRIPTION this is a comment
GENRE unittest
DATE 2020
LOCATION none
CONTACT none
ISRC 666
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-ffprobe tags < <( ffprobe-show-stream-tags "${fileA}" )

  # NOTE ffmpeg puts an additional 'encoder' tag on file creation
  [ ${#tags[*]} -eq 16 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_ffprobe_all[@]}")

  dbg-print-metadata-tags tags
  dbg-print-file-header-hex "${fileA}"
}

@test "invoke aio-metadata-set - id3v2 tags - ffmpeg - mp3" {

  aio-generate-mp3-file-sine "${fileAMp3}" 1

  # all id3v2 tags are taken from https://id3.org/id3v2.3.0#Declared_ID3v2_frames
  run ${__bin_path}/aio-metadata-set --fff individual "${fileAMp3}" <<EOF
# accepted by ffmpeg but probably wrong value
# AENC    [[#sec4.20|Audio encryption]]
# COMR    [#sec4.25 Commercial frame]
# EQUA    [#sec4.13 Equalization]
# ETCO    [#sec4.6 Event timing codes]
# APIC    [#sec4.15 Attached picture]

# accepted by ffmpeg but not set
# TLAN    [#TLAN Language(s)]
# TSSE    [#TSEE Software/Hardware and settings used for encoding]

# accepted by ffmpeg
COMM    [#sec4.11 Comments]
ENCR    [#sec4.26 Encryption method registration]
GEOB    [#sec4.16 General encapsulated object]
GRID    [#sec4.27 Group identification registration]
IPLS    [#sec4.4 Involved people list]
LINK    [#sec4.21 Linked information]
MCDI    [#sec4.5 Music CD identifier]
MLLT    [#sec4.7 MPEG location lookup table]
OWNE    [#sec4.24 Ownership frame]
PRIV    [#sec4.28 Private frame]
PCNT    [#sec4.17 Play counter]
POPM    [#sec4.18 Popularimeter]
POSS    [#sec4.22 Position synchronisation frame]
RBUF    [#sec4.19 Recommended buffer size]
RVAD    [#sec4.12 Relative volume adjustment]
RVRB    [#sec4.14 Reverb]
SYLT    [#sec4.10 Synchronized lyric/text]
SYTC    [#sec4.8 Synchronized tempo codes]
TALB    [#TALB Album/Movie/Show title]
TBPM    [#TBPM BPM (beats per minute)]
TCOM    [#TCOM Composer]
TCON    [#TCON Content type]
TCOP    [#TCOP Copyright message]
TDAT    [#TDAT Date]
TDLY    [#TDLY Playlist delay]
TENC    [#TENC Encoded by]
TEXT    [#TEXT Lyricist/Text writer]
TFLT    [#TFLT File type]
TIME    [#TIME Time]
TIT1    [#TIT1 Content group description]
TIT2    [#TIT2 Title/songname/content description]
TIT3    [#TIT3 Subtitle/Description refinement]
TKEY    [#TKEY Initial key]
TLEN    [#TLEN Length]
TMED    [#TMED Media type]
TOAL    [#TOAL Original album/movie/show title]
TOFN    [#TOFN Original filename]
TOLY    [#TOLY Original lyricist(s)/text writer(s)]
TOPE    [#TOPE Original artist(s)/performer(s)]
TORY    [#TORY Original release year]
TOWN    [#TOWN File owner/licensee]
TPE1    [#TPE1 Lead performer(s)/Soloist(s)]
TPE2    [#TPE2 Band/orchestra/accompaniment]
TPE3    [#TPE3 Conductor/performer refinement]
TPE4    [#TPE4 Interpreted, remixed, or otherwise modified by]
TPOS    [#TPOS Part of a set]
TPUB    [#TPUB Publisher]
TRCK    [#TRCK Track number/Position in set]
TRDA    [#TRDA Recording dates]
TRSN    [#TRSN Internet radio station name]
TRSO    [#TRSO Internet radio station owner]
TSIZ    [#TSIZ Size]
TSRC    [#TSRC ISRC (international standard recording code)]
TYER    [#TYER Year]
TXXX    [#TXXX User defined text information frame]
UFID    [#sec4.1 Unique file identifier]
USER    [#sec4.23 Terms of use]
USLT    [#sec4.9 Unsychronized lyric/text transcription]
WCOM    [#WCOM Commercial information]
WCOP    [#WCOP Copyright/Legal information]
WOAF    [#WOAF Official audio file webpage]
WOAR    [#WOAR Official artist/performer webpage]
WOAS    [#WOAS Official audio source webpage]
WORS    [#WORS Official internet radio station homepage]
WPAY    [#WPAY Payment]
WPUB    [#WPUB Publishers official webpage]
WXXX    [#WXXX User defined URL link frame]
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-ffprobe tags < <( ffprobe-show-format-tags "${fileAMp3}" )

  [ ${#tags[*]} -eq 68 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_id3v2_ffprobe_subset_ffmpeg[@]}")
}

@test "invoke aio-metadata-set - riff info tags - ffmpeg - wav" {

  aio-generate-wav-file-sine "${fileAWav}" 1

  # all riff info tags are taken from http://www.robotplanet.dk/audio/wav_meta_data/
  run ${__bin_path}/aio-metadata-set --fff individual "${fileAWav}" <<EOF
INAM	Track title.
IPRD	The album title.
IART	The artist who created this.
ICRD	The creation date in YYYY-MM-DD format, however you should expect to see just YYYY in this field.
ITRK	Track number.
ICMT	A text comment.
IKEY	The keywords for the project or file.
ISFT	The software used to create the file.
ITCH	The technician.
IGNR	Genre of content.
ICOP	The copyright information.
ISBJ	The subject.
IENG	The name(s) of the engineer. Multiple names separated by a semicolon and a blank.
ISRC	Source: the name of the person or organization that supplied the original subject of the file.
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-ffprobe tags < <( ffprobe-show-format-tags "${fileAWav}" )

  [ ${#tags[*]} -eq 14 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_riff_ffprobe_subset[@]}")

  dbg-print-metadata-tags tags
  dbg-print-metadata-ffprobe "${fileAWav}"
  dbg-print-file-header-hex "${fileAWav}"
}

@test "invoke aio-metadata-set - vorbis tags - ffmpeg - mixed" {

  aio-generate-ogg-file-sine "${fileA}" 1
  aio-generate-opus-file-sine "${fileAOpus}" 1
  aio-generate-flac-file-sine "${fileAFlac}" 1

  # all vorbis comment tags are taken from https://www.xiph.org/vorbis/doc/v-comment.html
  run ${__bin_path}/aio-metadata-set --fff common "${fileA}" "${fileAOpus}" "${fileAFlac}" <<EOF
TITLE a foo bar
VERSION remix
ALBUM bar
TRACKNUMBER 1
ARTIST foo
PERFORMER foo-2
COPYRIGHT copyleft
LICENSE copyleft
ORGANIZATION none
DESCRIPTION this is a comment
GENRE unittest
DATE 2020
LOCATION none
CONTACT none
ISRC 666
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-ffprobe tags < <( ffprobe-show-stream-tags "${fileA}" )

  # NOTE ffmpeg puts an additional 'encoder' tag
  [ ${#tags[*]} -eq 16 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_ffprobe_all[@]}")

  tags=()
  read-metadata-ffprobe tags < <( ffprobe-show-stream-tags "${fileAOpus}" )

  # NOTE ffmpeg puts an additional 'encoder' tag on file creation
  [ ${#tags[*]} -eq 16 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_ffprobe_all[@]}")

  tags=()
  read-metadata-ffprobe tags < <( ffprobe-show-format-tags "${fileAFlac}" )

  # NOTE ffmpeg puts an additional 'encoder' tag on file creation
  [ ${#tags[*]} -eq 16 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_vorbis_comment_ffprobe_all[@]}")

  dbg-print-metadata-tags tags
  dbg-print-file-header-hex "${fileA}"
}

@test "invoke aio-metadata-set - generic tags - ffmpeg - mixed" {

  aio-generate-ogg-file-sine "${fileA}" 1
  aio-generate-opus-file-sine "${fileAOpus}" 1
  aio-generate-flac-file-sine "${fileAFlac}" 1
  aio-generate-wav-file-sine "${fileAWav}" 1
  aio-generate-mp3-file-sine "${fileAMp3}" 1

  # all vorbis comment tags are taken from https://www.xiph.org/vorbis/doc/v-comment.html
  run ${__bin_path}/aio-metadata-set --fff common "${fileA}" "${fileAOpus}" "${fileAFlac}" "${fileAWav}" "${fileAMp3}" <<EOF
album bar
album_artist foo-1
artist foo
comment this is a comment
composer foo-2
copyrigh copyleft
creation_time 2020
date 2021
disc 1
encoder none
encoded_by who knows
filename file.orig
genre unittest
language en
performer bats
publisher world
service_name none
service_provider none
title a foo bar
track 1
variant_bitrate none
EOF

  [ ${status} -eq 0 ]

  declare -A tags=()
  read-metadata-ffprobe tags < <( ffprobe-show-format-tags "${fileAWav}" )

  [ ${#tags[*]} -eq 10 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_ffmpeg_generic_ffprobe_subset[@]}")

  tags=()
  read-metadata-ffprobe tags < <( ffprobe-show-stream-tags "${fileAOpus}" )

  [ ${#tags[*]} -eq 21 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_ffmpeg_generic_ffprobe_all[@]}")

  tags=()
  read-metadata-ffprobe tags < <( ffprobe-show-stream-tags "${fileA}" )

  [ ${#tags[*]} -eq 21 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_ffmpeg_generic_ffprobe_all[@]}")

  tags=()
  read-metadata-ffprobe tags < <( ffprobe-show-format-tags "${fileAFlac}" )

  [ ${#tags[*]} -eq 21 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_ffmpeg_generic_ffprobe_all[@]}")

  tags=()
  read-metadata-ffprobe tags < <( ffprobe-show-format-tags "${fileAMp3}" )

  [ ${#tags[*]} -eq 21 ]

  assert-metadata tags < <(printf '%s\n' "${__expected_tags_ffmpeg_generic_ffprobe_all[@]}")

  dbg-print-metadata-tags tags
  dbg-print-file-header-hex "${fileA}"
}
