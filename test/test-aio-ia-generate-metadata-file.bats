#!/usr/bin/env bats

# This file is part of aio-proc
#
#     aio-proc is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     aio-proc is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with aio-proc. If not, see <http://www.gnu.org/licenses/>.

###########
# Test Data
###########

declare -g -r -a __stdin_metadata_key_list=(
  "identifier"
  "creator"
  "collection"
  "title"
  "date"
  "description"
)

declare -g -r -a __cl_metadata_key_list=(
  "identifier"
  "creator"
  "collection"
)

declare -g -r -a __creator_option_list=(
  '-c me'
  '--creator me'
)

declare -g -r -a __collection_option_list=(
  '-g podcast'
  '--collection podcast'
)

declare -g -r -a __identifier_option_list=(
  '-i testitem'
  '--identifier testitem'
)

# same order as __creator_collector_expected_column_header_list
declare -g -r -a __creator_collector_option_list=(
  '-c me'
  '--creator me'
  '-g podcast'
  '--collection podcast'
)

# same order as __creator_collector_option_list
declare -g -r -a __creator_collector_expected_column_header_list=(
  'creator'
  'creator'
  'collection'
  'collection'
)

# same order as __expected_placeholder_key_list
declare -g -r -a __placeholder_option_list=(
  "-I"
  "--placeholder-identifier"
  "-C"
  "--placeholder-creator"
  "-G"
  "--placeholder-collection"
)

# same order as __placeholder_option_list
declare -g -r -a __expected_placeholder_key_list=(
  "identifier"
  "identifier"
  "creator"
  "creator"
  "collection"
  "collection"
)

declare -g -r -a __collection_short_option_shortcut_list=(
  '--ga'
  '--gm'
  '--ge'
  '--gt'
  '--gi'
  '--gs'
)

# same order as in __collection_short_option_shortcut_list
declare -g -r -a __collection_long_option_shortcut_list=(
  '--collection-audio'
  '--collection-movie'
  '--collection-test'
  '--collection-text'
  '--collection-image'
  '--collection-software'
)

# same order as in __collection_short_option_shortcut_list
declare -g -r -a __collection_row_data_list=(
  'opensource_audio'
  'opensource_movies'
  'test_collection'
  'opensource'
  'opensource_image'
  'open_source_software'
)

declare -g -r __line_with_whitespaces='      '
declare -g -r __line_with_tabs="$(printf '\t\t\t')"

###########
# Helpers
###########

declare -g -r __test_path="$(pwd)"
declare -g -r __bin_path="$(realpath ${__test_path}/..)"

# files are expected to reside in __workspace_path
declare -g -r __expected_csv_file="testitem-ia-metadata.csv"
declare -g -r __expected_csv_file_custom="my-metadata.csv"
declare -g -r __expected_csv_file_default="ia-metadata.csv"

declare -g -r -a __files_to_cleanup=(
  "${__expected_csv_file}"
  "${__expected_csv_file_custom}"
  "${__expected_csv_file_default}"
  "subjects.txt"
  "subjects-common.txt"
  "description.txt"
)

declare -g __workspace_path

remove-temporary-files() {
  # cd "${__workspace_path}"
  rm -f "${__files_to_cleanup[@]}"
}

###########
# Asserts
###########

assert-csv-file-is-complete() { 
  local -r csvFile="$1"
  local -r numExpectedLines="$2"
  
  [ -f "${csvFile}" ]

  local lineCount=$( wc -l <"${csvFile}" )
  [ "${lineCount}" -eq "${numExpectedLines}" ]
}

assert-csv-file-header() {
  local -r csvFile="$1"
  
  local expectedHeader
  read -r expectedHeader

  local -r availableHeader=$(head -1 "${csvFile}")
  [ "${availableHeader}" == "${expectedHeader}" ]
}

assert-csv-file-rows() {
  local -r csvFile="$1"
  local -r numRows="$2"
  
  local expectedLine
  local expectedLines=()

  while read -r expectedLine; do
    expectedLines+=("${expectedLine}")
  done
  
  local index=0
  local availableLine

  while read -r availableLine; do
    [ "${index}" -lt "${#expectedLines[*]}" ]
    [ "${availableLine}" == "${expectedLines[${index}]}" ]
    index=$((index+1))
  done < <(tail -"${numRows}" "${csvFile}")
}

###########
# Setup and Teardown
###########

setup() {
  remove-temporary-files
}

teardown() {
  remove-temporary-files
}

setup_file() {

  __workspace_path=$(mktemp -d -p "${XDG_RUNTIME_DIR}" "aio-test-XXX")
  mkdir -p "${__workspace_path}"

  export __workspace_path
}

teardown_file() {
  local -r workspacePath=$(realpath "${__workspace_path}")

  [[ -d "${workspacePath}" ]] && [[ "${__workspace_path}" != '/' ]] && {
    rm -r "${__workspace_path}"
  }
}

###########
# Tests
###########

@test "invoke aio-ia-generate-metadata-file - help" {
  run ${__bin_path}/aio-ia-generate-metadata-file -h
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-ia-generate-metadata-file --help
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-ia-generate-metadata-file help
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-ia-generate-metadata-file h
  [ ${status} -eq 0 ]
}

@test "invoke aio-ia-generate-metadata-file - help short" {

  run ${__bin_path}/aio-ia-generate-metadata-file help short
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-ia-generate-metadata-file help s
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-ia-generate-metadata-file h s
  [ ${status} -eq 0 ]
}

@test "invoke aio-ia-generate-metadata-file - help details" {

  run ${__bin_path}/aio-ia-generate-metadata-file help details
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-ia-generate-metadata-file help d
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-ia-generate-metadata-file h d
  [ ${status} -eq 0 ]
}

@test "invoke aio-ia-generate-metadata-file - help archive" {

  run ${__bin_path}/aio-ia-generate-metadata-file help archive
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-ia-generate-metadata-file help a
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-ia-generate-metadata-file h a
  [ ${status} -eq 0 ]
}

@test "invoke aio-ia-generate-metadata-file - help metadata" {

  run ${__bin_path}/aio-ia-generate-metadata-file help metadata
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-ia-generate-metadata-file help m
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-ia-generate-metadata-file h m
  [ ${status} -eq 0 ]
}

@test "invoke aio-ia-generate-metadata-file - help output" {

  run ${__bin_path}/aio-ia-generate-metadata-file help output
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-ia-generate-metadata-file help o
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-ia-generate-metadata-file h o
  [ ${status} -eq 0 ]
}

@test "invoke aio-ia-generate-metadata-file - version" {
  run ${__bin_path}/aio-ia-generate-metadata-file --version
  [ ${status} -eq 0 ]
}

@test "invoke aio-ia-generate-metadata-file - no command" {
  run ${__bin_path}/aio-ia-generate-metadata-file

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: no valid command provided [] ... stopping' ]
}

@test "invoke aio-ia-generate-metadata-file - unknown command" {
  run ${__bin_path}/aio-ia-generate-metadata-file generate

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: no valid command provided [generate] ... stopping' ]
}

@test "invoke aio-ia-generate-metadata-file - no arguments" {
  run ${__bin_path}/aio-ia-generate-metadata-file new

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: you need to provide metadata item [identifier] ... stopping' ]
  
  run ${__bin_path}/aio-ia-generate-metadata-file append

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: you need to provide metadata item [identifier] ... stopping' ]
}

@test "invoke aio-ia-generate-metadata-file - unknown argument" {

  run ${__bin_path}/aio-ia-generate-metadata-file new \
    -i testitem dummyArg

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: unknown option [dummyArg] ... stopping' ]

  # -a does not exist as option, script immediately exits and does not check remaining options
  run ${__bin_path}/aio-ia-generate-metadata-file new \
    -a image -i testitem dummyArg
  
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: unknown option [-a] ... stopping' ]
}

@test "invoke aio-ia-generate-metadata-file - cl - new - metadata - values" {

  cd "${__workspace_path}"

  local expectedColumnHeader
  local option
  local value
  
  for index in "${!__creator_collector_option_list[@]}"; do

    expectedColumnHeader="${__creator_collector_expected_column_header_list[${index}]}"
    read -r option optionValue <<<"${__creator_collector_option_list[${index}]}"

    run ${__bin_path}/aio-ia-generate-metadata-file new \
      -i testitem \
      "${option}" "${value}"

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 2

    assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,${expectedColumnHeader}
EOF

    assert-csv-file-rows "${__expected_csv_file}" 1 <<EOF
testitem,${value}
EOF

  done

}

@test "invoke aio-ia-generate-metadata-file - cl - new - metadata - value with whitespace" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-metadata-file new \
    -i testitem \
    --creator 'its me'

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 2
        
  assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,creator
EOF

  assert-csv-file-rows "${__expected_csv_file}" 1 <<<"testitem,its me"
}

@test "invoke aio-ia-generate-metadata-file - cl - new - metadata - value with whitespace - not allowed" {

  cd "${__workspace_path}"

  local -r metadataWithSpaces=(
    "identifier a identifier"
    "collection a collection"
  )
  
  local metadata
  local key
  local value

  for metadata in "${metadataWithSpaces[@]}"; do
  
    read -r key value <<<"${metadata}"
    
    # no need to provide identifier as we expect an error during input validation
    run ${__bin_path}/aio-ia-generate-metadata-file new \
      "--${key}" "${value}"

    [ ${status} -eq 1 ]
    [ "${output}" == "error: value for key [${key}] contains one or more whitespaces [${value}] ... stopping" ]
  
  done
}

@test "invoke aio-ia-generate-metadata-file - cl - new - metadata - value with comma - not allowed" {

  cd "${__workspace_path}"

  local -r metadataWithComma=(
    "identifier a,identifier"
    "collection a,collection"
    "creator its,me"
  )
  
  local metadata
  local key
  local value

  for metadata in "${metadataWithComma[@]}"; do
  
    read -r key value <<<"${metadata}"
    # no need to provide identifier as we expect an error during input validation
    run ${__bin_path}/aio-ia-generate-metadata-file new \
      "--${key}" "${value}"

    [ ${status} -eq 1 ]
    # TODO currently, comma is not checked on cl
    [ "${output}" == "error: value for key [${key}] contains a comma [${value}] ... stopping" ]
  
  done
}

@test "invoke aio-ia-generate-metadata-file - cl - new - metadata - shortcuts" {

  cd "${__workspace_path}"

  local lineCount
  local header
  local row
  local index
  local collectionShortcutOption
  local expectedCollectionName

  for index in "${!__collection_short_option_shortcut_list[@]}"; do
    
    collectionShortcutOption="${__collection_short_option_shortcut_list[${index}]}"
    expectedCollectionName="${__collection_row_data_list[${index}]}"
    
    run ${__bin_path}/aio-ia-generate-metadata-file new \
      --identifier testitem \
      "${collectionShortcutOption}"
  
    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 2

    assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,collection
EOF

    assert-csv-file-rows "${__expected_csv_file}" 1 <<EOF
testitem,${expectedCollectionName}
EOF

  done
  
  for index in "${!__collection_long_option_shortcut_list[@]}"; do
    
    collectionShortcutOption="${__collection_long_option_shortcut_list[${index}]}"
    expectedCollectionName="${__collection_row_data_list[${index}]}"
    
    run ${__bin_path}/aio-ia-generate-metadata-file new \
      --identifier testitem \
      "${collectionShortcutOption}"
  
    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 2

    assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,collection
EOF

    assert-csv-file-rows "${__expected_csv_file}" 1 <<EOF
testitem,${expectedCollectionName}
EOF

  done
}

@test "invoke aio-ia-generate-metadata-file - cl - new - metadata - placeholders" {

  cd "${__workspace_path}"

  local lineCount
  local header
  local row
  
  run ${__bin_path}/aio-ia-generate-metadata-file new \
    --placeholder-creator \
    --placeholder-collection \
    --placeholder-identifier
  
  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_default}" 2

  assert-csv-file-header "${__expected_csv_file_default}" <<EOF
creator,collection,identifier
EOF

  assert-csv-file-rows "${__expected_csv_file_default}" 1 <<EOF
,,
EOF
  
  run ${__bin_path}/aio-ia-generate-metadata-file new \
    -C -G -I
  
  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_default}" 2

  assert-csv-file-header "${__expected_csv_file_default}" <<EOF
creator,collection,identifier
EOF

  assert-csv-file-rows "${__expected_csv_file_default}" 1 <<EOF
,,
EOF

}

@test "invoke aio-ia-generate-metadata-file - cl - new - metadata - mixed" {

  cd "${__workspace_path}"

  local lineCount
  local header
  local row
  local index
  
  local collectionShortcutOption
  local expectedCollectionName

  for index in "${!__collection_short_option_shortcut_list[@]}"; do

    collectionShortcutOption="${__collection_short_option_shortcut_list[${index}]}"
    expectedCollectionName="${__collection_row_data_list[${index}]}"

    run ${__bin_path}/aio-ia-generate-metadata-file new \
      -c me \
      "${collectionShortcutOption}" \
      -I

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file_default}" 2

    assert-csv-file-header "${__expected_csv_file_default}" <<EOF
creator,collection,identifier
EOF

    assert-csv-file-rows "${__expected_csv_file_default}" 1 <<EOF
me,${expectedCollectionName},
EOF

  done

  for index in "${!__collection_long_option_shortcut_list[@]}"; do
    
    collectionShortcutOption="${__collection_long_option_shortcut_list[${index}]}"
    expectedCollectionName="${__collection_row_data_list[${index}]}"

    run ${__bin_path}/aio-ia-generate-metadata-file new \
      --creator me \
      "${collectionShortcutOption}" \
      --placeholder-identifier

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file_default}" 2

    assert-csv-file-header "${__expected_csv_file_default}" <<EOF
creator,collection,identifier
EOF

    assert-csv-file-rows "${__expected_csv_file_default}" 1 <<EOF
me,${expectedCollectionName},
EOF

  done
}

@test "invoke aio-ia-generate-metadata-file - stdin - new - metadata - read timeout" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-metadata-file new
  
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: you need to provide metadata item [identifier] ... stopping' ]
  [ ! -f "${__expected_csv_file}" ]

  run ${__bin_path}/aio-ia-generate-metadata-file new < <(sleep 2; printf 'identifier testitem')
  
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: you need to provide metadata item [identifier] ... stopping' ]
  [ ! -f "${__expected_csv_file}" ]
}

@test "invoke aio-ia-generate-metadata-file - stdin - new - metadata - comments" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
# a single line comment
date 2020-07-07
description this is a test item
# a
${__line_with_tabs}# multi
${__line_with_whitespaces}# line
${__line_with_tabs}${__line_with_whitespaces}# comment
identifier testitem
title a test item
subject accustic
subject music
subject pop
EOF

  [ "${status}" -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 2

  assert-csv-file-header "${__expected_csv_file}" <<EOF
date,description,identifier,title,subject[0],subject[1],subject[2]
EOF

  assert-csv-file-rows "${__expected_csv_file}" 1 <<EOF
2020-07-07,this is a test item,testitem,a test item,accustic,music,pop
EOF

}

@test "invoke aio-ia-generate-metadata-file - stdin - new - metadata - with empty lines" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
# single empty line

date 2020-07-07
description this is a test item
# multiple empty lines with and without whitespaces and tabs

${__line_with_whitespaces}


${__line_with_tabs}
${__line_with_whitespaces}
${__line_with_tabs}${__line_with_whitespaces}
identifier testitem
title a test item
subject accustic
subject music
subject pop
EOF

  [ "${status}" -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 2

  assert-csv-file-header "${__expected_csv_file}" <<EOF
date,description,identifier,title,subject[0],subject[1],subject[2]
EOF

  assert-csv-file-rows "${__expected_csv_file}" 1 <<EOF
2020-07-07,this is a test item,testitem,a test item,accustic,music,pop
EOF

}

@test "invoke aio-ia-generate-metadata-file - stdin - new - metadata - empty lines only" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF

${__line_with_whitespaces}
${__line_with_tabs}
${__line_with_whitespaces}${__line_with_tabs}
EOF

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: you need to provide metadata item [identifier] ... stopping' ]
  [ ! -f "${__expected_csv_file}" ]

  # empty value
  run ${__bin_path}/aio-ia-generate-metadata-file new <<<""

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: you need to provide metadata item [identifier] ... stopping' ]
  [ ! -f "${__expected_csv_file}" ]

  # empty value - whitespaces only
  run ${__bin_path}/aio-ia-generate-metadata-file new <<<"${__line_with_whitespaces}"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: you need to provide metadata item [identifier] ... stopping' ]
  [ ! -f "${__expected_csv_file}" ]

  # empty value - tabs only
  run ${__bin_path}/aio-ia-generate-metadata-file new <<<"${__line_with_tabs}"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: you need to provide metadata item [identifier] ... stopping' ]
  [ ! -f "${__expected_csv_file}" ]

  # empty value - whitespaces and tabs only
  run ${__bin_path}/aio-ia-generate-metadata-file new <<<"${__line_with_whitespaces}${__line_with_tabs}"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: you need to provide metadata item [identifier] ... stopping' ]
  [ ! -f "${__expected_csv_file}" ]

}

@test "invoke aio-ia-generate-metadata-file - stdin - new - metadata - value with whitespace" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
identifier
title a title
description a description
subject a subject
creator a creator
date c.a. 1020
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_default}" 2

  assert-csv-file-header "${__expected_csv_file_default}" <<EOF
identifier,title,description,subject[0],creator,date
EOF

  assert-csv-file-rows "${__expected_csv_file_default}" 1 <<EOF
,a title,a description,a subject,a creator,c.a. 1020
EOF

}

@test "invoke aio-ia-generate-metadata-file - stdin - new - metadata - value with whitespace - not allowed" {

  cd "${__workspace_path}"

  local -r metadataWithSpaces=(
    "identifier a identifier"
    "collection a collection"
    "date 1020 2020"
    "date c..a.. 1020"
    "date ca 1020"
    "date c.a. a"
  )
  
  local metadata
  local key
  local value

  for metadata in "${metadataWithSpaces[@]}"; do

    run ${__bin_path}/aio-ia-generate-metadata-file new <<<"${metadata}"
    
    [ ${status} -eq 1 ]
    [ ! -f "${__expected_csv_file}" ]

    read -r key value <<<"${metadata}"
    [ "${output}" == "error: value for key [${key}] contains one or more whitespaces [${value}] ... stopping" ]

  done
}

@test "invoke aio-ia-generate-metadata-file - stdin - new - metadata - value with comma" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
identifier testitem
title a title
description its a description, yeah
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 2

  assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,title,description
EOF

  assert-csv-file-rows "${__expected_csv_file}" 1 <<EOF
testitem,a title,its a description&#44; yeah
EOF
  
    printf '%s' "\
this
is a description
, yeah
" >description.txt
  
  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
identifier testitem
title a title
description_file description.txt
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 2

  assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,title,description
EOF

  assert-csv-file-rows "${__expected_csv_file}" 1 <<EOF
testitem,a title,this<br/>is a description<br/>&#44; yeah<br/>
EOF

}

@test "invoke aio-ia-generate-metadata-file - stdin - new - metadata - value with comma - not allowed" {

  cd "${__workspace_path}"

  local -r metadataWithComma=(
    "title a test item, v2"
    "identifier Archive,No.2"
    "date 20200909,20201010"
    "creator i,and,i"
    "collection opensource_audio,podcast"
    "subject music, concert"
    "description_file a,file.txt"
    "subject_file a,file.txt"
  )
  
  local metadata
  local key
  local value

  for metadata in "${metadataWithComma[@]}"; do
  
    read -r key value <<<"${metadata}"
    run ${__bin_path}/aio-ia-generate-metadata-file new <<<"${key} ${value}"

    [ ${status} -eq 1 ]
    [ "${output}" == "error: value for key [${key}] contains a comma [${value}] ... stopping" ]
    [ ! -f "${__expected_csv_file}" ]

  done
}


@test "invoke aio-ia-generate-metadata-file - stdin - new - metadata - key with comma - not allowed" {

  cd "${__workspace_path}"
  
  local -r metadataWithComma=(
    "a,title a test item"
    "a,identifier testitem"
    "a,date 20200909"
    "a,creator me"
    "a,collection podcast"
    "a,subject music"
    "a,description some description"
    "a,description_file description.txt"
    "a,subject_file subject.txt"
  )
  
  local metadata

  for metadata in "${metadataWithComma[@]}"; do
    
    # no need to provide identifier and mediatype as we expect an error during input validation
    run ${__bin_path}/aio-ia-generate-metadata-file new <<<"${metadata}"

    [ ${status} -eq 1 ]
    
    read -r key value <<<"${metadata}"
    [ "${output}" == "error: key [${key}] contains special char(s) ... stopping" ]

  done
}

@test "invoke aio-ia-generate-metadata-file - stdin - new - metadata - with indented data" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
${__line_with_whitespaces}date 2020-07-07
description${__line_with_whitespaces}this is a test item
${__line_with_whitespaces}${__line_with_tabs}identifier${__line_with_whitespaces}testitem${__line_with_tabs}
${__line_with_tabs}title a test item
${__line_with_whitespaces}${__line_with_tabs}subject${__line_with_tabs}accustic${__line_with_tabs}
subject${__line_with_tabs}music
${__line_with_whitespaces}${__line_with_tabs}subject pop${__line_with_whitespaces}
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 2

  assert-csv-file-header "${__expected_csv_file}" <<EOF
date,description,identifier,title,subject[0],subject[1],subject[2]
EOF

  assert-csv-file-rows "${__expected_csv_file}" 1 <<EOF
2020-07-07,this is a test item,testitem,a test item,accustic,music,pop
EOF

}

@test "invoke aio-ia-generate-metadata-file - stdin - new - metadata - missing identifier" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-metadata-file new  <<<"date 2020-07-07"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: you need to provide metadata item [identifier] ... stopping' ]
  [ ! -f "${__expected_csv_file}" ]
}

@test "invoke aio-ia-generate-metadata-file - stdin - new - metadata - identifier only" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-metadata-file new  <<<"identifier testitem"
  
  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 2
  assert-csv-file-header "${__expected_csv_file}" <<< "identifier"
  assert-csv-file-rows "${__expected_csv_file}" 1 <<< "testitem"

}

@test "invoke aio-ia-generate-metadata-file - stdin - new - metadata" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
identifier testitem
subject d.i.y. radio show
subject music
subject no borders
subject semi-loud
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 2

  assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,subject[0],subject[1],subject[2],subject[3]
EOF

  assert-csv-file-rows "${__expected_csv_file}" 1 <<EOF
testitem,d.i.y. radio show,music,no borders,semi-loud
EOF

  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
date 2020-07-07
description this is a test item
identifier testitem
title a test item
subject accustic
subject music
subject pop
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 2

  assert-csv-file-header "${__expected_csv_file}" <<EOF
date,description,identifier,title,subject[0],subject[1],subject[2]
EOF

  assert-csv-file-rows "${__expected_csv_file}" 1 <<EOF
2020-07-07,this is a test item,testitem,a test item,accustic,music,pop
EOF

}

@test "invoke aio-ia-generate-metadata-file - stdin - new - metadata - placeholders" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
identifier
subject
subject
subject
subject
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_default}" 2

  assert-csv-file-header "${__expected_csv_file_default}" <<EOF
identifier,subject[0],subject[1],subject[2],subject[3]
EOF

  assert-csv-file-rows "${__expected_csv_file_default}" 1 <<EOF
,,,,
EOF
  
  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
identifier
date
title
description
creator
collection
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_default}" 2

  assert-csv-file-header "${__expected_csv_file_default}" <<EOF
identifier,date,title,description,creator,collection
EOF

  assert-csv-file-rows "${__expected_csv_file_default}" 1 <<EOF
,,,,,
EOF

}

@test "invoke aio-ia-generate-metadata-file - stdin - new - metadata - placeholders - not allowed" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
identifier
description_file
EOF

  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file_default}" ]
  [ "${lines[0]}" == 'error: value for key [description_file] is empty ... stopping' ]
  
  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
identifier
description
description_file
EOF

  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file_default}" ]
  [ "${lines[0]}" == 'error: value for key [description_file] is empty ... stopping' ]
  
  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
identifier
subject_file
EOF

  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file_default}" ]
  [ "${lines[0]}" == 'error: value for key [subject_file] is empty ... stopping' ]
  
  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
identifier
subject
subject_file
EOF

  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file_default}" ]
  [ "${lines[0]}" == 'error: value for key [subject_file] is empty ... stopping' ]
}

@test "invoke aio-ia-generate-metadata-file - stdin - new - metadata - dublicates - not allowed" {

  cd "${__workspace_path}"

  local key
  for key in "${__stdin_metadata_key_list[@]}"; do

    run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
${key}
${key}
EOF

    [ ${status} -eq 1 ]
    [ ! -f "${__expected_csv_file_default}" ]
    [ "${lines[0]}" == "error: key [${key}] can only be provided once ... stopping" ]
  
  done
  
  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
description_file dummy-a
description_file dummy-b
EOF

  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file_default}" ]
  [ "${lines[0]}" == "error: key [description_file] can only be provided once ... stopping" ]
  
  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
description_file dummy-a
description a dummy description
EOF

  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file_default}" ]
  [ "${lines[0]}" == "error: only one of metadata keys [description] or [description_file] can be provided at once, not both at the same time ... stopping" ]
  
  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
description a dummy description
description_file dummy-a
EOF

  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file_default}" ]
  [ "${lines[0]}" == "error: only one of metadata keys [description] or [description_file] can be provided at once, not both at the same time ... stopping" ]
}

@test "invoke aio-ia-generate-metadata-file - stdin - new - metadata - dublicates - subject values - not allowed" {

  cd "${__workspace_path}"

  printf '%s\n' '
accustic
music
pop
accustic
music
pop
' >subjects.txt
  
  # dublicates only in subjects.txt
  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
subject_file ./subjects.txt
identifier testitem
EOF

  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file}" ]
  [ "${output}" == "error: subject value [accustic] is given more than once ... stopping" ]
  
  printf '%s\n' '
music
accustic
pop
' >subjects.txt

   # dublicates on stdin and in subjects.txt
  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
subject music
subject pop
subject accustic
subject_file ./subjects.txt
identifier testitem
EOF

  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file}" ]
  [ "${output}" == "error: subject value [music] is given more than once ... stopping" ]
  
  # dublicates only on stdin
  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
subject music
subject pop
subject accustic
subject pop
subject music
subject accustic
identifier testitem
EOF

  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file}" ]
  [ "${output}" == "error: subject value [pop] is given more than once ... stopping" ]
}

@test "invoke aio-ia-generate-metadata-file - stdin - new - subjects-file" {

  cd "${__workspace_path}"

  printf '%s\n' '
d.i.y. radio show
music
no borders
semi-loud
' >subjects.txt

  run ${__bin_path}/aio-ia-generate-metadata-file new  <<EOF
identifier testitem
subject_file subjects.txt
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 2

  assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,subject[0],subject[1],subject[2],subject[3]
EOF

  assert-csv-file-rows "${__expected_csv_file}" 1 <<EOF
testitem,d.i.y. radio show,music,no borders,semi-loud
EOF

    printf '%s\n' '
d.i.y. radio show
music
no borders
semi-loud
' >subjects.txt

  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
identifier testitem
subject_file subjects.txt
subject addition
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 2

  assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,subject[0],subject[1],subject[2],subject[3],subject[4]
EOF

  assert-csv-file-rows "${__expected_csv_file}" 1 <<EOF
testitem,d.i.y. radio show,music,no borders,semi-loud,addition
EOF

}

@test "invoke aio-ia-generate-metadata-file - stdin - new - subjects-file - multiple" {

  cd "${__workspace_path}"

  printf '%s\n' '
d.i.y. radio show
semi-loud
' >subjects.txt

  printf '%s\n' '
music
no borders
' >subjects-common.txt

  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
identifier testitem
subject_file subjects.txt
subject_file subjects-common.txt
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 2

  assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,subject[0],subject[1],subject[2],subject[3]
EOF

  assert-csv-file-rows "${__expected_csv_file}" 1 <<EOF
testitem,d.i.y. radio show,semi-loud,music,no borders
EOF

}

@test "invoke aio-ia-generate-metadata-file - stdin - new - subjects-file - comments and empty lines" {

  cd "${__workspace_path}"

  printf '%s\n' '
# whitespaces at begin of line
      d.i.y. radio show
# a comment
music
no borders
# some empty lines


semi-loud
# lines containing only whitespaces
        
        
' >subjects.txt

  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
identifier testitem
subject_file subjects.txt
EOF
  
  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 2

  assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,subject[0],subject[1],subject[2],subject[3]
EOF

  assert-csv-file-rows "${__expected_csv_file}" 1 <<EOF
testitem,d.i.y. radio show,music,no borders,semi-loud
EOF

}

@test "invoke aio-ia-generate-metadata-file - stdin - new - subjects-file - empty" {

  cd "${__workspace_path}"

  touch subjects.txt

  # subject_file exists but is empty
  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
identifier testitem
subject_file ./subjects.txt
EOF

  [ ${status} -eq 1 ]
  [ "${output}" == "error: input-file [./subjects.txt] is empty ... stopping" ]

  printf '%s\n' "
# all subjects commented out
# no borders
# music
# some empty lines


# semi-loud
# empty line containing only whitespaces

# d.i.y. radio show
${__line_with_whitespaces}
${__line_with_tabs}
" >subjects.txt

  # subject_file only contains comments and empty lines/lines with spaces
  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
identifier testitem
subject_file ./subjects.txt
EOF

  [ ${status} -eq 1 ]
  [ "${output}" == "error: input-file [./subjects.txt] is empty ... stopping" ]

}

@test "invoke aio-ia-generate-metadata-file - stdin - new - subjects-file - not existing" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
identifier testitem
subject_file dummy
EOF
  [ ${status} -eq 1 ]

  [ "${output}" == "error: input-file [dummy] does not exist ... stopping" ]
}

@test "invoke aio-ia-generate-metadata-file - stdin - new - subjects-file - with comma" {

  cd "${__workspace_path}"

  printf '%s\n' '
no borders
music
semi, loud
d.i.y. radio show
' >subjects.txt

  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
identifier testitem
subject_file ./subjects.txt
EOF

  [ "${status}" -eq 1 ]
  [ "${output}" == 'error: subject [semi, loud] in file [./subjects.txt] contains a comma ... stopping' ]
}

@test "invoke aio-ia-generate-metadata-file - stdin - new - description-file" {

  cd "${__workspace_path}"

  printf '%s' "\
this
is a
test item
" >description.txt

  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
date 2020-07-07
description_file ./description.txt
identifier testitem
title a test item
subject accustic
subject music
subject pop
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 2

  assert-csv-file-header "${__expected_csv_file}" <<EOF
date,description,identifier,title,subject[0],subject[1],subject[2]
EOF

  assert-csv-file-rows "${__expected_csv_file}" 1 <<EOF
2020-07-07,this<br/>is a<br/>test item<br/>,testitem,a test item,accustic,music,pop
EOF

}

@test "invoke aio-ia-generate-metadata-file - stdin - new - description-file - empty" {

  cd "${__workspace_path}"

  touch description.txt

  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
description_file ./description.txt
identifier testitem
EOF

  [ ${status} -eq 1 ]
  [ "${output}" == "error: input-file [./description.txt] is empty ... stopping" ]

  printf '%s\n' "
${__line_with_whitespaces}
${__line_with_tabs}
${__line_with_whitespaces}${__line_with_tabs}

" >description.txt

  # description_file only contains empty lines
  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
description_file ./description.txt
identifier testitem
EOF

  [ ${status} -eq 1 ]
  [ "${output}" == "error: input-file [./description.txt] is empty ... stopping" ]

}

@test "invoke aio-ia-generate-metadata-file - stdin - new - description-file - with comma" {

  cd "${__workspace_path}"

  printf '%s' "\
this
is a
test, item
" >description.txt

  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
description_file ./description.txt
identifier testitem
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 2

  assert-csv-file-header "${__expected_csv_file}" <<EOF
description,identifier
EOF

  assert-csv-file-rows "${__expected_csv_file}" 1 <<EOF
this<br/>is a<br/>test&#44; item<br/>,testitem
EOF

}

@test "invoke aio-ia-generate-metadata-file - stdin - new - description-file - not existing" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
description_file dummy
identifier testitem
EOF

  [ ${status} -eq 1 ]
  [ "${output}" == "error: input-file [dummy] does not exist ... stopping" ]

}

@test "invoke aio-ia-generate-metadata-file - mixed - new - metadata - values" {

  cd "${__workspace_path}"

  local identifierOption
  local identifierValue
  local creatorOption
  local creatorValue
  local collectionOption
  local collectionValue

  local index
  for index in "${!__identifier_option_list[@]}"; do

    read -r identifierOption identifierValue <<<"${__identifier_option_list[${index}]}"

    run ${__bin_path}/aio-ia-generate-metadata-file new \
      "${identifierOption}" "${identifierValue}" <<EOF
identifier someid
creator somecreator
collection somecollection
EOF

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 2

    assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,creator,collection
EOF

    assert-csv-file-rows "${__expected_csv_file}" 1 <<EOF
${identifierValue},somecreator,somecollection
EOF

  done

  for index in "${!__collection_option_list[@]}"; do

    read -r collectionOption collectionValue <<<"${__collection_option_list[${index}]}"

    run ${__bin_path}/aio-ia-generate-metadata-file new \
    "${collectionOption}" "${collectionValue}" <<EOF
identifier testitem
creator somecreator
collection somecollection
EOF

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 2

    assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,creator,collection
EOF

    assert-csv-file-rows "${__expected_csv_file}" 1 <<EOF
testitem,somecreator,${collectionValue}
EOF

  done
  
  for index in "${!__creator_option_list[@]}"; do

    read -r creatorOption creatorValue <<<"${__creator_option_list[${index}]}"

    run ${__bin_path}/aio-ia-generate-metadata-file new \
    "${creatorOption}" "${creatorValue}" <<EOF
identifier testitem
creator somecreator
collection somecollection
EOF

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 2

    assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,creator,collection
EOF

    assert-csv-file-rows "${__expected_csv_file}" 1 <<EOF
testitem,${creatorValue},somecollection
EOF

  done
  
  for index in "${!__creator_option_list[@]}"; do

    read -r identifierOption identifierValue <<<"${__identifier_option_list[${index}]}"
    read -r creatorOption creatorValue <<<"${__creator_option_list[${index}]}"
    read -r collectionOption collectionValue <<<"${__collection_option_list[${index}]}"

    run ${__bin_path}/aio-ia-generate-metadata-file new \
    "${identifierOption}" "${identifierValue}" "${creatorOption}" "${creatorValue}" "${collectionOption}" "${collectionValue}"<<EOF
identifier someid
creator somecreator
collection somecollection
EOF

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 2

    assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,creator,collection
EOF

    assert-csv-file-rows "${__expected_csv_file}" 1 <<EOF
${identifierValue},${creatorValue},${collectionValue}
EOF

  done
}

@test "invoke aio-ia-generate-metadata-file - mixed - new - metadata - shortcuts" {

  cd "${__workspace_path}"

  local index
  local collectionShortcutOption
  local expectedCollectionName

  for index in "${!__collection_short_option_shortcut_list[@]}"; do
    
    collectionShortcutOption="${__collection_short_option_shortcut_list[${index}]}"
    expectedCollectionName="${__collection_row_data_list[${index}]}"
    
    run ${__bin_path}/aio-ia-generate-metadata-file new \
      --identifier testitem "${collectionShortcutOption}" <<EOF
identifier someid
creator somecreator
collection somecollection
EOF
  
    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 2

    assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,creator,collection
EOF

    assert-csv-file-rows "${__expected_csv_file}" 1 <<EOF
testitem,somecreator,${expectedCollectionName}
EOF

  done
  
  for index in "${!__collection_long_option_shortcut_list[@]}"; do
    
    collectionShortcutOption="${__collection_long_option_shortcut_list[${index}]}"
    expectedCollectionName="${__collection_row_data_list[${index}]}"
    
    run ${__bin_path}/aio-ia-generate-metadata-file new \
      --identifier testitem "${collectionShortcutOption}" <<EOF
identifier someid
creator somecreator
collection somecollection
EOF
  
    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 2

    assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,creator,collection
EOF

    assert-csv-file-rows "${__expected_csv_file}" 1 <<EOF
testitem,somecreator,${expectedCollectionName}
EOF

  done
}

@test "invoke aio-ia-generate-metadata-file - mixed - new - metadata - placeholders" {

  cd "${__workspace_path}"

  # placeholders on cl
  run ${__bin_path}/aio-ia-generate-metadata-file new \
    --placeholder-creator \
    --placeholder-collection \
    --placeholder-identifier <<EOF
identifier someid
creator somecreator
collection somecollection
EOF
  
  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_default}" 2

  assert-csv-file-header "${__expected_csv_file_default}" <<EOF
identifier,creator,collection
EOF

  assert-csv-file-rows "${__expected_csv_file_default}" 1 <<EOF
,,
EOF
  
  # placeholders on cl
  run ${__bin_path}/aio-ia-generate-metadata-file new \
    -C -G -I <<EOF
identifier someid
creator somecreator
collection somecollection
EOF
  
  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_default}" 2

  assert-csv-file-header "${__expected_csv_file_default}" <<EOF
identifier,creator,collection
EOF

  assert-csv-file-rows "${__expected_csv_file_default}" 1 <<EOF
,,
EOF
  
  # placeholders on cl and stdin
  run ${__bin_path}/aio-ia-generate-metadata-file new \
    -C -G -I <<EOF
identifier
creator
collection
EOF
  
  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_default}" 2

  assert-csv-file-header "${__expected_csv_file_default}" <<EOF
identifier,creator,collection
EOF

  assert-csv-file-rows "${__expected_csv_file_default}" 1 <<EOF
,,
EOF
  
  # placeholders on stdin
  run ${__bin_path}/aio-ia-generate-metadata-file new \
    -c me -g podcast -i testitem <<EOF
identifier
creator
collection
EOF
  
  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 2

  assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,creator,collection
EOF

  assert-csv-file-rows "${__expected_csv_file}" 1 <<EOF
testitem,me,podcast
EOF

}

@test "invoke aio-ia-generate-metadata-file - mixed - new - metadata - dublicates" {

  cd "${__workspace_path}"

  # dublicates on stdin
  local key
  for key in "${__cl_metadata_key_list[@]}"; do

    run ${__bin_path}/aio-ia-generate-metadata-file new \
      -I -C -G <<EOF
${key}
${key}
EOF

    [ ${status} -eq 1 ]
    [ ! -f "${__expected_csv_file_default}" ]
    [ "${lines[0]}" == "error: key [${key}] can only be provided once ... stopping" ]
  
  done
  
  local option
  local expectedKey
  
  # dublicates on cl
  for index in "${!__placeholder_option_list[@]}"; do
  
    option="${__placeholder_option_list[${index}]}"
    expectedKey="${__expected_placeholder_key_list[${index}]}"

    run ${__bin_path}/aio-ia-generate-metadata-file new \
      "${option}" "${option}" <<EOF
creator me
collection podcast
identifier testitem
EOF

    [ ${status} -eq 1 ]
    [ ! -f "${__expected_csv_file}" ]
    [ "${lines[0]}" == "error: the following metadata keys have been provided more than once on cl [${expectedKey}] ... stopping" ]
  
  done
  
  # dublicates on cl and stdin
  for index in "${!__placeholder_option_list[@]}"; do

    option="${__placeholder_option_list[${index}]}"
    expectedKey="${__expected_placeholder_key_list[${index}]}"

    run ${__bin_path}/aio-ia-generate-metadata-file new \
      "${option}" "${option}" <<EOF
${expectedKey}
${expectedKey}
EOF

    [ ${status} -eq 1 ]
    [ ! -f "${__expected_csv_file}" ]
    [ "${lines[0]}" == "error: the following metadata keys have been provided more than once on cl [${expectedKey}] ... stopping" ]
  
  done
}

@test "invoke aio-ia-generate-metadata-file - cl - append - metadata - values" {

  cd "${__workspace_path}"

  local expectedColumnHeader
  local option
  local value
  
  local index
  for index in "${!__creator_collector_option_list[@]}"; do
  
    expectedColumnHeader="${__creator_collector_expected_column_header_list[${index}]}"
    read -r option value <<<"${__creator_collector_option_list[${index}]}"
  
    # generate new file first
    run ${__bin_path}/aio-ia-generate-metadata-file new \
      -i testitem "${option}" 'dummy'

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 2
    
    # now append to csv file
    run ${__bin_path}/aio-ia-generate-metadata-file append \
      -i testitem "${option}" "${value}"

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 3

    assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,${expectedColumnHeader}
EOF

    assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
testitem,dummy
testitem,${value}
EOF

  done
}

@test "invoke aio-ia-generate-metadata-file - cl - append - metadata - value with whitespace" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-metadata-file append \
    -i testitem \
    --creator 'its me'

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 1

  assert-csv-file-rows "${__expected_csv_file}" 1 <<<"testitem,its me"
}

@test "invoke aio-ia-generate-metadata-file - cl - append - metadata - value with whitespace - not allowed" {

  cd "${__workspace_path}"

  local -r metadataWithSpaces=(
    "identifier a identifier"
    "collection a collection"
  )
  
  local metadata
  local key
  local value

  for metadata in "${metadataWithSpaces[@]}"; do
  
    read -r key value <<<"${metadata}"
    # no need to provide identifier as we expect an error during input validation
    run ${__bin_path}/aio-ia-generate-metadata-file append \
      "--${key}" "${value}"

    [ ${status} -eq 1 ]
    # TODO currently, whitespace is not checked on cl
    [ "${output}" == "error: value for key [${key}] contains one or more whitespaces [${value}] ... stopping" ]
  
  done
}

@test "invoke aio-ia-generate-metadata-file - cl - append - metadata - value with comma - not allowed" {

  cd "${__workspace_path}"

  local -r metadataWithComma=(
    "identifier a,identifier"
    "collection a,collection"
    "creator its,me"
  )
  
  local metadata
  local key
  local value

  for metadata in "${metadataWithComma[@]}"; do
  
    read -r key value <<<"${metadata}"
    # no need to provide identifier as we expect an error during input validation
    run ${__bin_path}/aio-ia-generate-metadata-file append \
      "--${key}" "${value}"

    [ ${status} -eq 1 ]
    # TODO currently, comma is not checked on cl
    [ "${output}" == "error: value for key [${key}] contains a comma [${value}] ... stopping" ]
  
  done
}

@test "invoke aio-ia-generate-metadata-file - cl - append - metadata - shortcuts" {

  cd "${__workspace_path}"

  local lineCount
  local header
  local row
  local index
  local collectionShortcutOption
  local expectedCollectionName

  for index in "${!__collection_short_option_shortcut_list[@]}"; do
    
    collectionShortcutOption="${__collection_short_option_shortcut_list[${index}]}"
    expectedCollectionName="${__collection_row_data_list[${index}]}"
    
    # generate new file first
    run ${__bin_path}/aio-ia-generate-metadata-file new \
      --identifier testitem \
      --collection dummy
      
    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 2
  
    # now append to csv file
    run ${__bin_path}/aio-ia-generate-metadata-file append \
      -i testitem \
      "${collectionShortcutOption}"

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 3

    assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,collection
EOF

    assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
testitem,dummy
testitem,${expectedCollectionName}
EOF

  done
  
  for index in "${!__collection_long_option_shortcut_list[@]}"; do
    
    collectionShortcutOption="${__collection_long_option_shortcut_list[${index}]}"
    expectedCollectionName="${__collection_row_data_list[${index}]}"
    
    # generate new file first
    run ${__bin_path}/aio-ia-generate-metadata-file new \
      --identifier testitem \
      --collection dummy
      
    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 2
  
    # now append to csv file
    run ${__bin_path}/aio-ia-generate-metadata-file append \
      -i testitem \
      "${collectionShortcutOption}"

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 3

    assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,collection
EOF

    assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
testitem,dummy
testitem,${expectedCollectionName}
EOF

  done
}

@test "invoke aio-ia-generate-metadata-file - cl - append - metadata - placeholders" {

  cd "${__workspace_path}"

  local lineCount
  local header
  local row
  
  # generate new file first
  run ${__bin_path}/aio-ia-generate-metadata-file new \
    --placeholder-creator \
    --placeholder-collection \
    --placeholder-identifier
  
  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_default}" 2
  
  # now append to csv file
  run ${__bin_path}/aio-ia-generate-metadata-file append \
    -C -G -I
  
  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_default}" 3
  
  assert-csv-file-header "${__expected_csv_file_default}" <<EOF
creator,collection,identifier
EOF

    assert-csv-file-rows "${__expected_csv_file_default}" 2 <<EOF
,,
,,
EOF
}

@test "invoke aio-ia-generate-metadata-file - cl - append - metadata - mixed" {

  cd "${__workspace_path}"

  local lineCount
  local header
  local row
  local index
  
  local collectionShortcutOption
  local expectedCollectionName

  for index in "${!__collection_short_option_shortcut_list[@]}"; do

    collectionShortcutOption="${__collection_short_option_shortcut_list[${index}]}"
    expectedCollectionName="${__collection_row_data_list[${index}]}"

    # generate new file first
    run ${__bin_path}/aio-ia-generate-metadata-file new \
      -c me -g dummy -I

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file_default}" 2
    
    # now append to csv-file
    run ${__bin_path}/aio-ia-generate-metadata-file append \
      -c me "${collectionShortcutOption}" --placeholder-identifier

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file_default}" 3

    assert-csv-file-header "${__expected_csv_file_default}" <<EOF
creator,collection,identifier
EOF

    assert-csv-file-rows "${__expected_csv_file_default}" 2 <<EOF
me,dummy,
me,${expectedCollectionName},
EOF

  done

  for index in "${!__collection_long_option_shortcut_list[@]}"; do
    
    collectionShortcutOption="${__collection_long_option_shortcut_list[${index}]}"
    expectedCollectionName="${__collection_row_data_list[${index}]}"

    # generate new file first
    run ${__bin_path}/aio-ia-generate-metadata-file new \
      -c me -g dummy -I

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file_default}" 2
    
    # now append to csv-file
    run ${__bin_path}/aio-ia-generate-metadata-file append \
      -c me "${collectionShortcutOption}" --placeholder-identifier

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file_default}" 3

    assert-csv-file-header "${__expected_csv_file_default}" <<EOF
creator,collection,identifier
EOF

    assert-csv-file-rows "${__expected_csv_file_default}" 2 <<EOF
me,dummy,
me,${expectedCollectionName},
EOF

  done
}

@test "invoke aio-ia-generate-metadata-file - stdin - append - metadata - read timeout" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-metadata-file append
  
  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file}" ]
  [ "${output}" == 'error: you need to provide metadata item [identifier] ... stopping' ]

  run ${__bin_path}/aio-ia-generate-metadata-file append < <(sleep 2; printf 'identifier testitem')
  
  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file}" ]
  [ "${output}" == 'error: you need to provide metadata item [identifier] ... stopping' ]
}

@test "invoke aio-ia-generate-metadata-file - stdin - append - metadata - comments" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-metadata-file append <<EOF
# a single line comment
date 2020-07-07
description this is a test item
# a
${__line_with_tabs}# multi
${__line_with_whitespaces}# line
${__line_with_tabs}${__line_with_whitespaces}# comment
identifier testitem
title a test item
subject accustic
subject music
subject pop
EOF

  [ "${status}" -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 1
      
  # in append mode, there is only one line
  assert-csv-file-rows "${__expected_csv_file}" 1 <<EOF
2020-07-07,this is a test item,testitem,a test item,accustic,music,pop
EOF

}

@test "invoke aio-ia-generate-metadata-file - stdin - append - metadata - with empty lines" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-metadata-file append <<EOF
# single empty line

date 2020-07-07
description this is a test item
# multiple empty lines with and without whitespaces and tabs

${__line_with_whitespaces}


${__line_with_tabs}
${__line_with_whitespaces}
${__line_with_tabs}${__line_with_whitespaces}
identifier testitem
title a test item
subject accustic
subject music
subject pop
EOF

  [ "${status}" -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 1
      
  # in append mode, there is only one line
  assert-csv-file-rows "${__expected_csv_file}" 1 <<EOF
2020-07-07,this is a test item,testitem,a test item,accustic,music,pop
EOF

}

@test "invoke aio-ia-generate-metadata-file - stdin - append - metadata - empty lines only" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-metadata-file append <<EOF

${__line_with_whitespaces}
${__line_with_tabs}
${__line_with_whitespaces}${__line_with_tabs}
EOF

  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file}" ]
  [ "${output}" == 'error: you need to provide metadata item [identifier] ... stopping' ]

  # empty value
  run ${__bin_path}/aio-ia-generate-metadata-file append <<<""

  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file}" ]
  [ "${output}" == 'error: you need to provide metadata item [identifier] ... stopping' ]

  # empty value - whitespaces only
  run ${__bin_path}/aio-ia-generate-metadata-file append <<<"${__line_with_whitespaces}"

  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file}" ]
  [ "${output}" == 'error: you need to provide metadata item [identifier] ... stopping' ]

  # empty value - tabs only
  run ${__bin_path}/aio-ia-generate-metadata-file append <<<"${__line_with_tabs}"

  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file}" ]
  [ "${output}" == 'error: you need to provide metadata item [identifier] ... stopping' ]

  # empty value - whitespaces and tabs only
  run ${__bin_path}/aio-ia-generate-metadata-file append <<<"${__line_with_whitespaces}${__line_with_tabs}"

  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file}" ]
  [ "${output}" == 'error: you need to provide metadata item [identifier] ... stopping' ]
}

@test "invoke aio-ia-generate-metadata-file - stdin - append - metadata - value with whitespace" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-metadata-file append <<EOF
identifier
title a title
description a description
subject a subject
creator a creator
date c.a. 1020
EOF

  [ "${status}" -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_default}" 1
      
  # in append mode, there is only one line
  assert-csv-file-rows "${__expected_csv_file_default}" 1 <<< ',a title,a description,a subject,a creator,c.a. 1020'

}

@test "invoke aio-ia-generate-metadata-file - stdin - append - metadata - value with whitespace - not allowed" {

  cd "${__workspace_path}"

  local -r metadataWithSpaces=(
    "identifier a identifier"
    "collection a collection"
    "date 1020 2020"
    "date c..a.. 1020"
    "date ca 1020"
    "date c.a. a"
  )
  
  local metadata
  local key
  local value

  for metadata in "${metadataWithSpaces[@]}"; do

    run ${__bin_path}/aio-ia-generate-metadata-file append <<<"${metadata}"
    
    [ ${status} -eq 1 ]
    [ ! -f "${__expected_csv_file}" ]

    read -r key value <<<"${metadata}"
    [ "${output}" == "error: value for key [${key}] contains one or more whitespaces [${value}] ... stopping" ]

  done
}

@test "invoke aio-ia-generate-metadata-file - stdin - append - metadata - value with comma" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-metadata-file append <<EOF
identifier testitem
title a title
description its a description, yeah
EOF

  [ "${status}" -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 1

  printf '%s' "\
this
is a description
, yeah
" >description.txt
  
  run ${__bin_path}/aio-ia-generate-metadata-file append <<EOF
identifier testitem
title a title
description_file description.txt
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 2
  
  # in append mode, there is no header
  assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
testitem,a title,its a description&#44; yeah
testitem,a title,this<br/>is a description<br/>&#44; yeah<br/>
EOF

}

@test "invoke aio-ia-generate-metadata-file - stdin - append - metadata - value with comma - not allowed" {

  cd "${__workspace_path}"

  local -r metadataWithComma=(
    "title a test item, v2"
    "identifier Archive,No.2"
    "date 20200909,20201010"
    "creator i,and,i"
    "collection opensource_audio,podcast"
    "subject music, concert"
    "description_file a,file.txt"
    "subject_file a,file.txt"
  )
  
  local metadata
  local key
  local value

  for metadata in "${metadataWithComma[@]}"; do
  
    read -r key value <<<"${metadata}"
    run ${__bin_path}/aio-ia-generate-metadata-file append <<<"${key} ${value}"

    [ ${status} -eq 1 ]
    [ ! -f "${__expected_csv_file}" ]
    [ "${output}" == "error: value for key [${key}] contains a comma [${value}] ... stopping" ]

  done
}

@test "invoke aio-ia-generate-metadata-file - stdin - append - metadata - key with comma - not allowed" {

  cd "${__workspace_path}"

  local -r metadataWithComma=(
    "a,title a test item"
    "a,identifier testitem"
    "a,date 20200909"
    "a,creator me"
    "a,collection podcast"
    "a,subject music"
    "a,description some description"
    "a,description_file description.txt"
    "a,subject_file subject.txt"
  )
  
  local metadata

  for metadata in "${metadataWithComma[@]}"; do
    
    # no need to provide identifier and mediatype as we expect an error during input validation
    run ${__bin_path}/aio-ia-generate-metadata-file append <<<"${metadata}"

    [ ${status} -eq 1 ]
    
    read -r key value <<<"${metadata}"
    [ "${output}" == "error: key [${key}] contains special char(s) ... stopping" ]

  done
}


@test "invoke aio-ia-generate-metadata-file - stdin - append - metadata - with indented data" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-metadata-file append <<EOF
${__line_with_whitespaces}date 2020-07-07
description${__line_with_whitespaces}this is a test item
${__line_with_whitespaces}${__line_with_tabs}identifier${__line_with_whitespaces}testitem${__line_with_tabs}
${__line_with_tabs}title a test item
${__line_with_whitespaces}${__line_with_tabs}subject${__line_with_tabs}accustic${__line_with_tabs}
subject${__line_with_tabs}music
${__line_with_whitespaces}${__line_with_tabs}subject pop${__line_with_whitespaces}
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 1
  
  # in append mode, there is only one line
  assert-csv-file-rows "${__expected_csv_file}" 1 <<< '2020-07-07,this is a test item,testitem,a test item,accustic,music,pop'

}

@test "invoke aio-ia-generate-metadata-file - stdin - append - metadata - missing identifier" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-metadata-file append  <<<"date 2020-07-07"

  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file}" ]
  [ "${output}" == 'error: you need to provide metadata item [identifier] ... stopping' ]
}

@test "invoke aio-ia-generate-metadata-file - stdin - append - metadata - identifier only" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-metadata-file append  <<<"identifier testitem"
  
  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 1
  
  # in append mode, there is only one line
  assert-csv-file-rows "${__expected_csv_file}" 1 <<< 'testitem'
}

@test "invoke aio-ia-generate-metadata-file - stdin - append - metadata" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-metadata-file append <<EOF
identifier testitem
subject d.i.y. radio show
subject music
subject no borders
subject accustic
subject pop
date 2020-07-07
title a test item
description this is a test item
creator me
collection podcast
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 1
  
  # in append mode, there is only one line
  assert-csv-file-rows "${__expected_csv_file}" 1 \
    <<< 'testitem,d.i.y. radio show,music,no borders,accustic,pop,2020-07-07,a test item,this is a test item,me,podcast'
}

@test "invoke aio-ia-generate-metadata-file - stdin - append - metadata - placeholders" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-metadata-file append <<EOF
identifier
subject
subject
subject
subject
date
title
description
creator
collection
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_default}" 1
  
  # in append mode, there is only one line
  assert-csv-file-rows "${__expected_csv_file_default}" 1 \
    <<< ',,,,,,,,,'
}

@test "invoke aio-ia-generate-metadata-file - stdin - append - metadata - placeholders - not allowed" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-metadata-file append <<EOF
identifier
description_file
EOF

  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file_default}" ]
  [ "${lines[0]}" == 'error: value for key [description_file] is empty ... stopping' ]
  
  # we can pass description and description_file at once, because dublicate check is done
  # after verifying allowed placeholders
  run ${__bin_path}/aio-ia-generate-metadata-file append <<EOF
identifier
description
description_file
EOF

  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file_default}" ]
  [ "${lines[0]}" == 'error: value for key [description_file] is empty ... stopping' ]
  
  run ${__bin_path}/aio-ia-generate-metadata-file append <<EOF
identifier
subject_file
EOF

  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file_default}" ]
  [ "${lines[0]}" == 'error: value for key [subject_file] is empty ... stopping' ]
  
  run ${__bin_path}/aio-ia-generate-metadata-file append <<EOF
identifier
subject
subject_file
EOF

  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file_default}" ]
  [ "${lines[0]}" == 'error: value for key [subject_file] is empty ... stopping' ]
}

@test "invoke aio-ia-generate-metadata-file - stdin - append - metadata - dublicates - not allowed" {

  cd "${__workspace_path}"

  local key
  for key in "${__stdin_metadata_key_list[@]}"; do

    run ${__bin_path}/aio-ia-generate-metadata-file append <<EOF
${key}
${key}
EOF

    [ ${status} -eq 1 ]
    [ ! -f "${__expected_csv_file_default}" ]
    [ "${lines[0]}" == "error: key [${key}] can only be provided once ... stopping" ]
  
  done
  
  run ${__bin_path}/aio-ia-generate-metadata-file append <<EOF
description_file dummy-a
description_file dummy-b
EOF

  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file_default}" ]
  [ "${lines[0]}" == "error: key [description_file] can only be provided once ... stopping" ]
  
  run ${__bin_path}/aio-ia-generate-metadata-file append <<EOF
description_file dummy-a
description a dummy description
EOF

  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file_default}" ]
  [ "${lines[0]}" == "error: only one of metadata keys [description] or [description_file] can be provided at once, not both at the same time ... stopping" ]
  
  run ${__bin_path}/aio-ia-generate-metadata-file append <<EOF
description a dummy description
description_file dummy-a
EOF

  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file_default}" ]
  [ "${lines[0]}" == "error: only one of metadata keys [description] or [description_file] can be provided at once, not both at the same time ... stopping" ]
}

@test "invoke aio-ia-generate-metadata-file - stdin - append - metadata - dublicates - subject values - not allowed" {

  cd "${__workspace_path}"

  printf '%s\n' '
accustic
music
pop
accustic
music
pop
' >subjects.txt
  
  # dublicates only in subjects.txt
  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
subject_file ./subjects.txt
identifier testitem
EOF

  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file}" ]
  [ "${output}" == "error: subject value [accustic] is given more than once ... stopping" ]
  
  printf '%s\n' '
music
accustic
pop
' >subjects.txt

   # dublicates on stdin and in subjects.txt
  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
subject music
subject pop
subject accustic
subject_file ./subjects.txt
identifier testitem
EOF

  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file}" ]
  [ "${output}" == "error: subject value [music] is given more than once ... stopping" ]
  
  # dublicates only on stdin
  run ${__bin_path}/aio-ia-generate-metadata-file new <<EOF
subject music
subject pop
subject accustic
subject pop
subject music
subject accustic
identifier testitem
EOF

  [ ${status} -eq 1 ]
  [ ! -f "${__expected_csv_file}" ]
  [ "${output}" == "error: subject value [pop] is given more than once ... stopping" ]
}

@test "invoke aio-ia-generate-metadata-file - stdin - append - subjects-file" {

  cd "${__workspace_path}"

  printf '%s\n' '
d.i.y. radio show
music
no borders
semi-loud
' >subjects.txt

  run ${__bin_path}/aio-ia-generate-metadata-file append  <<EOF
identifier testitem
subject_file subjects.txt
subject addition
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 1
  
  # in append mode, there is only one line
  assert-csv-file-rows "${__expected_csv_file}" 1 \
    <<< 'testitem,d.i.y. radio show,music,no borders,semi-loud,addition'
}

@test "invoke aio-ia-generate-metadata-file - stdin - append - subjects-file - multiple" {

  cd "${__workspace_path}"

  printf '%s\n' '
d.i.y. radio show
semi-loud
' >subjects.txt

  printf '%s\n' '
music
no borders
' >subjects-common.txt

  run ${__bin_path}/aio-ia-generate-metadata-file append <<EOF
identifier testitem
subject_file subjects.txt
subject_file subjects-common.txt
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 1
  
  # in append mode, there is only one line
  assert-csv-file-rows "${__expected_csv_file}" 1 \
    <<< 'testitem,d.i.y. radio show,semi-loud,music,no borders'
}

@test "invoke aio-ia-generate-metadata-file - stdin - append - subjects-file - comments and empty lines" {

  cd "${__workspace_path}"

  printf '%s\n' "
# whitespaces at begin of line
      d.i.y. radio show
# a comment
music
no borders
# some empty lines


semi-loud

${__line_with_whitespaces}
${__line_with_tabs}
${__line_with_whitespaces}${__line_with_tabs}

# lines containing only whitespaces
        
        
" >subjects.txt

  run ${__bin_path}/aio-ia-generate-metadata-file append <<EOF
identifier testitem
subject_file subjects.txt
EOF
  
  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 1
  
  # in append mode, there is only one line
  assert-csv-file-rows "${__expected_csv_file}" 1 \
    <<< 'testitem,d.i.y. radio show,music,no borders,semi-loud'
}

@test "invoke aio-ia-generate-metadata-file - stdin - append - subjects-file - empty" {

  cd "${__workspace_path}"

  touch subjects.txt

  # subject_file exists but is empty
  run ${__bin_path}/aio-ia-generate-metadata-file append <<EOF
identifier testitem
subject_file ./subjects.txt
EOF

  [ ${status} -eq 1 ]
  [ "${output}" == "error: input-file [./subjects.txt] is empty ... stopping" ]
  
  printf '%s\n' "
# all subjects commented out
# no borders
# music
# some empty lines


# semi-loud
# empty line containing only whitespaces

# d.i.y. radio show


${__line_with_whitespaces}
${__line_with_tabs}
${__line_with_whitespaces}${__line_with_tabs}
" >subjects.txt

  # subject_file only contains comments and empty lines/lines with spaces
  run ${__bin_path}/aio-ia-generate-metadata-file append <<EOF
identifier testitem
subject_file ./subjects.txt
EOF

  [ ${status} -eq 1 ]
  [ "${output}" == "error: input-file [./subjects.txt] is empty ... stopping" ]

}

@test "invoke aio-ia-generate-metadata-file - stdin - append - subjects-file - not existing" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-metadata-file append <<EOF
identifier testitem
subject_file dummy
EOF

  [ ${status} -eq 1 ]
  [ "${output}" == "error: input-file [dummy] does not exist ... stopping" ]
}

@test "invoke aio-ia-generate-metadata-file - stdin - append - subjects-file - with comma" {

  cd "${__workspace_path}"

  printf '%s\n' '
no borders
music
semi, loud
d.i.y. radio show
' >subjects.txt

  run ${__bin_path}/aio-ia-generate-metadata-file append <<EOF
identifier testitem
subject_file ./subjects.txt
EOF

  [ "${status}" -eq 1 ]
  [ "${output}" == 'error: subject [semi, loud] in file [./subjects.txt] contains a comma ... stopping' ]
}

@test "invoke aio-ia-generate-metadata-file - stdin - append - description-file" {

  cd "${__workspace_path}"

  printf '%s' "\
this
is a
test item
" >description.txt

  run ${__bin_path}/aio-ia-generate-metadata-file append <<EOF
date 2020-07-07
description_file ./description.txt
identifier testitem
title a test item
subject accustic
subject music
subject pop
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 1
  
  # in append mode, there is only one line
  assert-csv-file-rows "${__expected_csv_file}" 1 \
    <<< '2020-07-07,this<br/>is a<br/>test item<br/>,testitem,a test item,accustic,music,pop'

}

@test "invoke aio-ia-generate-metadata-file - stdin - append - description-file - empty" {

  cd "${__workspace_path}"

  touch description.txt

  run ${__bin_path}/aio-ia-generate-metadata-file append <<EOF
description_file ./description.txt
identifier testitem
EOF

  [ ${status} -eq 1 ]
  [ "${output}" == "error: input-file [./description.txt] is empty ... stopping" ]

  printf '%s\n' "
${__line_with_whitespaces}
${__line_with_tabs}
${__line_with_whitespaces}${__line_with_tabs}

" >description.txt

  run ${__bin_path}/aio-ia-generate-metadata-file append <<EOF
description_file ./description.txt
identifier testitem
EOF

  [ ${status} -eq 1 ]
  [ "${output}" == "error: input-file [./description.txt] is empty ... stopping" ]

}

@test "invoke aio-ia-generate-metadata-file - stdin - append - description-file - with comma" {

  cd "${__workspace_path}"

  printf '%s' "\
this
is a
test, item
" >description.txt

  run ${__bin_path}/aio-ia-generate-metadata-file append <<EOF
description_file ./description.txt
identifier testitem
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file}" 1
  
  # in append mode, there is only one line
  assert-csv-file-rows "${__expected_csv_file}" 1 \
    <<< 'this<br/>is a<br/>test&#44; item<br/>,testitem'

}

@test "invoke aio-ia-generate-metadata-file - stdin - append - description-file - not existing" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-metadata-file append <<EOF
description_file dummy
identifier testitem
EOF

  [ ${status} -eq 1 ]
  [ "${output}" == "error: input-file [dummy] does not exist ... stopping" ]
}

@test "invoke aio-ia-generate-metadata-file - mixed - append - metadata - values" {

  cd "${__workspace_path}"

  local identifierOption
  local identifierValue
  local creatorOption
  local creatorValue
  local collectionOption
  local collectionValue

  local index
  for index in "${!__identifier_option_list[@]}"; do

    read -r identifierOption identifierValue <<<"${__identifier_option_list[${index}]}"

    # generate new file first
    run ${__bin_path}/aio-ia-generate-metadata-file new \
      -i testitem -C -G

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 2

    # now append to csv-file
    run ${__bin_path}/aio-ia-generate-metadata-file append \
      "${identifierOption}" "${identifierValue}" --collection-test <<EOF
identifier
creator somecreator
EOF
    
    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 3

    assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,creator,collection
EOF

    assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
testitem,,
${identifierValue},somecreator,test_collection
EOF

  done

  for index in "${!__collection_option_list[@]}"; do

    read -r collectionOption collectionValue <<<"${__collection_option_list[${index}]}"

    # generate new file first
    run ${__bin_path}/aio-ia-generate-metadata-file new \
      -i testitem -C -G

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 2

    # now append to csv-file
    run ${__bin_path}/aio-ia-generate-metadata-file append \
      "${collectionOption}" "${collectionValue}" <<EOF
identifier testitem
creator somecreator
EOF
    
    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 3

    assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,creator,collection
EOF

    assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
testitem,,
testitem,somecreator,${collectionValue}
EOF

  done
  
  for index in "${!__creator_option_list[@]}"; do

    read -r creatorOption creatorValue <<<"${__creator_option_list[${index}]}"

    # generate new file first
    run ${__bin_path}/aio-ia-generate-metadata-file new \
      -i testitem -C -G

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 2

    # now append to csv-file
    run ${__bin_path}/aio-ia-generate-metadata-file append \
      "${creatorOption}" "${creatorValue}" <<EOF
identifier testitem
creator
collection dummy
EOF
    
    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 3

    assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,creator,collection
EOF

    assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
testitem,,
testitem,${creatorValue},dummy
EOF

  done
  
  for index in "${!__creator_option_list[@]}"; do

    read -r identifierOption identifierValue <<<"${__identifier_option_list[${index}]}"
    read -r creatorOption creatorValue <<<"${__creator_option_list[${index}]}"
    read -r collectionOption collectionValue <<<"${__collection_option_list[${index}]}"

    # generate new file first
    run ${__bin_path}/aio-ia-generate-metadata-file new \
      -i testitem -C -G

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 2

    # now append to csv-file
    run ${__bin_path}/aio-ia-generate-metadata-file append \
    "${identifierOption}" "${identifierValue}" "${creatorOption}" "${creatorValue}" "${collectionOption}" "${collectionValue}"<<EOF
identifier someid
creator somecreator
collection somecollection
EOF

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 3

    assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,creator,collection
EOF

    assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
testitem,,
${identifierValue},${creatorValue},${collectionValue}
EOF

  done  
}

@test "invoke aio-ia-generate-metadata-file - mixed - append - metadata - shortcuts" {

  cd "${__workspace_path}"

  local lineCount
  local header
  local row
  local index
  local collectionShortcutOption
  local expectedCollectionName

  for index in "${!__collection_short_option_shortcut_list[@]}"; do
    
    collectionShortcutOption="${__collection_short_option_shortcut_list[${index}]}"
    expectedCollectionName="${__collection_row_data_list[${index}]}"
    
    # generate new file first
    run ${__bin_path}/aio-ia-generate-metadata-file new \
      -i testitem -C -G

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 2
    
    # now append to csv-file
    run ${__bin_path}/aio-ia-generate-metadata-file append \
      --identifier testitem "${collectionShortcutOption}" <<EOF
identifier
creator somecreator
collection somecollection
EOF
  
    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 3
      
    assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,creator,collection
EOF

    assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
testitem,,
testitem,somecreator,${expectedCollectionName}
EOF

  done
  
  for index in "${!__collection_long_option_shortcut_list[@]}"; do
    
    collectionShortcutOption="${__collection_long_option_shortcut_list[${index}]}"
    expectedCollectionName="${__collection_row_data_list[${index}]}"
    
    # generate new file first
    run ${__bin_path}/aio-ia-generate-metadata-file new \
      -i testitem -C -G

    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 2
    
    # now append to csv-file
    run ${__bin_path}/aio-ia-generate-metadata-file append \
      --identifier testitem "${collectionShortcutOption}" <<EOF
identifier
creator somecreator
collection somecollection
EOF
  
    [ ${status} -eq 0 ]
    assert-csv-file-is-complete "${__expected_csv_file}" 3
      
    assert-csv-file-header "${__expected_csv_file}" <<EOF
identifier,creator,collection
EOF

    assert-csv-file-rows "${__expected_csv_file}" 2 <<EOF
testitem,,
testitem,somecreator,${expectedCollectionName}
EOF

  done
}

@test "invoke aio-ia-generate-metadata-file - mixed - append - metadata - placeholders" {
  local lineCount
  local header
  local row
  
  # generate new file first
  run ${__bin_path}/aio-ia-generate-metadata-file new \
    -C -G -I
  
  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_default}" 2

  # append to csv-file
  # placeholders on cl
  run ${__bin_path}/aio-ia-generate-metadata-file append \
    -C -G -I <<EOF
creator somecreator
collection somecollection
identifier someid
EOF
  
  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_default}" 3
  
  # append to csv-file
  # placeholders on cl and stdin
  run ${__bin_path}/aio-ia-generate-metadata-file append \
    --placeholder-collection --placeholder-identifier --placeholder-creator <<EOF
creator
collection
identifier
EOF
  
  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_default}" 4

  # append to csv-file
  # placeholders on stdin
  run ${__bin_path}/aio-ia-generate-metadata-file append \
    -I -c me -g podcast <<EOF
creator
collection
identifier
EOF
  
  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_default}" 5
      
  assert-csv-file-header "${__expected_csv_file_default}" <<EOF
creator,collection,identifier
EOF

  assert-csv-file-rows "${__expected_csv_file_default}" 4 <<EOF
,,
,,
,,
me,podcast,
EOF

}

@test "invoke aio-ia-generate-metadata-file - mixed - append - metadata - dublicates" {

  cd "${__workspace_path}"

  # dublicates on stdin
  local key  
  for key in "${__cl_metadata_key_list[@]}"; do

    # directly use append mode, as we expect an error anyways
    run ${__bin_path}/aio-ia-generate-metadata-file append \
      -I -C -G <<EOF
${key}
${key}
EOF

    [ ${status} -eq 1 ]
    [ ! -f "${__expected_csv_file_default}" ]
    [ "${lines[0]}" == "error: key [${key}] can only be provided once ... stopping" ]
  
  done
  
  local option
  local expectedKey
  
  # dublicates on cl
  for index in "${!__placeholder_option_list[@]}"; do
  
    option="${__placeholder_option_list[${index}]}"
    expectedKey="${__expected_placeholder_key_list[${index}]}"

    # directly use append mode, as we expect an error anyways
    run ${__bin_path}/aio-ia-generate-metadata-file append \
      "${option}" "${option}" <<EOF
creator me
collection podcast
identifier testitem
EOF

    [ ${status} -eq 1 ]
    [ ! -f "${__expected_csv_file}" ]
    [ "${lines[0]}" == "error: the following metadata keys have been provided more than once on cl [${expectedKey}] ... stopping" ]
  
  done
  
  # dublicates on cl and stdin
  for index in "${!__placeholder_option_list[@]}"; do

    option="${__placeholder_option_list[${index}]}"
    expectedKey="${__expected_placeholder_key_list[${index}]}"

    # directly use append mode, as we expect an error anyways
    run ${__bin_path}/aio-ia-generate-metadata-file append \
      "${option}" "${option}" <<EOF
${expectedKey}
${expectedKey}
EOF

    [ ${status} -eq 1 ]
    [ ! -f "${__expected_csv_file}" ]
    [ "${lines[0]}" == "error: the following metadata keys have been provided more than once on cl [${expectedKey}] ... stopping" ]
  
  done
}

@test "invoke aio-ia-generate-metadata-file - stdin - new - dry-run" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-metadata-file new --dry-run <<EOF
identifier testitem
subject d.i.y. radio show
subject music
subject no borders
subject semi-loud
EOF

  [ ${status} -eq 0 ]
  [ ! -f "${__expected_csv_file}" ]

  # header on stdout
  [ "${lines[0]}" == 'identifier,subject[0],subject[1],subject[2],subject[3]' ]
  # row on stdout
  [ "${lines[1]}" == 'testitem,d.i.y. radio show,music,no borders,semi-loud' ]

  run ${__bin_path}/aio-ia-generate-metadata-file new -d <<EOF
date 2020-07-07
description this is a test item
identifier testitem
title a test item
subject accustic
subject music
subject pop
EOF

  [ ${status} -eq 0 ]
  [ ! -f "${__expected_csv_file}" ]

  # header on stdout
  [ "${lines[0]}" == 'date,description,identifier,title,subject[0],subject[1],subject[2]' ]
  # row on stdout
  [ "${lines[1]}" == '2020-07-07,this is a test item,testitem,a test item,accustic,music,pop' ]

}

@test "invoke aio-ia-generate-metadata-file - stdin - append - dry-run" {

  cd "${__workspace_path}"

  # only append, do not create column headers
  run ${__bin_path}/aio-ia-generate-metadata-file append --dry-run <<EOF
identifier someitem
subject d.i.y. radio show
title some item
date 2020-07-07
description this is some item
EOF

  [ ${status} -eq 0 ]
  [ ! -f "someitem-ia-metadata.csv" ]

  # only one row on stdout
  [ "${lines[0]}" == 'someitem,d.i.y. radio show,some item,2020-07-07,this is some item' ]

  run ${__bin_path}/aio-ia-generate-metadata-file append -d <<EOF
identifier anotheritem
subject accustic
title another item
date 1999-05-05
description this is another item
EOF

  [ ${status} -eq 0 ]
  [ ! -f "anotheritem-ia-metadata.csv" ]

  # only one row on stdout
  [ "${lines[0]}" == 'anotheritem,accustic,another item,1999-05-05,this is another item' ]
}

@test "invoke aio-ia-generate-metadata-file - stdin - new - custom output file" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-metadata-file new -o "${__expected_csv_file_custom}" <<EOF
identifier testitem
subject d.i.y. radio show
subject music
subject no borders
subject semi-loud
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 2
      
  assert-csv-file-header "${__expected_csv_file_custom}" <<EOF
identifier,subject[0],subject[1],subject[2],subject[3]
EOF

  assert-csv-file-rows "${__expected_csv_file_custom}" 1 <<EOF
testitem,d.i.y. radio show,music,no borders,semi-loud
EOF

  run ${__bin_path}/aio-ia-generate-metadata-file new --output-file "${__expected_csv_file_custom}" <<EOF
date 2020-07-07
description this is a test item
identifier testitem
title a test item
subject accustic
subject music
subject pop
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 2
      
  assert-csv-file-header "${__expected_csv_file_custom}" <<EOF
date,description,identifier,title,subject[0],subject[1],subject[2]
EOF

  assert-csv-file-rows "${__expected_csv_file_custom}" 1 <<EOF
2020-07-07,this is a test item,testitem,a test item,accustic,music,pop
EOF
  
}

@test "invoke aio-ia-generate-metadata-file - stdin - append - custom output file" {

  cd "${__workspace_path}"

  # only append, do not create column headers
  run ${__bin_path}/aio-ia-generate-metadata-file append -o "${__expected_csv_file_custom}" <<EOF
identifier someitem
subject d.i.y. radio show
title some item
date 2020-07-07
description this is some item
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 1

  run ${__bin_path}/aio-ia-generate-metadata-file append --output-file "${__expected_csv_file_custom}" <<EOF
identifier anotheritem
subject accustic
title another item
date 1999-05-05
description this is another item
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 2

  assert-csv-file-rows "${__expected_csv_file_custom}" 2 <<EOF
someitem,d.i.y. radio show,some item,2020-07-07,this is some item
anotheritem,accustic,another item,1999-05-05,this is another item
EOF

  # create a new file with column headers first
  run ${__bin_path}/aio-ia-generate-metadata-file new -o "${__expected_csv_file_custom}" <<EOF
identifier someitem
subject d.i.y. radio show
title some item
date 2020-07-07
description this is some item
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 2

  run ${__bin_path}/aio-ia-generate-metadata-file append --output-file "${__expected_csv_file_custom}" <<EOF
identifier anotheritem
subject accustic
title another item
date 1999-05-05
description this is another item
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 3
  
  assert-csv-file-header "${__expected_csv_file_custom}" <<EOF
identifier,subject[0],title,date,description
EOF

  assert-csv-file-rows "${__expected_csv_file_custom}" 2 <<EOF
someitem,d.i.y. radio show,some item,2020-07-07,this is some item
anotheritem,accustic,another item,1999-05-05,this is another item
EOF
  
}

@test "invoke aio-ia-generate-metadata-file - stdin - append - custom output file - existing - no linefeed" {

  cd "${__workspace_path}"

  # just write column headers into custom csv-file
  # NOTE the written line does not end with a linefeed => printf '%s'
  printf '%s' "title,description,date,identifier,subject[0]" >"${__expected_csv_file_custom}"

  run ${__bin_path}/aio-ia-generate-metadata-file append -o "${__expected_csv_file_custom}" <<EOF
title some item
description this is some item
date 2020-07-07
identifier someitem
subject d.i.y. radio show
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 2

  run ${__bin_path}/aio-ia-generate-metadata-file append --output-file "${__expected_csv_file_custom}" <<EOF
title another item
description this is another item
date 1999-05-05
identifier anotheritem
subject accustic
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 3
  
  assert-csv-file-header "${__expected_csv_file_custom}" <<EOF
title,description,date,identifier,subject[0]
EOF

  assert-csv-file-rows "${__expected_csv_file_custom}" 2 <<EOF
some item,this is some item,2020-07-07,someitem,d.i.y. radio show
another item,this is another item,1999-05-05,anotheritem,accustic
EOF

}

@test "invoke aio-ia-generate-metadata-file - stdin - append - custom output file - existing - with linefeed" {

  cd "${__workspace_path}"

  # just write column headers into custom csv-file
  # NOTE the written line does end with a linefeed => printf '%s\n'
  printf '%s\n' "title,description,date,identifier,subject[0]" >"${__expected_csv_file_custom}"

  run ${__bin_path}/aio-ia-generate-metadata-file append -o "${__expected_csv_file_custom}" <<EOF
title some item
description this is some item
date 2020-07-07
identifier someitem
subject d.i.y. radio show
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 2

  run ${__bin_path}/aio-ia-generate-metadata-file append --output-file "${__expected_csv_file_custom}" <<EOF
title another item
description this is another item
date 1999-05-05
identifier anotheritem
subject accustic
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 3
  
  assert-csv-file-header "${__expected_csv_file_custom}" <<EOF
title,description,date,identifier,subject[0]
EOF

  assert-csv-file-rows "${__expected_csv_file_custom}" 2 <<EOF
some item,this is some item,2020-07-07,someitem,d.i.y. radio show
another item,this is another item,1999-05-05,anotheritem,accustic
EOF

}

@test "invoke aio-ia-generate-metadata-file - stdin - append - custom output file - mixed up order" {

  cd "${__workspace_path}"

  # just write column headers into custom csv-file
  # NOTE the written line does end with a linefeed => printf '%s\n'
  printf '%s\n' "title,description,date,identifier,subject[0]" >"${__expected_csv_file_custom}"

  run ${__bin_path}/aio-ia-generate-metadata-file append --output-file "${__expected_csv_file_custom}" <<EOF
identifier anotheritem
subject accustic
subject music
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 2

  run ${__bin_path}/aio-ia-generate-metadata-file append --output-file "${__expected_csv_file_custom}" <<EOF
title some title
description some description
creator me
collection podcast
identifier someitem
EOF

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 3
  
  assert-csv-file-header "${__expected_csv_file_custom}" <<EOF
title,description,date,identifier,subject[0]
EOF

  # csv-file row content does not match the column headers
  assert-csv-file-rows "${__expected_csv_file_custom}" 2 <<EOF
anotheritem,accustic,music
some title,some description,me,podcast,someitem
EOF
}

@test "invoke aio-ia-generate-metadata-file - cl - new - dry-run" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-metadata-file new --dry-run \
    --identifier testitem --creator me --collection podcast

  [ ${status} -eq 0 ]
  [ ! -f "${__expected_csv_file}" ]

  # header on stdout
  [ "${lines[0]}" == 'identifier,creator,collection' ]
  # row on stdout
  [ "${lines[1]}" == 'testitem,me,podcast' ]

  run ${__bin_path}/aio-ia-generate-metadata-file new -d \
    --identifier testitem --creator me --collection podcast

  [ ${status} -eq 0 ]
  [ ! -f "${__expected_csv_file}" ]

  # header on stdout
  [ "${lines[0]}" == 'identifier,creator,collection' ]
  # row on stdout
  [ "${lines[1]}" == 'testitem,me,podcast' ]

}

@test "invoke aio-ia-generate-metadata-file - cl - append - dry-run" {

  cd "${__workspace_path}"

  # only append, do not create column headers
  run ${__bin_path}/aio-ia-generate-metadata-file append --dry-run \
    --identifier someitem --creator me --collection podcast

  [ ${status} -eq 0 ]
  [ ! -f "someitem-ia-metadata.csv" ]

  # only one row on stdout
  [ "${lines[0]}" == 'someitem,me,podcast' ]

  run ${__bin_path}/aio-ia-generate-metadata-file append -d \
    --identifier anotheritem --creator me --collection podcast

  [ ${status} -eq 0 ]
  [ ! -f "anotheritem-ia-metadata.csv" ]

  # only one row on stdout
  [ "${lines[0]}" == 'anotheritem,me,podcast' ]
}

@test "invoke aio-ia-generate-metadata-file - cl - new - custom output file" {

  cd "${__workspace_path}"

  run ${__bin_path}/aio-ia-generate-metadata-file new -o "${__expected_csv_file_custom}" \
    --identifier someitem --creator me --collection podcast

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 2

  assert-csv-file-header "${__expected_csv_file_custom}" <<EOF
identifier,creator,collection
EOF

  assert-csv-file-rows "${__expected_csv_file_custom}" 1 <<EOF
someitem,me,podcast
EOF

  run ${__bin_path}/aio-ia-generate-metadata-file new --output-file "${__expected_csv_file_custom}" \
    --identifier anotheritem --creator you --collection test_collection

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 2

  assert-csv-file-header "${__expected_csv_file_custom}" <<EOF
identifier,creator,collection
EOF

  assert-csv-file-rows "${__expected_csv_file_custom}" 1 <<EOF
anotheritem,you,test_collection
EOF
  
}

@test "invoke aio-ia-generate-metadata-file - cl - append - custom output file" {

  cd "${__workspace_path}"

  # only append, do not create column headers
  run ${__bin_path}/aio-ia-generate-metadata-file append -o "${__expected_csv_file_custom}" \
    --identifier someitem --creator me --collection podcast
  
  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 1

  run ${__bin_path}/aio-ia-generate-metadata-file append --output-file "${__expected_csv_file_custom}" \
    --identifier anotheritem --creator you --collection test_collection

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 2

  assert-csv-file-rows "${__expected_csv_file_custom}" 2 <<EOF
someitem,me,podcast
anotheritem,you,test_collection
EOF

  # create a new file with column headers first
  run ${__bin_path}/aio-ia-generate-metadata-file new -o "${__expected_csv_file_custom}" \
    --identifier someitem --creator me --collection podcast

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 2

  run ${__bin_path}/aio-ia-generate-metadata-file append --output-file "${__expected_csv_file_custom}" \
    --identifier anotheritem --creator you --collection test_collection

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 3
  
  assert-csv-file-header "${__expected_csv_file_custom}" <<EOF
identifier,creator,collection
EOF

  assert-csv-file-rows "${__expected_csv_file_custom}" 2 <<EOF
someitem,me,podcast
anotheritem,you,test_collection
EOF
  
}

@test "invoke aio-ia-generate-metadata-file - cl - append - custom output file - existing - no linefeed" {

  cd "${__workspace_path}"

  # just write column headers into custom csv-file
  # NOTE the written line does not end with a linefeed => printf '%s'
  printf '%s' "identifier,creator,collection" >"${__expected_csv_file_custom}"

  run ${__bin_path}/aio-ia-generate-metadata-file append -o "${__expected_csv_file_custom}" \
    --identifier someitem --creator me --collection podcast

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 2

  run ${__bin_path}/aio-ia-generate-metadata-file append --output-file "${__expected_csv_file_custom}" \
    --identifier anotheritem --creator you --collection test_collection

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 3
  
  assert-csv-file-header "${__expected_csv_file_custom}" <<EOF
identifier,creator,collection
EOF

  assert-csv-file-rows "${__expected_csv_file_custom}" 2 <<EOF
someitem,me,podcast
anotheritem,you,test_collection
EOF

}

@test "invoke aio-ia-generate-metadata-file - cl - append - custom output file - existing - with linefeed" {

  cd "${__workspace_path}"

  # just write column headers into custom csv-file
  # NOTE the written line does end with a linefeed => printf '%s\n'
  printf '%s\n' "identifier,creator,collection" >"${__expected_csv_file_custom}"

  run ${__bin_path}/aio-ia-generate-metadata-file append -o "${__expected_csv_file_custom}" \
    --identifier someitem --creator me --collection podcast

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 2

  run ${__bin_path}/aio-ia-generate-metadata-file append --output-file "${__expected_csv_file_custom}" \
    --identifier anotheritem --creator you --collection test_collection

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 3
  
  assert-csv-file-header "${__expected_csv_file_custom}" <<EOF
identifier,creator,collection
EOF

  assert-csv-file-rows "${__expected_csv_file_custom}" 2 <<EOF
someitem,me,podcast
anotheritem,you,test_collection
EOF

}

@test "invoke aio-ia-generate-metadata-file - cl - append - custom output file - mixed up order" {

  cd "${__workspace_path}"

  # just write column headers into custom csv-file
  # NOTE the written line does end with a linefeed => printf '%s\n'
  printf '%s\n' "title,description,date,identifier,subject[0]" >"${__expected_csv_file_custom}"

  run ${__bin_path}/aio-ia-generate-metadata-file append --output-file "${__expected_csv_file_custom}" \
    --identifier someitem

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 2
    
  run ${__bin_path}/aio-ia-generate-metadata-file append --output-file "${__expected_csv_file_custom}" \
    --collection test_collection --creator you --identifier anotheritem

  [ ${status} -eq 0 ]
  assert-csv-file-is-complete "${__expected_csv_file_custom}" 3
  
  assert-csv-file-header "${__expected_csv_file_custom}" <<EOF
title,description,date,identifier,subject[0]
EOF

  # csv-file row content does not match the column headers
  assert-csv-file-rows "${__expected_csv_file_custom}" 2 <<EOF
someitem
test_collection,you,anotheritem
EOF
}

@test "invoke aio-ia-generate-metadata-file - cl - template - default" {

  run ${__bin_path}/aio-ia-generate-metadata-file template
  
  [ ${status} -eq 0 ]

  [ "${lines[0]}" == "identifier" ]
  [ "${lines[1]}" == "creator" ]
  [ "${lines[2]}" == "collection" ]
  [ "${lines[3]}" == "date" ]
  [ "${lines[4]}" == "description" ]
  [ "${lines[5]}" == "description_file" ]
  [ "${lines[6]}" == "subject" ]
  [ "${lines[7]}" == "subject_file" ]
  [ "${lines[8]}" == "title" ]
}

@test "invoke aio-ia-generate-metadata-file - cl - template - placeholders" {
  run ${__bin_path}/aio-ia-generate-metadata-file template \
    -I -C -G
  
  [ ${status} -eq 0 ]

  [ "${lines[0]}" == "identifier" ]
  [ "${lines[1]}" == "creator" ]
  [ "${lines[2]}" == "collection" ]
  
  run ${__bin_path}/aio-ia-generate-metadata-file template \
    --placeholder-identifier \
    --placeholder-creator \
    --placeholder-collection
  
  [ ${status} -eq 0 ]

  [ "${lines[0]}" == "identifier" ]
  [ "${lines[1]}" == "creator" ]
  [ "${lines[2]}" == "collection" ]
}

@test "invoke aio-ia-generate-metadata-file - cl - template - values" {
  run ${__bin_path}/aio-ia-generate-metadata-file template \
    -c 'its me' \
    -g podcast \
    -i someitem
  
  [ ${status} -eq 0 ]

  [ "${lines[0]}" == "creator its me" ]
  [ "${lines[1]}" == "collection podcast" ]
  [ "${lines[2]}" == "identifier someitem" ]
  
  run ${__bin_path}/aio-ia-generate-metadata-file template \
    --creator 'its me' \
    --collection podcast \
    --identifier someitem
  
  [ ${status} -eq 0 ]

  [ "${lines[0]}" == "creator its me" ]
  [ "${lines[1]}" == "collection podcast" ]
  [ "${lines[2]}" == "identifier someitem" ]
  
  # in template mode, identifier is not required
  run ${__bin_path}/aio-ia-generate-metadata-file template \
    --creator 'its me' \
    --collection podcast \
  
  [ ${status} -eq 0 ]

  [ "${lines[0]}" == "creator its me" ]
  [ "${lines[1]}" == "collection podcast" ]

}

@test "invoke aio-ia-generate-metadata-file - cl - template - shortcuts" {
  
  local index
  local collectionShortcutOption
  local expectedCollectionValue

  for index in "${!__collection_short_option_shortcut_list[@]}"; do
    collectionShortcutOption="${__collection_short_option_shortcut_list[${index}]}"
    expectedCollectionValue="${__collection_row_data_list[${index}]}"
    
    run ${__bin_path}/aio-ia-generate-metadata-file template \
        "${collectionShortcutOption}" -i someitem
        
    [ ${status} -eq 0 ]

    [ "${lines[0]}" == "collection ${expectedCollectionValue}" ]
    [ "${lines[1]}" == "identifier someitem" ]
    
  done
  
  for index in "${!__collection_long_option_shortcut_list[@]}"; do
    collectionShortcutOption="${__collection_long_option_shortcut_list[${index}]}"
    expectedCollectionValue="${__collection_row_data_list[${index}]}"
    
    run ${__bin_path}/aio-ia-generate-metadata-file template \
        "${collectionShortcutOption}" -i someitem
        
    [ ${status} -eq 0 ]

    [ "${lines[0]}" == "collection ${expectedCollectionValue}" ]
    [ "${lines[1]}" == "identifier someitem" ]
    
  done
}

@test "invoke aio-ia-generate-metadata-file - cl - template - mixed" {
  run ${__bin_path}/aio-ia-generate-metadata-file template \
    -I -c me --gi
  
  [ ${status} -eq 0 ]

  [ "${lines[0]}" == "identifier" ]
  [ "${lines[1]}" == "creator me" ]
  [ "${lines[2]}" == "collection opensource_image" ]
  
}

@test "invoke aio-ia-generate-metadata-file - cl - template - dublicates" {
  
  # in template mode, dublicates are allowed
  run ${__bin_path}/aio-ia-generate-metadata-file template \
    -I -c me -c you --gi -i anotheritem --collection podcast
  
  [ ${status} -eq 0 ]

  [ "${lines[0]}" == "identifier" ]
  [ "${lines[1]}" == "creator me" ]
  [ "${lines[2]}" == "creator you" ]
  [ "${lines[3]}" == "collection opensource_image" ]
  [ "${lines[4]}" == "identifier anotheritem" ]
  [ "${lines[5]}" == "collection podcast" ]
}
