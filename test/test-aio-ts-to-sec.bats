#!/usr/bin/env bats

# This file is part of aio-proc
#
#     aio-proc is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     aio-proc is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with aio-proc. If not, see <http://www.gnu.org/licenses/>.

##########
# Testdata
###########

declare -A -r -g __valid_timestamps=(
  ["00:00:01.000"]=1
  ["00:01.000"]=1
  ["01.000"]=1
  ["1"]=1
  [".1"]=0.1
  ["00:05:00.000"]=300
  ["05:00.000"]=300
  ["05:00"]=300
  ["5:0"]=300
  ["00:09:27.900"]=567.9
  ["02:46:41.001"]=10001.001
  ["23:59:59.999"]=86399.999
  ["23:59:59.999999"]=86399.999999
)

# the timestamp formats are not valid. they contain
# - invalid characters
# - invalid timestamp parts
declare -r -g __invalid_timestamp_formats=(
  "00 00 01.001"
  "00:00:01.001 99"
  "00:00:01:001"
  "00.00.01.001"
  "00:00:01:001:"
  "00.00.01.001."
  "00::00::00.001"
  "-00:00:01.000"
  "-0.00001"
  "abc"
  "05,00.00"
  "02:46:41.001.99"
  "23:59:59.a999"
  "23:59:59:00:00.999.00.00:99"
)

# the timestamp values are not valid. they contain
# - values that exceed the maximum possible values for hh:mm:ss.ms
declare -r -g __invalid_timestamp_values=(
  "60"
  "60:60"
  "60:59"
  "24:60:60"
  "24:59:59"
)

declare -r -g __valid_invalid_timestamps=(
  "00:00:01.000"
  "-0.00"
  "abc"
  "00:05:00.000"
  "00:09:27.900"
  "-00:05:00.000"
)

###########
# Helpers
###########

declare -g -r __bin_path='..'

###########
# Tests
###########

@test "invoke aio-ts-to-sec - help" {

  run ${__bin_path}/aio-mix -h
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-ts-to-sec --help
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-ts-to-sec h
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-ts-to-sec help
  [ ${status} -eq 0 ]
}

@test "invoke aio-ts-to-sec - help short" {

  run ${__bin_path}/aio-ts-to-sec help short
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-ts-to-sec help s
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-ts-to-sec h s
  [ ${status} -eq 0 ]
}

@test "invoke aio-ts-to-sec - help details" {

  run ${__bin_path}/aio-ts-to-sec help details
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-ts-to-sec help d
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-ts-to-sec h d
  [ ${status} -eq 0 ]
}

@test "invoke aio-ts-to-sec - help examples" {

  run ${__bin_path}/aio-ts-to-sec help examples
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-ts-to-sec help e
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-ts-to-sec h e
  [ ${status} -eq 0 ]
}

@test "invoke aio-ts-to-sec --version" {
  run ${__bin_path}/aio-ts-to-sec --version
  [ ${status} -eq 0 ]
}

@test "invoke aio-ts-to-sec - no timestamps" {
  run ${__bin_path}/aio-ts-to-sec
  [ ${status} -eq 1 ]
  [ "${output}" == "error: no timestamps provided ... stopping" ]

  run ${__bin_path}/aio-ts-to-sec ""
  [ ${status} -eq 1 ]
  [ "${output}" == "error: no timestamps provided ... stopping" ]

  run ${__bin_path}/aio-ts-to-sec "" "" "  "
  [ ${status} -eq 1 ]
  [ "${output}" == "error: no timestamps provided ... stopping" ]
}

@test "invoke aio-ts-to-sec - valid timestamps" {
  local -r expectedSecs=("${__valid_timestamps[@]}")

  run ${__bin_path}/aio-ts-to-sec "${!__valid_timestamps[@]}"

  [ ${status} -eq 0 ]
  [ ${#lines[*]} -eq ${#expectedSecs[*]} ]

  for i in "${!expectedSecs[@]}"; do
    [ "${lines[$i]}" == "${expectedSecs[$i]}" ]
  done
}

@test "invoke aio-ts-to-sec - invalid timestamps - format" {
  run ${__bin_path}/aio-ts-to-sec "${__invalid_timestamp_formats[@]}"
  [ ${status} -eq 1 ]
  [ "${output}" == "error: the following values are invalid [${__invalid_timestamp_formats[*]}] ... stopping" ]
}

@test "invoke aio-ts-to-sec - invalid timestamps - value range" {
  run ${__bin_path}/aio-ts-to-sec "${__invalid_timestamp_values[@]}"
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: some parts of the given timestamp exceed their maximum values ... stopping" ]
}

@test "invoke aio-ts-to-sec - mixed valid and invalid timestamps" {
  run ${__bin_path}/aio-ts-to-sec "${__valid_invalid_timestamps[@]}"
  [ ${status} -eq 1 ]
  [ "${output}" == "error: the following values are invalid [-0.00 abc -00:05:00.000] ... stopping" ]
}

@test "invoke aio-ts-to-sec - stdin - no timestamps - read timeout" {

  # no value at all - should trigger timeout
  run ${__bin_path}/aio-ts-to-sec --

  [ ${status} -eq 1 ]
  [ "${output}" == "error: no timestamps provided ... stopping" ]

  # value available after delay - should trigger timeout
  run ${__bin_path}/aio-ts-to-sec -- < <(sleep 2; printf '1\n')

  [ ${status} -eq 1 ]
  [ "${output}" == "error: no timestamps provided ... stopping" ]
}

@test "invoke aio-ts-to-sec - stdin - no timestamps - empty lines only" {

  local -r lineWithWhitespaces='      '
  local -r lineWithTabs="$(printf '\t\t\t')"

  # empty value
  run ${__bin_path}/aio-ts-to-sec -- <<<""
  [ ${status} -eq 1 ]
  [ "${output}" == "error: no timestamps provided ... stopping" ]

  # empty value - whitespaces only
  run ${__bin_path}/aio-ts-to-sec -- <<<"${lineWithWhitespaces}"
  [ ${status} -eq 1 ]
  [ "${output}" == "error: no timestamps provided ... stopping" ]

  # empty value - tabs only
  run ${__bin_path}/aio-ts-to-sec -- <<<"${lineWithTabs}"
  [ ${status} -eq 1 ]
  [ "${output}" == "error: no timestamps provided ... stopping" ]

  # empty value - tabs and whitespaces only
  run ${__bin_path}/aio-ts-to-sec -- <<<"${lineWithTabs}${lineWithWhitespaces}"
  [ ${status} -eq 1 ]
  [ "${output}" == "error: no timestamps provided ... stopping" ]

  # lines are empty
  run ${__bin_path}/aio-ts-to-sec -- <<EOF
${lineWithWhitespaces}
${lineWithTabs}
${lineWithWhitespaces}${lineWithTabs}
EOF

  [ ${status} -eq 1 ]
  [ "${output}" == "error: no timestamps provided ... stopping" ]
}

@test "invoke aio-ts-to-sec - stdin - with comments - heredoc" {

  local -r indentWhitespaces='      '
  local -r indentTabs="$(printf '\t\t')"

  run ${__bin_path}/aio-ts-to-sec -- <<EOF
# a single line comment
01:01
02:02
# a
${indentTabs}# multi
${indentWhitespaces}# line
${indentTabs}${indentWhitespaces}# comment
03:03
EOF

  [ ${status} -eq 0 ]

  [ "${lines[0]}" == "61" ]
  [ "${lines[1]}" == "122" ]
  [ "${lines[2]}" == "183" ]
}

@test "invoke aio-ts-to-sec - stdin - with empty lines - heredoc" {

  local -r lineWithWhitespaces='      '
  local -r lineWithTabs="$(printf '\t\t\t')"

  run ${__bin_path}/aio-ts-to-sec -- <<EOF
# single empty line

01:01
# empty line containing only whitespaces
${lineWithWhitespaces}
# empty line containing only tabs
${lineWithTabs}
02:02
# multiple empty lines with and without whitespaces and tabs

${lineWithWhitespaces}


${lineWithTabs}
${lineWithWhitespaces}
${lineWithTabs}${lineWithWhitespaces}
03:03
EOF

  [ ${status} -eq 0 ]

  [ "${lines[0]}" == "61" ]
  [ "${lines[1]}" == "122" ]
  [ "${lines[2]}" == "183" ]
}

@test "invoke aio-ts-to-sec - stdin - with indented data - heredoc" {

  local -r indentWhitespaces='      '
  local -r indentTabs="$(printf '\t\t')"

  run ${__bin_path}/aio-ts-to-sec -- <<EOF
${indentWhitespaces}01:01${indentTabs}
${indentTabs}02:02
${indentTabs}${indentWhitespaces}03:03${indentWhitespaces}
EOF

  [ ${status} -eq 0 ]

  [ "${lines[0]}" == "61" ]
  [ "${lines[1]}" == "122" ]
  [ "${lines[2]}" == "183" ]
}

@test "invoke aio-ts-to-sec - stdin - valid timestamps" {
  local -r expectedSecs=("${__valid_timestamps[@]}")

  run ${__bin_path}/aio-ts-to-sec "${!__valid_timestamps[@]}"

  [ ${status} -eq 0 ]
  [ ${#lines[*]} -eq ${#expectedSecs[*]} ]

  for i in "${!expectedSecs[@]}"; do
    [ "${lines[$i]}" == "${expectedSecs[$i]}" ]
  done
}

@test "invoke aio-ts-to-sec - stdin - invalid timestamps - format" {
  run ${__bin_path}/aio-ts-to-sec -- < <( printf '%s\n' "${__invalid_timestamp_formats[@]}" )
  [ ${status} -eq 1 ]
  [ "${output}" == "error: the following values are invalid [${__invalid_timestamp_formats[*]}] ... stopping" ]
}

@test "invoke aio-ts-to-sec - stdin - invalid timestamps - value range" {
  run ${__bin_path}/aio-ts-to-sec -- < <( printf '%s\n' "${__invalid_timestamp_values[@]}" )
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: some parts of the given timestamp exceed their maximum values ... stopping" ]
}

@test "invoke aio-ts-to-sec - stdin - mixed valid and invalid timestamps" {
  run ${__bin_path}/aio-ts-to-sec -- < <( printf '%s\n' "${__valid_invalid_timestamps[@]}" )
  [ ${status} -eq 1 ]
  [ "${output}" == "error: the following values are invalid [-0.00 abc -00:05:00.000] ... stopping" ]
}
