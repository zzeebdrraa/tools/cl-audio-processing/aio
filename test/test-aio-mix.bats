#!/usr/bin/env bats

# This file is part of aio-proc
#
#     aio-proc is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     aio-proc is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with aio-proc. If not, see <http://www.gnu.org/licenses/>.

###########
# TODO
#
#   - test output file with no extension
#   - test input files with no extension
#   - test --force-overwrite cl-option
#   - test invalid quality settings
#
# more:
#
#   - use bats hooks setup_file() amd teardown_file() in order to create temporary working dirs
#
###########

###########
# Test Data
###########

declare -g -r testOutFile="test-out-mix.ogg"
declare -g -r testInFileA="test-in-a.ogg"
declare -g -r testInFileB="test-in-b.ogg"

declare -g -r testInFileMp3A="test-in-a.mp3"
declare -g -r testOutFileMp3="test-out-mix.mp3"

###########
# Helpers
###########

declare -g -r __bin_path='..'

load test-aio-common-file-generators
load test-aio-common-asserts

###########
# Setup
###########

setup() {
  rm -f "${testOutFile}"
  rm -f "${testOutFileMp3}"
  rm -f "${testInFileA}"
  rm -f "${testInFileMp3A}"
  rm -f "${testInFileB}"
}

teardown() {
  rm -f "${testOutFile}"
  rm -f "${testOutFileMp3}"
  rm -f "${testInFileA}"
  rm -f "${testInFileMp3A}"
  rm -f "${testInFileB}"
}

###########
# Tests
###########

@test "invoke aio-mix - help" {

  run ${__bin_path}/aio-mix -h
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-mix --help
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-mix h
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-mix help
  [ ${status} -eq 0 ]
}

@test "invoke aio-mix - help short" {

  run ${__bin_path}/aio-mix help short
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-mix help s
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-mix h s
  [ ${status} -eq 0 ]
}

@test "invoke aio-mix - help details" {

  run ${__bin_path}/aio-mix help details
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-mix help d
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-mix h d
  [ ${status} -eq 0 ]
}

@test "invoke aio-mix - help examples" {

  run ${__bin_path}/aio-mix help examples
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-mix help e
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-mix h e
  [ ${status} -eq 0 ]
}

@test "invoke aio-mix --version" {
  run ${__bin_path}/aio-mix --version
  [ ${status} -eq 0 ]
}

@test "invoke aio-mix - insufficient number of input files" {
  run ${__bin_path}/aio-mix

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: at least two input files must be specified ... stopping' ]

  run ${__bin_path}/aio-mix dummyIn1

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: at least two input files must be specified ... stopping' ]
}

@test "invoke aio-mix - non-existing input files" {
  run ${__bin_path}/aio-mix dummyIn1 dummyIn2

  [ ${status} -eq 1 ]

  [ "${#lines[*]}" -eq 2 ]
  [ "${lines[0]}" == 'error: input file [dummyIn1] does not exist ... skipping' ]
  [ "${lines[1]}" == 'error: input file [dummyIn2] does not exist ... skipping' ]
}

@test "invoke aio-mix - non-audio input file" {
  rm -f noaudioIn1 noaudioIn2 dummyOut
  touch noaudioIn1 noaudioIn2 dummyOut
  echo "text" > noaudioIn2

  run ${__bin_path}/aio-mix noaudioIn1 noaudioIn2

  [ ${status} -eq 1 ]

  [ "${#lines[*]}" -eq 2 ]
  [ "${lines[0]}" == 'error: input file [noaudioIn1] is no audio file ... skipping' ]
  [ "${lines[1]}" == 'error: input file [noaudioIn2] is no audio file ... skipping' ]

  aio-generate-ogg-file-sine "${testInFileA}"
  run ${__bin_path}/aio-mix "${testInFileA}" noaudioIn2

  [ ${status} -eq 1 ]

  [ "${output}" == 'error: input file [noaudioIn2] is no audio file ... skipping' ]

  rm -f noaudioIn1 noaudioIn2 dummyOut
}

@test "invoke aio-mix - output file - no extension" {
  aio-generate-ogg-file-sine "${testInFileA}"
  rm -f dummyOut dummyOut.ogg
  touch dummyOut

  # mix fails due to invalid output file without extension
  run ${__bin_path}/aio-mix -o dummyOut "${testInFileA}" "${testInFileA}"

  [ ${status} -eq 0 ]
  [ -f dummyOut.ogg ]

  rm -f dummyOut dummyOut.ogg
}

@test "invoke aio-mix - mix - short" {
  aio-generate-ogg-file-noise "${testInFileA}" 4 0.5
  aio-generate-ogg-file-sine "${testInFileB}" 5 4

  # output file is expected to have a length of 4 sec
  # as we pass the shorter audio file first
  run ${__bin_path}/aio-mix -o "${testOutFile}" "${testInFileA}" "${testInFileB}"

  [ ${status} -eq 0 ]
  [ -f "${testOutFile}" ]

  aio-assert-duration-ffmpeg "${testOutFile}" 4
  aio-assert-volume-begin-ffmpeg "${testOutFile}" 4 -1.5 -4.5 -12 -13.5
}

@test "invoke aio-mix - mix - long" {
  aio-generate-ogg-file-noise "${testInFileA}" 4 0.5
  aio-generate-ogg-file-sine "${testInFileB}" 5 4

  # output file is expected to have a length of 5sec
  # as we pass the longer audio file first
  # the last second is supposed to consists of only
  # the audio of testInFileB (a sine wave)
  run ${__bin_path}/aio-mix -o "${testOutFile}" "${testInFileB}" "${testInFileA}"

  [ ${status} -eq 0 ]
  [ -f "${testOutFile}" ]

  aio-assert-duration-ffmpeg "${testOutFile}" 5
  aio-assert-volume-begin-ffmpeg "${testOutFile}" 4 -1.5 -4.5 -12 -13.5
  aio-assert-volume-end-ffmpeg "${testOutFile}" 4 -5.8 -5.8 -8 -10
}

@test "invoke aio-mix - mix - ogg and mp3 - mp3 out - long" {

  aio-generate-mp3-file-noise "${testInFileMp3A}" 4 0.5
  aio-generate-ogg-file-sine "${testInFileB}" 5 4

  # output file is expected to have a length of 5sec
  # the last second is supposed to consists of only
  # the audio of testInFileB (a sine wave)
  run ${__bin_path}/aio-mix -o "${testOutFileMp3}" "${testInFileB}" "${testInFileMp3A}"

  printf '# %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -f "${testOutFileMp3}" ]

  aio-assert-duration-ffmpeg "${testOutFileMp3}" 5
  aio-assert-volume-begin-ffmpeg "${testOutFileMp3}" 4 -1.5 -4.5 -12 -14
  aio-assert-volume-end-ffmpeg "${testOutFileMp3}" 4 -5 -6.4 -8.5 -9.5
}

@test "invoke aio-mix - mix - ogg and mp3 - mp3 out - low quality - long" {

  aio-generate-mp3-file-noise "${testInFileMp3A}" 4 0.5
  aio-generate-ogg-file-sine "${testInFileB}" 5 4

  # output file is expected to have a length of 5sec
  # the last second is supposed to consists of only
  # the audio of testInFileB (a sine wave)
  run ${__bin_path}/aio-mix -q 7 -o "${testOutFileMp3}" "${testInFileB}" "${testInFileMp3A}"

  [ ${status} -eq 0 ]
  [ -f "${testOutFileMp3}" ]

  aio-assert-duration-ffmpeg "${testOutFileMp3}" 5
  aio-assert-volume-begin-ffmpeg "${testOutFileMp3}" 4 -1.5 -4.5 -12 -14
  aio-assert-volume-end-ffmpeg "${testOutFileMp3}" 4 -5 -6.4 -8.5 -9.5
}

@test "invoke aio-mix - mix - ogg and mp3 - mp3 out - high quality - long" {

  aio-generate-mp3-file-noise "${testInFileMp3A}" 4 0.5
  aio-generate-ogg-file-sine "${testInFileB}" 5 4

  # output file is expected to have a length of 5sec
  # the last second is supposed to consists of only
  # the audio of testInFileB (a sine wave)
  run ${__bin_path}/aio-mix -q 2 -o "${testOutFileMp3}" "${testInFileB}" "${testInFileMp3A}"

  [ ${status} -eq 0 ]
  [ -f "${testOutFileMp3}" ]

  aio-assert-duration-ffmpeg "${testOutFileMp3}" 5
  aio-assert-volume-begin-ffmpeg "${testOutFileMp3}" 4 -1.5 -4.5 -12 -14
  aio-assert-volume-end-ffmpeg "${testOutFileMp3}" 4 -5 -6.4 -8.5 -9.5
}

@test "invoke aio-mix - mix - ogg and mp3 - ogg out - long" {

  aio-generate-mp3-file-noise "${testInFileMp3A}" 4 0.5
  aio-generate-ogg-file-sine "${testInFileB}" 5 4
  # output file is expected to have a length of 5sec
  # the last second is supposed to consists of only
  # the audio of testInFileB (a sine wave)
  run ${__bin_path}/aio-mix -o "${testOutFile}" "${testInFileB}" "${testInFileMp3A}"

  [ ${status} -eq 0 ]
  [ -f "${testOutFile}" ]

  aio-assert-duration-ffmpeg "${testOutFile}" 5
  aio-assert-volume-begin-ffmpeg "${testOutFile}" 4 -1 -4 -12 -14
  aio-assert-volume-end-ffmpeg "${testOutFile}" 4 -5.8 -5.8 -8.5 -9.5
}

@test "invoke aio-mix - mix - ogg and mp3 - ogg out - low quality - long" {

  aio-generate-mp3-file-noise "${testInFileMp3A}" 4 0.5
  aio-generate-ogg-file-sine "${testInFileB}" 5 4

  # output file is expected to have a length of 5sec
  # the last second is supposed to consists of only
  # the audio of testInFileB (a sine wave)
  run ${__bin_path}/aio-mix -q 2 -o "${testOutFile}" "${testInFileB}" "${testInFileMp3A}"

  [ ${status} -eq 0 ]
  [ -f "${testOutFile}" ]

  aio-assert-duration-ffmpeg "${testOutFile}" 5
  aio-assert-volume-begin-ffmpeg "${testOutFile}" 4 -1 -4 -12 -14
  aio-assert-volume-end-ffmpeg "${testOutFile}" 4 -5.8 -5.8 -8.5 -9.5
}

@test "invoke aio-mix - mix - ogg and mp3 - ogg out - high quality - long" {

  # skip "due to bitrate > 182k for ogg output file encoding"

  aio-generate-mp3-file-noise "${testInFileMp3A}" 4 0.5
  aio-generate-ogg-file-sine "${testInFileB}" 5 4

  # output file is expected to have a length of 5sec
  # the last second is supposed to consists of only
  # the audio of testInFileB (a sine wave)
  run ${__bin_path}/aio-mix -q 9 -o "${testOutFile}" "${testInFileB}" "${testInFileMp3A}"

  [ ${status} -eq 0 ]
  [ -f "${testOutFile}" ]

  aio-assert-duration-ffmpeg "${testOutFile}" 5
  aio-assert-volume-begin-ffmpeg "${testOutFile}" 4 -2 -4 -12 -14
  aio-assert-volume-end-ffmpeg "${testOutFile}" 4 -5.8 -5.8 -8.5 -9.5
}
