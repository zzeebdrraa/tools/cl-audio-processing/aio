#!/usr/bin/env bats

# This file is part of aio-proc
#
#     aio-proc is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     aio-proc is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with aio-proc. If not, see <http://www.gnu.org/licenses/>.

###########
# TODO
#
# add more tests:
#
#   - test input files with no extension
#
# more:
#
#   - use bats hooks setup_file() amd teardown_file() in order to create temporary working dirs
#
###########

###########
# Test Data
###########

declare -g -r testOutFileOgg="test-out.ogg"
declare -g -r testInFileOgg="test-in.ogg"
declare -g -r testInFilesilenceOgg="test-in-silence.ogg"

declare -g -r invalidDurations=(
  abc
  1sec
  0
  -0
  -2
  -0.000
  -0.001
  1o1
  0,765
  0.76.5
)

declare -g -r invalidTresholds=(
  1
  10db
  -10db
  -0
  -0.000
  abc
  -1000
  -10,00
  -10.0.0
)

###########
# Envvars
###########

export ENV_AIO_DETECT_SILENCE_SEC_TO_TS_BIN_PATH='..'

###########
# Helpers
###########

declare -g -r __bin_path='..'

load test-aio-common-asserts
load test-aio-common-file-generators

###########
# Setup
###########

setup() {
  rm -f "${testOutFileOgg}"
  rm -f "${testInFileOgg}"
  rm -f "${testInFilesilenceOgg}"
}

teardown() {
  rm -f "${testOutFileOgg}"
  rm -f "${testInFileOgg}"
  rm -f "${testInFilesilenceOgg}"
}

###########
# Tests
###########

@test "invoke aio-detect-silence - help" {
  run ${__bin_path}/aio-detect-silence -h
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-detect-silence --help
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-detect-silence help
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-detect-silence h
  [ ${status} -eq 0 ]
}

@test "invoke aio-detect-silence - help short" {

  run ${__bin_path}/aio-detect-silence help short
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-detect-silence help s
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-detect-silence h s
  [ ${status} -eq 0 ]
}

@test "invoke aio-detect-silence - help details" {

  run ${__bin_path}/aio-detect-silence help details
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-detect-silence help d
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-detect-silence h d
  [ ${status} -eq 0 ]
}

@test "invoke aio-detect-silence --version" {
  run ${__bin_path}/aio-detect-silence --version
  [ ${status} -eq 0 ]
}

@test "invoke aio-detect-silence without input file" {
  run ${__bin_path}/aio-detect-silence

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: no input files provided .. stopping' ]
}

@test "invoke aio-detect-silence with non-existing input file" {
  run ${__bin_path}/aio-detect-silence dummyIn1

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: file [dummyIn1] does not exist .. skipping' ]
}

@test "invoke aio-detect-silence for input file with silence" {
  aio-generate-ogg-file-sine "${testInFileOgg}"
  aio-generate-ogg-file-silence "${testInFilesilenceOgg}"

  ffmpeg -i "concat:${testInFilesilenceOgg}|${testInFileOgg}|${testInFilesilenceOgg}|${testInFileOgg}" -acodec copy "${testOutFileOgg}"

  run ${__bin_path}/aio-detect-silence "${testOutFileOgg}"

  [ ${status} -eq 0 ]

  [ ${#lines[*]} -eq 2 ]

  aio-assert-silence-sec "${testOutFileOgg}" "0" "4.98" "4.98" ${lines[0]}
  aio-assert-silence-sec "${testOutFileOgg}" "9.99" "14.98" "4.98" ${lines[1]}
}

@test "invoke aio-detect-silence for input file with silence - begin only" {
  aio-generate-ogg-file-silence "${testInFileOgg}"

  run ${__bin_path}/aio-detect-silence --ab "${testInFileOgg}"

  [ ${status} -eq 0 ]

  aio-assert-silence-sec-begin-only "${testInFileOgg}" "5" ${output}
}

@test "invoke aio-detect-silence for input file with silence - end only" {
  aio-generate-ogg-file-sine "${testInFileOgg}"
  aio-generate-ogg-file-silence "${testInFilesilenceOgg}"

  ffmpeg -i "concat:${testInFileOgg}|${testInFilesilenceOgg}" -acodec copy "${testOutFileOgg}"

  run ${__bin_path}/aio-detect-silence --ae "${testOutFileOgg}"

  [ ${status} -eq 0 ]
  [ ${#lines[*]} -eq 1 ]

  aio-assert-silence-sec-end-only "${testOutFileOgg}" "5" ${output}
}

@test "invoke aio-detect-silence for input file with silence - begin and end only" {
  aio-generate-ogg-file-sine "${testInFileOgg}"
  aio-generate-ogg-file-silence "${testInFilesilenceOgg}"

  ffmpeg -i "concat:${testInFilesilenceOgg}|${testInFileOgg}|${testInFilesilenceOgg}" -acodec copy "${testOutFileOgg}"

  run ${__bin_path}/aio-detect-silence --ab --ae "${testOutFileOgg}"

  [ ${status} -eq 0 ]
  [ ${#lines[*]} -eq 1 ]

  aio-assert-silence-sec-begin-end-only "${testOutFileOgg}" "5" "10" ${output}
}

@test "invoke aio-detect-silence for input file with silence - as timestamp" {
  aio-generate-ogg-file-silence "${testInFileOgg}"

  run ${__bin_path}/aio-detect-silence --ts "${testInFileOgg}"
  # printf '# 0 %s\n'  "${output}" >&3

  [ ${status} -eq 0 ]
  [ "${output}" == "${testInFileOgg} 00:00:00.000 00:00:05.005 00:00:05.005" ]
}

@test "invoke aio-detect-silence for input file with silence - as timestamp and begin only" {
  aio-generate-ogg-file-silence "${testInFileOgg}"

  run ${__bin_path}/aio-detect-silence --ab --ts "${testInFileOgg}"

  [ ${status} -eq 0 ]

  local result=(${output})
  # there may be a ns/ms part that we dont need here,
  # so remove everything from RHS of timestamp till the dot
  result[1]=${result[1]%.*}

  [ "${result[0]}" == "${testInFileOgg}" ]
  [ "${result[1]}" == "00:00:05" ]
}

@test "invoke aio-detect-silence for input file with silence - as timestamp and end only" {
  aio-generate-ogg-file-sine "${testInFileOgg}"
  aio-generate-ogg-file-silence "${testInFilesilenceOgg}"

  ffmpeg -i "concat:${testInFilesilenceOgg}|${testInFileOgg}|${testInFilesilenceOgg}" -acodec copy "${testOutFileOgg}"

  run ${__bin_path}/aio-detect-silence --ae --ts "${testOutFileOgg}"

  [ ${status} -eq 0 ]

#  printf '# o %s\n' "${output}" >&3

  local result=(${output})
  # there may be a ns/ms part that we dont need here,
  # so remove everything from RHS of timestamp till the dot
  result[1]=${result[1]%.*}

  [ "${result[0]}" == "${testOutFileOgg}" ]
  # instead of 00:00:10 we use 00:00:09 as the real position is only almost 10 => 9-997 seconds
  # TODO this may break if audio file is generated differently (ie different ffmpeg version)
  #   and the end-time starts at a value like 10.003 or the like
  [ "${result[1]}" == "00:00:09" ]
}

@test "invoke aio-detect-silence for input file with silence - as timestamp and begin and end only" {
  aio-generate-ogg-file-sine "${testInFileOgg}"
  aio-generate-ogg-file-silence "${testInFilesilenceOgg}"

  ffmpeg -i "concat:${testInFilesilenceOgg}|${testInFileOgg}|${testInFilesilenceOgg}" -acodec copy "${testOutFileOgg}"

  run ${__bin_path}/aio-detect-silence --ab --ae --ts "${testOutFileOgg}"

  [ ${status} -eq 0 ]

#  printf '# o %s\n' "${output}" >&3

  local result=(${output})

  # there may be a ns/ms part that we dont need here,
  # so remove everything from RHS of timestamp till the dot
  result[1]=${result[1]%.*}
  result[2]=${result[2]%.*}

  [ "${result[0]}" == "${testOutFileOgg}" ]
  # TODO this may break if audio file is generated differently (ie different ffmpeg version)
  #   and the begin-time starts at a value like 5.003 or the like. The same holds true for the
  #   end-time
  [ "${result[1]}" == "00:00:04" ]
  [ "${result[2]}" == "00:00:09" ]
}

@test "invoke aio-detect-silence for input file with silence - treshold" {
  aio-generate-ogg-file-sine "${testInFileOgg}" 9.9 8 0.1

  run ${__bin_path}/aio-detect-silence -t -12 "${testInFileOgg}"

  # printf '# l %s\n'  "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ ${#lines[*]} -eq 1 ]

  aio-assert-silence-sec "${testInFileOgg}" "4.59" "5.4" "0.8" ${lines[0]}
}

@test "invoke aio-detect-silence for input file with silence - duration" {
  aio-generate-ogg-file-sine "${testInFileOgg}" 9.9 8 0.1

  run ${__bin_path}/aio-detect-silence -d 0.05 "${testInFileOgg}"

  # printf '# l %s\n'  "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ ${#lines[*]} -eq 1 ]

  aio-assert-silence-sec "${testInFileOgg}" "4.99" "5" "0.005" ${lines[0]}
}

@test "invoke aio-detect-silence for input file with silence - invalid treshold" {
  touch "${testInFileOgg}"

  run ${__bin_path}/aio-detect-silence -t --50 "${testInFileOgg}"

  [ ${status} -eq 1 ]
  [ "${output}" == "error: invalid threshold [--50] .. stopping" ]
}

@test "invoke aio-detect-silence for input file with silence - invalid duration" {
  touch "${testInFileOgg}"

  run ${__bin_path}/aio-detect-silence -d 0-1 "${testInFileOgg}"

  [ ${status} -eq 1 ]
  [ "${output}" == "error: invalid duration [0-1] .. stopping" ]
}

@test "invoke aio-detect-silence for input file with silence - treshold and duration" {
  aio-generate-ogg-file-sine "${testInFileOgg}" 9.9 8 0.1

  run ${__bin_path}/aio-detect-silence -t -25 -d 0.1 "${testInFileOgg}"

  # printf '# l %s\n'  "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ ${#lines[*]} -eq 1 ]

  aio-assert-silence-sec "${testInFileOgg}" "4.91" "5.08" "0.17" ${lines[0]}
}

@test "invoke aio-detect-silence for input file without silence" {
  aio-generate-ogg-file-sine "${testInFileOgg}" 9.9 8 0.1

  run ${__bin_path}/aio-detect-silence "${testInFileOgg}"

  [ ${status} -eq 1 ]
  [ "${#output}" -eq 0 ]
}
