#!/usr/bin/env bats

# This file is part of aio-proc
#
#     aio-proc is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     aio-proc is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with aio-proc. If not, see <http://www.gnu.org/licenses/>.

###########
# Testdata
###########

declare -A -r -g __valid_seconds=(
  [1]="00:00:01.000"
  [1.0]="00:00:01.000"
  [1.000000]="00:00:01.000"
  [300]="00:05:00.000"
  [300.00]="00:05:00.000"
  [567.9]="00:09:27.900"
  [10001.001]="02:46:41.001"
  [86399.999]="23:59:59.999"
  [86399.999999]="23:59:59.999"
)
declare -r -g __invalid_seconds=(
  -0
  -0.00001
  abc
  300-00
  567.9.99
  100001.a0001
)
declare -r -g __valid_invalid_seconds=(
  1
  -0
  abc
  300
  567.9
  -300
)

###########
# Helpers
###########

declare -g -r __bin_path='..'

###########
# Tests
###########

@test "invoke aio-sec-to-ts - help" {

  run ${__bin_path}/aio-sec-to-ts -h
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-sec-to-ts --help
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-sec-to-ts h
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-sec-to-ts help
  [ ${status} -eq 0 ]
}

@test "invoke aio-sec-to-ts - help short" {

  run ${__bin_path}/aio-sec-to-ts help short
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-sec-to-ts help s
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-sec-to-ts h s
  [ ${status} -eq 0 ]
}

@test "invoke aio-sec-to-ts - help details" {

  run ${__bin_path}/aio-sec-to-ts help details
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-sec-to-ts help d
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-sec-to-ts h d
  [ ${status} -eq 0 ]
}

@test "invoke aio-sec-to-ts - help examples" {

  run ${__bin_path}/aio-sec-to-ts help examples
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-sec-to-ts help e
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-sec-to-ts h e
  [ ${status} -eq 0 ]
}

@test "invoke aio-sec-to-ts --version" {
  run ${__bin_path}/aio-sec-to-ts --version
  [ ${status} -eq 0 ]
}

@test "invoke aio-sec-to-ts - no seconds" {
  run ${__bin_path}/aio-sec-to-ts
  [ ${status} -eq 1 ]
  [ "${output}" == "error: no seconds provided .. stopping" ]

  run ${__bin_path}/aio-sec-to-ts ""
  [ ${status} -eq 1 ]
  [ "${output}" == "error: no seconds provided .. stopping" ]

  run ${__bin_path}/aio-sec-to-ts "" "" "  "
  [ ${status} -eq 1 ]
  [ "${output}" == "error: no seconds provided .. stopping" ]
}

@test "invoke aio-sec-to-ts - valid seconds" {
  local -r expectedTs=("${__valid_seconds[@]}")

  run ${__bin_path}/aio-sec-to-ts "${!__valid_seconds[@]}"

  [ ${status} -eq 0 ]
  [ ${#lines[*]} -eq ${#expectedTs[*]} ]

  for i in "${!expectedTs[@]}"; do
    [ "${lines[$i]}" == "${expectedTs[$i]}" ]
  done
}

@test "invoke aio-sec-to-ts - invalid seconds" {
  run ${__bin_path}/aio-sec-to-ts "${__invalid_seconds[@]}"

  [ ${status} -eq 1 ]
  [ "${output}" == "error: the following values are invalid [${__invalid_seconds[*]}] .. stopping" ]
}

@test "invoke aio-sec-to-ts - valid and invalid seconds" {
  run ${__bin_path}/aio-sec-to-ts "${__valid_invalid_seconds[@]}"
  [ ${status} -eq 1 ]
  [ "${output}" == "error: the following values are invalid [-0 abc -300] .. stopping" ]
}

@test "invoke aio-sec-to-ts - stdin - no seconds - read timeout" {

  # no value at all - should trigger timeout
  run ${__bin_path}/aio-sec-to-ts --

  [ ${status} -eq 1 ]
  [ "${output}" == "error: no seconds provided .. stopping" ]

  # value available after delay - should trigger timeout
  run ${__bin_path}/aio-sec-to-ts -- < <(sleep 2; printf '1\n')

  [ ${status} -eq 1 ]
  [ "${output}" == "error: no seconds provided .. stopping" ]
}

@test "invoke aio-sec-to-ts - stdin - no seconds - empty lines only" {

  local -r lineWithWhitespaces='      '
  local -r lineWithTabs="$(printf '\t\t\t')"

  # empty value
  run ${__bin_path}/aio-sec-to-ts -- <<<""

  [ ${status} -eq 1 ]
  [ "${output}" == "error: no seconds provided .. stopping" ]

  # empty value - whitespaces only
  run ${__bin_path}/aio-sec-to-ts -- <<<"${lineWithWhitespaces}"

  [ ${status} -eq 1 ]
  [ "${output}" == "error: no seconds provided .. stopping" ]

  # empty value - tabs only
  run ${__bin_path}/aio-sec-to-ts -- <<<"${lineWithTabs}"

  [ ${status} -eq 1 ]
  [ "${output}" == "error: no seconds provided .. stopping" ]

  # empty value - tabs and whitespaces only
  run ${__bin_path}/aio-sec-to-ts -- <<<"${lineWithTabs}${lineWithWhitespaces}"

  [ ${status} -eq 1 ]
  [ "${output}" == "error: no seconds provided .. stopping" ]

  # lines are empty
  run ${__bin_path}/aio-sec-to-ts -- <<EOF

${lineWithWhitespaces}
${lineWithTabs}
${lineWithWhitespaces}${lineWithTabs}
EOF

  [ ${status} -eq 1 ]
  [ "${output}" == "error: no seconds provided .. stopping" ]
}

@test "invoke aio-sec-to-ts - stdin - with comments - heredoc" {

  local -r indentWhitespaces='      '
  local -r indentTabs="$(printf '\t\t')"

  # empty value
  run ${__bin_path}/aio-sec-to-ts -- <<EOF
# a single line comment
61
122
# a
${indentTabs}# multi
${indentWhitespaces}# line
${indentTabs}${indentWhitespaces}# comment
183
EOF

  [ ${status} -eq 0 ]

  [ "${lines[0]}" == "00:01:01.000" ]
  [ "${lines[1]}" == "00:02:02.000" ]
  [ "${lines[2]}" == "00:03:03.000" ]
}

@test "invoke aio-sec-to-ts - stdin - with empty lines - heredoc" {

  local -r lineWithWhitespaces='      '
  local -r lineWithTabs="$(printf '\t\t\t')"

  # empty value
  run ${__bin_path}/aio-sec-to-ts -- <<EOF
# single empty line

61
# empty line containing only whitespaces
${lineWithWhitespaces}
# empty line containing only tabs
${lineWithTabs}
122
# multiple empty lines with and without whitespaces and tabs

${lineWithWhitespaces}


${lineWithTabs}
${lineWithWhitespaces}
${lineWithTabs}${lineWithWhitespaces}
183
EOF

  [ ${status} -eq 0 ]

  [ "${lines[0]}" == "00:01:01.000" ]
  [ "${lines[1]}" == "00:02:02.000" ]
  [ "${lines[2]}" == "00:03:03.000" ]
}

@test "invoke aio-sec-to-ts - stdin - with indented data - heredoc" {

  local -r indentWhitespaces='      '
  local -r indentTabs="$(printf '\t\t')"

  # empty value
  run ${__bin_path}/aio-sec-to-ts -- <<EOF
${indentWhitespaces}61
${indentTabs}122${indentWhitespaces}
${indentTabs}${indentWhitespaces}183${indentTabs}
EOF

  [ ${status} -eq 0 ]

  [ "${lines[0]}" == "00:01:01.000" ]
  [ "${lines[1]}" == "00:02:02.000" ]
  [ "${lines[2]}" == "00:03:03.000" ]
}

@test "invoke aio-sec-to-ts - stdin - valid seconds" {
  local -r expectedTs=("${__valid_seconds[@]}")

  run ${__bin_path}/aio-sec-to-ts -- < <( printf '%s\n' "${!__valid_seconds[@]}" )

  [ ${status} -eq 0 ]
  [ ${#lines[*]} -eq ${#expectedTs[*]} ]

  for i in "${!expectedTs[@]}"; do
    [ "${lines[$i]}" == "${expectedTs[$i]}" ]
  done
}

@test "invoke aio-sec-to-ts - stdin - invalid seconds" {
  run ${__bin_path}/aio-sec-to-ts -- < <( printf '%s\n' "${__invalid_seconds[@]}" )

  [ ${status} -eq 1 ]
  [ "${output}" == "error: the following values are invalid [${__invalid_seconds[*]}] .. stopping" ]
}

@test "invoke aio-sec-to-ts - stdin - valid and invalid seconds" {
  run ${__bin_path}/aio-sec-to-ts -- < <( printf '%s\n' "${__valid_invalid_seconds[@]}" )
  [ ${status} -eq 1 ]
  [ "${output}" == "error: the following values are invalid [-0 abc -300] .. stopping" ]
}
