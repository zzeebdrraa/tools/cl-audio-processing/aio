#!/usr/bin/env bats

# This file is part of aio
#
#     aio is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     aio is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with aio. If not, see <http://www.gnu.org/licenses/>.

###########
# TODO add tests for:
#
#   - test --force-overwrite flag
#   - test -c flag
#   - test output of --dry-run and --show-only
#
#   - test more command syntax errors
#   - test more input files containing whitespaces
#   - test more renamed files containing whitespaces
#   - test whitespace as --delimiter-in in cut|keep command
#
#   - test with input files that do not have a file extension
#   - test with input files that contain one or more input-delimiters and one or more whitespaces
#
#   - test for 'splitting of value resulted in no parts'
#
#   - failing: aio-rename-files s 1 3 ::: out prepend ready steady go ::: -d text-1.txt text-2.txt 
#     => error: the length of one or both of the given sets [2, 2] must be greater or equal than the max-index value [3] ... stopping
#
#   - failing: aio-rename-files s 1 3 ::: out append ready steady go ::: -d text-1.txt text-2.txt 
#     => error: the length of one or both of the given sets [2, 2] must be greater or equal than the max-index value [3] ... stopping
#
#   - failing: aio-rename-files in value ::: \
#                    out prepend ready steady go ::: \
#                    select 1 3 ::: \
#                    rec-1.ogg rec-2.ogg <<EOF
#                      aaa
#                      bbb
#                    EOF
#     => error: the length of one or both of the given sets [2, 2] must be greater or equal than the max-index value [3] ... stopping
#
# more:
#
#   - use bats hooks setup_file() amd teardown_file() in order to create temporary working dirs
#
###########

###########
# Test Data
###########

declare -g -r testInFileUnderscoreA="test_session_a.ogg"
declare -g -r testInFileUnderscoreB="test_session_b.ogg"
declare -g -r testInFileDoubleUnderscoreA="__test__session__a__out__.ogg"
declare -g -r testInFileDoubleUnderscoreB="__test__session__b__out__.ogg"

declare -g -r testInFileSpacesA="test  session a.ogg"
declare -g -r testInFileSpacesB="test  session b.ogg"

declare -g -r testInFileASession="test-a-session.ogg"
declare -g -r testInFileBSession="test-b-session.ogg"
declare -g -r testInFileATest="test-session-a-test.ogg"
declare -g -r testInFileBTest="test-session-b-test.ogg"

declare -g -r xxxTestInFileA="xxxtestxxxsessionxxxa.ogg"
declare -g -r xxxTestInFileB="xxxtestxxxsessionxxxb.ogg"
declare -g -r testInFileAxxx="testxxxsessionxxxaxxx.ogg"
declare -g -r testInFileBxxx="testxxxsessionxxxbxxx.ogg"

declare -g -r xxxxxxxxxFile="xxxxxxxxx.ogg"
declare -g -r yyyxyyyFile="yyyxyyy.ogg"

declare -g -r testInFileA="test-session-a.ogg"
declare -g -r testInFileB="test-session-b.ogg"
declare -g -r testInFileC="test-session-c.ogg"
declare -g -r testInFileD="test-session-d.ogg"
declare -g -r testInFileE="test-session-e.ogg"
declare -g -r testInFileF="test-session-f.ogg"

declare -A -g -r __output_file_map=(
  [test-session-a-test-session-a]=test-session-a-test-session-a.ogg
  [foo-test-session-a-none]=footest-session-a.ogg
  [foo-test-session-b-none]=footest-session-b.ogg
  [foo-test-session-a-underscore]=foo_test-session-a.ogg
  [foo-test-session-b-underscore]=foo_test-session-b.ogg
  [foo-test-session-a-abc]=fooabctest-session-a.ogg
  [foo-test-session-b-abc]=fooabctest-session-b.ogg
  [foo-test-session-a]=foo-test-session-a.ogg
  [foo-test-session-b]=foo-test-session-b.ogg
  [foo-test-session-c]=foo-test-session-c.ogg
  [foo-test-session-d]=foo-test-session-d.ogg
  [foo-test-session-e]=foo-test-session-e.ogg
  [foo-test-session-f]=foo-test-session-f.ogg
  [bar-test-session-b]=bar-test-session-b.ogg
  [bar-test-session-d]=bar-test-session-d.ogg
  [foo-bar-foo-bar-test-session-a-narrow]="foo bar foo bar-test-session-a.ogg"
  [foo-bar-foo-bar-test-session-b-narrow]="foo bar foo bar-test-session-b.ogg"
  [foo-bar-foo-bar-test-session-a-wide]="foo        bar     foo       bar-test-session-a.ogg"
  [foo-bar-foo-bar-test-session-b-wide]="foo        bar     foo       bar-test-session-b.ogg"
  [a]=a.ogg
  [b]=b.ogg
  [test-a]=test-a.ogg
  [test-b]=test-b.ogg
  [test-replace-a]=test-replace-a.ogg
  [test-replace-b]=test-replace-b.ogg
  [test-session-xxx]=test-session-xxx.ogg
  [test-session-yyy]=test-session-yyy.ogg
  [test-a-xxx]=test-a-xxx.ogg
  [test-b-xxx]=test-b-xxx.ogg
  [test-b-yyy]=test-b-yyy.ogg
  [xxx-session-a]=xxx-session-a.ogg
  [xxx-session-b]=xxx-session-b.ogg
  [yyy-session-b]=yyy-session-b.ogg
  [xxx-session-a-xxx]=xxx-session-a-xxx.ogg
  [xxx-session-b-xxx]=xxx-session-b-xxx.ogg
  [yyy-session-b-yyy]=yyy-session-b-yyy.ogg
  [_test_session_a]=_test_session_a.ogg
  [_test_session_b]=_test_session_b.ogg
  [__test__session__b]=__test__session__b.ogg
  [__test-session-a__]=__test-session-a__.ogg
  [__test-session-b__]=__test-session-b__.ogg
  [test--session--a]=test--session--a.ogg
  [test__session__a]=test__session__a.ogg
  [test__session__b]=test__session__b.ogg
  [testsessiona]=testsessiona.ogg
  [testsessionb]=testsessionb.ogg
  [test_session_a_]=test_session_a_.ogg
  [test_session_b_]=test_session_b_.ogg
  [test-session-a-out]=test-session-a-out.ogg
  [test-session-b-out]=test-session-b-out.ogg
  [test_session_a_out]=test_session_a_out.ogg
  [test__session__b__]=test__session__b__.ogg
  [test---session---a]=test---session---a.ogg
  [test---session---b]=test---session---b.ogg
  [test---session-a]=test---session-a.ogg
  [test---session-b]=test---session-b.ogg
  [testxxxsession-a]=testxxxsession-a.ogg
  [testyyysession-b]=testyyysession-b.ogg
  [testxxxsessionxxxa]=testxxxsessionxxxa.ogg
  [testyyysessionyyyb]=testyyysessionyyyb.ogg
  [test-session---a]=test-session---a.ogg
  [test-session---b]=test-session---b.ogg
  [test-sessionnnna]=test-sessionnnna.ogg
  [test-sessionmmmb]=test-sessionmmmb.ogg
  [test-session_a]=test-session_a.ogg
  [test-session_b]=test-session_b.ogg
  [2020-test-session-a-01-foo]=2020-test-session-a-01-foo.ogg
  [2020-test-session-b-02-bar]=2020-test-session-b-02-bar.ogg
  [2020-session-foo]=2020-session-foo.ogg
  [2020-session-bar]=2020-session-bar.ogg
  [2020-01-foo]=2020-01-foo.ogg
  [2020-02-bar]=2020-02-bar.ogg
  [test-sessiona]=test-sessiona.ogg
  [test-sessionb]=test-sessionb.ogg
  [test-sessionxxxa]=test-sessionxxxa.ogg
  [test-sessionyyyb]=test-sessionyyyb.ogg
  [session-a]=session-a.ogg
  [session-b]=session-b.ogg
  [session-foo]=session-foo.ogg
  [session-bar]=session-bar.ogg
  [session-foo-w]='session foo w.ogg'
  [session-bar-w]='session bar w.ogg'
  [this-is a-file]='this-is a-file'
  [ths-s a-whtespace-fle]='ths-s a-whtespace-fle'
  [ths-is a-whitespace-file]='ths-is a-whitespace-file'
  [this-is a-whitespace-fle]='this-is a-whitespace-fle'
  [ths-is a-whitespace-fle]='ths-is a-whitespace-fle'
  [01-session-foo]=01-session-foo.ogg
  [02-session-bar]=02-session-bar.ogg
  [03-session-foobar]=03-session-foobar.ogg
  [04-session-barfoo]=04-session-barfoo.ogg
  [05-session-boofar]=05-session-boofar.ogg
  [06-session-farboo]=06-session-farboo.ogg
  [foo]=foo.ogg
  [bar]=bar.ogg
  [yyy]=yyy.ogg
  [xxx]=xxx.ogg
  [yyyyx]=yyyyx.ogg
  [yx]=yx.ogg
  [y]=y.ogg
  [test-xxx-session]=test-xxx-session.ogg
  [test-xxxxxxxxx-session]=test-xxxxxxxxx-session.ogg
  [test-xxx]=test-xxx.ogg
  [test-xxxxxxxxx]=test-xxxxxxxxx.ogg
  [xxx-session]=xxx-session.ogg
  [xxxxxxxxx-session]=xxxxxxxxx-session.ogg
  [xxxtestxxxsession_a]=xxxtestxxxsession_a.ogg
  [xxxtestxxxsession-a]=xxxtestxxxsession-a.ogg
  [xxxtestxxxsession-b]=xxxtestxxxsession-b.ogg
  [xxxtestxxxsessiona]=xxxtestxxxsessiona.ogg
  [xxxtestxxxsessionb]=xxxtestxxxsessionb.ogg
  [2020-test-a-underscore]=2020_test_a.ogg
  [2020-test-b-underscore]=2020_test_b.ogg
  [2020-test-a-abc]=2020abctestabca.ogg
  [2020-test-b-abc]=2020abctestabcb.ogg
  [2020-test-a-none]=2020testa.ogg
  [2020-test-b-none]=2020testb.ogg
  [2020-test-a]=2020-test-a.ogg
  [2020-test-b]=2020-test-b.ogg
  [2020-test-c]=2020-test-c.ogg
  [2020-test-d]=2020-test-d.ogg
  [2020-test-e]=2020-test-e.ogg
  [2020-test-f]=2020-test-f.ogg
  [2020-session-foo]=2020-session-foo.ogg
  [2020-session-bar]=2020-session-bar.ogg
  [2020-test-session-foo]=2020-test-session-foo.ogg
  [2020-test-session-bar]=2020-test-session-bar.ogg
  [2020-test-session-foo-underscore]=2020_test_session_foo.ogg
  [2020-test-session-bar-underscore]=2020_test_session_bar.ogg
  [2020-test-session-a-session-foo]=2020-test-session-a-session-foo.ogg
  [2020-test-session-b-session-bar]=2020-test-session-b-session-bar.ogg
  [2020-test_session_a-01-foo]=2020-test_session_a-01-foo.ogg
  [2020-test_session_b-02-bar]=2020-test_session_b-02-bar.ogg
  [2020-test-session-foo-abc]=2020abctestabcsessionabcfoo.ogg
  [2020-test-session-bar-abc]=2020abctestabcsessionabcbar.ogg
  [2020-test-session-foo-none]=2020testsessionfoo.ogg
  [2020-test-session-bar-none]=2020testsessionbar.ogg
  [01]=01.ogg
  [02]=02.ogg
  [01-foo]=01-foo.ogg
  [02-bar]=02-bar.ogg
  [2020-01]=2020-01.ogg
  [2020-02]=2020-02.ogg
  [2020-01-underscore]=2020_01.ogg
  [2020-02-underscore]=2020_02.ogg
  [2020-01-abc]=2020abc01.ogg
  [2020-02-abc]=2020abc02.ogg
  [2020-01-none]=202001.ogg
  [2020-02-none]=202002.ogg
  [2020-test-01-foo]=2020-test-01-foo.ogg
  [2020-test-02-bar]=2020-test-02-bar.ogg
  [2020-test-01-foo-underscore]=2020_test_01_foo.ogg
  [2020-test-02-bar-underscore]=2020_test_02_bar.ogg
  [2020-test-01-foo-abc]=2020abctestabc01abcfoo.ogg
  [2020-test-02-bar-abc]=2020abctestabc02abcbar.ogg
  [2020-test-01-foo-none]=2020test01foo.ogg
  [2020-test-02-bar-none]=2020test02bar.ogg
)

declare -g -r -a __special_chars_list=(
  '\'
  '/'
)
  
# does not include the tab-char '\x09' '\x0a' '\x0b' '\x0c' '\x0d' 
#   as those chars are interpreted by printf in the tests below
# does not include the nul-char '\x00', as this char wont be acepted 
#   by printf in the tests below
declare -g -r -a __control_chars_list=(
  '\x01' '\x02' '\x03' '\x04' '\x05' '\x06' '\x07' '\x08'
  '\x0e' '\x0f'
  '\x10' '\x11' '\x12' '\x13' '\x14' '\x15' '\x16' '\x17' '\x18' '\x19' 
  '\x1a' '\x1b' '\x1c' '\x1d' '\x1e' '\x1f'
  '\x7f'
)

declare -g -r __bin_path='..'
declare -g -r __inout_file_path='/dev/shm/aio/rename-files'

###########
# Helpers
###########

create-empty-files-tmp() {
  local fileName
  local filePath
  local fileList=()

  for fileName in "$@"; do
    filePath="${__inout_file_path}/${fileName}"
    fileList+=("${filePath}")
  done
  
  [[ ${#fileList[*]} -gt 0 ]] && {
    touch "${fileList[@]}"
    printf '%s\n' "${fileList[@]}"
  }
  return 0
}

remove-file-tmps() {
  local fileName
  local filePath

  for fileName in "$@"; do
    filePath="${__inout_file_path}/${fileName}"
    [[ -f "${filePath}" ]] && rm "${filePath}"
  done

  return 0
}

remove-expected-output-files-tmp() {
  local fileKey
  local fileName
  local filePath

  for fileKey in "$@"; do
    fileName="${__output_file_map[${fileKey}]}"
    filePath="${__inout_file_path}/${fileName}"
    [[ -f "${filePath}" ]] && rm "${filePath}"
  done

  return 0
}

###########
# Asserts
###########

assert-files-not-exist-tmp() {
  local filePath
  local fileName
  local fileKey

  for fileKey in "$@"; do
    fileName="${__output_file_map[${fileKey}]}"
    [[ -z "${fileName}" ]] && {
      printf 'no filename for key [%s]\n' "${fileKey}" >&2
      return 1
    }
    filePath="${__inout_file_path}/${fileName}"
    [[ -f "${filePath}" ]] && {
      printf 'file exists at [%s]\n' "${filePath}" >&2
      return 1
    }
  done

  return 0
}

assert-files-exist-tmp() {
  local filePath
  local fileName
  local fileKey
  for fileKey in "$@"; do
    fileName="${__output_file_map[${fileKey}]}"
    [[ -z "${fileName}" ]] && {
      printf 'no filename for key [%s]\n' "${fileKey}" >&2
      return 1
    }
    filePath="${__inout_file_path}/${fileName}"
    [[ ! -f "${filePath}" ]] && {
      printf 'no file at [%s]\n' "${filePath}" >&2
      return 1
    }
  done
  return 0
}

assert-files-exist() {
  local fileName
  local filePath

  for fileName in "$@"; do
    filePath="${__inout_file_path}/${fileName}"
    [[ ! -f "${filePath}" ]] && {
      printf 'no file at [%s]\n' "${filePath}" >&2
      return 1
    }
  done

  return 0
}

assert-files-not-exist() {
  local fileName
  local filePath

  for fileName in "$@"; do
    filePath="${__inout_file_path}/${fileName}"
    [[ -f "${filePath}" ]] && {
      printf 'file exists at [%s]\n' "${filePath}" >&2
      return 1
    }
  done

  return 0
}

assert-files-exist-here() {
  local fileName
  local filePath

  for fileName in "$@"; do
    filePath="./${fileName}"
    [[ ! -f "${filePath}" ]] && {
      printf 'no file at [%s]\n' "${filePath}" >&2
      return 1
    }
  done

  return 0
}

###########
# Setup
###########

setup() {
  mkdir -p "${__inout_file_path}"
}

teardown() {
  rm -r $(dirname "${__inout_file_path}")
}

###########
# Tests
###########

@test "invoke aio-rename-files - help" {

  run ${__bin_path}/aio-rename-files -h
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-rename-files --help
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-rename-files h
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-rename-files help
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - help short" {

  run ${__bin_path}/aio-rename-files help short
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-rename-files help s
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-rename-files h s
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - help details" {

  run ${__bin_path}/aio-rename-files help details
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-rename-files help d
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-rename-files h d
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - help select" {

  run ${__bin_path}/aio-rename-files help select
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-rename-files h select
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - help in" {

  run ${__bin_path}/aio-rename-files help in
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-rename-files h in
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - help stdin" {

  run ${__bin_path}/aio-rename-files help stdin
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-rename-files h stdin
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - help out" {

  run ${__bin_path}/aio-rename-files help out
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-rename-files h out
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - help mix" {

  run ${__bin_path}/aio-rename-files help mix
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-rename-files h mix
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - examples" {

  run ${__bin_path}/aio-rename-files examples select
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-rename-files examples in
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-rename-files examples stdin
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-rename-files examples out
  [ ${status} -eq 0 ]
  
  run ${__bin_path}/aio-rename-files examples mix
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files --version" {
  run ${__bin_path}/aio-rename-files --version
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - no command" {
  run ${__bin_path}/aio-rename-files

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: at least one of the available rename commands must be provided ... stopping' ]

  run ${__bin_path}/aio-rename-files "${testInFileA}" "${testInFileB}"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: at least one of the available rename commands must be provided ... stopping' ]

  run ${__bin_path}/aio-rename-files select 1 ::: dummyIn1

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: at least one of the available rename commands must be provided ... stopping' ]
}

@test "invoke aio-rename-files - non-existing input file" {
  run ${__bin_path}/aio-rename-files out prepend 'foo' ::: dummyIn1
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: inputfile [dummyIn1] does not exist ... stopping' ]

  run ${__bin_path}/aio-rename-files out append 'foo' ::: dummyIn1

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: inputfile [dummyIn1] does not exist ... stopping' ]

  run ${__bin_path}/aio-rename-files in keep 1 ::: dummyIn1

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: inputfile [dummyIn1] does not exist ... stopping' ]

  run ${__bin_path}/aio-rename-files in cut 1 ::: dummyIn1

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: inputfile [dummyIn1] does not exist ... stopping' ]

  run ${__bin_path}/aio-rename-files stdin value ::: dummyIn1

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: inputfile [dummyIn1] does not exist ... stopping' ]

  run ${__bin_path}/aio-rename-files stdin kv song ::: dummyIn1

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: inputfile [dummyIn1] does not exist ... stopping' ]
}

@test "invoke aio-rename-files - missing command terminator" {
  run ${__bin_path}/aio-rename-files out prepend 'foo' "${testInFileA}" "${testInFileB}"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: expected command terminator [:::] but got ['"${testInFileB}"'] ... stopping' ]

  run ${__bin_path}/aio-rename-files out append 'foo' "${testInFileA}" "${testInFileB}"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: expected command terminator [:::] but got ['"${testInFileB}"'] ... stopping' ]

  run ${__bin_path}/aio-rename-files in keep 1 "${testInFileA}" "${testInFileB}"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: expected command terminator [:::] but got ['"${testInFileB}"'] ... stopping' ]

  run ${__bin_path}/aio-rename-files in cut 1 "${testInFileA}" "${testInFileB}"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: expected command terminator [:::] but got ['"${testInFileB}"'] ... stopping' ]

  run ${__bin_path}/aio-rename-files stdin value "${testInFileA}" "${testInFileB}"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: expected command terminator [:::] but got ['"${testInFileA}"'] ... stopping' ]

  run ${__bin_path}/aio-rename-files stdin kv song "${testInFileA}" "${testInFileB}"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: expected command terminator [:::] but got ['"${testInFileB}"'] ... stopping' ]

  run ${__bin_path}/aio-rename-files select 1 "${testInFileA}" "${testInFileB}"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: expected select terminator [:::] but got ['"${testInFileB}"'] ... stopping' ]

  run ${__bin_path}/aio-rename-files stdin value ::: select 1 "${testInFileA}" "${testInFileB}"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: expected select terminator [:::] but got ['"${testInFileB}"'] ... stopping' ]

  # missing ::: for 'stdin value' will always be detected, no matter the
  # position in the command chain as 'stdin value' expects no further option
  # but must always be followed by a :::
  run ${__bin_path}/aio-rename-files stdin value select 1 ::: "${testInFileA}" "${testInFileB}"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: expected command terminator [:::] but got [select] ... stopping' ]
}

@test "invoke aio-rename-files - missing command terminator - not detected" {

  skip 'missing command terminator not yet detected'

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # a missing ::: for an earlier command in the command chain might
  # cause an different error because there is still a ::: in the last command
  # of the command chain
  # this is always the case for commands that allow arbitrary string
  # as options, such as stdin key-value, prepend, append
  #
  # for commands that expect a certain type of option, another error
  # will be issued. this is the case for 'in cut/keep' and 'select'
  # as those commands expect integrs or integer-ranges
  #
  # currently 'stdin value' is the only command where a missing :::
  # can be detected reliably, as no further option needs to be given
  # thus 'stdin value' must always be followed by a :::
  #
  # we use --dry-run in order to avoid copying files

  # 'select 1' is considered for append value
  run ${__bin_path}/aio-rename-files --dry-run out append 'foo' select 1 ::: "${fileList[@]}"
  [ ${status} -eq 1 ]

  # 'select 1' is considered for prepend value
  run ${__bin_path}/aio-rename-files --dry-run out prepend 'foo' select 1 ::: "${fileList[@]}"
  [ ${status} -eq 1 ]

  # 'select 1' is considered as key-names
  run ${__bin_path}/aio-rename-files --dry-run stdin kv song select 1 ::: "${fileList[@]}" <<<"song foo"
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: std input expected [3] keys per section but only got [1] ... stopping" ]
  [ "${lines[1]}" == "error: could not prepare data for command [key-value] ... stopping" ]

  # 'select 1' will cause an invalid-format error as select is neither an integer nor a integer-range
  run ${__bin_path}/aio-rename-files --dry-run in keep 1 select 1 ::: "${fileList[@]}"
  [ ${status} -eq 1 ]
  [ "${output}" == "error: invalid index selector [select]" ]

  # 'select 1' will cause an invalid-format error as select is neither an integer nor a integer-range
  run ${__bin_path}/aio-rename-files --dry-run in cut 1 select 1 ::: "${fileList[@]}"
  [ ${status} -eq 1 ]
  [ "${output}" == "error: invalid index selector [select]" ]

  # 'in cut 1' will cause an invalid-format error as 'in' and 'cut' are neither integers nor a integer-range
  run ${__bin_path}/aio-rename-files --dry-run select 1 in cut 1 ::: "${fileList[@]}"
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: invalid index selector [in]" ]
  [ "${lines[1]}" == "error: invalid index selector [cut]" ]
}

@test "invoke aio-rename-files - different location - out - replace - last - single value" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # replace '-' with '---'
  # "test-session-a.ogg" => "test-session---a.ogg"
  # "test-session-b.ogg" => "test-session---b.ogg"
  run ${__bin_path}/aio-rename-files out replace last '-' with '---' ::: "${fileList[@]}"
  
  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp 'test-session---a' 'test-session---b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - last - single value" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # replace '-' with '---'
  # "test-session-a.ogg" => "test-session---a.ogg"
  # "test-session-b.ogg" => "test-session---b.ogg"
  run ${__bin_path}/aio-rename-files out replace last '-' with '---' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'test-session---a' 'test-session---b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - last - single value - at begin" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # replace 'test' with 'xxx'
  # "test-session-a.ogg" => "xxx-session-a.ogg"
  # "test-session-b.ogg" => "xxx-session-b.ogg"
  run ${__bin_path}/aio-rename-files out replace last 'test' with 'xxx' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'xxx-session-a' 'xxx-session-b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - last - single value - at begin - remove" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # replace 'test-' with ''
  # "test-session-a.ogg" => "session-a.ogg"
  # "test-session-b.ogg" => "session-b.ogg"
  run ${__bin_path}/aio-rename-files out replace last 'test-' with '' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'session-a' 'session-b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - last - single value - at end" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileASession}" "${testInFileBSession}") )

  # replace 'session' with 'xxx'
  # "test-a-session.ogg" => "test-a-xxx.ogg"
  # "test-b-session.ogg" => "test-b-xxx.ogg"
  run ${__bin_path}/aio-rename-files out replace last 'session' with 'xxx' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'test-a-xxx' 'test-b-xxx'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - last - single value - at end - remove" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileASession}" "${testInFileBSession}") )

  # replace '-session-' with ''
  # "test-a-session.ogg" => "test-a.ogg"
  # "test-b-session.ogg" => "test-b.ogg"
  run ${__bin_path}/aio-rename-files out replace last '-session' with '' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'test-a' 'test-b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - last - multiple values" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # replace '-' with 'xxx' and 'yyy'
  # "test-session-a.ogg" => "test-sessionxxxa.ogg"
  # "test-session-b.ogg" => "test-sessionyyyb.ogg"
  run ${__bin_path}/aio-rename-files out replace last '-' with 'xxx' 'yyy' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'test-sessionxxxa' 'test-sessionyyyb'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - last - multiple values - at begin" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # replace 'test' with 'xxx' and 'yyy'
  # "test-session-a.ogg" => "xxx-session-a.ogg"
  # "test-session-b.ogg" => "yyy-session-b.ogg"
  run ${__bin_path}/aio-rename-files out replace last 'test' with 'xxx' 'yyy' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'xxx-session-a' 'yyy-session-b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - last - multiple values - at end" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileASession}" "${testInFileBSession}") )

  # replace 'session' with 'xxx' and 'yyy'
  # "test-a-session.ogg" => "test-a-xxx.ogg"
  # "test-b-session.ogg" => "test-b-yyy.ogg"
  run ${__bin_path}/aio-rename-files out replace last 'session' with 'xxx' 'yyy' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'test-a-xxx' 'test-b-yyy'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - last - single value - squeeze" {

  skip "squeeze not yet implemented"
  
  local fileList
  fileList=( $(create-empty-files-tmp "${xxxTestInFileA}" "${xxxTestInFileB}") )

  # replace squeezed 'x' with '-'
  # "xxxtestxxxsessionxxxa.ogg" => "xxxtestxxxsession-a.ogg"
  # "xxxtestxxxsessionxxxb.ogg" => "xxxtestxxxsession-b.ogg"
  run ${__bin_path}/aio-rename-files out replace -s last 'x' with '-' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'xxxtestxxxsession-a' 'xxxtestxxxsession-b'
  [ ${status} -eq 0 ]
}


@test "invoke aio-rename-files - out - replace - last - single value - squeeze - remove" {

  skip "squeeze not yet implemented"
  
  local fileList
  fileList=( $(create-empty-files-tmp "${xxxTestInFileA}" "${xxxTestInFileB}") )

  # replace squeezed 'x' with ''
  # "xxxtestxxxsessionxxxa.ogg" => "xxxtestxxxsessiona.ogg"
  # "xxxtestxxxsessionxxxb.ogg" => "xxxtestxxxsessionb.ogg"
  run ${__bin_path}/aio-rename-files out replace -s last 'x' with '' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'xxxtestxxxsessiona' 'xxxtestxxxsessionb'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - last - multiple value - squeeze" {

  skip "squeeze not yet implemented"
  
  local fileList
  fileList=( $(create-empty-files-tmp "${xxxTestInFileA}" "${xxxTestInFileB}") )

  # replace squeezed 'x' with 'nnn' and 'mmm'
  # "xxxtestxxxsessionxxxa.ogg" => "xxxtestxxxsession-a.ogg"
  # "xxxtestxxxsessionxxxb.ogg" => "xxxtestxxxsession_b.ogg"
  run ${__bin_path}/aio-rename-files out replace -s last 'x' with '-' '_' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'xxxtestxxxsession-a' 'xxxtestxxxsession-b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - first - single value" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # replace '-' with '---'
  # "test-session-a.ogg" => "test---session-a.ogg"
  # "test-session-b.ogg" => "test---session-b.ogg"
  run ${__bin_path}/aio-rename-files out replace first '-' with '---' ::: "${fileList[@]}"

  # printf '# l1 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'test---session-a' 'test---session-b'
  [ ${status} -eq 0 ]
  
  fileList=( $(create-empty-files-tmp "${testInFileA}") )

  # verify that the whole filename can be replaced as well
  run ${__bin_path}/aio-rename-files out replace first 'test-session-a' with 'xxx-session-a' ::: "${fileList[@]}"

  # printf '# l2 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'xxx-session-a'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - first - single value - at begin" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # replace 'test' with 'xxx'
  # "test-session-a.ogg" => "xxx-session-a.ogg"
  # "test-session-b.ogg" => "xxx-session-b.ogg"
  run ${__bin_path}/aio-rename-files out replace first 'test' with 'xxx' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'xxx-session-a' 'xxx-session-b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - first - single value - at begin - remove" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # replace 'test-' with ''
  # "test-session-a.ogg" => "session-a.ogg"
  # "test-session-b.ogg" => "session-b.ogg"
  run ${__bin_path}/aio-rename-files out replace --sb first 'test-' with '' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'session-a' 'session-b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - first - single value - at end" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileASession}" "${testInFileBSession}") )

  # replace 'session' with 'xxx'
  # "test-a-session.ogg" => "test-a-xxx.ogg"
  # "test-b-session.ogg" => "test-b-xxx.ogg"
  run ${__bin_path}/aio-rename-files out replace first 'session' with 'xxx' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'test-a-xxx' 'test-b-xxx'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - first - single value - at end - remove" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileASession}" "${testInFileBSession}") )

  # replace '-session' with ''
  # "test-a-session.ogg" => "test-a.ogg"
  # "test-b-session.ogg" => "test-b.ogg"
  run ${__bin_path}/aio-rename-files out replace first '-session' with '' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp 'test-a' 'test-b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - first - multiple values" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # replace '-' with 'xxx' and 'yyy'
  # "test-session-a.ogg" => "testxxxsession-a.ogg"
  # "test-session-b.ogg" => "testyyysession-b.ogg"
  run ${__bin_path}/aio-rename-files out replace first '-' with 'xxx' 'yyy' ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'testxxxsession-a' 'testyyysession-b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - first - multiple values - at begin" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # replace 'test' with 'xxx' and 'yyy'
  # "test-session-a.ogg" => "xxx-session-a.ogg"
  # "test-session-b.ogg" => "yyy-session-b.ogg"
  run ${__bin_path}/aio-rename-files out replace first 'test' with 'xxx' 'yyy' ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'xxx-session-a' 'yyy-session-b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - first - multiple values - at end" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileASession}" "${testInFileBSession}") )

  # replace 'session' with 'xxx' and 'yyy'
  # "test-a-session.ogg" => "test-a-xxx.ogg"
  # "test-b-session.ogg" => "test-b-yyy.ogg"
  run ${__bin_path}/aio-rename-files out replace first 'session' with 'xxx' 'yyy' ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'test-a-xxx' 'test-b-yyy'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - firstlast - single value" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # replace '-' with '---'
  # "test-session-a.ogg" => "test---session---a.ogg"
  # "test-session-b.ogg" => "test---session---b.ogg"
  run ${__bin_path}/aio-rename-files out replace firstlast '-' with '---' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp 'test---session---a' 'test---session---b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - firstlast - single value - remove" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # remove '-'
  # "test-session-a.ogg" => "testsessiona.ogg"
  # "test-session-b.ogg" => "testsessionb.ogg"
  run ${__bin_path}/aio-rename-files out replace firstlast '-' with '' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'testsessiona' 'testsessionb'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - firstlast - multiple values" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # replace '-' with 'xxx' and 'yyy'
  # "test-session-a.ogg" => "testxxxsessionxxxa.ogg"
  # "test-session-b.ogg" => "testyyysessionyyyb.ogg"
  run ${__bin_path}/aio-rename-files out replace firstlast '-' with 'xxx' 'yyy' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp 'testxxxsessionxxxa' 'testyyysessionyyyb'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - all - single value" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # replace '-' with '---'
  # "test-session-a.ogg" => "test---session---a.ogg"
  # "test-session-b.ogg" => "test---session---b.ogg"
  run ${__bin_path}/aio-rename-files out replace all '-' with '---' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp 'test---session---a' 'test---session---b'
  [ ${status} -eq 0 ]

  # verify that a delimiter-only filename is correctly replaced
  fileList=( $(create-empty-files-tmp "${xxxxxxxxxFile}") )
  run ${__bin_path}/aio-rename-files out replace all 'xxx' with 'y' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'yyy'
  [ ${status} -eq 0 ]

  # verify that a delimiter-only filename is correctly replaced
  fileList=( $(create-empty-files-tmp "${xxxxxxxxxFile}") )
  run ${__bin_path}/aio-rename-files out replace all 'xx' with 'y' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'yyyyx'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - all - single value - squeeze" {

  local fileList
  fileList=( $(create-empty-files-tmp "${xxxxxxxxxFile}") )

  # verify that a delimiter-only filename is correctly replaced and squeezed

  # replace 'xxx' with 'y' and remove repeats of 'xxx'
  # "xxxxxxxxx.ogg" => "y.ogg"
  run ${__bin_path}/aio-rename-files out replace -s all 'xxx' with 'y' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'y'
  [ ${status} -eq 0 ]

  # replace 'xx' with 'y' and remove repeats of 'xx'
  # "xxxxxxxxx.ogg" => "yx.ogg"
  run ${__bin_path}/aio-rename-files out replace -s all 'xx' with 'y' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'yx'
  [ ${status} -eq 0 ]

  # replace 'y' with 'x' and remove repeats of 'y'
  # "yyyxyyy.ogg" => "xxx.ogg"
  fileList=( $(create-empty-files-tmp "${yyyxyyyFile}") )
  run ${__bin_path}/aio-rename-files out replace -s all 'y' with 'x' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'xxx'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - all - single value - at begin" {

  local fileList
  fileList=( $(create-empty-files-tmp "${xxxTestInFileA}" "${xxxTestInFileB}") )

  # replace 'xxx' with '_'
  # "xxxtestxxxsessionxxxa.ogg" => "_test_session_a.ogg"
  # "xxxtestxxxsessionxxxb.ogg" => "_test_session_b.ogg"
  run ${__bin_path}/aio-rename-files out replace all 'xxx' with '_' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp '_test_session_a' '_test_session_b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - all - single value - at begin - strip" {

  local fileList
  fileList=( $(create-empty-files-tmp "${xxxTestInFileA}" "${xxxTestInFileB}") )

  # replace 'xxx' with '__' but remove the pattern from filename begin
  # "xxxtestxxxsessionxxxa.ogg" => "__test__session__a.ogg" => "test__session__a.ogg"
  # "xxxtestxxxsessionxxxb.ogg" => "__test__session__b.ogg" => "test__session__b.ogg"
  run ${__bin_path}/aio-rename-files out replace --sb all 'xxx' with '__' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp 'test__session__a' 'test__session__b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - all - single value - at end" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileAxxx}" "${testInFileBxxx}") )

  # replace 'xxx' with '_'
  # "testxxxsessionxxxaxxx.ogg" => "test_session_a_.ogg"
  # "testxxxsessionxxxbxxx.ogg" => "test_session_b_.ogg"
  run ${__bin_path}/aio-rename-files out replace all 'xxx' with '_' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp 'test_session_a_' 'test_session_b_'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - all - single value - at end - strip" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileAxxx}" "${testInFileBxxx}") )

  # replace 'xxx' with '__' but remove the pattern from filename end
  # "testxxxsessionxxxaxxx.ogg" => "test_session_a_.ogg" => "test__session__a.ogg"
  # "testxxxsessionxxxbxxx.ogg" => "test_session_b_.ogg" => "test__session__b.ogg"
  run ${__bin_path}/aio-rename-files out replace --se all 'xxx' with '__' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp 'test__session__a' 'test__session__b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - all - single value - at begin and end" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileATest}" "${testInFileBTest}") )

  # replace 'test' with 'xxx'
  # "test-session-a-test.ogg" => "xxx-session-a-xxx.ogg"
  # "test-session-b-test.ogg" => "xxx-session-b-xxx.ogg"
  run ${__bin_path}/aio-rename-files out replace all 'test' with 'xxx' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp 'xxx-session-a-xxx' 'xxx-session-b-xxx'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - all - single value - at begin and end - strip" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileDoubleUnderscoreA}" "${testInFileDoubleUnderscoreB}") )

  # replace '__' with '-' but only at filename begin and end
  # "__test__session__a__out__.ogg" => "-test-session-a-out-.ogg" => "test-session-a-out.ogg"
  # "__test__session__b__out__.ogg" => "-test-session-b-out-.ogg" => "test-session-b-out.ogg"
  run ${__bin_path}/aio-rename-files out replace --sb --se all '__' with '-' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp 'test-session-a-out' 'test-session-b-out'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - all - single value - at begin and end - strip - errors" {

  local fileList
  fileList=( $(create-empty-files-tmp 'xx') )

  # replace 'x' with 'y' but 'x' is stripped from begin and end
  # => output file does not change
  run ${__bin_path}/aio-rename-files out replace --sb --se all 'x' with 'y' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]
  
  [ "${lines[0]}" == "error: renamed file is the same as input file [${__inout_file_path}/xx] ... skipping" ]
  [ "${lines[1]}" == "error: only [0] of [1] files have been renamed ... stopping" ]
  
  # replace 'xx' with 'yy' but 'xx' is stripped from begin and end
  # => output file does not change
  run ${__bin_path}/aio-rename-files out replace --sb --se all 'xx' with 'yy' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]
  
  [ "${lines[0]}" == "error: renamed file is the same as input file [${__inout_file_path}/xx] ... skipping" ]
  [ "${lines[1]}" == "error: only [0] of [1] files have been renamed ... stopping" ]
}

@test "invoke aio-rename-files - out - replace - all - single value - at begin and end - squeeze and strip - errors" {

  local fileList
  fileList=( $(create-empty-files-tmp 'xxx') )

  # replace 'x' with 'y' but repetitions of 'x' are squeezed and resulting 'x' ist stripped from begin and end
  # => output file does not change
  run ${__bin_path}/aio-rename-files out replace -s --sb --se  all 'x' with 'y' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]
  
  [ "${lines[0]}" == "error: renamed file is the same as input file [${__inout_file_path}/xxx] ... skipping" ]
  [ "${lines[1]}" == "error: only [0] of [1] files have been renamed ... stopping" ]
}

@test "invoke aio-rename-files - out - replace - all - multiple values" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # replace '-' with 'xxx' and 'yyy'
  # "test-session-a.ogg" => "testxxxsessionxxxa.ogg"
  # "test-session-b.ogg" => "testyyysessionyyyb.ogg"
  run ${__bin_path}/aio-rename-files out replace all '-' with 'xxx' 'yyy' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp 'testxxxsessionxxxa' 'testyyysessionyyyb'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - all - multiple values - at begin" {

  local fileList
  fileList=( $(create-empty-files-tmp "${xxxTestInFileA}" "${xxxTestInFileB}") )

  # replace 'xxx' with '_' '__'
  # "xxxtestxxxsessionxxxa.ogg" => "_test_session_a.ogg"
  # "xxxtestxxxsessionxxxb.ogg" => "__test__session__b.ogg"
  run ${__bin_path}/aio-rename-files out replace all 'xxx' with '_' '__' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp '_test_session_a' '__test__session__b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - all - multiple values - at begin - strip" {

  local fileList
  fileList=( $(create-empty-files-tmp "${xxxTestInFileA}" "${xxxTestInFileB}") )

  # replace 'xxx' with '--' and '__' but only from filename begin
  # "xxxtestxxxsessionxxxa.ogg" => "--test--session--a.ogg" => "test--session--a.ogg"
  # "xxxtestxxxsessionxxxb.ogg" => "__test__session__b.ogg" => "test__session__b.ogg"
  run ${__bin_path}/aio-rename-files out replace --sb all 'xxx' with '--' '__' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp 'test--session--a' 'test__session__b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - all - multiple values - at end" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileAxxx}" "${testInFileBxxx}") )

  # replace 'xxx' with '_' '__'
  # "testxxxsessionxxxaxxx.ogg" => "test_session_a_.ogg"
  # "testxxxsessionxxxbxxx.ogg" => "test__session_b__.ogg"
  run ${__bin_path}/aio-rename-files out replace all 'xxx' with '_' '__' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp 'test_session_a_' 'test__session__b__'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - all - multiple values - at end - strip" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileAxxx}" "${testInFileBxxx}") )

  # replace 'xxx' with '--' '__' but remove pattern from filename end
  # "testxxxsessionxxxaxxx.ogg" => "test--session--a--.ogg" => "test--session--a.ogg"
  # "testxxxsessionxxxbxxx.ogg" => "test__session__b__.ogg" => "test__session__b.ogg"
  run ${__bin_path}/aio-rename-files out replace --se all 'xxx' with '--' '__' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp 'test--session--a' 'test__session__b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - all - multiple values - at begin and end" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileATest}" "${testInFileBTest}") )

  # replace 'test' with 'xxx' and 'yyy'
  # "test-session-a-test.ogg" => "xxx-session-a-xxx.ogg"
  # "test-session-b-test.ogg" => "yyy-session-b-yyy.ogg"
  run ${__bin_path}/aio-rename-files out replace all 'test' with 'xxx' 'yyy' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp 'xxx-session-a-xxx' 'yyy-session-b-yyy'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - all - multiple values - at begin and end - strip" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileDoubleUnderscoreA}" "${testInFileDoubleUnderscoreB}") )

  # replace '___' with '_' and '-'
  # "__test__session__a__out__.ogg" => "_test_session_a_out_.ogg" => "test_session_a_out.ogg"
  # "__test__session__b__out__.ogg" => "-test-session-b-out_.ogg" => "test-session-b-out.ogg"
  run ${__bin_path}/aio-rename-files out replace --sb --se all '__' with '_' '-' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp 'test_session_a_out' 'test-session-b-out'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - all - single value - not all files" {

  local fileList
  fileList=( $(create-empty-files-tmp "${xxxxxxxxxFile}" "${yyyxyyyFile}") )

  # replace all consecutive repetitions of 'y' with a single 'x'
  # "xxxxxxxxx.ogg" => remains as-is and generates a warning about already existing file
  # "yyyxyyy.ogg"   => "xxx.ogg"
  run ${__bin_path}/aio-rename-files out replace --squeeze-repeats all 'y' with 'x' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: renamed file is the same as input file [${__inout_file_path}/xxxxxxxxx.ogg] ... skipping" ]
  [ "${lines[2]}" == "error: only [1] of [2] files have been renamed ... stopping" ]
  
  run assert-files-exist-tmp 'xxx'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - mixed - in - whitespace - out - replace - all - single char" {

  local fileList
  local line

  while read -r line; do
    fileList+=("${line}") 
  done < <(create-empty-files-tmp 'ARC 6A 1 Bitches & Witches, womens prog.ogg' 'Bitches + Witches, womens prog.ogg')
  
  run ${__bin_path}/aio-rename-files in --di ' ' keep 1: ::: out replace all ',' with '' ::: --dry-run "${fileList[@]}"

  # printf '# l1 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  [ "${lines[0]}" == "[0]: ${fileList[0]} -> ${__inout_file_path}/ARC-6A-1-Bitches-&-Witches-womens-prog.ogg" ]
  [ "${lines[1]}" == "[1]: ${fileList[1]} -> ${__inout_file_path}/Bitches-+-Witches-womens-prog.ogg" ]

  run ${__bin_path}/aio-rename-files in --di ' ' keep 3: ::: out replace all '&' with '' ::: --dry-run "${fileList[@]}"

  # printf '# l2 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  [ "${lines[0]}" == "[0]: ${fileList[0]} -> ${__inout_file_path}/1-Bitches--Witches,-womens-prog.ogg" ]
  [ "${lines[1]}" == "[1]: ${fileList[1]} -> ${__inout_file_path}/Witches,-womens-prog.ogg" ]
}

@test "invoke aio-rename-files - mixed - in - whitespace - out - replace - all - chars pattern" {

  skip "char pattern to replace not yet supported"

  local fileList
  local line

  while read -r line; do
    fileList+=("${line}") 
  done < <(create-empty-files-tmp 'ARC 6A 1 Bitches & Witches, womens prog.ogg' 'Bitches + Witches, womens prog.ogg')
 
  run ${__bin_path}/aio-rename-files in --di ' ' keep 3: ::: out replace all '[&,]' with '' ::: -d "${fileList[@]}"

  printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - all - delimiter whitespace" {

  local fileList
  local line

  while read -r line; do
    fileList+=("${line}") 
  done < <(create-empty-files-tmp 'ARC 6A 1 Bitches & Witches, womens prog.ogg' 'Bitches + Witches, womens prog.ogg')

  run ${__bin_path}/aio-rename-files o r a ' ' with '-' ::: -d "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - all - preserve whitespaces" {

  local fileList
  fileList=( "$(create-empty-files-tmp "this-is a-whitespace-file")" )

  # test if whitespace in filename will be preserved
  run ${__bin_path}/aio-rename-files o r a 'i' with '' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp 'ths-s a-whtespace-fle'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - first - preserve whitespaces" {

  local fileList
  fileList=( "$(create-empty-files-tmp "this-is a-whitespace-file")" )

  # test if whitespace in filename will be preserved
  run ${__bin_path}/aio-rename-files o r f 'i' with '' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp 'ths-is a-whitespace-file'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - last - preserve whitespaces" {

  local fileList
  fileList=( "$(create-empty-files-tmp "this-is a-whitespace-file")" )

  # test if whitespace in filename will be preserved
  run ${__bin_path}/aio-rename-files o r l 'i' with '' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp 'this-is a-whitespace-fle'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - first last - preserve whitespaces" {

  local fileList
  fileList=( "$(create-empty-files-tmp "this-is a-whitespace-file")" )

  # test if whitespace in filename will be preserved
  run ${__bin_path}/aio-rename-files o r fl 'i' with '' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp 'ths-is a-whitespace-fle'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - replace - not allowed - multiple statements " {

  # fail, wrong cl argument parsing => error: expected command terminator [:::] but got [-d] ... stopping
  run ${__bin_path}/aio-rename-files in --di ' ' keep 1: ::: out replace all ',' with '' ::: out replace all '&' with '' ::: -d dummy-file

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: replace command can only be provided once ... stopping' ]
}

@test "invoke aio-rename-files - out - replace - not allowed - replace pattern" {

  local fileList
  fileList=( $(create-empty-files-tmp "${xxxxxxxxxFile}") )

  local value
  local char

  for char in "${__special_chars_list[@]}"; do
    
    value="foo${char}bar"
    
    run ${__bin_path}/aio-rename-files out replace first 'x' with "${value}" ::: "${fileList[@]}"

    [ ${status} -eq 1 ]
    [ "${output}"  == "error: value [${value}] contains special char(s) ... stopping" ]

    run ${__bin_path}/aio-rename-files out replace last 'x' with "${value}" ::: "${fileList[@]}"

    [ ${status} -eq 1 ]
    [ "${output}"  == "error: value [${value}] contains special char(s) ... stopping" ]

    run ${__bin_path}/aio-rename-files out replace all 'x' with "${value}" ::: "${fileList[@]}"

    [ ${status} -eq 1 ]
    [ "${output}"  == "error: value [${value}] contains special char(s) ... stopping" ]
  
  done
  
  for char in "${__control_chars_list[@]}"; do

    value=$(printf "foo${char}bar")
    
    run ${__bin_path}/aio-rename-files out replace first 'x' with "${value}" ::: "${fileList[@]}"

    [ ${status} -eq 1 ]
    [ "${output}"  == "error: value [${value}] contains control char(s) ... stopping" ]
    
    run ${__bin_path}/aio-rename-files out replace last 'x' with "${value}" ::: "${fileList[@]}"

    [ ${status} -eq 1 ]
    [ "${output}"  == "error: value [${value}] contains control char(s) ... stopping" ]
    
    run ${__bin_path}/aio-rename-files out replace all 'x' with "${value}" ::: "${fileList[@]}"

    [ ${status} -eq 1 ]
    [ "${output}"  == "error: value [${value}] contains control char(s) ... stopping" ]
  
  done
}

@test "invoke aio-rename-files - out - replace - not allowed - delimiter" {
  
  local fileList
  fileList=( $(create-empty-files-tmp "${xxxxxxxxxFile}") )
  
  local delim
  local char

  for char in "${__special_chars_list[@]}"; do
    
    run ${__bin_path}/aio-rename-files --delimiter-out "${char}" out replace first 'x' with 'y' ::: "${fileList[@]}"

    [ ${status} -eq 1 ]
    [ "${output}"  == "error: delimiter [${char}] contains special char ... stopping" ]

    run ${__bin_path}/aio-rename-files --delimiter-out "${char}" out replace last 'x' with 'y' ::: "${fileList[@]}"

    [ ${status} -eq 1 ]
    [ "${output}"  == "error: delimiter [${char}] contains special char ... stopping" ]

    run ${__bin_path}/aio-rename-files --delimiter-out "${char}" out replace all 'x' with 'y' ::: "${fileList[@]}"

    [ ${status} -eq 1 ]
    [ "${output}"  == "error: delimiter [${char}] contains special char ... stopping" ]
  
  done
  
  for char in "${__control_chars_list[@]}"; do

    delim=$(printf "${char}")
    
    run ${__bin_path}/aio-rename-files --delimiter-out "${delim}" out replace first 'x' with 'y' ::: "${fileList[@]}"

    [ ${status} -eq 1 ]
    [ "${output}"  == "error: delimiter [${delim}] contains control char ... stopping" ]
    
    run ${__bin_path}/aio-rename-files --delimiter-out "${delim}" out replace last 'x' with 'y' ::: "${fileList[@]}"

    [ ${status} -eq 1 ]
    [ "${output}"  == "error: delimiter [${delim}] contains control char ... stopping" ]
    
    run ${__bin_path}/aio-rename-files --delimiter-out "${delim}" out replace all 'x' with 'y' ::: "${fileList[@]}"

    [ ${status} -eq 1 ]
    [ "${output}"  == "error: delimiter [${delim}] contains control char ... stopping" ]
  
  done

}

@test "invoke aio-rename-files - out - append - single value" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # prepend value "foo" to filename
  run ${__bin_path}/aio-rename-files out append "out" ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'test-session-a-out' 'test-session-b-out'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - append - special chars - not allowed" {
  
  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}") )
  
  local value
  local char

  for char in "${__special_chars_list[@]}"; do

    value="foo${char}bar"
    run ${__bin_path}/aio-rename-files out append  "${value}" ::: "${fileList[@]}"

    [ ${status} -eq 1 ]
    [ "${output}"  == "error: value [${value}] contains special char(s) ... stopping" ]
  
  done
  
  for char in "${__control_chars_list[@]}"; do

    value=$(printf "foo${char}bar")
    run ${__bin_path}/aio-rename-files out append  "${value}" ::: "${fileList[@]}"

    [ ${status} -eq 1 ]
    [ "${output}"  == "error: value [${value}] contains control char(s) ... stopping" ]
  
  done
}

@test "invoke aio-rename-files - out - append - delimiter" {
  skip "test to be implemented"
}

@test "invoke aio-rename-files - out - append - delimiter - special chars - not allowed" {
  
  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}") )
  
  local delim
  local char

  for char in "${__special_chars_list[@]}"; do

    run ${__bin_path}/aio-rename-files --delimiter-out "${char}" out append  'dummy' ::: "${fileList[@]}"

    [ ${status} -eq 1 ]
    [ "${output}"  == "error: delimiter [${char}] contains special char ... stopping" ]
  
  done
  
  for char in "${__control_chars_list[@]}"; do

    delim=$(printf "${char}")
    run ${__bin_path}/aio-rename-files --delimiter-out "${delim}" out append  'dummy' ::: "${fileList[@]}"

    [ ${status} -eq 1 ]
    [ "${output}"  == "error: delimiter [${delim}] contains control char ... stopping" ]
  
  done
}

@test "invoke aio-rename-files - out - prepend - single value" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # prepend value "foo" to filename
  run ${__bin_path}/aio-rename-files out prepend "foo" ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'foo-test-session-a' 'foo-test-session-b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - prepend - special chars - not allowed" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}") )

  local value
  local char

  for char in "${__special_chars_list[@]}"; do

    value="foo${char}bar"
    run ${__bin_path}/aio-rename-files out prepend "${value}" ::: "${fileList[@]}" <<< "${value}"

    [ ${status} -eq 1 ]
    [ "${output}"  == "error: value [${value}] contains special char(s) ... stopping" ]
  
  done
  
  for char in "${__control_chars_list[@]}"; do

    value=$(printf "foo${char}bar")
    run ${__bin_path}/aio-rename-files out prepend "${value}" ::: "${fileList[@]}" <<< "${value}"

    [ ${status} -eq 1 ]
    [ "${output}"  == "error: value [${value}] contains control char(s) ... stopping" ]
  
  done
}

@test "invoke aio-rename-files - out - prepend - delimiter" {
  skip "to be implemented"
}

@test "invoke aio-rename-files - out - prepend - delimiter - special chars - not allowed" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}") )

  local delim
  local char

  for char in "${__special_chars_list[@]}"; do

    run ${__bin_path}/aio-rename-files --delimiter-out "${char}" out prepend  'dummy' ::: "${fileList[@]}"

    [ ${status} -eq 1 ]
    [ "${output}"  == "error: delimiter [${char}] contains special char ... stopping" ]
  
  done
  
  for char in "${__control_chars_list[@]}"; do

    delim=$(printf "${char}")
    run ${__bin_path}/aio-rename-files --delimiter-out "${delim}" out prepend  'dummy' ::: "${fileList[@]}"

    [ ${status} -eq 1 ]
    [ "${output}"  == "error: delimiter [${delim}] contains control char ... stopping" ]
  
  done
}

@test "invoke aio-rename-files - out - mixed" {

  local fileList
  fileList=( $(create-empty-files-tmp "${xxxxxxxxxFile}" "${yyyxyyyFile}") )

  # replace 'y' with 'x'
  # "xxxxxxxxx.ogg" => "xxxxxxxxx.ogg" => "test-xxxxxxxxx-session.ogg"
  # "yyyxyyy.ogg"   => "xxx.ogg"       => "test-xxx-session.ogg"
  run ${__bin_path}/aio-rename-files out append session ::: out prepend test ::: out replace --squeeze-repeats all 'y' with 'x' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'test-xxx-session' 'test-xxxxxxxxx-session'
  [ ${status} -eq 0 ]

  # "xxxxxxxxx.ogg" => "xxxxxxxxx.ogg" => "xxxxxxxxx-session.ogg"
  # "yyyxyyy.ogg"   => "xxx.ogg"       => "xxx-session.ogg"
  run ${__bin_path}/aio-rename-files out append session ::: out replace --squeeze-repeats all 'y' with 'x' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'xxx-session' 'xxxxxxxxx-session'
  [ ${status} -eq 0 ]

  # "xxxxxxxxx.ogg" => "xxxxxxxxx.ogg" => "test-xxxxxxxxx.ogg"
  # "yyyxyyy.ogg"   => "xxx.ogg"       => "test-xxx.ogg"
  run ${__bin_path}/aio-rename-files out prepend test ::: out replace --squeeze-repeats all 'y' with 'x' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'test-xxx' 'test-xxxxxxxxx'
  [ ${status} -eq 0 ]

  # output delimiter is empty
  # "test-session-a.ogg" => "__test-session-a__.ogg"
  # "test-session-b.ogg" => "__test-session-b__.ogg"
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )
  run ${__bin_path}/aio-rename-files --do '' out append '__' ::: out prepend '__' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '__test-session-a__' '__test-session-b__'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - prepend - whitespace" {
  
  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )
  
  run ${__bin_path}/aio-rename-files out prepend 'foo bar foo bar' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp 'foo-bar-foo-bar-test-session-a-narrow' 'foo-bar-foo-bar-test-session-b-narrow'
  [ ${status} -eq 0 ]

  remove-expected-output-files-tmp 'foo-bar-foo-bar-test-session-a-narrow' 'foo-bar-foo-bar-test-session-b-narrow'

  run ${__bin_path}/aio-rename-files out prepend 'foo        bar     foo       bar' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp 'foo-bar-foo-bar-test-session-a-wide' 'foo-bar-foo-bar-test-session-b-wide'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - prepend - multiple values" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}") )

  # prepend multiple values to filename
  # whitespaces in prepend values will be replaced by --delimiter-out

  run ${__bin_path}/aio-rename-files out prepend 'foo' 'bar' 'foo' 'bar' ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'foo-test-session-a' 'bar-test-session-b' 'foo-test-session-c' 'bar-test-session-d'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - prepend - empty value" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # prepend value is missing
  run ${__bin_path}/aio-rename-files out prepend ::: "${fileList[@]}"

  [ ${status} -eq 1 ]
  [ "${output}" == "error: no values provided for [prepend] ... stopping" ]

  # prepend value is empty string
  run ${__bin_path}/aio-rename-files out prepend '' ::: "${fileList[@]}"

  [ ${status} -eq 1 ]
  [ "${output}" == "error: no values provided for [prepend] ... stopping" ]
}

@test "invoke aio-rename-files - out - prepend - delimiter out" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # prepend value "foo" to filename
  run ${__bin_path}/aio-rename-files --do '_' out prepend "foo" ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'foo-test-session-a-underscore' 'foo-test-session-b-underscore'
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-rename-files --do 'abc' out prepend "foo" ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'foo-test-session-a-abc' 'foo-test-session-b-abc'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - prepend - delimiter out - none" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # prepend value "foo" to filename
  run ${__bin_path}/aio-rename-files --do '' out prepend "foo" ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'foo-test-session-a-none' 'foo-test-session-b-none'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - prepend - select - index - single" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # prepend value "foo" to filename
  run ${__bin_path}/aio-rename-files out prepend "foo" ::: select 2 ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp 'foo-test-session-b'
  [ ${status} -eq 0 ]

  run assert-files-not-exist-tmp 'foo-test-session-a'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - prepend - select - index - multiple" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}") )

  # prepend value "foo" to filename
  run ${__bin_path}/aio-rename-files out prepend "foo" ::: select 2 3 ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'foo-test-session-b' 'foo-test-session-c'
  [ ${status} -eq 0 ]

  run assert-files-not-exist-tmp 'foo-test-session-a' 'foo-test-session-d'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - prepend - select - index range - single" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}") )

  # prepend value "foo" to filename
  run ${__bin_path}/aio-rename-files out prepend "foo" ::: select 2:4 ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'foo-test-session-b' 'foo-test-session-c' 'foo-test-session-d'
  [ ${status} -eq 0 ]

  run assert-files-not-exist-tmp 'foo-test-session-a'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - prepend - select - index range - multiple - non-overlapping" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}") )

  # prepend value "foo" to filename
  run ${__bin_path}/aio-rename-files out prepend "foo" ::: select 1:2 3:4 ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'foo-test-session-a' 'foo-test-session-b' 'foo-test-session-c' 'foo-test-session-d'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - prepend - select - index range - multiple - overlapping" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}") )

  # prepend value "foo" to filename
  run ${__bin_path}/aio-rename-files out prepend "foo" ::: select 1:3 2:3 ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'foo-test-session-a' 'foo-test-session-b' 'foo-test-session-c'
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp 'foo-test-session-d'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - prepend - select - index range - cover all - single" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}") )

  # prepend value "foo" to filename
  run ${__bin_path}/aio-rename-files out prepend "foo" ::: select 2: ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp 'foo-test-session-b' 'foo-test-session-c' 'foo-test-session-d'
  [ ${status} -eq 0 ]

  run assert-files-not-exist-tmp 'foo-test-session-a'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - prepend - select - index range - cover all - overlapping" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}") )

  # prepend value "foo" to filename
  run ${__bin_path}/aio-rename-files out prepend "foo" ::: select 3: 2: ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'foo-test-session-b' 'foo-test-session-c' 'foo-test-session-d'
  [ ${status} -eq 0 ]

  run assert-files-not-exist-tmp 'foo-test-session-a'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - prepend - select - index - mixed" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}" "${testInFileE}" "${testInFileF}") )

  run ${__bin_path}/aio-rename-files out prepend "foo" ::: select 4: 1 3 5:6 ::: "${fileList[@]}"

  # printf '# l1 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp 'foo-test-session-a'  'foo-test-session-c' 'foo-test-session-d' 'foo-test-session-e' 'foo-test-session-f'
  [ ${status} -eq 0 ]

  run assert-files-not-exist-tmp 'foo-test-session-b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - prepend - select - index range - cover all - mask index out of range" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}") )

  run ${__bin_path}/aio-rename-files out prepend "foo" ::: select 5 3: ::: "${fileList[@]}"
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp 'foo-test-session-c' 'foo-test-session-d'
  [ ${status} -eq 0 ]

  run assert-files-not-exist-tmp 'foo-test-session-a'  'foo-test-session-b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - prepend - select - index - single - out of range" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # prepend value "foo" to filename
  run ${__bin_path}/aio-rename-files out prepend "foo" ::: select 5 ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]
  [ "${output}" == "error: the length of one or both of the given sets [2, 2] must be greater or equal than the max-index value [5] ... stopping" ]

  run assert-files-not-exist-tmp 'foo-test-session-a'  'foo-test-session-b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - prepend - select - less than expected number of input files" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileB}") )

  # select two tag sections on stdin but only provide one input file => results in an error
  run ${__bin_path}/aio-rename-files out prepend "foo" ::: select 1 2 ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]
  [ "${output}" == "error: the length of one or both given sets [1, 1] does neither fit the given number of indeces [2] or the largest index value [2] ... stopping" ]
}

@test "invoke aio-rename-files - out - prepend - select - more than necessary number of input files" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}") )

  # select two tag sections on stdin but only provide one input file => results in an error
  run ${__bin_path}/aio-rename-files out prepend "foo" ::: select 2 ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp 'foo-test-session-b'
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp 'foo-test-session-a' 'foo-test-session-c' 'foo-test-session-d'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - out - prepend - same as input" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}") )

  # even though prepend value is the same as inputfile name, renamed filename
  # wont be the same as input, as value is prepended to input filename
  run ${__bin_path}/aio-rename-files out prepend "test-session-a" ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'test-session-a-test-session-a'
  [ ${status} -eq 0 ]
}


@test "invoke aio-rename-files - in cut - out prepend" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in cut 2 ::: "${fileList[@]}"

  # printf '# l1 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp '2020-test-a' '2020-test-b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in cut - delimiter-in - not matching delimiter" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # input filenames do not contain the specified delimiter, thus
  # cut index 2 does not cut any part
  run ${__bin_path}/aio-rename-files in --di '__' cut 2 ::: "${fileList[@]}"

  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ ${#lines[@]} -eq 3 ]
  [ "${lines[0]}" == "error: renamed file is the same as input file [${fileList[0]}] ... skipping" ]
  [ "${lines[1]}" == "error: renamed file is the same as input file [${fileList[1]}] ... skipping" ]
  [ "${lines[2]}" == "error: only [0] of [2] files have been renamed ... stopping" ]

  # input filenames do not contain the specified delimiter, thus
  # cut index 1 remove the whole file name
  run ${__bin_path}/aio-rename-files in --di '__' cut 1 ::: "${fileList[@]}"

  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ ${#lines[@]} -eq 3 ]
  [ "${lines[0]}" == "error: no new name generated for input file [${fileList[0]}] ... skipping" ]
  [ "${lines[1]}" == "error: no new name generated for input file [${fileList[1]}] ... skipping" ]
  [ "${lines[2]}" == "error: only [0] of [2] files have been renamed ... stopping" ]
}

@test "invoke aio-rename-files - in cut - delimiter-in - no parts" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}") )
  
  # name: test-session-a
  # two parts separated by delimiter-in: 'tes' 'session-a'
  # no part to cut due to cut index too high
  run ${__bin_path}/aio-rename-files i --di 't-' c 3 ::: "${fileList[@]}"

  # printf '# l1 %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: renamed file is the same as input file [${fileList[0]}] ... skipping" ]
  [ "${lines[1]}" == "error: only [0] of [1] files have been renamed ... stopping" ]
  
  # name: test-session-a
  # two parts separated by delimiter-in: 'es' '-session-a'
  # no part to cut due to cut index too high
  run ${__bin_path}/aio-rename-files i --di 't' c 3 ::: "${fileList[@]}"

  # printf '# l2 %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: renamed file is the same as input file [${fileList[0]}] ... skipping" ]
  [ "${lines[1]}" == "error: only [0] of [1] files have been renamed ... stopping" ]

  fileList=( $(create-empty-files-tmp "${xxxxxxxxxFile}") )
  
  # name: xxxxxxxxx
  # no part separated by delimiter-in
  # no part to cut due to cut index too high
  run ${__bin_path}/aio-rename-files i --di 'x' c 1 ::: "${fileList[@]}"

  # printf '# l3 %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: renamed file is the same as input file [${fileList[0]}] ... skipping" ]
  [ "${lines[1]}" == "error: only [0] of [1] files have been renamed ... stopping" ]
  
  # name: xxxxxxxxx
  # one part separated by delimiter-in: last 'x'
  # no new name generated due to cut of only part
  run ${__bin_path}/aio-rename-files i --di 'xx' c 1 ::: "${fileList[@]}"

  # printf '# l4 %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: no new name generated for input file [${fileList[0]}] ... skipping" ]
  [ "${lines[1]}" == "error: only [0] of [1] files have been renamed ... stopping" ]
  
  fileList=( $(create-empty-files-tmp "${yyyxyyyFile}") )
  
  # name: yyyxyyy
  # one part separated by delimiter-in: 'x'
  # no part to cut due to cut index too high
  run ${__bin_path}/aio-rename-files i --di 'y' c 2 ::: "${fileList[@]}"

  # printf '# l5 %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: renamed file is the same as input file [${fileList[0]}] ... skipping" ]
  [ "${lines[1]}" == "error: only [0] of [1] files have been renamed ... stopping" ]

  # name: yyyxyyy
  # one part separated by delimiter-in: 'x'
  # no new name generated due to cut of only part
  run ${__bin_path}/aio-rename-files i --di 'y' c 1 ::: "${fileList[@]}"

  # printf '# l6 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: no new name generated for input file [${fileList[0]}] ... skipping" ]
  [ "${lines[1]}" == "error: only [0] of [1] files have been renamed ... stopping" ]
  
  # name: yyyxyyy
  # one part separated by delimiter-in: 'x'
  # no new name generated due to cut of only part
  run ${__bin_path}/aio-rename-files i --di 'yyy' c 1 ::: "${fileList[@]}"

  # printf '# l7 %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: no new name generated for input file [${fileList[0]}] ... skipping" ]
  [ "${lines[1]}" == "error: only [0] of [1] files have been renamed ... stopping" ]
}

@test "invoke aio-rename-files - in cut - delimiter-in - preserve whitespaces" {

  local fileList
  fileList=( "$(create-empty-files-tmp "this-is a-whitespace-file")" )

  run ${__bin_path}/aio-rename-files in --di '-' cut 3 ::: "${fileList[0]}"
  
  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp 'this-is a-file'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in cut - out - prepend - delimiter out" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # use underscore as delimiter in output-file to separate remmaining name parts
  run ${__bin_path}/aio-rename-files --do '_' out prepend "2020" ::: in cut 2 ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-a-underscore' '2020-test-b-underscore'
  [ ${status} -eq 0 ]

  # use abc as delimiter in output-file to separate remaining name parts
  run ${__bin_path}/aio-rename-files --do 'abc' out prepend "2020" ::: in cut 2 ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-a-abc' '2020-test-b-abc'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in cut - out - prepend - delimiter out - none" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # dont use a delimiter in output file to separate remaining parts
  run ${__bin_path}/aio-rename-files --do '' out prepend "2020" ::: in cut 2 ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp '2020-test-a-none' '2020-test-b-none'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in cut - out - prepend - delimiter in - delimiter out" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileUnderscoreA}" "${testInFileUnderscoreB}") )

  # input-file names have a underscore that separates name parts
  # use dash as delimiter in output-file to separate remaiing name parts
  run ${__bin_path}/aio-rename-files --do '-' out prepend "2020" ::: in --di '_' cut 2 ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-a' '2020-test-b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in cut - out - prepend - delimiter in - delimiter out - none" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileUnderscoreA}" "${testInFileUnderscoreB}") )

  # input-file names have a underscore that separates name parts
  # use dash as delimiter in output-file to separate remaining name parts
  run ${__bin_path}/aio-rename-files --do '' out prepend "2020" ::: in --di '_' cut 2 ::: "${fileList[@]}"

  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-a-none' '2020-test-b-none'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in cut - one part" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # cut the first word of input filename (index starts from 1)
  run ${__bin_path}/aio-rename-files in cut 1 ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'session-a' 'session-b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in cut - more parts" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # keep the first and second word of input filename (index starts from 0)
  run ${__bin_path}/aio-rename-files in cut 1 2 ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'a' 'b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in cut - index too high" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # no input filename part exists for index 10
  run ${__bin_path}/aio-rename-files in cut 10 ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: renamed file is the same as input file [${fileList[0]}] ... skipping" ]
  [ "${lines[1]}" == "error: renamed file is the same as input file [${fileList[1]}] ... skipping" ]
  [ "${lines[2]}" == "error: only [0] of [2] files have been renamed ... stopping" ]

  # only index 1 is taken into account. index 10 is too high
  run ${__bin_path}/aio-rename-files in cut 1 10 ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'session-a' 'session-b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in cut - erroneous index" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # the first index is no positive integer, thus no further indeces are read
  run ${__bin_path}/aio-rename-files in cut a 1 2 ::: "${fileList[@]}"

  [ ${status} -eq 1 ]
  [ "${output}" == "error: invalid index selector [a]" ]

  # the first index is no positive integer, thus no further indeces are read
  run ${__bin_path}/aio-rename-files in cut -1 1 2 ::: "${fileList[@]}"

  [ ${status} -eq 1 ]
  [ "${output}" == "error: invalid index selector [-1]" ]

  # the first index is no positive integer, thus no further indeces are read
  run ${__bin_path}/aio-rename-files in cut 1a1 ::: "${fileList[@]}"

  [ ${status} -eq 1 ]
  [ "${output}" == "error: invalid index selector [1a1]" ]

  # the second index "a" is no positive integer
  run ${__bin_path}/aio-rename-files in cut 1 a 2 b ::: "${fileList[@]}"

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: invalid index selector [a]" ]
  [ "${lines[1]}" == "error: invalid index selector [b]" ]
}

@test "invoke aio-rename-files - in cut - output same as input" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # cut all parts of input filename (index starts from 1)
  run ${__bin_path}/aio-rename-files in cut 1:3 ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ ${#lines[@]} -eq 3 ]
  [ "${lines[0]}" == "error: no new name generated for input file [${fileList[0]}] ... skipping" ]
  [ "${lines[1]}" == "error: no new name generated for input file [${fileList[1]}] ... skipping" ]
  [ "${lines[2]}" == "error: only [0] of [2] files have been renamed ... stopping" ]
}

@test "invoke aio-rename-files - in cut - out - prepend - select - index - single" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in cut 2 ::: select 2 ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp '2020-test-a'
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp '2020-test-b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in cut - out - prepend - select - index - multiple" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}") )

  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in cut 2 ::: select 2 4 ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp '2020-test-a' '2020-test-c'
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp '2020-test-b' '2020-test-d'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in cut - out - prepend - select - index range - single" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}") )

  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in cut 2 ::: select 2:4 ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp '2020-test-a'
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp '2020-test-b' '2020-test-c' '2020-test-d'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in cut - out - prepend - select - index range - multiple - non-overlapping" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}" "${testInFileE}" "${testInFileF}") )

  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in cut 2 ::: select 1:2 4:6 ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp '2020-test-c'
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp '2020-test-a' '2020-test-b' '2020-test-d' '2020-test-e' '2020-test-f'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in cut - out - prepend - select - index range - multiple - overlapping" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}" "${testInFileE}" "${testInFileF}") )

  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in cut 2 ::: select 3:5 2:4 ::: "${fileList[@]}"
  [ ${status} -eq 0 ]

  run assert-files-not-exist-tmp '2020-test-a' '2020-test-f'
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp '2020-test-b' '2020-test-c' '2020-test-d' '2020-test-e' 
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in cut - out - prepend - select - index range - cover all - single" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}" "${testInFileE}" "${testInFileF}") )

  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in cut 2 ::: select 3: ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp '2020-test-a' '2020-test-b'
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp '2020-test-c' '2020-test-d' '2020-test-e' '2020-test-f'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in cut - out - prepend - select - index range - cover all - overlapping" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}" "${testInFileE}" "${testInFileF}") )

  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in cut 2 ::: select 5: 3: ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp '2020-test-a' '2020-test-b'
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp '2020-test-c' '2020-test-d' '2020-test-e' '2020-test-f'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in cut - out - prepend - select - index - mixed" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}" "${testInFileE}" "${testInFileF}") )

  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in cut 2 ::: select 4: 1 3 5:6 ::: "${fileList[@]}"

  # printf '# l1 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp '2020-test-b'
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp '2020-test-a' '2020-test-c' '2020-test-d' '2020-test-e' '2020-test-f'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in cut - out - prepend - select - index range - cover all - mask index out of range" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}") )

  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in cut 2 ::: select 5 3: ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp '2020-test-a' '2020-test-b'
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp '2020-test-c' '2020-test-d'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in cut - out - prepend - select - index - single - out of range" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}") )

  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in cut 2 ::: select 5 ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]
  [ "${output}" == "error: the length of one or both of the given sets [4, 4] must be greater or equal than the max-index value [5] ... stopping" ]

  run assert-files-not-exist-tmp '2020-test-a' '2020-test-b' '2020-test-c' '2020-test-d'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in cut - out - prepend - select - less than expected number of input files" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileB}") )

  # select two tag sections on stdin but only provide one input file => results in an error
  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in cut 2 ::: select 1 2 ::: "${fileList[@]}" <<EOF
01 session foo
02 session bar
EOF

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]
  [ "${output}" == "error: the length of one or both given sets [1, 1] does neither fit the given number of indeces [2] or the largest index value [2] ... stopping" ]
}

@test "invoke aio-rename-files - in cut - out - prepend - select - more than necessary number of input files" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}") )

  # select two tag sections on stdin but only provide one input file => results in an error
  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in cut 2 ::: select 2 ::: "${fileList[@]}" <<EOF
01 session foo
02 session bar
EOF

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-not-exist-tmp '2020-test-a' '2020-test-c' '2020-test-d'
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp '2020-test-b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in keep - one part" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # keep the third word of input filename (index starts from 1)
  run ${__bin_path}/aio-rename-files in keep 3 ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'a' 'b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in keep - more parts" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # keep the first and third word of input filename (index starts from 0)
  run ${__bin_path}/aio-rename-files in keep 1 3 ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'test-a' 'test-b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in keep - index too high" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # no input filename part exists for index 10
  run ${__bin_path}/aio-rename-files in keep 10 ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]
  
  [ "${lines[0]}" == "error: no new name generated for input file [${fileList[0]}] ... skipping" ]
  [ "${lines[1]}" == "error: no new name generated for input file [${fileList[1]}] ... skipping" ]
  [ "${lines[2]}" == "error: only [0] of [2] files have been renamed ... stopping" ]

  # only index 3 is taken into account. index 10 is too high
  run ${__bin_path}/aio-rename-files in keep 3 10 ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'a' 'b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in keep - erroneous index" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # the first index is no positive integer, thus no further indeces are read
  run ${__bin_path}/aio-rename-files in keep a 1 2 ::: "${testInFileA}" "${testInFileB}"

  [ ${status} -eq 1 ]
  [ "${output}" == "error: invalid index selector [a]" ]

  # the first index is no positive integer, thus no further indeces are read
  run ${__bin_path}/aio-rename-files in keep -1 1 2 ::: "${fileList[@]}"

  [ ${status} -eq 1 ]
  [ "${output}" == "error: invalid index selector [-1]" ]

  # the first index is no positive integer, thus no further indeces are read
  run ${__bin_path}/aio-rename-files in keep 1a1 ::: "${fileList[@]}"

  [ ${status} -eq 1 ]
  [ "${output}" == "error: invalid index selector [1a1]" ]

  # the second index "a" is no positive integer
  run ${__bin_path}/aio-rename-files in keep 1 a 2 b ::: "${fileList[@]}"

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: invalid index selector [a]" ]
  [ "${lines[1]}" == "error: invalid index selector [b]" ]
}

@test "invoke aio-rename-files - in keep - delimiter-in - no parts" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # input filenames do not contain the specified delimiter, thus
  # keep index 1 does select the whole name: output is then the same as input
  run ${__bin_path}/aio-rename-files in --di '__' keep 1 3 ::: "${fileList[@]}"
  # printf '# l1 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]

  [ ${#lines[@]} -eq 3 ]
  [ "${lines[0]}" == "error: renamed file is the same as input file [${__inout_file_path}/${testInFileA}] ... skipping" ]
  [ "${lines[1]}" == "error: renamed file is the same as input file [${__inout_file_path}/${testInFileB}] ... skipping" ]
  [ "${lines[2]}" == "error: only [0] of [2] files have been renamed ... stopping" ]

  # input filenames do not contain the specified delimiter, thus
  # keep index range 2: does not select anything and no name is generated at all
  run ${__bin_path}/aio-rename-files in --di '__' keep 2: ::: "${fileList[@]}"
  # printf '# l2 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]

  [ ${#lines[@]} -eq 3 ]
  [ "${lines[0]}" == "error: no new name generated for input file [${__inout_file_path}/${testInFileA}] ... skipping" ]
  [ "${lines[1]}" == "error: no new name generated for input file [${__inout_file_path}/${testInFileB}] ... skipping" ]
  [ "${lines[2]}" == "error: only [0] of [2] files have been renamed ... stopping" ]
}

@test "invoke aio-rename-files - in keep - delimiter-in - no match" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}") )
  
  # only two parts separated by delimiter-in 
  # no part to keep
  run ${__bin_path}/aio-rename-files i --di 't-' k 3 ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: no new name generated for input file [${fileList[0]}] ... skipping" ]
  [ "${lines[1]}" == "error: only [0] of [1] files have been renamed ... stopping" ]
  
  # only one part due to delimiter-in  at name end
  # no part to keep
  run ${__bin_path}/aio-rename-files i --di '-a' k 3 ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: no new name generated for input file [${fileList[0]}] ... skipping" ]
  [ "${lines[1]}" == "error: only [0] of [1] files have been renamed ... stopping" ]
  
  run ${__bin_path}/aio-rename-files i --di '-s' k 3 ::: "${fileList[@]}"
}

@test "invoke aio-rename-files - in keep - replace delimiters" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileUnderscoreA}" "${testInFileUnderscoreB}") )

  # we keep all parts and just replace all underscores in input files by dashes
  run ${__bin_path}/aio-rename-files --do '-' in --di '_' keep 1: ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  [ -f "${__inout_file_path}/${testInFileA}" ]
  [ -f "${__inout_file_path}/${testInFileB}" ]

  rm -f "${__inout_file_path}/${testInFileA}"
  rm -f "${__inout_file_path}/${testInFileB}"

  fileList=()
  while read -r line; do
    fileList+=("${line}")
  done < <( create-empty-files-tmp "${testInFileSpacesA}" "${testInFileSpacesB}" )
  
  # we keep all parts and just replace all spaces in input files by dashes
  run ${__bin_path}/aio-rename-files --do '-' in --di ' ' keep 1: ::: "${fileList[@]}"
  [ ${status} -eq 0 ]

  [ -f "${__inout_file_path}/${testInFileA}" ]
  [ -f "${__inout_file_path}/${testInFileB}" ]
}

@test "invoke aio-rename-files - in keep - replace single word" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # we keep all parts and just replace the 'session' in input file by "replace"
  # using the input delimiter will replace the given word or char at any position,
  # except at the very begin of the name
  run ${__bin_path}/aio-rename-files --do 'replace' in --di 'session' keep 1: ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'test-replace-a' 'test-replace-b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in keep - replace single word - at begin" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  run ${__bin_path}/aio-rename-files --do 'replace' in --di 'test-' keep 1: ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'session-a' 'session-b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in keep - replace single word - at begin - error" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # this call will fail because delimiter-in only exists at filename begin
  # in keep will therefore extract everything after 'test' as one string but since
  # there is a '-' char now directly at filename begin, an error is issued
  # as that char is not allowed
  run ${__bin_path}/aio-rename-files --do 'replace' in --di 'test' keep 1: ::: "${fileList[@]}"

  # printf '# l1 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]
  [ "${output}" == "error: file-name [-session-a] starts with not-allowed char ... stopping" ]
  
  # this call will fail because delimiter-in only exists at filename begin
  # thus there is only one part available and in keep wont select anything
  # the output name remains as-is
  run ${__bin_path}/aio-rename-files --do 'replace' in --di 'test-' keep 2 ::: "${fileList[@]}"

  # printf '# l2 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: no new name generated for input file [${fileList[0]}] ... skipping" ]
  [ "${lines[1]}" == "error: no new name generated for input file [${fileList[1]}] ... skipping" ]
  [ "${lines[2]}" == "error: only [0] of [2] files have been renamed ... stopping" ]
}

@test "invoke aio-rename-files - in keep - delimiter-in - at end" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}") )

  # delimiter-in only exists at filename end in keep will select
  # the part before 'a' as one string as there is only one 'a' 
  # in used filename
  run ${__bin_path}/aio-rename-files --do 'replace' in --di 'a' keep 1: ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == "'${fileList[0]}' -> '${__inout_file_path}/test-session-.ogg'" ]
}

@test "invoke aio-rename-files - in keep - delimiter-in - at end - error" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}") )

  # this call will fail because delimiter-in only exists at filename end
  # in keep will therefore extract everything before 'a' (there is only one
  # 'a' in used filename) as one string but since there is no part on the right
  # hand side of 'a', no output-delimiter could be be inserted

  run ${__bin_path}/aio-rename-files --do 'replace' in --di 'a' keep 2: ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: no new name generated for input file [${fileList[0]}] ... skipping" ]
  [ "${lines[1]}" == "error: only [0] of [1] files have been renamed ... stopping" ]
}

@test "invoke aio-rename-files - in keep - remove single word" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # we keep all parts and just replace the '-session' in input file by an empty output-delimiter
  # in this this example we could also use 'in cut 2' but using the input delimiter will replace
  # the given word or char at any position, except at the very begin of the name
  run ${__bin_path}/aio-rename-files --do '' in --di '-session' keep 1: ::: "${fileList[@]}"
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp 'test-a' 'test-b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in keep - output same as input" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # keep the all parts of input filename (index starts from 1)
  run ${__bin_path}/aio-rename-files in keep 1: ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ ${#lines[@]} -eq 3 ]
  [ "${lines[0]}" == "error: renamed file is the same as input file [${__inout_file_path}/${testInFileA}] ... skipping" ]
  [ "${lines[1]}" == "error: renamed file is the same as input file [${__inout_file_path}/${testInFileB}] ... skipping" ]
  [ "${lines[2]}" == "error: only [0] of [2] files have been renamed ... stopping" ]
}

@test "invoke aio-rename-files - in keep - out prepend" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in keep 1 3 ::: "${fileList[@]}"
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp '2020-test-a' '2020-test-b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in keep - out prepend - delimiter out" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # use underscore as delimiter in output-file to separate kept name parts
  run ${__bin_path}/aio-rename-files --do '_' out prepend "2020" ::: in keep 1 3 ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-a-underscore' '2020-test-b-underscore'
  [ ${status} -eq 0 ]

  # use abc as delimiter in output-file to separate kept name parts
  run ${__bin_path}/aio-rename-files --do 'abc' out prepend "2020" ::: in keep 1 3 ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-a-abc' '2020-test-b-abc'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in keep - out prepend - delimiter out - none" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # dont use a delimiter in output file to separate name parts
  run ${__bin_path}/aio-rename-files --do '' out prepend "2020" ::: in keep 1 3 ::: "${fileList[@]}"

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-a-none' '2020-test-b-none'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in keep - out prepend - delimiter in - delimiter out" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileUnderscoreA}" "${testInFileUnderscoreB}") )

  # input-file names have a underscore that separates name parts
  # use dash as delimiter in output-file to separate kept name parts
  run ${__bin_path}/aio-rename-files --do '-' out prepend "2020" ::: in --di '_' keep 1 3 ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-a' '2020-test-b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in keep - out prepend - delimiter in - delimiter out - none" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileUnderscoreA}" "${testInFileUnderscoreB}") )

  # input-file names have a underscore that separates name parts
  # use dash as delimiter in output-file to separate kept name parts
  run ${__bin_path}/aio-rename-files --do '' out prepend "2020" ::: in --di '_' keep 1 3 ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-a-none' '2020-test-b-none'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in keep - out prepend - select - index - single" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in keep 1 3 ::: select 1 ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-a'
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp '2020-test-b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in keep - out prepend - select - index - multiple" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}") )

  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in keep 1 3 ::: select 1 3 ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-a' '2020-test-c'
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp '2020-test-b' '2020-test-d'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in keep - out prepend - select - index range - single" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}") )

  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in keep 1 3 ::: select 1:3 ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-a' '2020-test-b' '2020-test-c'
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp '2020-test-d'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in keep - out prepend - select - index range - multiple - non overlapping" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}" "${testInFileE}" "${testInFileF}") )

  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in keep 1 3 ::: select 1:3 5:6 ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-a' '2020-test-b' '2020-test-c' '2020-test-e' '2020-test-f'
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp '2020-test-d'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in keep - out prepend - select - index range - multiple - overlapping" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}" "${testInFileE}" "${testInFileF}") )

  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in keep 1 3 ::: select 3:5 2:6 ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-b' '2020-test-c' '2020-test-d' '2020-test-e' '2020-test-f'
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp '2020-test-a' 
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in keep - out prepend - select - index range - cover all - single" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}" "${testInFileE}" "${testInFileF}") )

  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in keep 1 3 ::: select 3: ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-c' '2020-test-d' '2020-test-e' '2020-test-f'
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp '2020-test-a' '2020-test-b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in keep - out prepend - select - index range - cover all - overlapping" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}" "${testInFileE}" "${testInFileF}") )

  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in keep 1 3 ::: select 5: 3: ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-c' '2020-test-d' '2020-test-e' '2020-test-f'
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp '2020-test-a' '2020-test-b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in keep - out prepend - select - index - mixed" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}" "${testInFileE}" "${testInFileF}") )

  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in keep 1 3 ::: select 4: 1 3 5:6 ::: "${fileList[@]}"

  # printf '# l1 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-a' '2020-test-c' '2020-test-d' '2020-test-e' '2020-test-f'
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp '2020-test-b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in keep - out prepend - select - index range - cover all - mask index out of range" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}") )

  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in keep 1 3 ::: select 5 3: ::: "${fileList[@]}"
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp '2020-test-c' '2020-test-d'
  [ ${status} -eq 0 ]

  run assert-files-not-exist-tmp '2020-test-a' '2020-test-b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in keep - out prepend - select - index - single - out of range" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in keep 1 3 ::: select 5 ::: "${fileList[@]}"

  [ ${status} -eq 1 ]
  [ "${output}" == "error: the length of one or both of the given sets [2, 2] must be greater or equal than the max-index value [5] ... stopping" ]

  run assert-files-not-exist-tmp '2020-test-a' '2020-test-b'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in keep - out prepend - select - less than expected number of input files" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileB}") )

  # select two tag sections on stdin but only provide one input file => results in an error
  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in keep 1 3 ::: select 1 2 ::: "${fileList[@]}" <<EOF
01 session foo
02 session bar
EOF

  [ ${status} -eq 1 ]
  [ "${output}" == "error: the length of one or both given sets [1, 1] does neither fit the given number of indeces [2] or the largest index value [2] ... stopping" ]
}

@test "invoke aio-rename-files - stdin value - out prepend - select - failure" {

  skip 'results in renaming failure'

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  run ${__bin_path}/aio-rename-files stdin value ::: out prepend ready steady go ::: select 1 3 ::: "${fileList[@]}" <<EOF
aaa
bbb
EOF

  # printf '# %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp 'aaa-ready.ogg' 'bbb-go.ogg'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in keep - out - prepend - select - more than necessary number of input files" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}") )

  # select two tag sections on stdin but only provide one input file => results in an error
  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in keep 1 3 ::: select 2 ::: "${fileList[@]}" <<EOF
01 session foo
02 session bar
EOF

  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-b'
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp '2020-test-a' '2020-test-c' '2020-test-d'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in - split - ordered" {

  local fileList
  fileList=( $(create-empty-files-tmp "asndlinasdlijn.ogg" "aaaaabbbbbcccccccdddd.ogg") )

  run ${__bin_path}/aio-rename-files in split 3 8 12 ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-exist 'asn-dlina-sdli-jn.ogg' 'aaa-aabbb-bbcc-cccccdddd.ogg'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in - split - unordered" {

  local fileList
  fileList=( $(create-empty-files-tmp "asndlinasdlijn.ogg" "aaaaabbbbbcccccccdddd.ogg") )

  run ${__bin_path}/aio-rename-files in split 12 8 3 ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-exist 'asn-dlina-sdli-jn.ogg' 'aaa-aabbb-bbcc-cccccdddd.ogg'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in - split - dublicates" {

  local fileList
  fileList=( $(create-empty-files-tmp "asndlinasdlijn.ogg" "aaaaabbbbbcccccccdddd.ogg") )

  run ${__bin_path}/aio-rename-files in split 3 12 12 8 3 ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-exist 'asn-dlina-sdli-jn.ogg' 'aaa-aabbb-bbcc-cccccdddd.ogg'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in - split - index is one" {

  local fileList
  fileList=( $(create-empty-files-tmp "asndlinasdlijn.ogg" "aaaaabbbbbcccccccdddd.ogg") )

  run ${__bin_path}/aio-rename-files in split 1 ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-exist 'a-sndlinasdlijn.ogg' 'a-aaaabbbbbcccccccdddd.ogg'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in - split - index partly too large" {

  local fileList
  fileList=( $(create-empty-files-tmp "asndlinasdlijn.ogg" "aaaaabbbbbcccccccdddd.ogg") )

  # asndlinasdlijn.ogg has less than 20 chars, thus the split at pos 20 wont happen
  # => filename can still  be splitted, thus no error occurs
  run ${__bin_path}/aio-rename-files in split 3 8 12 20 ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-exist 'asn-dlina-sdli-jn.ogg' 'aaa-aabbb-bbcc-cccccddd-d.ogg'
  [ ${status} -eq 0 ]
  
  # asndlinasdlijn.ogg has less than 20 chars, thus the split at pos 20 wont happen
  # => the filename remains as is and hence creates an error
  run ${__bin_path}/aio-rename-files in split 20 ::: "${fileList[@]}"
  [ ${status} -eq 1 ]
  
  [ "${lines[0]}" == "error: renamed file is the same as input file [/dev/shm/aio/rename-files/asndlinasdlijn.ogg] ... skipping" ]
  [ "${lines[1]}" == "'/dev/shm/aio/rename-files/aaaaabbbbbcccccccdddd.ogg' -> '/dev/shm/aio/rename-files/aaaaabbbbbcccccccddd-d.ogg'" ]
  [ "${lines[2]}" == "error: only [1] of [2] files have been renamed ... stopping" ]
  
  run assert-files-exist 'aaa-aabbb-bbcc-cccccddd-d.ogg'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - in - split - index too large" {

  local fileList
  fileList=( $(create-empty-files-tmp "asndlinasdlijn.ogg" "aaaaabbbbbcccccccdddd.ogg") )

  # here, both filenames are too short for splitting
  # => no renaming takes place at all and hence, output filenames are the same as input filenames which is results in an error
  run ${__bin_path}/aio-rename-files in split 30 ::: "${fileList[@]}"
  [ ${status} -eq 1 ]
  
  [ "${lines[0]}" == "error: renamed file is the same as input file [/dev/shm/aio/rename-files/asndlinasdlijn.ogg] ... skipping" ]
  [ "${lines[1]}" == "error: renamed file is the same as input file [/dev/shm/aio/rename-files/aaaaabbbbbcccccccdddd.ogg] ... skipping" ]
  [ "${lines[2]}" == "error: only [0] of [2] files have been renamed ... stopping" ]
}

@test "invoke aio-rename-files - in - split - index invalid" {

  local fileList
  fileList=( $(create-empty-files-tmp "asndlinasdlijn.ogg" "aaaaabbbbbcccccccdddd.ogg") )

  # negative index is not allowed
  run ${__bin_path}/aio-rename-files in split -5 3 8 12 20 ::: "${fileList[@]}"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: invalid index selector [-5]' ]

  # negative index is not allowed
  run ${__bin_path}/aio-rename-files in split -0 3 8 12 20 ::: "${fileList[@]}"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: invalid index selector [-0]' ]
  
  # index must be always greater 0 as a split a filename begin makes no sense
  run ${__bin_path}/aio-rename-files in split 0 ::: "${fileList[@]}"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: invalid index selector [0]' ]
}

@test "invoke aio-rename-files - mix - in - split - in - cut - error - not allowed" {

  local fileList
  fileList=( $(create-empty-files-tmp "asndlinasdlijn.ogg" "aaaaabbbbbcccccccdddd.ogg") )

  # currently fails
  run ${__bin_path}/aio-rename-files in split 3 ::: in cut 1 ::: "${fileList[@]}"
  
  # printf '# %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: expected only one [in] command but got [2] commands ... stopping' ]
}

@test "invoke aio-rename-files - mix - in - split - out - replace - error" {

  local fileList
  fileList=( $(create-empty-files-tmp "asndlinasdlijn.ogg" "aaaaabbbbbcccccccdddd.ogg") )

  run ${__bin_path}/aio-rename-files in split 3 ::: out replace all 'a' with  '' ::: "${fileList[@]}"
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: file-name [-bbbbbcccccccdddd] starts with not-allowed char ... stopping' ]
  
  run assert-files-not-exist 'sn-dlinsdlijn.ogg'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - mix - in - split - out - replace" {

  local fileList
  fileList=( $(create-empty-files-tmp "asndlinasdlijn.ogg" "aaaaabbbbbcccccccdddd.ogg") )

  run ${__bin_path}/aio-rename-files in split 3 ::: out replace all 'd' with  '' ::: "${fileList[@]}"
  [ ${status} -eq 0 ]
  
  run assert-files-exist 'asn-linaslijn.ogg' 'aaa-aabbbbbccccccc.ogg'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin value - one word" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  run ${__bin_path}/aio-rename-files stdin value ::: "${fileList[@]}" <<EOF
foo
bar
EOF

  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'foo' 'bar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin value - more words" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  run ${__bin_path}/aio-rename-files stdin value ::: "${fileList[@]}" <<EOF
01 session foo
02 session bar
EOF

  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '01-session-foo' '02-session-bar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin value - special chars - not allowed" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}") )

  local value
  local char
  
  for char in "${__special_chars_list[@]}"; do

    value="foo${char}bar"
    run ${__bin_path}/aio-rename-files stdin value ::: "${fileList[@]}" <<< "${value}"

    [ ${status} -eq 1 ]
    [ "${lines[0]}"  == "error: value [${value}] contains special char(s) ... stopping" ]
  
  done
  
  for char in "${__control_chars_list[@]}"; do

    value=$(printf "foo${char}bar")
    run ${__bin_path}/aio-rename-files stdin value ::: "${fileList[@]}" <<< "${value}"
    
    [ ${status} -eq 1 ]
    [ "${lines[0]}"  == "error: value [${value}] contains control char(s) ... stopping" ]
  
  done
}

@test "invoke aio-rename-files - stdin value - delimiter" {
  skip "to be implemented"
}

@test "invoke aio-rename-files - stdin value - delimiter - special chars - not allowed" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}") )

  local delim
  local char
  
  for char in "${__special_chars_list[@]}"; do

    run ${__bin_path}/aio-rename-files --delimiter-out "${char}" stdin value ::: "${fileList[@]}" <<< 'dummy'

    [ ${status} -eq 1 ]
    [ "${lines[0]}"  == "error: delimiter [${char}] contains special char ... stopping" ]
  
  done
  
  for char in "${__control_chars_list[@]}"; do

    delim=$(printf "${char}")
    run ${__bin_path}/aio-rename-files --delimiter-out "${delim}" stdin value ::: "${fileList[@]}" <<< "${value}"
    
    [ ${status} -eq 1 ]
    [ "${lines[0]}"  == "error: delimiter [${delim}] contains control char ... stopping" ]
  
  done
}

@test "invoke aio-rename-files - stdin value - with comments - heredoc" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  local -r indentWhitespaces='      '
  local -r indentTabs="$(printf '\t\t')"

  run ${__bin_path}/aio-rename-files stdin value ::: "${fileList[@]}" <<EOF
# a single line comment
01 session foo
# a
${indentTabs}# multi
${indentWhitespaces}# line
${indentTabs}${indentWhitespaces}# comment
02 session bar
EOF

  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '01-session-foo' '02-session-bar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin value - with empty lines - heredoc" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  local -r lineWithWhitespaces='      '
  local -r lineWithTabs="$(printf '\t\t\t')"

  run ${__bin_path}/aio-rename-files stdin value ::: "${fileList[@]}" <<EOF
# single empty line

01 session foo
# multiple empty lines with and without whitespaces and tabs

${lineWithWhitespaces}


${lineWithTabs}
${lineWithWhitespaces}
${lineWithTabs}${lineWithWhitespaces}
02 session bar
EOF

  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '01-session-foo' '02-session-bar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin value - empty lines only" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  local -r lineWithWhitespaces='      '
  local -r lineWithTabs="$(printf '\t\t\t')"

  # just pass empty lines
  run ${__bin_path}/aio-rename-files stdin value ::: "${fileList[@]}" <<EOF

${lineWithWhitespaces}
${lineWithTabs}
${lineWithWhitespaces}${lineWithTabs}
EOF

  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: value input on stdin is empty ... stopping" ]
  [ "${lines[1]}" == "error: could not prepare data for command [value] ... stopping" ]

  # empty value
  run ${__bin_path}/aio-rename-files stdin value ::: "${fileList[@]}" <<<""

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: value input on stdin is empty ... stopping" ]
  [ "${lines[1]}" == "error: could not prepare data for command [value] ... stopping" ]

  # empty value - whitespaces only
  run ${__bin_path}/aio-rename-files stdin value ::: "${fileList[@]}" <<<"${lineWithWhitespaces}"

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: value input on stdin is empty ... stopping" ]
  [ "${lines[1]}" == "error: could not prepare data for command [value] ... stopping" ]

  # empty value - tabs only
  run ${__bin_path}/aio-rename-files stdin value ::: "${fileList[@]}" <<<"${lineWithTabs}"

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: value input on stdin is empty ... stopping" ]
  [ "${lines[1]}" == "error: could not prepare data for command [value] ... stopping" ]

  # empty value - tabs and whitespaces only
  run ${__bin_path}/aio-rename-files stdin value ::: "${fileList[@]}" <<<"${lineWithTabs}${lineWithWhitespaces}"

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: value input on stdin is empty ... stopping" ]
  [ "${lines[1]}" == "error: could not prepare data for command [value] ... stopping" ]
}

@test "invoke aio-rename-files - stdin value - with indented data - heredoc" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  local -r indentWhitespaces='      '
  local -r indentTabs="$(printf '\t\t')"

  run ${__bin_path}/aio-rename-files stdin value ::: "${fileList[@]}" <<EOF
${indentWhitespaces}01${indentTabs}session${indentTabs}foo
${indentTabs}02${indentWhitespaces}session bar${indentWhitespaces}
EOF

  # printf "# %s\n" "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '01-session-foo' '02-session-bar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin value - read timeout" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # no value at all - should trigger timeout
  run ${__bin_path}/aio-rename-files stdin value ::: "${fileList[@]}"
  # printf "# l1 %s\n" "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: value input on stdin is empty ... stopping" ]
  [ "${lines[1]}" == "error: could not prepare data for command [value] ... stopping" ]

  # value available after delay - should trigger timeout
  run ${__bin_path}/aio-rename-files stdin value ::: "${fileList[@]}" < <(sleep 2; printf 'bar\n')
  # printf "# l2 %s\n" "${lines[@]}" >&3
  
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: value input on stdin is empty ... stopping" ]
  [ "${lines[1]}" == "error: could not prepare data for command [value] ... stopping" ]
}

@test "invoke aio-rename-files - stdin value - incomplete occurences" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # provide only one value for two files
  run ${__bin_path}/aio-rename-files stdin value ::: "${fileList[@]}" <<EOF

bar
EOF

  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: expected that command [value] generated [2] value(s) for [2] input file(s) but only got [1] value(s) ... stopping' ]

  run assert-files-not-exist-tmp 'session-foo' 'session-bar'
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-rename-files stdin value ::: "${fileList[@]}" <<< "bar"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: expected that command [value] generated [2] value(s) for [2] input file(s) but only got [1] value(s) ... stopping' ]

  run assert-files-not-exist-tmp 'session-foo' 'session-bar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin value - more names than input files" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # pass more lines via stdin than file to process
  run ${__bin_path}/aio-rename-files stdin value ::: "${fileList[@]}" <<EOF
session foo
session bar
session hello
session world
EOF

  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp 'session-foo' 'session-bar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin value - output same as input" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # renamed files have the same name as input files
  run ${__bin_path}/aio-rename-files stdin value ::: "${fileList[@]}" <<EOF
test session a
test session b
EOF

  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ ${#lines[@]} -eq 3 ]
  [ "${lines[0]}" == "error: renamed file is the same as input file [${fileList[0]}] ... skipping" ]
  [ "${lines[1]}" == "error: renamed file is the same as input file [${fileList[1]}] ... skipping" ]
  [ "${lines[2]}" == "error: only [0] of [2] files have been renamed ... stopping" ]
}

@test "invoke aio-rename-files - stdin value - here-string unquoted with whitespace" {

  # ./aio-rename-files stdin v ::: -d text-1.txt <<<a new name
  
  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}") )

  # here, "foo" and "w" are not part of the here-string and are considered as regular
  # cli argument, in this case both strins are interpreted as file-names
  run ${__bin_path}/aio-rename-files stdin value ::: "${fileList[@]}" <<<session foo w
  
  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]
  
  [ "${lines[0]}" == "error: inputfile [foo] does not exist ... stopping" ]
  [ "${lines[1]}" == "error: inputfile [w] does not exist ... stopping" ]
}

@test "invoke aio-rename-files - stdin value - in keep - prepend" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in keep 1 ::: stdin value ::: "${fileList[@]}" <<EOF
session foo
session bar
EOF

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-session-foo' '2020-test-session-bar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin value - in keep - out - prepend - delimiter out" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # use underscore as delimiter in output-file to separate kept name parts
  run ${__bin_path}/aio-rename-files --do '_' out prepend "2020" ::: in keep 1 ::: stdin value ::: "${fileList[@]}" <<EOF
session foo
session bar
EOF

  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-session-foo-underscore' '2020-test-session-bar-underscore'
  [ ${status} -eq 0 ]  

  # use abc as delimiter in output-file to separate kept name parts
  run ${__bin_path}/aio-rename-files --do 'abc' out prepend "2020" ::: in keep 1 ::: stdin value ::: "${fileList[@]}" <<EOF
session foo
session bar
EOF

  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-session-foo-abc' '2020-test-session-bar-abc'
  [ ${status} -eq 0 ]  
}

@test "invoke aio-rename-files - stdin value - in keep - out - prepend - delimiter out - none" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # dont use a delimiter in output file to separate name parts
  run ${__bin_path}/aio-rename-files --do '' out prepend "2020" ::: in keep 1 ::: stdin value ::: "${fileList[@]}" <<EOF
session foo
session bar
EOF

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-session-foo-none' '2020-test-session-bar-none'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin value - in keep - out - prepend - delimiter in - delimiter out" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileUnderscoreA}" "${testInFileUnderscoreB}") )

  # input-file names have a underscore that separates name parts
  # use dash as delimiter in output-file to separate kept name parts
  run ${__bin_path}/aio-rename-files --do '-' out prepend "2020" ::: in --di '_' keep 1 ::: stdin value ::: "${fileList[@]}" <<EOF
session foo
session bar
EOF

  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-session-foo' '2020-test-session-bar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin value - in keep - out - prepend - delimiter in - delimiter out - none" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileUnderscoreA}" "${testInFileUnderscoreB}") )

  # input-file names have a underscore that separates name parts
  # use dash as delimiter in output-file to separate kept name parts
  run ${__bin_path}/aio-rename-files --do '' out prepend "2020" ::: in --di '_' keep 1 ::: stdin value ::: "${fileList[@]}" <<EOF
session foo
session bar
EOF

  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-session-foo-none' '2020-test-session-bar-none'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin value - in keep - out - prepend - delimiter in - no parts" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # input filenames do not contain the specified delimiter, thus
  # keep index 2 does not select any filename part
  # output file name is solely composed of given data from stdin and prefix
  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in --di '__' keep 2 ::: stdin value ::: "${fileList[@]}" <<EOF
session foo
session bar
EOF

  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  # [ "${lines[0]}" == "'${fileList[0]}' -> '${__inout_file_path}/2020-session-foo.ogg'" ]
  # [ "${lines[1]}" == "'${fileList[1]}' -> '${__inout_file_path}/2020-session-bar.ogg'" ]
  
  run assert-files-exist-tmp '2020-session-foo' '2020-session-bar'
  [ ${status} -eq 0 ]

  # input filenames do not contain the specified delimiter, thus
  # keep index 1 selects the whole file name
  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in --di '__' keep 1 ::: stdin value ::: "${fileList[@]}" <<EOF
session foo
session bar
EOF

  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == "'${fileList[0]}' -> '${__inout_file_path}/2020-test-session-a-session-foo.ogg'" ]
  [ "${lines[1]}" == "'${fileList[1]}' -> '${__inout_file_path}/2020-test-session-b-session-bar.ogg'" ]
}

@test "invoke aio-rename-files - stdin value - in cut - prepend" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # delimiter-in: '-'
  # cut:          2:
  # file:         test-session-a > test-session
  # file:         test-session-b > test-session
  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in cut 2: ::: stdin value ::: "${fileList[@]}" <<EOF
session foo
session bar
EOF

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-session-foo' '2020-test-session-bar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin value - in cut - out - prepend - delimiter out" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # use underscore as delimiter in output-file to separate kept name parts
  run ${__bin_path}/aio-rename-files --do '_' out prepend "2020" ::: in cut 2: ::: stdin value ::: "${fileList[@]}" <<EOF
session foo
session bar
EOF

  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-session-foo-underscore' '2020-test-session-bar-underscore'
  [ ${status} -eq 0 ]

  # use abc as delimiter in output-file to separate kept name parts
  run ${__bin_path}/aio-rename-files --do 'abc' out prepend "2020" ::: in cut 2: ::: stdin value ::: "${fileList[@]}" <<EOF
session foo
session bar
EOF

  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-session-foo-abc' '2020-test-session-bar-abc'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin value - in cut - out - prepend - delimiter out - none" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # dont use a delimiter in output file to separate name parts
  run ${__bin_path}/aio-rename-files --do '' out prepend "2020" ::: in cut 2: ::: stdin value ::: "${fileList[@]}" <<EOF
session foo
session bar
EOF

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-session-foo-none' '2020-test-session-bar-none'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin value - in cut - out - prepend - delimiter in - delimiter out" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileUnderscoreA}" "${testInFileUnderscoreB}") )

  # input-file names have a underscore that separates name parts
  # use dash as delimiter in output-file to separate kept name parts
  run ${__bin_path}/aio-rename-files --do '-' out prepend "2020" ::: in --di '_' cut 2: ::: stdin value ::: "${fileList[@]}" <<EOF
session foo
session bar
EOF

  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-session-foo' '2020-test-session-bar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin value - in cut - out - prepend - delimiter in - delimiter out - none" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileUnderscoreA}" "${testInFileUnderscoreB}") )

  # input-file names have a underscore that separates name parts
  # use dash as delimiter in output-file to separate kept name parts
  run ${__bin_path}/aio-rename-files --do '' out prepend "2020" ::: in --di '_' cut 2: ::: stdin value ::: "${fileList[@]}" <<EOF
session foo
session bar
EOF

  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-session-foo-none' '2020-test-session-bar-none'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin value - in cut - out - prepend - delimiter in - no parts" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # input filenames do not contain the specified delimiter, thus
  # cut does not remove any filename part
  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in --di '__' cut 2: ::: stdin value ::: "${fileList[@]}" <<EOF
session foo
session bar
EOF

  # printf '# l1 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp '2020-test-session-a-session-foo' '2020-test-session-b-session-bar'
  [ ${status} -eq 0 ]

  # input filenames do not contain the specified delimiter, thus
  # cut does remove the whole filename
  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in --di '__' cut 1: ::: stdin value ::: "${fileList[@]}" <<EOF
session foo
session bar
EOF

  # printf '# l2 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp '2020-session-foo' '2020-session-bar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin value - select - one file - one index - more than one names" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileB}") )

  # select the 2nd line on stdin
  run ${__bin_path}/aio-rename-files stdin value ::: select 2 ::: "${fileList[@]}" <<EOF
01 session foo
02 session bar
EOF

  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '02-session-bar'
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp '01-session-foo'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin value - select - two files - two indeces - more than two names " {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # select the 3rd and 4th lines on stdin
  run ${__bin_path}/aio-rename-files stdin value ::: select 3 4 ::: "${fileList[@]}" <<EOF
01 session foo
02 session bar
03 session foobar
04 session barfoo
EOF

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '03-session-foobar' '04-session-barfoo'
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp '01-session-foo' '02-session-bar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin value - select - two files - two indeces cover all- more than two names " {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # select the 3rd and 4th lines on stdin
  run ${__bin_path}/aio-rename-files stdin value ::: select 3: ::: "${fileList[@]}" <<EOF
01 session foo
02 session bar
03 session foobar
04 session barfoo
EOF

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '03-session-foobar' '04-session-barfoo'
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp '01-session-foo' '02-session-bar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin value - select - more files - index-range - single" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}" "${testInFileE}" "${testInFileF}") )

  # select the 2nd line on stdin
  run ${__bin_path}/aio-rename-files stdin value ::: select 2:4 ::: "${fileList[@]}" <<EOF
01 session foo
02 session bar
03 session foobar
04 session barfoo
05 session boofar
06 session farboo
EOF

  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '02-session-bar' '03-session-foobar' '04-session-barfoo'
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp '01-session-foo' '05-session-boofar' '06-session-farboo'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin value - select - more files - index-range - multiple - non-overlapping" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}" "${testInFileE}" "${testInFileF}") )

  # select the 2nd line on stdin
  run ${__bin_path}/aio-rename-files stdin value ::: select 1:3 5:6 ::: "${fileList[@]}" <<EOF
01 session foo
02 session bar
03 session foobar
04 session barfoo
05 session boofar
06 session farboo
EOF

  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '01-session-foo' '02-session-bar' '03-session-foobar' '05-session-boofar' '06-session-farboo'
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp '04-session-barfoo'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin value - select - more files - index-range - multiple - overlapping" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}" "${testInFileE}" "${testInFileF}") )

  # select the 2nd line on stdin
  run ${__bin_path}/aio-rename-files stdin value ::: select 2:4 3:5 ::: "${fileList[@]}" <<EOF
01 session foo
02 session bar
03 session foobar
04 session barfoo
05 session boofar
06 session farboo
EOF

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp  '02-session-bar' '03-session-foobar' '04-session-barfoo' '05-session-boofar'
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp '01-session-foo' '06-session-farboo'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin value - select - more files - index range - cover all - single" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}" "${testInFileE}" "${testInFileF}") )

  # select the 2nd line on stdin
  run ${__bin_path}/aio-rename-files stdin value ::: select 3: ::: "${fileList[@]}" <<EOF
01 session foo
02 session bar
03 session foobar
04 session barfoo
05 session boofar
06 session farboo
EOF

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '03-session-foobar' '04-session-barfoo' '05-session-boofar' '06-session-farboo'
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp '01-session-foo' '02-session-bar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin value - select - more files - index range - cover all - overlapping" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}" "${testInFileE}" "${testInFileF}") )

  # select the 2nd line on stdin
  run ${__bin_path}/aio-rename-files stdin value ::: select 5: 3: ::: "${fileList[@]}" <<EOF
01 session foo
02 session bar
03 session foobar
04 session barfoo
05 session boofar
06 session farboo
EOF

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '03-session-foobar' '04-session-barfoo' '05-session-boofar' '06-session-farboo'
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp '01-session-foo' '02-session-bar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin value - select - more files - index - mixed" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}" "${testInFileE}" "${testInFileF}") )

  run ${__bin_path}/aio-rename-files stdin value ::: select 4: 1 3 5:6 ::: "${fileList[@]}" <<EOF
01 session foo
02 session bar
03 session foobar
04 session barfoo
05 session boofar
06 session farboo
EOF

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '01-session-foo' '03-session-foobar' '04-session-barfoo' '05-session-boofar' '06-session-farboo'
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp '02-session-bar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin value - select - one file - index - single - out of range" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # select the 5th line on stdin => file wont be renamed and a warning is written to stdout
  run ${__bin_path}/aio-rename-files stdin value ::: select 5 ::: "${fileList[@]}" <<EOF
01 session foo
02 session bar
EOF

  # printf '# %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]
  [ "${output}" == "error: the length of one or both of the given sets [2, 2] must be greater or equal than the max-index value [5] ... stopping" ]

  run assert-files-not-exist-tmp '01-session-foo' '02-session-bar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin value - select - one file - index range - cover all - mask index out of range" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}") )

  # index 5 wont break the command, because the cover-all range starting from 3 contains index 5, but cover-all
  # just covers all names from 3 till the last available name (4). thus index 5 is dismissed
  run ${__bin_path}/aio-rename-files stdin value ::: select 5 3: ::: "${fileList[@]}" <<EOF
01 session foo
02 session bar
03 session foobar
04 session barfoo
EOF

  # printf '# %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '03-session-foobar' '04-session-barfoo'
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp '01-session-foo' '02-session-bar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin value - select - one file - two indeces - two names - less than expected number of input files" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}") )

  # select two lines on stdin but only provide one input file => results in an error
  run ${__bin_path}/aio-rename-files stdin value ::: select 1 2 ::: "${fileList[@]}" <<EOF
01 session foo
02 session bar
EOF

  [ ${status} -eq 1 ]
  [ "${output}" == "error: the length of one or both given sets [1, 2] does neither fit the given number of indeces [2] or the largest index value [2] ... stopping" ]

  run assert-files-not-exist-tmp '01-session-foo' '02-session-bar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin value - select - two files - one index - more than two names - not enough select indeces" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # select the 3rd and 4th lines on stdin
  run ${__bin_path}/aio-rename-files stdin value ::: select 4 ::: "${fileList[@]}" <<EOF
01 session foo
02 session bar
03 session foobar
04 session barfoo
EOF

  [ ${status} -eq 1 ]
  [ "${output}" == "error: the length of one or both of the given sets [2, 4] must be greater or equal than the max-index value [4] ... stopping" ]
  
  run assert-files-not-exist-tmp '03-session-foobar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin value - select - from files and names  - two indeces - four names - four files" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}") )

  # number of files == number of indeces
  # => select the 1st and 4th names on stdin
  run ${__bin_path}/aio-rename-files stdin value ::: select 1 4 ::: "${fileList[@]}" <<EOF
01 session foo
02 session bar
03 session foobar
04 session barfoo
EOF

  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '01-session-foo' '04-session-barfoo'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin value - select - from names only - two indeces - two names - map to files" {

  # skip 'undetected error'

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # number of files == number of indeces
  # => select the 1st and 4th names on stdin
  run ${__bin_path}/aio-rename-files stdin value ::: select 1 4 ::: "${fileList[@]}" <<EOF
01 session foo
02 session bar
03 session foobar
04 session barfoo
EOF

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '01-session-foo' '04-session-barfoo'
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp '02-session-bar' '03-session-foobar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin value - select - from names only - two indeces - two names - select files" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}") )

  # number of files == number of indeces
  # => select the 1st and 4th names on stdin
  run ${__bin_path}/aio-rename-files stdin value ::: select 1 4 ::: "${fileList[@]}" <<EOF
01 session foo
02 session bar
03 session foobar
04 session barfoo
EOF

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '01-session-foo' '04-session-barfoo'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin value - select - from files only - two indeces - two names" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}") )

  # number of names on stdin == number of indeces
  # => select the 1st and 4th files
  run ${__bin_path}/aio-rename-files stdin value ::: select 1 4 ::: "${fileList[@]}" <<EOF
01 session foo
02 session bar
EOF

  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '01-session-foo' '02-session-bar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin key-value - one key" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  run ${__bin_path}/aio-rename-files stdin kv track ::: "${fileList[@]}" <<EOF
track 01
song foo
album session

track 02
song bar
album session
EOF

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '01' '02'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin key-value - multiple keys" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # here the order of selected keys differs from the order of keys on stdin
  run ${__bin_path}/aio-rename-files stdin kv track album song ::: "${fileList[@]}" <<EOF
track 01
song foo
album session

track 02
song bar
album session
EOF

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '01-session-foo' '02-session-bar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin key-value - unordered sections" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # each tag section lists tagnames in different order. renamed files are
  # supposed to contain tag values in order given by --stdin key-values
  run ${__bin_path}/aio-rename-files stdin kv track album song ::: "${fileList[@]}" <<EOF
album session
song foo
track 01

song bar
track 02
album session
EOF

  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '01-session-foo' '02-session-bar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin key-value - incomplete sections" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # album tag is missing in all sections/for all files thus no album information is used for renaming
  run ${__bin_path}/aio-rename-files stdin kv track album song ::: "${fileList[@]}" <<EOF
track 01
song foo

track 02
song bar
EOF

  # printf '# %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: std input expected [3] keys per section but only got [2] ... stopping" ]
  [ "${lines[1]}" == "error: could not prepare data for command [key-value] ... stopping" ]

  run assert-files-not-exist-tmp '01-foo' '02-bar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin key-value - wrong section keys" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # key names are not consistent in each section. the first section specifies
  # two track keys while the second section specifies two album keys
  run ${__bin_path}/aio-rename-files stdin kv track album song ::: "${fileList[@]}" <<EOF
track 01
song foo
track 02

album session
song bar
album session
EOF

  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: std input seems to ĺack one or more of expected keys [track album song] ... stopping" ]
  [ "${lines[1]}" == "error: could not prepare data for command [key-value] ... stopping" ]

  # key names are not consistent in each section. the second section specifies
  # two song keys while the third section specifies two track keys
  run ${__bin_path}/aio-rename-files stdin kv track song ::: "${fileList[@]}" <<EOF
track 01
song foo

song bar
album session
song foobar

track 02
track 03
album session
EOF

  # printf '# l2 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: std input seems to ĺack one or more of expected keys [track song] ... stopping" ]
  [ "${lines[1]}" == "error: could not prepare data for command [key-value] ... stopping" ]
}

@test "invoke aio-rename-files - stdin key-value - inconsistent sections" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # first section does not specify an album tag while second section has one
  run ${__bin_path}/aio-rename-files stdin kv track album song ::: "${fileList[@]}" <<EOF
track 01
song foo

track 02
song bar
album session
EOF

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: std input seems to ĺack one or more of expected keys [track album song] ... stopping" ]
  [ "${lines[1]}" == "error: could not prepare data for command [key-value] ... stopping" ]
}

@test "invoke aio-rename-files - stdin key-value - non-matching occurences" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # sections on stdin does not contain any of the selected tags
  run ${__bin_path}/aio-rename-files stdin kv album year ::: "${fileList[@]}" <<EOF
track 01
song foo

track 02
song bar
EOF

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: std input seems to ĺack one or more of expected keys [album year] ... stopping" ]
  [ "${lines[1]}" == "error: could not prepare data for command [key-value] ... stopping" ]

}

@test "invoke aio-rename-files - stdin key-value - less than expected occurences" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # only one tag section specified thus only first file is supposed to be renamed
  run ${__bin_path}/aio-rename-files stdin kv track album song ::: "${fileList[@]}" <<EOF
track 01
song foo
EOF

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: std input expected [3] keys per section but only got [2] ... stopping" ]
  [ "${lines[1]}" == "error: could not prepare data for command [key-value] ... stopping" ]

  [ ! -f "${__output_file_map[01-foo]}" ]
}

@test "invoke aio-rename-files - stdin key-value - empty values" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # sections on stdin does not contain any of the selected tags
  run ${__bin_path}/aio-rename-files stdin kv track song ::: "${fileList[@]}" <<EOF
track
song

track
song
EOF

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: values for keys on stdin are missing. expected valid values for each key [track song] ... stopping" ]
  [ "${lines[1]}" == "error: could not prepare data for command [key-value] ... stopping" ]

  # sections on stdin does not contain any of the selected tags
  run ${__bin_path}/aio-rename-files stdin kv track song ::: "${fileList[@]}" <<EOF
track 01
song

track 02
song bar
EOF

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: values for keys on stdin are missing. expected valid values for each key [track song] ... stopping" ]
  [ "${lines[1]}" == "error: could not prepare data for command [key-value] ... stopping" ]
}

@test "invoke aio-rename-files - stdin key-value - with comments - heredoc" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  local -r indentWhitespaces='      '
  local -r indentTabs="$(printf '\t\t')"

  # here the order of selected keys differs from the order of keys on stdin
  run ${__bin_path}/aio-rename-files stdin kv track album song ::: "${fileList[@]}" <<EOF
# a single line comment
track 01
song foo
album session

# a
${indentTabs}# multi
track 02
${indentWhitespaces}# line
${indentTabs}${indentWhitespaces}# comment
song bar
album session
EOF

  [ ${status} -eq 0 ]

  run assert-files-exist-tmp '01-session-foo' '02-session-bar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin key-value - with empty lines - heredoc" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  local -r lineWithWhitespaces='      '
  local -r lineWithTabs="$(printf '\t\t\t')"

  # here the order of selected keys differs from the order of keys on stdin
  run ${__bin_path}/aio-rename-files stdin kv track album song ::: "${fileList[@]}" <<EOF
# single empty line

track 01
song foo
album session
# multiple empty lines with and without whitespaces and tabs

${lineWithWhitespaces}


${lineWithTabs}
${lineWithWhitespaces}
${lineWithTabs}${lineWithWhitespaces}

track 02
song bar
album session
EOF

  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '01-session-foo' '02-session-bar'
  [ ${status} -eq 0 ]

  # [ -f "${__output_file_map[01-session-foo]}" ]
  # [ -f "${__output_file_map[02-session-bar]}" ]
}

@test "invoke aio-rename-files - stdin key-value - with indented data - heredoc" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  local -r indentWhitespaces='      '
  local -r indentTabs="$(printf '\t\t')"

  # here the order of selected keys differs from the order of keys on stdin
  run ${__bin_path}/aio-rename-files stdin kv track album song ::: "${fileList[@]}" <<EOF
${indentWhitespaces}track 01
${indentTabs}song${indentWhitespaces}foo${indentTabs}
${indentWhitespaces}${indentTabs}album${indentTabs}session${indentWhitespaces}

${indentWhitespaces}track 02
${indentTabs}song${indentWhitespaces}bar${indentTabs}
${indentWhitespaces}${indentTabs}album${indentTabs}session${indentWhitespaces}
EOF

  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '01-session-foo' '02-session-bar'
  [ ${status} -eq 0 ]
  
  # [ -f "${__output_file_map[01-session-foo]}" ]
  # [ -f "${__output_file_map[02-session-bar]}" ]
}

@test "invoke aio-rename-files - stdin key-value - special chars - not allowed" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}") )

  local char
  local value

  for char in "${__special_chars_list[@]}"; do

    value="foo${char}bar"
    run ${__bin_path}/aio-rename-files stdin key-value DUMMY ::: "${fileList[@]}" <<< "DUMMY ${value}"

    [ ${status} -eq 1 ]
    [ "${lines[0]}"  == "error: value [${value}] contains special char(s) ... stopping" ]
  
  done
  
  for char in "${__control_chars_list[@]}"; do

    value=$(printf "foo${char}bar")
    run ${__bin_path}/aio-rename-files stdin key-value DUMMY ::: "${fileList[@]}" <<< "DUMMY ${value}"

    [ ${status} -eq 1 ]
    [ "${lines[0]}"  == "error: value [${value}] contains control char(s) ... stopping" ]
  
  done
}

@test "invoke aio-rename-files - stdin key-value - delimiter" {
  skip "to be implemented"
}

@test "invoke aio-rename-files - stdin key-value - delimiter - special chars - not allowed" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}") )

  local delim
  local char

  for char in "${__special_chars_list[@]}"; do

    run ${__bin_path}/aio-rename-files --delimiter-out "${char}" stdin key-value DUMMY ::: "${fileList[@]}" <<< "DUMMY ${value}"

    [ ${status} -eq 1 ]
    [ "${lines[0]}"  == "error: delimiter [${char}] contains special char ... stopping" ]
  
  done
  
  for char in "${__control_chars_list[@]}"; do

    delim=$(printf "${char}")
    run ${__bin_path}/aio-rename-files --delimiter-out "${delim}" stdin key-value DUMMY ::: "${fileList[@]}" <<< "DUMMY ${value}"

    [ ${status} -eq 1 ]
    [ "${lines[0]}"  == "error: delimiter [${delim}] contains control char ... stopping" ]
  
  done
}

@test "invoke aio-rename-files - stdin key-value - output same as input" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # renamed files have teh same name as input files
  run ${__bin_path}/aio-rename-files stdin kv album artist song ::: "${fileList[@]}" <<EOF
  album test
  artist session
  song a

  album test
  artist session
  song b
EOF

  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ ${#lines[@]} -eq 3 ]
  [ "${lines[0]}" == "error: renamed file is the same as input file [${__inout_file_path}/${testInFileA}] ... skipping" ]
  [ "${lines[1]}" == "error: renamed file is the same as input file [${__inout_file_path}/${testInFileB}] ... skipping" ]
  [ "${lines[2]}" == "error: only [0] of [2] files have been renamed ... stopping" ]
}

@test "invoke aio-rename-files - stdin key-value - prepend" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  run ${__bin_path}/aio-rename-files out prepend "2020" ::: stdin kv track ::: "${fileList[@]}" <<EOF
track 01
song foo
album session

track 02
song bar
alnum session
EOF

  [ ${status} -eq 0 ]

  run assert-files-exist-tmp '2020-01' '2020-02'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin key-value - out - prepend - delimiter out" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  run ${__bin_path}/aio-rename-files --do '_' out prepend "2020" ::: stdin kv track ::: "${fileList[@]}" <<EOF
track 01
song foo
album session

track 02
song bar
alnum session
EOF

  [ ${status} -eq 0 ]

  run assert-files-exist-tmp '2020-01-underscore' '2020-02-underscore'
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-rename-files --do 'abc' out prepend "2020" ::: stdin kv track ::: "${fileList[@]}" <<EOF
track 01
song foo
album session

track 02
song bar
alnum session
EOF

  [ ${status} -eq 0 ]

  run assert-files-exist-tmp '2020-01-abc' '2020-02-abc'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin key-value - out - prepend - delimiter out - none" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  run ${__bin_path}/aio-rename-files --do '' out prepend "2020" ::: stdin kv track ::: "${fileList[@]}" <<EOF
track 01
song foo
album session

track 02
song bar
alnum session
EOF

  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-01-none' '2020-02-none'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin key-value - in keep - prepend" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in keep 1 ::: stdin kv track song ::: "${fileList[@]}" <<EOF
track 01
song foo
album session

track 02
song bar
alnum session
EOF

  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-01-foo' '2020-test-02-bar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin key-value - in keep - out - prepend - delimiter out" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  run ${__bin_path}/aio-rename-files --do '_' out prepend "2020" ::: in keep 1 ::: stdin kv track song ::: "${fileList[@]}" <<EOF
track 01
song foo
album session

track 02
song bar
alnum session
EOF

  # printf '# l1 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-01-foo-underscore' '2020-test-02-bar-underscore'
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-rename-files --do 'abc' out prepend "2020" ::: in keep 1 ::: stdin kv track song ::: "${fileList[@]}" <<EOF
track 01
song foo
album session

track 02
song bar
alnum session
EOF

  # printf '# l2 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-01-foo-abc' '2020-test-02-bar-abc'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin key-value - in keep - out - prepend - delimiter out - none" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  run ${__bin_path}/aio-rename-files --do '' out prepend "2020" ::: in keep 1 ::: stdin kv track song ::: "${fileList[@]}" <<EOF
track 01
song foo
album session

track 02
song bar
alnum session
EOF

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp '2020-test-01-foo-none' '2020-test-02-bar-none'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin key-value - in keep - out - prepend - delimiter in - delimiter out" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileUnderscoreA}" "${testInFileUnderscoreB}") )

  run ${__bin_path}/aio-rename-files --do '-' out prepend "2020" ::: in --di '_' keep 1 ::: stdin kv track song ::: "${fileList[@]}" <<EOF
track 01
song foo
album session

track 02
song bar
alnum session
EOF

  [ ${status} -eq 0 ]

  run assert-files-exist-tmp '2020-test-01-foo' '2020-test-02-bar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin key-value - in keep - out - prepend - delimiter in - delimiter out - none" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileUnderscoreA}" "${testInFileUnderscoreB}") )

  run ${__bin_path}/aio-rename-files --do '' out prepend "2020" ::: in --di '_' keep 1 ::: stdin kv track song ::: "${fileList[@]}" <<EOF
track 01
song foo
album session

track 02
song bar
alnum session
EOF

  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-01-foo-none' '2020-test-02-bar-none'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin key-value - in keep - out - prepend - delimiter in - no parts" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # delimiter '___' does not exist in input files
  # thus nothing is selected from input name
  # only prepend and stdin is taken into account
  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in --di '___' keep 2 ::: stdin kv track song ::: "${fileList[@]}" <<EOF
track 01
song foo
album session

track 02
song bar
alnum session
EOF

  # printf '# l1 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp '2020-01-foo' '2020-02-bar'
  [ ${status} -eq 0 ]

  # delimiter '___' does not exist in input files
  # thus the whole name is being kept
  # and prepend and stdin are taken into account
  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in --di '___' keep 1 ::: stdin kv track song ::: "${fileList[@]}" <<EOF
track 01
song foo
album session

track 02
song bar
alnum session
EOF

  # printf '# l2 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp "2020-test-session-a-01-foo" "2020-test-session-b-02-bar"
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin key-value - in cut - prepend" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in cut 2: ::: stdin kv track song ::: "${fileList[@]}" <<EOF
track 01
song foo
album session

track 02
song bar
album session
EOF

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-01-foo' '2020-test-02-bar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin key-value - in cut - out - prepend - delimiter out" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  run ${__bin_path}/aio-rename-files --do '_' out prepend "2020" ::: in cut 2: ::: stdin kv track song ::: "${fileList[@]}" <<EOF
track 01
song foo
album session

track 02
song bar
album session
EOF

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-01-foo-underscore' '2020-test-02-bar-underscore'
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-rename-files --do 'abc' out prepend "2020" ::: in cut 2: ::: stdin kv track song :::  "${fileList[@]}" <<EOF
track 01
song foo
album session

track 02
song bar
album session
EOF

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp '2020-test-01-foo-abc' '2020-test-02-bar-abc'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin key-value - in cut - out - prepend - delimiter out - none" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  run ${__bin_path}/aio-rename-files --do '' out prepend "2020" ::: in cut 2: ::: stdin kv track song ::: "${fileList[@]}" <<EOF
track 01
song foo
album session

track 02
song bar
album session
EOF

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-01-foo-none' '2020-test-02-bar-none'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin key-value - in cut - out - prepend - delimiter in - delimiter out" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileUnderscoreA}" "${testInFileUnderscoreB}") )

  run ${__bin_path}/aio-rename-files --do '-' out prepend "2020" ::: in --di '_' cut 2: ::: stdin kv track song ::: "${fileList[@]}" <<EOF
track 01
song foo
album session

track 02
song bar
album session
EOF

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-01-foo' '2020-test-02-bar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin key-value - in cut - out - prepend - delimiter in - delimiter out - none" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileUnderscoreA}" "${testInFileUnderscoreB}") )

  run ${__bin_path}/aio-rename-files --do '' out prepend "2020" ::: in --di '_' cut 2: ::: stdin kv track song ::: "${fileList[@]}" <<EOF
track 01
song foo
album session

track 02
song bar
album session
EOF

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test-01-foo-none' '2020-test-02-bar-none'
  [ ${status} -eq 0 ]

}

@test "invoke aio-rename-files - stdin key-value - in cut - out - prepend - delimiter in - no parts" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileUnderscoreA}" "${testInFileUnderscoreB}") )

  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in --di '___' cut 1 ::: stdin kv track song ::: "${fileList[@]}" <<EOF
track 01
song foo
album session

track 02
song bar
album session
EOF

  # printf '# l1 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-01-foo' '2020-02-bar'
  [ ${status} -eq 0 ]

  run ${__bin_path}/aio-rename-files out prepend "2020" ::: in --di '___' cut 2 ::: stdin kv track song ::: "${fileList[@]}" <<EOF
track 01
song foo
album session

track 02
song bar
album session
EOF

  # printf '# l2 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-tmp '2020-test_session_a-01-foo' '2020-test_session_b-02-bar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin key-value - select - index - single" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileB}") )

  # select the 1st tag section for file renaming
  run ${__bin_path}/aio-rename-files stdin kv track album song ::: select 1 ::: "${fileList[@]}" <<EOF
track 01
song foo
album session

track 02
song bar
album session
EOF

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-exist-tmp '01-session-foo'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin key-value - select - index - multiple" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}" ) )

  # select the 1st and 3rd tag section for file renaming
  run ${__bin_path}/aio-rename-files stdin kv track album song ::: select 1 3 ::: "${fileList[@]}" <<EOF
track 01
song foo
album session

track 02
song bar
album session

track 03
song foobar
album session

track 04
song barfoo
album session
EOF

  # printf '# %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp '02-session-bar' '04-session-barfoo'
  [ ${status} -eq 0 ]
  run assert-files-exist-tmp '01-session-foo' '03-session-foobar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin key-value - select - index range - single" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}" ) )

  # given range selects files 1,2,3 for renaming
  run ${__bin_path}/aio-rename-files stdin kv track album song ::: select 1:3 ::: "${fileList[@]}" <<EOF
track 01
song foo
album session

track 02
song bar
album session

track 03
song foobar
album session

track 04
song barfoo
album session
EOF

  # printf '# %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp '04-session-barfoo'
  [ ${status} -eq 0 ]
  run assert-files-exist-tmp '01-session-foo' '02-session-bar' '03-session-foobar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin key-value - select - index range - multiple - non-overlapping" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}" "${testInFileE}" "${testInFileF}" ) )

  # given range selects files 1,2 and 4,5 for renaming
  run ${__bin_path}/aio-rename-files stdin kv track album song ::: select 1:2 4:5 ::: "${fileList[@]}" <<EOF
track 01
song foo
album session

track 02
song bar
album session

track 03
song foobar
album session

track 04
song barfoo
album session

track 05
song boofar
album session

track 06
song farboo
album session
EOF

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-not-exist-tmp '03-session-foobar' '06-session-farboo'
  [ ${status} -eq 0 ]
  run assert-files-exist-tmp '01-session-foo' '02-session-bar' '04-session-barfoo' '05-session-boofar' 
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin key-value - select - index range - multiple - overlapping" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}" "${testInFileE}" "${testInFileF}" ) )

  # both ranges select files 2 to 5 for renaming
  run ${__bin_path}/aio-rename-files stdin kv track album song ::: select 3:4 2:5 ::: "${fileList[@]}" <<EOF
track 01
song foo
album session

track 02
song bar
album session

track 03
song foobar
album session

track 04
song barfoo
album session

track 05
song boofar
album session

track 06
song farboo
album session
EOF

  # printf '# %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  run assert-files-not-exist-tmp '01-session-foo' '06-session-farboo'
  [ ${status} -eq 0 ]
  run assert-files-exist-tmp '02-session-bar' '03-session-foobar' '04-session-barfoo' '05-session-boofar' 
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin key-value - select - index range - cover all - single" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}" "${testInFileE}" "${testInFileF}" ) )

  # cover all range selects files 3 till 6 (number of given files)
  run ${__bin_path}/aio-rename-files stdin kv track album song ::: select 3: ::: "${fileList[@]}" <<EOF
track 01
song foo
album session

track 02
song bar
album session

track 03
song foobar
album session

track 04
song barfoo
album session

track 05
song boofar
album session

track 06
song farboo
album session
EOF

  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp '01-session-foo' '02-session-bar'
  [ ${status} -eq 0 ]
  run assert-files-exist-tmp  '03-session-foobar' '04-session-barfoo' '05-session-boofar' '06-session-farboo'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin key-value - select - index range - cover all - overlapping" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}" "${testInFileE}" "${testInFileF}" ) )

  # cover all range 3: contains the second cover-all range 5:
  run ${__bin_path}/aio-rename-files stdin kv track album song ::: select 3: 5: ::: "${fileList[@]}" <<EOF
track 01
song foo
album session

track 02
song bar
album session

track 03
song foobar
album session

track 04
song barfoo
album session

track 05
song boofar
album session

track 06
song farboo
album session
EOF

  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp '01-session-foo' '02-session-bar'
  [ ${status} -eq 0 ]
  run assert-files-exist-tmp  '03-session-foobar' '04-session-barfoo' '05-session-boofar' '06-session-farboo'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin key-value - select - index - mixed" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}" "${testInFileE}" "${testInFileF}" ) )

  #
  run ${__bin_path}/aio-rename-files stdin kv track album song ::: select 4: 1 3 5:6 ::: "${fileList[@]}" <<EOF
track 01
song foo
album session

track 02
song bar
album session

track 03
song foobar
album session

track 04
song barfoo
album session

track 05
song boofar
album session

track 06
song farboo
album session
EOF

  # printf '# %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp '02-session-bar'
  [ ${status} -eq 0 ]
  run assert-files-exist-tmp '01-session-foo' '03-session-foobar' '04-session-barfoo' '05-session-boofar' '06-session-farboo'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin key-value - select - index range - cover all - mask index out of range" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}") )

  # index 5 wont break the command, because the cover-all range starting from 3 contains index 5, but cover-all
  # just covers all names from 3 till the last available name (4). thus index 5 is dismissed
  run ${__bin_path}/aio-rename-files stdin kv track album song ::: select 5 3: ::: "${fileList[@]}" <<EOF
track 01
song foo
album session

track 02
song bar
album session

track 03
song foobar
album session

track 04
song barfoo
album session
EOF

  # printf '# %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-not-exist-tmp '01-session-foo' '02-session-bar'
  [ ${status} -eq 0 ]
  run assert-files-exist-tmp '03-session-foobar' '04-session-barfoo'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin key-value - select - index - single - out of range" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # select the 5th tag section on stdin => file wont be renamed and a warning is written to stdout
  run ${__bin_path}/aio-rename-files stdin kv track album song ::: select 5 ::: "${fileList[@]}" <<EOF
track 01
song foo
album session

track 02
song bar
album session
EOF

  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${output}" == "error: the length of one or both of the given sets [2, 2] must be greater or equal than the max-index value [5] ... stopping" ]

  run assert-files-not-exist-tmp '01-session-foo' '02-session-bar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin key-value - select - less than expected number of input files" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileB}") )

  # select two tag sections on stdin but only provide one input file => results in an error
  run ${__bin_path}/aio-rename-files stdin kv track album song ::: select 1 2 ::: "${fileList[@]}" <<EOF
track 01
song foo
album session

track 02
song bar
album session
EOF

  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${output}" == "error: the length of one or both given sets [1, 2] does neither fit the given number of indeces [2] or the largest index value [2] ... stopping" ]
  
  run assert-files-not-exist-tmp '02-session-bar'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin key-value - select - more than necessary number of input files" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}" "${testInFileC}" "${testInFileD}") )

  # select two tag sections on stdin but only select one of the input files
  # => results in an error
  run ${__bin_path}/aio-rename-files stdin kv track album song ::: select 2 ::: "${fileList[@]}" <<EOF
track 01
song foo
album session

track 02
song bar
album session
EOF

  [ ${status} -eq 0 ]

  run assert-files-exist-tmp '02-session-bar'
  [ ${status} -eq 0 ]
  run assert-files-not-exist-tmp '01-session-foo'
  [ ${status} -eq 0 ]
}

@test "invoke aio-rename-files - stdin key-value - read timeout" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # no value at all at stdin - should trigger timeout
  run ${__bin_path}/aio-rename-files stdin kv track album song ::: "${fileList[@]}"

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: key-value input on stdin is empty ... stopping" ]
  [ "${lines[1]}" == "error: could not prepare data for command [key-value] ... stopping" ]

  # value available after delay - should trigger timeout
  run ${__bin_path}/aio-rename-files stdin kv track album song ::: "${fileList[@]}" < <(sleep 2; printf 'track 01\n\ntrack 02')

  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: key-value input on stdin is empty ... stopping" ]
  [ "${lines[1]}" == "error: could not prepare data for command [key-value] ... stopping" ]
}

@test "invoke aio-rename-files - stdin key-value - empty lines only" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  local -r lineWithWhitespaces='      '
  local -r lineWithTabs="$(printf '\t\t\t')"

  # just pass empty lines
  run ${__bin_path}/aio-rename-files stdin kv track album song ::: "${fileList[@]}" <<EOF

${lineWithWhitespaces}
${lineWithTabs}
${lineWithWhitespaces}${lineWithTabs}
EOF

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: key-value input on stdin is empty ... stopping" ]
  [ "${lines[1]}" == "error: could not prepare data for command [key-value] ... stopping" ]

  # empty value
  run ${__bin_path}/aio-rename-files stdin kv track album song ::: "${fileList[@]}" <<<""

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: key-value input on stdin is empty ... stopping" ]
  [ "${lines[1]}" == "error: could not prepare data for command [key-value] ... stopping" ]

  # empty value - whitespaces only
  run ${__bin_path}/aio-rename-files stdin kv track album song ::: "${fileList[@]}" <<<"${lineWithWhitespaces}"

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: key-value input on stdin is empty ... stopping" ]
  [ "${lines[1]}" == "error: could not prepare data for command [key-value] ... stopping" ]

  # empty value - tabs only
  run ${__bin_path}/aio-rename-files stdin kv track album song ::: "${fileList[@]}" <<<"${lineWithTabs}"

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: key-value input on stdin is empty ... stopping" ]
  [ "${lines[1]}" == "error: could not prepare data for command [key-value] ... stopping" ]

  # empty value - tabs and whitespaces only
  run ${__bin_path}/aio-rename-files stdin kv track album song ::: "${fileList[@]}" <<<"${lineWithWhitespaces}${lineWithTabs}"

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == "error: key-value input on stdin is empty ... stopping" ]
  [ "${lines[1]}" == "error: could not prepare data for command [key-value] ... stopping" ]
}

@test "invoke aio-rename-files - in - missing command - error" {

  local fileList
  fileList=( $(create-empty-files-tmp "${xxxxxxxxxFile}") )

  # missing in sub-command, ok to fail
  run ${__bin_path}/aio-rename-files in --di ' ' ::: "${fileList[@]}"

  [ ${status} -eq 1 ]
  [ "${output}"  == "error: found in-command delimiter [:::] but in-command has not been provided ... stopping" ]
}

@test "invoke aio-rename-files - stdin - missing command - error" {

  local fileList
  fileList=( $(create-empty-files-tmp "${xxxxxxxxxFile}") )

  # missing stdin sub-ommand, ok to fail
  run ${__bin_path}/aio-rename-files stdin ::: "${fileList[@]}"

  [ ${status} -eq 1 ]
  [ "${output}"  == "error: found stdin-command delimiter [:::] but stdin-command has not been provided ... stopping" ]
}

@test "invoke aio-rename-files - out - missing command - error" {

  local fileList
  fileList=( $(create-empty-files-tmp "${xxxxxxxxxFile}") )

  # missing out sub-command, ok to fail
  run ${__bin_path}/aio-rename-files out ::: "${fileList[@]}"

  [ ${status} -eq 1 ]
  [ "${output}"  == "error: found out-command delimiter [:::] but out-command has not been provided ... stopping" ]
}

@test "invoke aio-rename-files - in - copy to current directory" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # no value at all at stdin - should trigger timeout
  run ${__bin_path}/aio-rename-files -c in split 4 ::: "${fileList[@]}"

  # printf '# %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-here 'test--session-a.ogg' 'test--session-b.ogg'
  [ ${status} -eq 0 ]
  
  rm 'test--session-a.ogg' 'test--session-b.ogg'
}

@test "invoke aio-rename-files - stdin - copy to current directory" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # no value at all at stdin - should trigger timeout
  run ${__bin_path}/aio-rename-files -c stdin value ::: "${fileList[@]}" <<EOF
xxx
yyy
EOF

  # printf '# %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-here 'xxx.ogg' 'yyy.ogg'
  [ ${status} -eq 0 ]
  
  rm 'xxx.ogg' 'yyy.ogg'
}

@test "invoke aio-rename-files - out - copy to current directory" {

  local fileList
  fileList=( $(create-empty-files-tmp "${testInFileA}" "${testInFileB}") )

  # no value at all at stdin - should trigger timeout
  run ${__bin_path}/aio-rename-files -c out replace first 'test-' with '' ::: "${fileList[@]}"

  # printf '# %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  run assert-files-exist-here 'session-a.ogg' 'session-b.ogg'
  [ ${status} -eq 0 ]
  
  rm 'session-a.ogg' 'session-b.ogg'
}
