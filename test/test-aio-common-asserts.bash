#!/usr/bin/env bash

# This file is part of aio-proc
#
#     aio-proc is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     aio-proc is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with aio-proc. If not, see <http://www.gnu.org/licenses/>.

# @param $1 filename
# @param $2 from-position in sec
#
# @output detected volumes on two lines:
#
#   <mean-volume>
#   <max-volume>
aio-detect-volume-out-ffmpeg() {
  local durationSec="$2"
  [[ "${durationSec}" =~ ^[.][0-9]+$ ]] && durationSec="0${durationSec}"

  local vol=()
  vol=( $(ffmpeg -ss "${durationSec}" -i "$1" -af volumedetect -f null - 2>&1 \
    | grep -o -E "(mean|max).*" \
    | cut -d ' ' -f 2 )
  )
  [[ "${vol[1]}" =~ ^-[0.]+ ]] && vol[1]="0.0"
  printf '%s\n' "${vol[@]}"
}

# @param $1 filename
# @param $2 to-position in sec
#
# @output detected volumes on two lines:
#
#   <mean-volume>
#   <max-volume>
aio-detect-volume-in-ffmpeg() {
  local durationSec="$2"
  [[ "${durationSec}" =~ ^[.][0-9]+$ ]] && durationSec="0${durationSec}"

  local vol=()
  vol=( $(ffmpeg -to "${durationSec}" -i "$1" -af volumedetect -f null - 2>&1 \
    | grep -o -E "(mean|max).*" \
    | cut -d ' ' -f 2 )
  )
  [[ "${vol[1]}" =~ ^-[0.]+ ]] && vol[1]="0.0"
  printf '%s\n' "${vol[@]}"
}

# @param $1 filename
aio-detect-bitrate-ffprobe() {
  ffprobe -loglevel error -select_streams a -show_entries stream:bitrate "$1"
}

# @param $1 filename
aio-detect-bitrate-mediainfo() {
  mediainfo --Output="Audio;%BitRate%" "$1"
}

# @param $1 filename
aio-detect-bitrate-mode-mediainfo() {
  mediainfo --Output="Audio;%BitRate_Mode%" "$1"
}

# @param $1 filename
#
# @output detected volumes on two lines:
#
#   <mean-volume>
#   <max-volume>
aio-detect-volume-ffmpeg() {
  local vol=()
  vol=( $(ffmpeg -i "$1" -af volumedetect -f null - 2>&1 \
    | grep -o -E "(mean|max).*" \
    | cut -d ' ' -f 2 )
  )

  [[ "${vol[1]}" =~ ^-[0.]+ ]] && vol[1]="0.0"
  printf '%s\n' "${vol[@]}"
}

# @param $1 filename
# @param $2 expected max-vol (minimum value)
# @param $3 expected max-vol (maximum value)
aio-assert-max-volume-ffmpeg() {
  local vol=()
  vol=( $(aio-detect-volume-ffmpeg "$1") )

  local maxLevel="${vol[1]}"
  maxLevel="${maxLevel// /}"

  [ $( bc -l <<<"${maxLevel} >= $2 && ${maxLevel} <= $3" ) -eq 1 ]
}

# @param $1 filename
# @param $2 expected duration in sec or sec.ms
#
# we add a value range of +-0.1 sec around the expected duration as sample
# based audio almost never has a sharp duration due to its non continues
# nature
aio-assert-duration-ffmpeg() {

  local duration=$(ffprobe -loglevel error -show_entries format=duration -of default=nk=1:nw=1 "${1}")

  local -r time=(${duration//./ })
  local -r sec="${time[0]}"
  local -r remainder="${time[1]}"

  if [[ -z "${remainder}" ]]; then
    duration="${sec}"
  else
    duration="${sec}.${remainder:0:2}"
  fi

  # printf '# file    : %s\n' "${1}" >&3
  # printf '# duration: %s\n' "${duration}" >&3
  # printf '# expected: %s\n' "${2}" >&3

  [ $(bc -l <<<" (${duration} >= $2 - 0.1) && ( ${duration} <= $2 + 0.1 ) ") -eq 1 ]
}

# @param $1 filename
# @param $2 from-position in sec
# @param $3 expected max-vol (minimum value)
# @param $4 expected max-vol (maximum value)
# @param $5 expected mean-vol (minimum value)
# @param $6 expected mean-vol (maximum value)
aio-assert-volume-end-ffmpeg() {
  # we test the mean volume of the audio starting from
  # second $2 till the end of the audio
  # mean and max values may vary in each test as we use noise as audio
  local vol=()
  vol=( $(aio-detect-volume-out-ffmpeg "$1" "$2") )

  # printf '# e f %s\n' "$1" >&3
  # printf '# e e %s\n' "$3" "$4" "$5" "$6" >&3
  # printf '# e v %s\n' "${vol[@]}" >&3

  # mean volume
  [ $( bc -l <<<"${vol[0]} <= $5 && ${vol[0]} >= $6" ) -eq 1 ]
  # max volume
  [ $( bc -l <<<"${vol[1]} <= $3 && ${vol[1]} >= $4" ) -eq 1 ]
}

# @param $1 filename
# @param $2 to-position in sec
# @param $3 expected max-vol (minimum value)
# @param $4 expected max-vol (maximum value)
# @param $5 expected mean-vol (minimum value)
# @param $6 expected mean-vol (maximum value)
aio-assert-volume-begin-ffmpeg() {
  # we test the mean volume of the audio starting from
  # 0s till second $2
  # mean and max values may vary in each test as we use noise as audio
  local vol=()
  vol=( $(aio-detect-volume-in-ffmpeg "$1" "$2") )

  # printf '# b f %s\n' "$1" >&3
  # printf '# b e %s\n' "$3" "$4" "$5" "$6" >&3
  # printf '# b v %s\n' "${vol[@]}" >&3

  # mean volume
  [ $( bc -l <<<"${vol[0]} <= $5 && ${vol[0]} >= $6" ) -eq 1 ]

  # max volume
  [ $( bc -l <<<"${vol[1]} <= $3 && ${vol[1]} >= $4" ) -eq 1 ]
}

# @param $1 expected filename
# @param $2 expected begin time in seconds
# @param $3 expected end time in seconds
# @param $4 expected duration time in seconds
#
# @param $5 resulting filename
# @param $6 resulting begin time in seconds
# @param $7 resulting end time in seconds
# @param $8 resulting duration time in seconds
#
# we add a value range of +-0.1 sec around the expected times as sample
# based audio almost never has a exact duration due to its discrete/sample
# based nature
aio-assert-silence-sec() {
  # filename must match exactly
  [ "$5" == "$1" ]
  # start time
  [ $(bc -l <<<" ($6 >= $2 - 0.1) && ( $6 <= $2 + 0.1 ) ") -eq 1 ]
  # end time
  [ $(bc -l <<<" ($7 >= $3 - 0.1) && ( $7 <= $3 + 0.1 ) ") -eq 1 ]
  # duration time
  [ $(bc -l <<<" ($8 >= $4 - 0.1) && ( $8 <= $4 + 0.1 ) ") -eq 1 ]
}

# @param $1 expected filename
# @param $2 expected duration in seconds
#
# @param $3 resulting filename
# @param $4 resulting duration time in seconds
#
# we add a value range of +-0.1 sec around the expected times as sample
# based audio almost never has a exact duration due to its discrete/sample
# based nature
aio-assert-silence-sec-begin-only() {
  # printf '# a: %s\n' "$@" >&3
  # filename must match exactly
  [ "$3" == "$1" ]
  # duration time
  [ $(bc -l <<<" ($4 >= $2 - 0.1) && ( $4 <= $2 + 0.1 ) ") -eq 1 ]
}

# @param $1 expected filename
# @param $2 expected duration in seconds
#
# @param $3 resulting filename
# @param $4 resulting duration time in seconds
#
# we add a value range of +-0.1 sec around the expected times as sample
# based audio almost never has a exact duration due to its discrete/sample
# based nature
aio-assert-silence-sec-end-only() {
  # printf '# a: %s\n' "$@" >&3
  # filename must match exactly
  [ "$3" == "$1" ]
  # duration time
  [ $(bc -l <<<" ($4 >= $2 - 0.1) && ( $4 <= $2 + 0.1 ) ") -eq 1 ]
}

# @param $1 expected filename
# @param $2 expected begin-time in seconds
# @param $3 expected end-time in seconds
#
# @param $4 resulting filename
# @param $5 resulting begin time in seconds
# @param $6 resulting end time in seconds
#
# we add a value range of +-0.1 sec around the expected times as sample
# based audio almost never has a exact duration due to its discrete/sample
# based nature
aio-assert-silence-sec-begin-end-only() {
  # printf '# a: %s\n' "$@" >&3
  # filename must match exactly
  [ "$4" == "$1" ]
  # start time
  [ $(bc -l <<<" ($5 >= $2 - 0.1) && ( $5 <= $2 + 0.1 ) ") -eq 1 ]
  # end time
  [ $(bc -l <<<" ($6 >= $3 - 0.1) && ( $6 <= $3 + 0.1 ) ") -eq 1 ]
}

# @param $1 filename
# @param $2 expected bitrate in bits
# @param $3 expected bitrate mode, one of [VBR, CBR]
#
# we add a value range of +-800 bits around the expected bitrate as compressed
# audio almost never has a exact bitrate
aio-assert-bitrate() {
  local -r bitrate=$(aio-detect-bitrate-mediainfo "$1")
  local -r bitrateMode=$(aio-detect-bitrate-mode-mediainfo "$1")

  local -r bitrateRange=800

  [ $(bc -l <<<" ($2 >= ${bitrate} - ${bitrateRange} ) && ( $2 <= ${bitrate} + ${bitrateRange} ) ") -eq 1 ]
  [[ "${bitrateMode}" == "$3" ]]
}
