[[_TOC_]]

## Note

*The content of this Readme and the Wiki is about to be moved into a ReadTheDocs documnt.*

## aio

`aio` contains a couple of `bash` scripts for bulk audio-processing, file-tagging and file-renaming tasks.

`aio` was developed in order to replace a number of repetitive steps usually done when cutting audio samples from videos, performing audio processing in `Audacity`, tagging and renaming audio files. 

`aio` is inspired by [Freds ImageMagic Scripts](http://www.fmwconcepts.com/imagemagick/index.php) in the sense that most of the `aio` scripts are wrappers around certain [ffmpeg](https://ffmpeg.org/) functionality. The scripts only expose a few of the many options of `ffmpeg`. For complicated tasks, it might be better to directly use `ffmpeg` or any other audio tool.

Usage should be as simple as possible and is aims towards the casual command line user or, in the best case, towards people with no shell experience at all. Therefore it would be great to receive feedback if the documentation on this site lacks information or needs to be more comprehensive or if script usage could be improved or simplified. 

## Where does it run

You need `bash` version `4.0.0` or higher, as all aio-scripts make use of `bash` language features, that haven't been available in earlier `bash` versions (ie. associative arrays, declaration of array references, etc). `aio` was developed on a Debian system and scripts are not posix-compliant.

- **Linux** `aio` should run on any Linux that has a proper `bash` version installed. 
- **MacOS** `aio` should run on a recent MacOS, but some `bash` features and programs work differently than on Linux. It would be interesting to receive some feedback from Mac users.
- **Windows** `aio` might work under `cygwin` on Windows, but this is just a guess. For windows it would also be interessting to receive some feedback.

## Features

A list of features to add [can be found in the wiki](https://gitlab.com/zzeebdrraa/tools/cl-audio-processing/aio/-/wikis/To-Be-Implemented).

### Audio Processing

* concatenation of an arbitrary number of audio files
  * [aio-concat](#aio-concat)
* splitting of an audio file into an arbitrary number of parts
  * [aio-split](#aio-split)
* cutting an arbitrary number of parts from an audio file
  * [aio-cut](#aio-cut)
* in and/or out-fading of an arbitrary number of audio files
  * [aio-fade](#aio-fade)
* silence-detection for an arbitrary number of audio files
  * [aio-detect-silence](aio-detect-silence)
* normalizing volume of an arbitrary number of audio files
  * [aio-normalize](#aio-normalize)
* mixing two or more audio files
  * [aio-mix](#aio-mix)
* converting timestamps to seconds and vice versa
  * [aio-ts-to-sec](#aio-ts-to-sec)
  * [aio-sec-to-ts](#aio-sec-to-ts)

### File Renaming

* bulk file renaming    
  * [aio-rename-files](#aio-rename-files)

### Metadata

* setting metadata tags on an arbitrary number of audio files 
  * [aio-metadata-set](#aio-metadata-set)
  * [aio-metadata-generate](#aio-metadata-generate)
  * [aio-metadata-transform](#aio-metadata-transform)

### Archive.org
  
* generation of spreadsheets for metadata and file uploading to [archive.org](https://archive.org) 
  * [aio-ia-generate-metadata-file](#aio-ia-generate-metadata-file)
  * [aio-ia-generate-upload-file](#aio-ia-generate-upload-file)

## Quickstart

### Dependencies

A number of programs need to be installed for using aio-scripts. You also need `bash` version `4.0.0` or higher in order to run aio-scripts in `bash`.

**Mandatory**

* `realpath`, `bc`, `sort` for some audio-processing scripts
* `ffmpeg` and `ffprobe` (version 4.2.3 or higher), for all audio-processing scripts

**Optional**

A number of optional programs should be installed. If not installed, some aio-scripts will provide less functionality.

* `id3v2`, `metaflac`, `vorbis-comment` for metadata tagging. _If not installed, `aio-metdata-set` can be adjusted to use `ffmpeg` as tagging tool for supported metadata standards._
* `bats`, `mediainfo`, `opusinfo` for running unit-tests. _Not necessary for using aio-scripts._
* `hexdump`, `eyeD3` for running unit-tests in debug mode. _Not necessary for using aio-scripts and for running unit-tests in normal mode._
* `ia` (the [archive.org cl-tool](https://archive.org/services/docs/api/internetarchive/cli.html)) for file uploading to [archive.org](https://archive.org). _`ia` is purely optional and only necessary if one uses `archive.org`._

### Install Dependencies 

More detailed informations about installing dependencies (or just a subset) [can be found in the wiki](https://gitlab.com/zzeebdrraa/tools/cl-audio-processing/aio/-/wikis/Installation).

**Install all dependencies**

```bash
# ffmpeg package usually includes ffprobe. version must be 4.2.3 or higher
sudo apt-get install ffmpeg realpath bc sort

# metadata tagging packages fora io-metadata-scripts
sudo apt-get install id3v2 flac vorbis-comment

# mandatory packages used by unit tests
sudo apt-get install mediainfo opustools

# optional packages used by unit tests in debug mode
sudo apt-get install eyeD3 hexdump

# bats and aio are downloaded with git
sudo apt-get install git

# optional, ia needs python and is downloaded with wget
sudo apt-get install python3 wget
```

**Install bats**

Detailed installation instructions for `bats`:

- https://github.com/bats-core/bats-core

```bash
# clone bats to your local machine. this creates a folder named bats-core
git clone https://github.com/bats-core/bats-core.git

# go to the bats-core repository folder
cd bats-core

# run the install script. bats will be installed to /usr/local/bin folder
sudo ./install.sh /usr/local

# OPTIONAL: delete the bats-core folder

cd ..
rm -rf bats-core

# test if bats is available. this should show the version info of bats
bats --version
```

**Install ia**

**Note: `ia` is pureley optional. No need to install if you do not use 'archive.org'**

Detailed installation instructions for `ia`:

- https://archive.org/services/docs/api/internetarchive/installation.html#binaries

```bash
# download ia and install system-wide (or to any path in $PATH variable).
wget https://archive.org/download/ia-pex/ia
chmod +x ia

# move ia to /usr/local/bin folder or to any other custom location like ${HOME}/bin
sudo mv ia /usr/local/bin/

# test if ia is available. this should show the version info of ia.
ia --version
```

### Install aio

**Clone `aio` repository to your local system**

```bash
# clone the aio repository on your local machine. this creates a folder named aio
git clone https://gitlab.com/zzeebdrraa/tools/cl-audio-processing/aio.git
```

**Install `aio` system-wide**

```bash
# go to the local aio repository folder
cd aio

# configure system-wide installation to /usr/local/bin
./configure

# install all scripts to /usr/local/bin
sudo make install
```

**Install `aio` at a custom location**

```bash
# go to the local aio repository folder
cd aio

# here we want a installation to the bin folder in your home directory
./configure --prefix ${HOME}

# install all scripts too  ${HOME}/bin
make install
```

In order to use aio-scripts after installation, you need to add the installation directory to your $PATH variable. 

```bash
export PATH=${HOME}/bin:${PATH}
```

Exporting `PATH` like that has the drawback that once you open a new shell or restart your computer, the changes in PATH will be gone.

Therefore better persist changes to your `PATH`. How this is done is explained in detail in the following article

- https://opensource.com/article/17/6/set-path-linux


### Uninstall aio

```bash
# go to the local aio repository folder
cd aio

# remove all aio files from the installation directory
make uninstall
```

After uninstalling `aio` you might want to remove the installation directory from your `PATH` variable as well, if it has been added previsouly.

### Run Unit-Tests

Run all tests with make

```bats
  # go to the local aio repository folder
  cd aio

  # run all tests. none of them should fail.
  make check
```

Run all tests with bats

```bats
  # go to the local aio repository folder
  cd aio/tests
  
  # run all tests. none of them should fail.
  bats .
```

## Script Design

More information about the design of aio-scripts [can be found in the wiki](https://gitlab.com/zzeebdrraa/tools/cl-audio-processing/aio/-/wikis/Script-Design). 

### Help and Version Infos

All aoi-scripts provide the following help options:

- `--help` or `help` to show the overall help, without examples
- `help short` to show a short help, without details and examples
- `help details` to show detailed help, without usage and examples
- `help examples` to show examples on how to use the script

```bash
# this works for all aio-scripts
aio-cut --help
aio-cut help
aio-cut help short
aio-cut help details
aio-cut help examples
```

All aoi-scripts provide the following version option:

- `--version`

```bash
# this works for all aio-scripts
aio-cut --version
```

## Audio Processing

### aio-cut

For details see [aio-cut](https://gitlab.com/zzeebdrraa/tools/cl-audio-processing/aio/-/wikis/AIO/splitting-and-cutting#aio-cut) in the wiki.

**Usage**

```bash
aio-cut -h|--help

aio-cut h|help
aio-cut h|help s|short
aio-cut h|help d|details
aio-cut h|help e|examples

aio-cut --version

aio-cut s|status

aio-cut [-f|--force-overwrite]
        f|from <timestamp> <inputfile> [<outputfile>]

aio-cut [-f|--force-overwrite]
        t|to <timestamp> <inputfile> [<outputfile>]

aio-cut [-f|--force-overwrite]
        f|from <timestamp> t|to <timestamp> <inputfile> [<outputfile>]

aio-cut [-f|--force-overwrite]
        --|stdin <file-timestamp-list

aio-cut [-f|--force-overwrite]
        --|stdin <inputfile> [<outputfile>] <timestamp-list
```

**Examples**

```bash
# cut from begin of file to the given <to-timestamp> at 10 minutes, 4 seconds and 123 milliseconds
aio-cut to 10:04.123 show-12.ogg
=> cut-show-12.ogg

# cut input file from <from-timestamp> to file end
aio-cut from 10:04.123 show-12.ogg

=> cut-show-12.ogg

# cut input file from <from-timestamp> to <to-timestamp>
aio-cut from 10:04.123 to 45:00 show-12.ogg

=> cut-show-12.ogg

# cut input file from <from-timestamp> to <to-timestamp>
# write a custom <output file> show-12-ready.ogg
aio-cut from 00:10:04.123 to 45:00 show-12.ogg show-12-ready.ogg

=> show-12-ready.ogg

# cut one <input-file> several times
# several <from-timestamp> and <to-timestamp> are read from stdin
# time-periods can overlap
aio-cut stdin show-12.ogg <<EOF
3 10
10:04.123 45:00
EOF

=> a-cut-show-12.ogg
=> b-cut-show-12.ogg

# cut one <input-file> several times
# several <from-timestamp> are read from stdin
# always cuts to file end
# write results to custom <output-file> ready.ogg
aio-cut stdin show-12.ogg ready.ogg<<EOF
3
10:04.123
EOF

=> a-cut-ready.ogg
=> b-cut-ready.ogg

# cut one <input-file> several times
# several <to-timestamp> are read from stdin
# cutting always start from file begin
# write results to custom <output-file> ready.ogg
aio-cut stdin show-12.ogg ready.ogg<<EOF
0 50
0 10:04.123
EOF

=> a-cut-ready.ogg
=> b-cut-ready.ogg

# cut several files
# <input-file> names, <from-timestamps> and <to-timestamps> are read from stdin
aio-cut stdin <<EOF
show-12.ogg 3 1:10
show-12.ogg 10:04.123 45:00
show-20.ogg 0 45
EOF

=> a-cut-show-12.ogg
=> b-cut-show-12.ogg
=> a-cut-show-20.ogg
```

### aio-split

For details see [aio-cut](https://gitlab.com/zzeebdrraa/tools/cl-audio-processing/aio/-/wikis/AIO/splitting-and-cutting#aio-split) in the wiki.

**Note: <output-file> names currently contain a `cut` prefix, as `aio-split` uses `aio-cut` under the hood.**

<**Usage**

```bash></**Usage**

```bash>
aio-split -h|--help

aio-split h|help
aio-split h|help s|short
aio-split h|help d|details
aio-split h|help e|examples

aio-split --version

aio-split s|status

aio-split [-f|--force-overwrite]
          a|at <timestamp> <inputfile>

aio-split [-f|--force-overwrite]
          a|at <timestamp> [a|at ...] <inputfile>

aio-split [-f|--force-overwrite]
          --|stdin <file-at-list

aio-split [-f|--force-overwrite]
          --|stdin <inputfile> <at-list
```

**Examples**

```bash
# split a input file at 10 seconds and 10 milliseconds
aio-split at 10.01 show.ogg

=> a-cut-show.ogg
=> b-cut-show.ogg

# split a input file at two positions
aio-split at 00:00:10.01 at 01:34:12.45 show.ogg

=> a-cut-show.ogg
=> b-cut-show.ogg
=> c-cut-show.ogg

# split several files
# <input-file> names and <at-timestamps> are read from stdin
aio-split stdin <<EOF
show-10.ogg 01:34:12.45
show-10.ogg 10.50
show-20.ogg 50
EOF

=> a-cut-show-10.ogg
=> b-cut-show-10.ogg
=> c-cut-show-10.ogg
=> a-cut-show-20.ogg
=> b-cut-show-20.ogg

# split one input file at several <at-timestamps> read from stdin
aio-split stdin show.ogg <<EOF
01:34:12.45
10.50
50
EOF

=> a-cut-show.ogg
=> b-cut-show.ogg
=> c-cut-show.ogg
=> d-cut-show.ogg
```

### aio-concat

For details see [aio-cut](https://gitlab.com/zzeebdrraa/tools/cl-audio-processing/aio/-/wikis/AIO/splitting-and-cutting#aio-concat) in the wiki.

**Usage**

```bash
aio-concat -h|--help

aio-concat h|help
aio-concat h|help s|short
aio-concat h|help d|details
aio-concat h|help e|examples

aio-concat --version

aio-concat [-f|--force-overwrite]
           <input-file-1> <input-file-2> [... <input-file-X>]

aio-concat [-f|--force-overwrite]
           [-o|--output-file <output-file>]
           <input-file-1> <input-file-2> [... <input-file-X>]
```

**Examples**

```bash
# concatenate two <input-files>
aio-concat track-1.ogg track-12.ogg

=> concat-track-1.ogg

# concatenate three <input-files>
# write to an explicitly given <output-file>
aio-concat -o new-track.ogg track-1.ogg track-3.ogg track-12.ogg

=> new-track.ogg

# concatenate three <input-files>
# write to an explicitly given <output-file>
# <output-file> name has no file extension, thus .ogg will be appended automatically
aio-concat -o new-track track-1.ogg track-3.ogg track-12.ogg

=> new-track.ogg

# concatenate two <input-files>
# force overwrite of an already existing <output-file>
aio-concat -f -o new-track.mp3 track-1.mp3 track-3.mp3

=> new-track.mp3
```

### aio-mix

**Usage**

```bash
aio-mix -h|--help

aio-mix h|help
aio-mix h|help s|short
aio-mix h|help d|details
aio-mix h|help e|examples

aio-mix --version

aio-mix s|settings

aio-mix [-d|--dry-run]
        [-f|--force-overwrite]
        [-q|--quality <value>]
        <input-file-1> <input-file-2> [... <input-file-X>]

aio-mix [-d|--dry-run]
        [-f|--force-overwrite]
        [-q|--quality <value>]
        [-o|--output-file <output-file>]
        <input-file-1> <input-file-2> [... <input-file-X>]
```

**Examples**

```bash
# mix two <input-files> 
aio-mix audio-1.ogg audio-2.ogg

=> mix-audio-1.ogg

# mix several <input-files> with differing codecs
# output-file will be a wav
aio-mix audio-1.wav audio-2.flac audio-3.flac audio-4.mp3

=> mix-audio-1.wav

# mix two <input-files>
# write to an explicitly named <output-file>
aio-mix -o mix.ogg audio-1.ogg audio-2.flac

=> mix.ogg

# mix two <input-files> 
# write to an explicitly named <output-file>
# force overwrite of already existing <output-file>
aio-mix -f -o mix.ogg audio-1.mp3 audio-2.wav

=> mix.ogg
```

### aio-fade

**Usage**

```bash
aio-fade -h|--help

aio-fade h|help
aio-fade h|help s|short
aio-fade h|help d|details
aio-fade h|help e|examples

aio-fade --version

aio-fade s|settings

aio-fade [-d|--dry-run]
         [-q|--quality <value>]
         [-f|--force-overwrite]
         i|in
           [--di <duration-sec>]
           [--ci <curve-type>]
           <input-file> [ ... <input-file-x>]

aio-fade [-d|--dry-run]
         [-q|--quality <value>]
         [-f|--force-overwrite]
         o|out
           [--do <duration-sec>]
           [--co <curve-type>]
           <input-file> [ ... <input-file-x>]

aio-fade [-d|--dry-run]
         [-q|--quality <value>]
         [-f|--force-overwrite]
         io|inout
           [--di <duration-sec>] [--do <duration-sec>]
           [--ci <curve-type>] [--co <curve-type>]
           <input-file> [ ... <input-file-x>]
```

**Examples**

```bash
# use the default <fade-duration> and <fade-curve> for fadding-in one <input-file>
aio-fade in audio.ogg

=> fade-in-audio.ogg

# use the default <fade-in-duration> and <fade-in-curve> for fadding-in several <input-files>
aio-fade in audio-a.ogg audio-b.mp3 audio-c.flac

=> fade-in-audio-a.ogg
=> fade-in-audio-b.mp3
=> fade-in-audio-c.flac

# set a custom <fade-out-curve> --co losi for in-out-fade
# use defaults otherwise
aio-fade inout --co losi audio.ogg

=> fade-inout-audio.ogg

# # set a custom <fade-in-curve> --ci par for in-out-fade 
# use defaults otherwise
aio-fade inout --ci par --co losi audio.ogg

=> fade-inout-audio.ogg

# set a custom <fade-out-duration> for fadding-out one <input-file>
# use defaults otherwise
aio-fade out --do 2.5 audio.ogg
=> fade-out-audio.ogg
```

### aio-detect-silence

**Usage**

```bash
aio-detect-silence h|help
aio-detect-silence h|help s|short
aio-detect-silence h|help d|details
aio-detect-silence h|help e|examples

aio-detect-silence --version

aio-detect-silence s|status

aio-detect-silence [--ts|--as-timestamp]
                   [-t|--treshold <-db>] [-d|--duration <sec>]
                   [--ab|--at-begin-only] [--ae|--at-end-only]
                   <input-file> [... <input-file-x>]

```

**Examples**

```bash
# detect all silence sections and print infos to standard output
# output is: <filename> <begin-silence-pos> <end-silence-pos> <duration-silence>
aio-detect-silence audio-1.ogg
> audio.ogg 9.99 14.98 4.98

./aio-detect-silence audio-1.ogg audio-2.ogg
> audio.ogg 9.99 14.98 4.98
> audio-2.ogg  0 4.98 4.98
> audio-2.ogg  5.01 10 4.99

# only print silence section at the begin of the file, if any
# output is: <filename> <duration-silence>
aio-detect-silence --ab audio-2.ogg
> audio-2.ogg 4.98

# use a different treshold => -12db
aio-detect-silence -t -12 audio-1.ogg

# use a different minimum duration of silence => 5sec
aio-detect-silence -d 5 audio-1.ogg

# or change both at a time
aio-detect-silence -t -12 -d 5 audio-1.ogg
```

### aio-normalize

**Usage**

```bash
aio-normalize -h|--help

aio-normalize h|help
aio-normalize h|help s|short
aio-normalize h|help d|details
aio-normalize h|help e|examples

aio-normalize --version

aio-normalize [-d|--dry-run]
              [-q|--quality <value>]
              [-f|--force-overwrite]
              p|peak
                [-l|--peak-level <peak-level-db>]
                <input-file> [ ... <input-file-x>]

aio-normalize [-d|--dry-run]
              [-q|--quality <value>]
              [-f|--force-overwrite]
              l|loudness
                <input-file> [ ... <input-file-x>]

aio-normalize v|volume
                <input-file> [ ... <input-file-x>]

aio-normalize s|settings
```

**Examples**

```bash
# peak normalisation to 0db of one <input-file>
aio-normalize peak audio.ogg

=> norm-peak-0db-audio.ogg

# peak normalisation to 0db of several <input-files>
aio-normalize peak audio.flac audio.wav audio.mp3

=> norm-peak-0db-audio.flac
=> norm-peak-0db-audio.wav
=> norm-peak-0db-audio.mp3

# peak normalisation to -3db of one <input-file>
aio-normalize peak audio.ogg
=> norm-peak--3db-audio.ogg

# peak normalisation of several <input-files>
# force overwrite of already existing <output-files>
aio-normalize -f peak audio.flac audio.wav
=> norm-peak--3db-audio.flac
=> norm-peak--3db-audio.wav

# show default quality settings
aio-normalize s

# show detected mean and max volumes of <input-file>
aio-normalize volume audio.ogg audio.wav
```

### aio-sec-to-ts

**Usage**

```bash
aio-sec-to-ts -h|--help

aio-sec-to-ts h|help
aio-sec-to-ts h|help s|short
aio-sec-to-ts h|help d|details
aio-sec-to-ts h|help e|examples

aio-sec-to-ts --version

aio-sec-to-ts <sec> [<sec> ... <sec>]

aio-sec-to-ts --|stdin <sec-list
```

**Examples**

```bash
# convert one value from <seconds> to <timestamp> format
aio-sec-to-ts 80
> 00:01:20.000

# convert several values from <seconds> to <timestamp> format
aio-sec-to-ts 80 160 240
> 00:01:20.000
> 00:02:40.000
> 00:04:00.000

# convert one value from <seconds> to <timestamp> format
# value is read from stdin
aio-sec-to-ts stdin <<<80
> 00:01:20.000

# convert several values from <seconds> to <timestamp> format
# values is read from stdin
aio-sec-to-ts stdin <<EOF
80
160
240
EOF

> 00:01:20.000
> 00:02:40.000
> 00:04:00.000
```

### aio-ts-to-sec

**Usage**

```bash
aio-ts-to-sec -h|--help

aio-ts-to-sec h|help
aio-ts-to-sec h|help s|short
aio-ts-to-sec h|help d|details
aio-ts-to-sec h|help e|examples

aio-ts-to-sec --version

aio-ts-to-sec <timestamp> [... <timestamp>]

aio-ts-to-sec --|stdin <timestamp-list
```

**Examples**

```bash
# convert a full <timestamp> to <seconds>
aio-ts-to-sec 23:59:35.5
> 86375.5

# convert a <timestamp> containing only <seconds> to <seconds>
# conversion does not change the value
aio-ts-to-sec 35.5
> 35.5

# convert several <timestamps> to <seconds>
aio-ts-to-sec 23:59:35.5 01:09:35 59:35 35
> 86375.5
> 4175
> 3575
> 35

# convert one <timestamp> to <seconds>
# data is read from stdin
aio-ts-to-sec stdin <<<00:59:35
> 3575

# convert one <timestamps> to <seconds>
# data is read from stdin
aio-ts-to-sec stdin <<EOF
23:59:35.5
01:09:35
59:35
35
EOF

> 86375.5
> 4175
> 3575
> 35
```

## Upload to Archive.org

### aio-ia-generate-metadata-file

**Usage**

```bash
aio-ia-generate-metadata-file -h|--help
  
aio-ia-generate-metadata-file h|help
aio-ia-generate-metadata-file h|help s|short
aio-ia-generate-metadata-file h|help d|details
aio-ia-generate-metadata-file h|help e|examples
aio-ia-generate-metadata-file h|help a|archive
aio-ia-generate-metadata-file h|help m|metadata
aio-ia-generate-metadata-file h|help o|output

aio-ia-generate-metadata-file --version

aio-ia-generate-metadata-file n|new
  [-d|--dry-run] 
  [-o|--output-file <csv-file>]
  [-c|--creator <creator>]        | [-C|--placeholder-creator]
  [-g|--collection <collection>]  | [-G|--placeholder-collection] | [shortcuts]
  [-i|--identifier <identifier>]  | [-I|--placeholder-identifier]
    <metadata-stdin

aio-ia-generate-metadata-file a|append
  [-d|--dry-run] 
  [-o|--output-file <csv-file>]
  [-c|--creator <creator>]        | [-C|--placeholder-creator]
  [-g|--collection <collection>]  | [-G|--placeholder-collection] | [shortcuts]
  [-i|--identifier <identifier>]  | [-I|--placeholder-identifier]
    <metadata-stdin

aio-ia-generate-metadata-file t|template
  [-c|--creator <creator>]        | [-C|--placeholder-creator]
  [-g|--collection <collection>]  | [-G|--placeholder-collection] | [shortcuts]
  [-i|--identifier <identifier>]  | [-I|--placeholder-identifier]
```

**Examples**

```bash
# pass metadata content via a here-doc

aio-ia-generate-metadata-file new <<EOF
identifier thishow
date 2000-01-01
description the millenium bug show
subject music
subject pop
subject mainstream
EOF

# as before but metadata is piped via a file

aio-ia-generate-metadata-file new <thishow-metadata.txt

# use files for decription and subjects, and also
# specify additional <subject> values

aio-ia-generate-metadata-file new <<EOF
identifier thishow
description_file ./description.txt
subject_file ./subjects-common.txt
subject_file ./subjects-new.txt
subject music
subject pop
subject mainstream
EOF

# append metadata to an existing metadata csv-file
# new and append use the same identifier value

aio-ia-generate-metadata-file new -i thishow -c me

aio-ia-generate-metadata-file append <<EOF
identifier thishow
description_file ./description.txt
subject_file ./subjects-common.txt
EOF
```

### aio-ia-generate-upload-file

**Usage**

```bash
aio-ia-generate-upload-file -h|--help

aio-ia-generate-upload-file h|help
aio-ia-generate-upload-file h|help s|short
aio-ia-generate-upload-file h|help d|details
aio-ia-generate-upload-file h|help e|examples
aio-ia-generate-upload-file h|help a|archive
aio-ia-generate-upload-file h|help m|metadata
aio-ia-generate-upload-file h|help o|output

aio-ia-generate-upload-file --version

aio-ia-generate-upload-file n|new
  [-d|--dry-run]
  [-o|--output-file <csv-file>]
  [-c|--creator <creator>]        | [-C|--placeholder-creator]
  [-g|--collection <collection>]  | [-G|--placeholder-collection] | [shortcuts]
  [-i|--identifier <identifier>]  | [-I|--placeholder-identifier]
  [-m|--mediatype <mediatype>]    | [-M|--placeholder-mediatype]  | [shortcuts]
    <file-a> [... <file-x>]
    <metadata-stdin

aio-ia-generate-upload-file a|append
  [-d|--dry-run]
  [-o|--output-file <csv-file>]
  [-c|--creator <creator>]        | [-C|--placeholder-creator]
  [-g|--collection <collection>]  | [-G|--placeholder-collection] | [shortcuts]
  [-i|--identifier <identifier>]  | [-I|--placeholder-identifier]
  [-m|--mediatype <mediatype>]    | [-M|--placeholder-mediatype]  | [shortcuts]
    <file-a> [... <file-x>]
    <metadata-stdin

aio-ia-generate-upload-file t|template
  [-c|--creator <creator>]        | [-C|--placeholder-creator]
  [-g|--collection <collection>]  | [-G|--placeholder-collection] | [shortcuts]
  [-i|--identifier <identifier>]  | [-I|--placeholder-identifier]
  [-m|--mediatype <mediatype>]    | [-M|--placeholder-mediatype]  | [shortcuts]
```

**Examples**

```bash
# select all .ogg files for item "foo"
# and write file content to stdout

aio-ia-generate-upload-file --dry-run new -i foo *.ogg

# select all this-show*.ogg files for item "thishow"
# and creator "jockey" and write csv-file

aio-ia-generate-upload-file new  -c jockey -i thishow this-show*.ogg

# dry-run mode to print to stdout only

aio-ia-generate-upload-file --dry-run new -i foo -m movies *.mkv

# join several mediatypes for the same identifier
#   into one file:
#
# 1. generate a new csv-file with movies content

aio-ia-generate-upload-file new -i foo -m movies *.mkv

# 2. then append the result for the new image media-type to
#   the previously generated file (-i is the same in both
#   calls)

aio-ia-generate-upload-file append -i foo -m image *.png

# join several mediatypes for the different identifiers
#   into one file:
#
# 1. generate a new csv-file with movies content for
#      a certain identifier

aio-ia-generate-upload-file new  -i foo -m movies *.mkv

# 2. now we add images for a differnet identifier to
#      the file generated in step 1.
#
#    for that we use the --apend mode and --dry-run
#      in order to just print the newly generated
#      csv data to stdout. we then redirect and append
#      stdout data to the existing csv file from step 1

aio-ia-generate-upload-file append --dry-run -i foo -m image *.png >>foo-ia-upload.csv
```

## Metadata Tagging

### aio-metadata-generate

**Usage**

```bash
aio-metadata-generate h|-h|--help

aio-metadata-generate h|help
aio-metadata-generate h|help s|short
aio-metadata-generate h|help d|details
aio-metadata-generate h|help e|examples
aio-metadata-generate h|help m|metadata

aio-metadata-generate --version

aio-metadata-generate c|common
                        [i3|vc|ff|ri]

aio-metadata-generate c|common
                        [<metadata-key-list>]

aio-metadata-generate i|individual <num-entries>
                        [i3|vc|ff|ri]

aio-metadata-generate i|individual <num-entries>
                        [<metadata-key-list>]
```

**Examples for generation of common metadata**

```bash
# generate a default set of common metadata tags (id3v2)
aio-metadata-generate common

  TPE1
  TALB
  TYER
  COMM

# generate a default set of common vorbis metadata tags
aio-metadata-generate common vc

  ARTIST
  ALBUM
  DATE
  DESCRIPTION

# generate a custom set of common vorbis metadata tags
aio-metadata-generate common TITLE ALBUM_ARTIST TRACKNUMBER DESCRIPTION

  TITLE
  ALBUM_ARTIST
  TRACKNUMBER
  DESCRIPTION
  
# generate a custom set of invented common tags. this basically
# transforms columns into rows 
aio-metadata-generate common this is just a sentence

  this
  is
  just
  a
  sentence
```

**Examples for generation of individual metadata**

```bash
# generate a default set of individual metadata tags (id3v2)
aio-metadata-generate individual 3

  TIT2
  TPE2
  TRCK
  
  TIT2
  TPE2
  TRCK
  
  TIT2
  TPE2
  TRCK
  
# generate a default set of individual RIFF metadata tags
aio-metadata-generate individual 2 ri

  INAM
  ITRK
  
  INAM
  ITRK

# generate a custom set of individual ffmpeg metadata tags
aio-metadata-generate individual 3 album date album_artist

  album
  date
  album_artist
  
  album
  date
  album_artist
  
  album
  date
  album_artist

# generate a custom set of invented individual metadata tags
aio-metadata-generate individual 3 a b c d e f

  a
  b
  c
  d
  e
  f
  
  a
  b
  c
  d
  e
  f
  
  a
  b
  c
  d
  e
  f
```

### aio-metadata-set

**Usage**

```bash
aio-metadata-set -h|--help

aio-metadata-set h|help
aio-metadata-set h|help s|short
aio-metadata-set h|help d|details
aio-metadata-set h|help e|examples
aio-metadata-set h|help m|metadata

aio-metadata-set --version

aio-metadata-set ls|list-files <file1> [<file2> ... <fileX>]

aio-metadata-set cn|count-files <file1> [<file2> ... <fileX>]

aio-metadata-set s|status

aio-metadata-set [-d|--dry-run]
                 [--fff|--force-ffmpeg]
                 i|individual
                   <file1> [<file2> ... <fileX>]

aio-metadata-set [-d|--dry-run]
                 [--fff|--force-ffmpeg]
                 c|common
                   <file1> [<file2> ... <fileX>]
```

**Examples**

```bash
# set <vorbis-comment> metadata that is common to two <input-files>
# the metadata standard is selected by the extension of <input-files>
# both files will be set with the same <vorbis-comment> metadata
aio-metadata-set common file-a.ogg file-b.ogg <<EOF
ALBUM bar
DESCRIPTION this is a comment
DATE 1977
EOF

# a file can be used as well for any supported metadata standard
# here, common-vc.txt contains common <vorbis-comment> metadata
aio-metadata-set common file-a.ogg file-b.ogg <common-vc.txt

# set <id3v2> metadata that is common to two <input-files>
# the metadata standard is selected by the extension of <input-files>
# both files will be set with the same <id3v2> metadata
aio-metadata-set common file-a.mp3 file-b.mp3 <<EOF
TALB bar
COMM this is a comment
TYER 1977
EOF

# use generic <ffmpeg> metadata tags to set <id3v2> metadata in mp3-files
# the metadata standard is selected by the extension of <input-files>
aio-metadata-set --fff common file-a.mp3 file-b.mp3 <<EOF
album bar
comment this is a comment
year 1977
EOF

# set individual <vorbis-comment> metadata for two <input-files>
# the metadata standard is selected by the extension of <input-files>
# we need a metadata section for each input-file
aio-metadata-set individual file-a.ogg file-b.ogg <<EOF
# section for file-a.ogg
ALBUM bar
DESCRIPTION this is a comment
DATE 1977

# section for file-b.ogg
ALBUM foo
DESCRIPTION this is another comment
DATE 1978
EOF

# a file can be used as well for any supported metadata standard
# here, individual-vc.txt contains individual <vorbis-comment> metadata sections
aio-metadata-set individual file-a.ogg file-b.ogg <individual-vc.txt
```

### aio-metadata-transform

**Usage**

```bash
aio-metadata-transform -h|--help

aio-metadata-transform h|help
aio-metadata-transform h|help s|short
aio-metadata-transform h|help d|details
aio-metadata-transform h|help e|examples

aio-metadata-transform --version

aio-metadata-transform kv|key-value
                <custom-tag> as <metadata-tag>
                <custom-tags-file>
                [s|selector <line-selector-string>]
                  < metadata-file

aio-metadata-transform c|column
                <col-num> as <metadata-tag>
                <custom-tags-file>
                [d|delimiter <col-separator-char>]
                  < metadata-file

aio-metadata-transform n|number
                <metadata-tag> [<start-num>] [<stop-num>]
                  < metadata-file

aio-metadata-transform v|value
                <string> as <metadata-tag>
                [s|select <metadata-tag-index-list>]
                  < metadata-file

aio-metadata-transform <cmd> <args> ::: <cmd> <args> ::: ... <cmd> <args>
                < metadata-file
```

**Examples**

```bash
Files used

cat tags.csv

  # year, artist,        album
  1991,   prince        ,diamonds and pearls
  1998,   massive attack,mezzanine
  1996,   tricky        ,nearly god

cat tags.txt

  # section one
  A prince
  T diamonds and pearls 
  Y 1991

  # section two
  A massive attack
  T mezzanine
  Y 1998

  # section three
  A tricky
  T nearly god 
  Y 1996

cat cut.txt

  # T sample one
  # A mouse
  04:14.5

  # T sample two
  # A elephant
  12:01.1

  # T sample three
  # A zebra
  05.87

cat metadata-individual.txt

  # section one
  YEAR
  ARTIST
  ALBUM
  TITLE
  TRACKNUMBER

cat metadata-multiple.txt

  # section one
  YEAR
  ARTIST
  ALBUM
  TITLE
  TRACKNUMBER

  # section two
  YEAR
  ARTIST
  ALBUM
  TITLE
  TRACKNUMBER

  # section three
  YEAR
  ARTIST
  ALBUM
  TITLE
  TRACKNUMBER
```

```bash
# use value of custom tag 'T' from 'tags.txt' as vorbis-comment 'TITLE'
# in 'metadata-individual.txt' and write the result to standard out

aio-metadata-transform kv T as TITLE tags.txt <metadata-individual.txt

  YEAR
  ARTIST
  ALBUM
  TITLE diamonds and pearls 
  TRACKNUMBER

# the same as before but this time, custom tag 'T' is written as a
# comment in 'cut.txt'

aio-metadata-transform kv T as TITLE cut.txt selector '#' <metadata-individual.txt

  YEAR
  ARTIST
  ALBUM
  TITLE sample one
  TRACKNUMBER

# use value of third column in 'tags.csv' as vorbis-comment 'ALBUM'
# in 'metadata-individual.txt' and write the result to standard out

aio-metadata-transform c 3 as ALBUM tags.csv delimiter ',' <metadata-individual.txt

  YEAR
  ARTIST
  ALBUM diamonds and pearls
  TITLE
  TRACKNUMBER

# the same as before, but now set all ARTIST tags in 'metadata-multiple.txt'

aio-metadata-transform c 2 as ARTIST tags.csv delimiter ',' <metadata-multiple.txt

  YEAR
  ARTIST prince
  ALBUM
  TITLE
  TRACKNUMBER
  
  YEAR
  ARTIST massive attack
  ALBUM
  TITLE
  TRACKNUMBER
  
  YEAR
  ARTIST tricky
  ALBUM
  TITLE
  TRACKNUMBER

# generate a number sequence for vorbis-comment tag 'TRACKNUMBER'
# starting from 1 until the last occurence of 'TRACKNUMBER'
# in file 'metadata-individual.txt'

aio-metadata-transform n TRACKNUMBER <metadata-individual.txt

  YEAR
  ARTIST
  ALBUM
  TITLE
  TRACKNUMBER 1

# the same as before but generate a number sequence for 'TRACKNUMBER'
# starting from 5 to 17 ( but only three items are available as metadata)

aio-metadata-transform n TRACKNUMBER 5 17 <metadata-multiple.txt

  YEAR
  ARTIST
  ALBUM
  TITLE
  TRACKNUMBER 5
  
  YEAR
  ARTIST
  ALBUM
  TITLE
  TRACKNUMBER 6
  
  YEAR
  ARTIST
  ALBUM
  TITLE
  TRACKNUMBER 7

# set a new value to occurence of vorbis-comment tag 'YEAR'
# in file 'metadata-individual.txt'

aio-metadata-transform v 1999 YEAR <metadata-individual.txt
  
  YEAR 1999
  ARTIST
  ALBUM
  TITLE
  TRACKNUMBER

# set a new value to all occurences of vorbis-comment tag 'ARTIST'
# in file 'metadata-multiple.txt'

aio-metadata-transform v portishead ARTIST <metadata-individual.txt

  YEAR
  ARTIST portishead
  ALBUM
  TITLE
  TRACKNUMBER
  
  YEAR
  ARTIST portishead
  ALBUM
  TITLE
  TRACKNUMBER
  
  YEAR
  ARTIST portishead
  ALBUM
  TITLE
  TRACKNUMBER

# the same as before but only change ARTIST of the 2nd and 3rd occurence

aio-metadata-transform v portishead ARTIST select 2 3 <metadata-multiple.txt

  YEAR
  ARTIST
  ALBUM
  TITLE
  TRACKNUMBER
  
  YEAR
  ARTIST portishead
  ALBUM
  TITLE
  TRACKNUMBER
  
  YEAR
  ARTIST portishead
  ALBUM
  TITLE
  TRACKNUMBER
  
# the same as before but change all occurences of ARTIST explicitly
# with a index-range 1-3

aio-metadata-transform v portishead ARTIST select 1-3 <metadata-multiple.txt

  YEAR
  ARTIST portishead
  ALBUM
  TITLE
  TRACKNUMBER
  
  YEAR
  ARTIST portishead
  ALBUM
  TITLE
  TRACKNUMBER
  
  YEAR
  ARTIST portishead
  ALBUM
  TITLE
  TRACKNUMBER

# generate a list of id3v2 metadata tags, pipe it to this
# script and generate numbers for the tracknunmber 'TRCK'
# result is written to file 'metadata-id3v2.txt'

printf '%s\n' 'TPE2' 'TIT2' 'TRCK' 'COMM'{reset} \
| aio-metadata-transform n TRCK >metadata-id3v2.txt

  TPE2
  TIT2
  TRCK 1
  COMM

# generate a list of id3v2 metadata tags, pipe it to this
# script and substitute title and artist from 'cut.txt'
# also generate the tracknumber. every sub-command is separated by ':::'
# result is written standard output and file 'metadata-id3v2.txt'

printf '%s\n' 'TPE2' 'TIT2' 'TRCK' 'COMM'{reset} \
| aio-metadata-transform n TRCK \
                     ::: kv A as TPE2 cut.txt \
                     ::: kv T as TIT2 cut.txt \
| tee metadata-id3v2.txt

  TPE2 mouse
  TIT2 sample one
  TRCK 1
  COMM

# the same as before but use already existing file 'metadata-individual.txt'
# and substitute its content. the tracknumbers are generated starting from 10.
#
# NOTE: this time we cannot pipe the result back to metadata-individual.txt
#   as the file would then be open for read and write at the same time.

aio-metadata-transform n TRCK 10 \
                   ::: kv A as TPE2 cut.txt \
                   ::: kv T as TIT2 cut.txt \
  <metadata-individual.txt | tee tmp.txt
  
  TPE2 mouse
  TIT2 sample one
  TRCK 10
  COMM

# use values of each column in tags.csv as YEAR, ARTIST and ALBUM
# in 'metadata-individual.txt' and write the result to standard out.
#
# NOTE: this time we cannot pipe the result back to metadata-individual.txt
#   as the file would then be open for read and write at the same time.

aio-metadata-transform c 1 as YEAR tags.csv d ',' \
                   ::: c 2 as ARTIST tags.csv d ',' \
                   ::: c 3 as ALBUM tags.csv d ',' \
  <metadata-multi.txt | tee tmp.txt
  
  # section one
  YEAR 1991
  ARTIST prince
  ALBUM diamonds and pearls
  TITLE
  TRACKNUMBER

  # section two
  YEAR 1998
  ARTIST massive attack
  ALBUM mezzanine
  TITLE
  TRACKNUMBER

  # section three
  YEAR 1996
  ARTIST tricky
  ALBUM nearly god
  TITLE
  TRACKNUMBER
```

## File Renaming

### aio-rename-files

**Usage**

```bash
aio-rename-files -h|--help
  
aio-rename-files help
aio-rename-files help s|short
aio-rename-files help d|details

aio-rename-files help select
aio-rename-files help in
aio-rename-files help stdin
aio-rename-files help out
aio-rename-files help mix

aio-rename-files examples select
aio-rename-files examples in
aio-rename-files examples stdin
aio-rename-files examples out
aio-rename-files examples mix

aio-rename-files --version

aio-rename-files [-d|--dry-run] [-s|--show-only]
                 [-f|--force-overwrite] [-c|--copy-to-current-directory]
                 [--do|--delimiter-out <delimiter>]
                 <cmd> ... :::
                 <file-list>

aio-rename-files [global-options]
                 s|select ... :::
                 i|in ... :::
                 stdin ... :::
                 o|out ... :::
                 <file-list>
                 < data-stdin

aio-rename-files [global-options]
                 in [--di|--delimiter-in <delimiter>]
                   k|keep <filename-part-index-list> :::
                 <file-list>

aio-rename-files [global-options]
                 in [--di|--delimiter-in <delimiter>]
                   c|cut <filename-part-index-list> :::
                 <file-list>

aio-rename-files [global-options]
                 in
                   s|split <pos-index-list> :::
                 <file-list>

aio-rename-files [global-options]
                 stdin
                   v|value :::
                 <file-list>
                 < value-list-stdin

aio-rename-files [global-options]
                 stdin
                   kv|key-value <key-list> :::
                 <file-list>
                 < key-value-list-stdin

aio-rename-files [global-options]
                 out
                   a|append <suffix|suffix-list> :::
                 <file-list>

aio-rename-files [global-options]
                 out
                   p|prepend <prefix|prefix-list> :::
                 <file-list>

aio-rename-files [global-options]
                 out
                   r|replace [-s|--squeeze-repeats]
                     <first|last|firstlast>
                     <string-to-replace> with <replace-value-list> :::
                 <file-list>

aio-rename-files [global-options]
                 out
                   r|replace [-s|--squeeze-repeats] [--sb|--strip-begin] [--se|--strip-end]
                     <all>
                     <string-to-replace> with <replace-value-list> :::
                 <file-list>first

aio-rename-files [global-options]
                 s|select <file-index-list> :::
                 o|out ... :::
                 <file-list>

aio-rename-files [global-options]
                 s|select <file-index-list> :::
                 i|in ... :::
                 <file-list>

aio-rename-files [global-options]
                 s|select <file-index-list> :::
                 stdin ... :::
                 <file-list>
                 < data-stdin
```

**Some Common Examples**

```bash
# dry-run: just verify that the files in this directory will be
# properly renamed

aio-rename-files --dry-run out prepend 'thishow' ::: *.flac
> [1] file-1.flac -> thisshow-file-1.flac
> [2] file-2.flac -> thisshow-file-2.flac
> [3] file-3.flac -> thisshow-file-3.flac

# rename files in this directory by prepending 'thisshow' to all
# filenames that are matching the given file pattern '*.flac'

aio-rename-files out prepend 'thishow' ::: *.flac

# rename files in this directory by prepending 'thisshow' but only
# select from 10th a file in list upwards for renaming

aio-rename-files out prepend 'thishow' ::: select 10: ::: *.flac

# keep the 2nd and 4th part of input filenames for renaming. a part
# is delimited by the default input-delimiter '-'

aio-rename-files in keep 2 4 ::: artist-01-album-tracka.flac artist-02-album-trackb.flac
> 01-tracka.flac
> 02-trackb.flac

# keep some parts of input-filenames, but only select a couple of
# input-files for renaming

aio-rename-files in keep 2 4 ::: select 1 3:7 ::: *.flac

# skip some parts of the input-filenames. all remaining parts are
# used for renaming

aio-rename-files in cut 1:3 ::: artist-01-album-tracka.flac artist-02-album-trackb.flac
> tracka.flac
> trackb.flac

# skip some parts of the input-filenames, but only select a couple of
# input-files for renaming

aio-rename-files in cut 1:3 ::: select 5 7 9 ::: *.flac

# rename files by using values read from stdin

aio-rename-files stdin value ::: *.flac <<EOF
  foo bar
  bar foo
  bar
  foo
EOF

> foo-bar.flac
> bar-foo.flac
> bar.flac
> foo.flac

# rename only one file by using the corresponding value from stdin

aio-rename-files stdin value ::: select 3 ::: somefile.flac <<EOF
  foo bar
  bar foo
  bar
  foo
EOF

> bar.flac

# rename files by using key-value pairs read from stdin

aio-rename-files stdin kv song ::: *.flac <<EOF
  song cattle call
  artist people like us

  song born in limbo
  artist endon

  song kwang bass
  artist squarepusher
EOF

> cattle-call.flac
> born-in-limbo.flac
> kwang-bass.flac

# rename files by using key-value pairs read from stdin
# but only select the second and third file for renaming

aio-rename-files stdin kv A T ::: select 1 2 ::: *.flac <<EOF
  T cattle call
  A people like us

  T born in limbo
  A bendon

  T kwang bass
  A squarepusher
EOF

> people-like-us-cattle-call.flac
> endon-born-in-limbo.flac

# mix commands and rename only a single file
# - cut first three parts from input filename => 'this-is-a'
# - append suffix 'various' to renamed file
# - select third value on stdin for renamed file => 'bar'

aio-rename-files out append 'various' ::: in cut 1:3 ::: stdin value ::: select 3 ::: this-is-a-recording.flac <<EOF
  foo bar
  bar foo
  bar
  foo
EOF

> recording-bar-various.flac

```

**Examples for select**

```bash
Examples - select - in

  # select two files from file-list and
  # cut a filname part
  
    aio-rename-files in cut 1 ::: select 1 3 ::: \
      rec-1.ogg rec-2.ogg rec-3.ogg rec-4.ogg 
      
    > rec-1.ogg -> 1.ogg
    > rec-3.ogg -> 3.ogg

  # select two files from file-list and
  # keep a filname part
  
    aio-rename-files in keep 2 ::: select 1 3 ::: \
      rec-1.ogg rec-2.ogg rec-3.ogg rec-4.ogg 
      
    > rec-1.ogg -> 1.ogg
    > rec-3.ogg -> 3.ogg

  # select two files from file-list and
  # split filename at several positions
  
    aio-rename-files in split 1 2 ::: select 1 3 ::: \
      rec-1.ogg rec-2.ogg rec-3.ogg rec-4.ogg 
      
    > rec-1.ogg -> r-e-c-1.ogg
    > rec-3.ogg -> r-e-c-3.ogg

Examples - select - stdin value

  # select mode #1:
  #
  # select two files from file-list and
  # the corresponding two values from stdin
  # for renaming
  
    aio-rename-files stdin value ::: select 1 3 ::: \
      rec-1.ogg rec-2.ogg rec-3.ogg rec-4.ogg <<EOF
        aaa
        bbb
        ccc
        ddd
        EOF
      
    > rec-1.ogg -> aaa.ogg
    > rec-3.ogg -> ccc.ogg

  # select mode #2:
  #
  # select two values from values on stdin
  # and use them for renaming the only two
  # given files
  
    aio-rename-files stdin value ::: select 1 3 ::: \
      rec-1.ogg rec-2.ogg <<EOF
        aaa
        bbb
        ccc
        ddd
        EOF
      
    > rec-1.ogg -> aaa.ogg
    > rec-2.ogg -> ccc.ogg

  # select mode #3:
  #
  # select two files from input file list
  # and use the only two values on stdin 
  # for renaming
  
    aio-rename-files stdin value ::: select 1 3 ::: \
      rec-1.ogg rec-2.ogg rec-3.ogg rec-4.ogg <<EOF
        abc
        xyz
        EOF
      
    > rec-1.ogg -> abc.ogg
    > rec-3.ogg -> xyz.ogg

Examples - select - stdin key-value

Examples - select - out
    
  # select two files from file-list and
  # append the same suffix
  
    aio-rename-files out append ready ::: select 1 3 ::: \
      rec-1.ogg rec-2.ogg rec-3.ogg rec-4.ogg
      
    > rec-1.ogg -> rec-1-ready.ogg
    > rec-3.ogg -> rec-3-ready.ogg
    
  # select two files from file-list and
  # two suffix values from append list
  
    aio-rename-files out append ready steady go ::: select 1 3 ::: \
      rec-1.ogg rec-2.ogg rec-3.ogg rec-4.ogg
      
    > rec-1.ogg -> rec-1-ready.ogg
    > rec-3.ogg -> rec-3-go.ogg

  # select two files from file-list and
  # prepend the same prefix
  
    aio-rename-files out prepend ready ::: select 1 3 ::: \
      rec-1.ogg rec-2.ogg rec-3.ogg rec-4.ogg
      
    > rec-1.ogg -> ready-rec-1.ogg
    > rec-3.ogg -> ready-rec-3.ogg
    
  # select two files from file-list and
  # two prefix values from prepend list
  
    aio-rename-files out prepend ready steady go ::: select 1 3 ::: \
      rec-1.ogg rec-2.ogg rec-3.ogg rec-4.ogg
      
    > rec-1.ogg -> ready-rec-1.ogg
    > rec-3.ogg -> go-rec-3.ogg

  # select two files from file-list and
  # replace one part
  
    aio-rename-files out replace first rec with ready ::: select 1 3 ::: \
      rec-1.ogg rec-2.ogg rec-3.ogg rec-4.ogg
      
    > rec-1.ogg -> ready-1.ogg
    > rec-3.ogg -> ready-3.ogg

Examples - select mixed
    
  # select two files from file-list,
  # two values from stdin and
  # two prefix values from prepend list
  
    aio-rename-files in value ::: \
                     out prepend ready steady go ::: \
                     select 1 3 ::: \
                     rec-1.ogg rec-2.ogg rec-3.ogg rec-4.ogg <<EOF
                       aaa
                       bbb
                       ccc
                       ddd
                     EOF

    > rec-1.ogg -> aaa-ready.ogg
    > rec-3.ogg -> ccc-go.ogg

  # select two files from file-list,
  # and use the two values from stdin and
  # the two prefix values from prepend list
  
    aio-rename-files in value ::: \
                     out prepend ready steady ::: \
                     select 1 3 ::: \
                     rec-1.ogg rec-2.ogg rec-3.ogg rec-4.ogg <<EOF
                       aaa
                       bbb
                     EOF

    > rec-1.ogg -> aaa-ready.ogg
    > rec-3.ogg -> bbb-steady.ogg
    
  # select two files from file-list,
  # and use the two values from stdin and
  # the two prefix values from prepend list
  
    aio-rename-files in value ::: \
                     out prepend ready steady go::: \
                     select 1 3 ::: \
                     rec-1.ogg rec-2.ogg  <<EOF
                       aaa
                       bbb
                       ccc
                       ddd
                     EOF

    > rec-1.ogg -> aaa-ready.ogg
    > rec-2.ogg -> ccc-go.ogg

Examples - select errors

  # NOTE not yet working correctly
  #
  # select two suffix values from append list
  # and append to the two given files
  
    aio-rename-files out append ready steady go ::: select 1 3 ::: \
      rec-1.ogg rec-2.ogg 
      
    > rec-1.ogg -> rec-1-ready.ogg
    > rec-2.ogg -> rec-2-go.ogg

  # NOTE not yet working correctly
  #
  # select two suffix values from prepend list
  # and prepend to the two given files
  
    aio-rename-files out prepend ready steady go ::: select 1 3 ::: \
      rec-1.ogg rec-2.ogg 
      
    > rec-1.ogg -> ready-rec-1.ogg
    > rec-2.ogg -> go-rec-2.ogg
    
  # select two files from file-list,
  # and use the two values from stdin and
  # the two prefix values from prepend list
  
    aio-rename-files stdin value ::: \
                     out prepend ready steady go::: \
                     select 1 3 ::: \
                     rec-1.ogg rec-2.ogg  <<EOF
                       aaa
                       bbb
                     EOF

    > rec-1.ogg -> aaa-ready.ogg
    > rec-2.ogg -> ccc-go.ogg
```

**Examples for in command**

```bash
Examples - overview

  delimiter-out: -
  delimiter-in : -
  
  input file   : test-aio-rename-files.bats
  name parts   : test aio rename files
  part indeces :  1    2     3     4
  
Examples - in cut

  aio-rename-files in cut 4 ::: test-aio-rename-files.bats
  > test-aio-rename.bats

  aio-rename-files in cut 1-3 ::: test-aio-rename-files.bats
  > files.bats

  aio-rename-files in cut 3: ::: test-aio-rename-files.bats
  > test-aio.bats

Examples - in keep
  
  aio-rename-files in keep 4 ::: test-aio-rename-files.bats
  > files.bats

  aio-rename-files in keep 1-3 ::: test-aio-rename-files.bats
  > test-aio-rename.bats

  aio-rename-files in keep 3: ::: test-aio-rename-files.bats
  > rename-files.bats

Examples - in split

  aio-rename-files in split 4 7 14 ::: testaiorenamefiles.bats
  > test-aio-rename-files.bats

  aio-rename-files --do 'ABC' in split 14 4 ::: testaiorenamefiles.bats
  > testABCaiorenameABCfiles.bats

  aio-rename-files in split 1 ::: testaiorenamefiles.bats
  > t-estaiorenamefiles.bats

Examples - in - errors

  # invalid position

    aio-rename-files in cut 0 ::: text-1.txt

    aio-rename-files in keep -1 ::: text-1.txt
    
    aio-rename-files in split 1.0 ::: text-1.txt

  # invalid range

    aio-rename-files in cut 1-4 ::: text-1.txt

    aio-rename-files in keep 1::4 ::: text-1.txt
  
  # range syntax not supported (yet)

    aio-rename-files in split 1:4 ::: text-1.txt

  # invalid (empty) input-delimiter

    aio-rename-files in --di '' cut 1:4 ::: text-1.txt

Examples - in - unexpected

    aio-rename-files --do '_' in --di '' cut 1:4 ::: text-1.txt
    >tex_1.txt
    
    ./aio-rename-files --do '__' in --di 't' cut 5 ::: text-1.txt
    >ex__-1.txt

```

**Examples for stdin command**

```bash
Examples - stdin value

  # rename a single file with a value on stdin
  
    aio-rename-files stdin value ::: file-1.ogg <<<'a new name'
    > a-new-name.ogg

  # rename each file with values on stdin
  
    aio-rename-files stdin value ::: file-1.ogg file-2.ogg file-3.ogg<<EOF
      foo
      bar
      foobar
    EOF
    
    > foo.flac
    > bar.flac
    > foobar.flac

Examples - stdin key-value

  # overview about <key value> groups

    aio-rename-files stdin key-value keyB keyC ::: file-1.ogg file-2.ogg file-3.ogg<<EOF

      # begin of valid groups
      # with common keys: keyB and keyC
      # the value of keyB is the same in each group
      # the value of keyC differs in each group
      # keyA only exists in this group
      # group 1 for file #1
      keyA some value
      keyB another value
      keyC yet

      # group 2 for file #2
      # keyX and keyY only exist in this group
      keyX ignored value 2
      keyY another ignored value 2
      keyB another value
      keyC more

      # group 3 for file #3
      keyC even
      keyB another value
      # end
    EOF
    
    > file-1.ogg => another-value-yet.ogg
    > file-2.ogg => another value-more.ogg
    > file-3.ogg => another value-even.ogg

  # keys artist and song are provided in <key-name-list>
  # each <key value> group only contains those keys
  # three <key value> groups are provided on stdin and three files on cl

    aio-rename-files stdin kv artist song ::: file-1.ogg file-2.ogg file-3.ogg<<EOF
      artist bar
      song my song

      artist bar
      song your song

      artist foo
      song my song
    EOF

    > file-1.ogg => bar-my-song.ogg
    > file-2.ogg => bar-your-song.ogg
    > file-3.ogg => foo-my-song.ogg

  # keys tracknumber and song are provided in <key-name-list>
  # only the tracknumber key is available in all groups
  # none of the groups have a song key
  # three <key value> groups are provided on stdin and two files on cl

    aio-rename-files stdin kv tracknumber song ::: file-1.ogg file-2.ogg <<EOF
      artist foo
      title my song
      tracknumber 11

      tracknumber 12
      artist foo
      title your song

      artist bar
      tracknumber 13
      title my song
    EOF

    > file-1.ogg => 11.ogg
    > file-2.ogg => 12.ogg

Examples - stdin key-value - invalid group

  # overview about invalid <key value> groups

    aio-rename-files stdin key-value song track ::: *.ogg <<EOF

      # begin of invalid groups
      # group 1 for file #1
      # completely different keys as in group 2
      song my song
      artist bar
      album bar-mix

      # group 2 for file #2
      # completely different keys as in group 1
      # key tracknumber is a dublicate
      track foo
      tracknumber 10
      tracknumber 11

      # group 3 for file #3
      # all keys of groups 1 and 2
      # all values are the same as in group 1 and 2 and would lead to files renamed alike
      song my song
      artist bar
      album bar-mix
      track foo
      tracknumber 10

    EOF

  # keys artist and song are provided in <key-name-list>
  # key artist is available in all groups
  # key song is only available in some groups

    aio-rename-files stdin kv artist song ::: *.ogg <<EOF
      artist foo
      title my song
      tracknumber 11

      tracknumber 12
      artist foobar
      song your song

      artist bar
      tracknumber 13
      song my song
    EOF

  # keys tracknumber and track are provided in <key-name-list>
  # none of those keys are available in any groups

    aio-rename-files stdin kv tracknumber track ::: *.ogg <<EOF
      artist foo
      title my song

      artist foo
      title your song

      artist bar
      title my song
    EOF

  # keys in <key-name-list> are available in all groups
  # but three files are given on cl while only two groups are available

    aio-rename-files stdin kv artist song ::: file-1.ogg file-2.ogg file-3.ogg <<EOF
      artist foo
      song my song
      tracknumber 12

      artist foo
      song your song
      tracknumber 13
    EOF

  # keys in <key-name-list> are available in all groups
  # but key tracknumber is dublicated in one group

    aio-rename-files stdin kv artist tracknumber ::: file-1.ogg file-2.ogg <<EOF
      artist foo
      song my song
      tracknumber 12
      tracknumber 12

      artist foo
      song your song
      tracknumber 13
    EOF

  # keys in <key-name-list> are available in all groups
  # but keys have the same values in both groups

    aio-rename-files stdin kv artist year ::: file-1.ogg file-2.ogg <<EOF
      artist foo
      song my song
      year 2000

      artist foo
      song your song
      year 2000
    EOF

Examples - stdin key-value - errors

  # not enough <key value> groups available for given <input-files>
  
    aio-rename-files stdin kv artist year ::: file-1.ogg file-2.ogg file-3.ogg <<EOF
      # group 1
      artist foo
      song my song
      year 2000

      # group 2
      artist foo
      song your song
      year 2000
    EOF

  # <key value> groups result in same name as given <input-files>
  
    aio-rename-files stdin kv artist track ::: file-1.ogg file-2.ogg file-3.ogg <<EOF
      artist file
      song foobar
      track 1

      artist file
      song bar
      track 2
      
      artist file
      song foo
      track 3
    EOF
    
Examples - stdin value - errors
    
  # not enough <values> available for given <input-files>
  
    aio-rename-files stdin value ::: file-1.ogg file-2.ogg file-3.ogg <<EOF
      some artist
      another artist
    EOF
    
  # <values> result in same name as given <input-files>
  
    aio-rename-files stdin value ::: file-1.ogg file-2.ogg file-3.ogg <<EOF
      file 1
      file 2
      file 3
    EOF

```

**Examples for out command**

```bash
Examples - out append

  # append a single suffix to all files

    aio-rename-files out append 'in-stereo' ::: a-record.flac b-record.flac c-record.flac
    > a-record-in-stereo.flac
    > b-record-in-stereo.flac
    > c-record-in-stereo.flac
  
  # append a individual suffix to each file
  
    aio-rename-files out append 1 2 3 ::: a-record.flac b-record.flac c-record.flac
    > a-record-1.flac
    > b-record-2.flac
    > c-record-3.flac

Examples - out prepend

  # prepend a single prefix to all files

    aio-rename-files out prepend 'in-stereo' ::: a-record.flac b-record.flac c-record.flac
    > in-stereo-a-record.flac
    > in-stereo-b-record.flac
    > in-stereo-c-record.flac
  
  # prepend a individual prefix to each file
  
    aio-rename-files out prepend 1 2 3 ::: a-record.flac b-record.flac c-record.flac
    > 1-a-record-1.flac
    > 2-b-record-2.flac
    > 3-c-record-3.flac

Examples - out replace

  # replace the first encountered character sequence 'ar'

    aio-rename-files out replace first 'ar' with 'a-r' ::: arecord-title-artist.flac
    > a-record-title-artist.flac
  
  # replace the last encountered character sequence 'a-r'
  
    aio-rename-files out replace last 'a-r' with 'ar' ::: a-record-title-a-rtist.flac
    > a-record-title-artist.flac
  
  # delete encountered character '_'
  
    aio-rename-files out replace firstlast '_' with '' ::: _a_record_title_artist_.flac
    > a_record_title_artist.flac
  
  # replace all ecncountered characters '_' with '-'
  
    aio-rename-files out replace all '_' with '-' ::: a_record_title_artist.flac
    > a-record-title-artist.flac

  # squeeze and replace first character squence containing at least a single '_' with '-'
  
    aio-rename-files out replace -s all '_' with '-' ::: a____record-1.flac a_record-2.flac a__record-3.flac
    > a-record-1.flac
    > a-record-2.flac
    > a-record-3.flac
    
  # squeeze and replace all character squences containing at least a single '_' with '-'
  
    aio-rename-files out replace -s all '_' with '-' ::: a_record__title__artist.flac
    > a-record-title-artist.flac

  # replace all characters '_' with '-' but strip at begin and end of filenames
  
    aio-rename-files out replace --sb --se all '_' with '-' ::: _a_record_1.flac a_record_2.flac _a_record_3_.flac
    > a-record-1.flac
    > a-record-2.flac
    > a-record-3.flac

  # provided individual replace values for each file
  
    aio-rename-files out replace first 'a' with 'my' 'your' 'their' ::: a-record-1.flac a-record-2.flac a-record-3.flac
    > my-record-1.flac
    > your-record-2.flac
    > their-record-3.flac

Examples - out errors

  # not enough individual suffixes provided for each file
  # expected 3 suffixes for 3 files but only provided 2 suffixes
  
    aio-rename-files out append 1 2 ::: a-record.flac b-record.flac c-record.flac

  # not enough individual prefixes provided for each file
  # expected 3 prefixes for 3 files but only provided 2 prefixes
  
    aio-rename-files out prepend 1 2 ::: a-record.flac b-record.flac c-record.flac

  # not enough individual replace values provided for each file
  # expected 3 replace values for 3 files but only provided 2 values
  
    aio-rename-files out replace all '-' with x y ::: a-record.flac b-record.flac c-record.flac
    
  # try to prepend a not allowed character '-'
  
    aio-rename-files out prepend '-' ::: a-record.flac b-record.flac c-record.flac

  # renamed files start with not allowed character '-' 
  # => -b-record.flac
  
    aio-rename-files out replace -s all '_' with '-' ::: _-b___record.flac

  # wildcard patterns are not supported and most likely do not result
  # in a found character sequence
  # => renamed files will then be the same as input-files
  
    aio-rename-files out replace all '_*' with '-' ::: a__record.flac b_record.flac c___record.flac

```

**Examples for command mixes**

```bash
# use out prepend and in commands
#   filename => <PREFIX>-<IN-PARTS>.ogg

  aio-rename-files out prepend 2000 ::: in keep 1:3 ::: \
                   a-radio-record-xyz.ogg
  
  > 2000-a-radio-record.ogg

  aio-rename-files out prepend 2000 ::: in cut 1:3 ::: \
                   a-radio-record-xyz.ogg
  
  > 2000-xyz.ogg

# use out append and stdin value commands
#   filename => <STDIN>-<SUFFIX>.ogg

  aio-rename-files out append xyz ::: stdin value ::: \
                   a-radio-record.ogg <<<'new-name'
  
  > new-name-xyz.ogg

  aio-rename-files out append xyz ::: stdin key-value N T ::: \
                   a-radio-record.ogg <<EOF
                     T a title
                     N 10
                   EOF
  
  > 10-a-title-xyz.ogg

# use in and stdin commands
#   filename => <IN-PARTS>-<STDIN>.ogg

  aio-rename-files in keep 1 3 ::: stdin value ::: \
                   a-radio-record.ogg <<<'name'
  
  > a-record-name.ogg

  aio-rename-files in cut 1 3 ::: stdin key-value N T ::: \
                   a-radio-record.ogg <<EOF
                     T a title
                     N 10
                   EOF
  
  > radio-10-a-title.ogg

# use out, in and select commands
#   selects files 1 to 3
#   selected filename => <PREFIX>-<IN-PARTS>.ogg

  aio-rename-files in keep 2 4 ::: out prepend 2000 ::: \
                   select 1:3 ::: \
                   a-radio-record-1.ogg a-radio-record-2.ogg a-radio-record-3.ogg a-radio-record-4.ogg

  > 2000-radio-1.ogg
  > 2000-radio-2.ogg
  > 2000-radio-3.ogg
                 
# use out, stdin and select commands
#   selects files 1 and 3
#   selected filename => <STDIN-KEY-VALUE>-<SUFFIX>.ogg

  aio-rename-files select 1 3 ::: \
                   out append 2000 ::: stdin key-value N T ::: \
                   a-radio-record-1.ogg a-radio-record-2.ogg a-radio-record-3.ogg a-radio-record-4.ogg <<EOF
                     N 10
                     T foo
                     
                     N 12
                     T bar
                   EOF

  > 10-foo-2000.ogg
  > 12-bar-2000.ogg

# use out, in and stdin commands
#   selected filename => <PREFIX>-<IN-PARTS>-<STDIN-KEY-VALUE>.ogg

  aio-rename-files out prepend 2000 ::: \
                   stdin key-value T N ::: in keep 3:4 ::: \
                   a-radio-record-1.ogg a-radio-record-2.ogg <<EOF
                     N 10
                     T foo
                     
                     N 12
                     T bar
                   EOF

  > 2000-record-1-foo-10.ogg
  > 2000-record-2-bar-12.ogg
                   
# use select, out, in and stdin commands
#   selects file 2
#   selected filename => <PREFIX>-<IN-PARTS>-<STDIN-KEY-VALUE>.ogg

  aio-rename-files select 2 ::: out prepend 2000 ::: \
                   stdin key-value N ::: in keep 3 ::: \
                   a-radio-record-1.ogg a-radio-record-2.ogg a-radio-record-3.ogg a-radio-record-4.ogg <<EOF
                     N 10
                     T foo
                     
                     N 12
                     T bar
                   EOF
   
   > 2000-record-12.ogg
```

## More Documentation/Wiki

* [The Wiki](https://gitlab.com/zzeebdrraa/tools/cl-audio-processing/aio/-/wikis/home)
* [Features to add](https://gitlab.com/zzeebdrraa/tools/cl-audio-processing/aio/-/wikis/To-Be-Implemented)
* Detailed description of each aio-script: [audio-processing](https://gitlab.com/zzeebdrraa/tools/cl-audio-processing/aio/-/wikis/AIO-Audio-Processing), [file-renaming](https://gitlab.com/zzeebdrraa/tools/cl-audio-processing/aio/-/wikis/AIO-File-Renaming), [metadata tagging](https://gitlab.com/zzeebdrraa/tools/cl-audio-processing/aio/-/wikis/AIO-File-Renaming), [upload to archive.org](https://gitlab.com/zzeebdrraa/tools/cl-audio-processing/aio/-/wikis/AIO-Archive.org)
* Detailed description of [aio-script design](https://gitlab.com/zzeebdrraa/tools/cl-audio-processing/aio/-/wikis/Script-Design)
* Workflows: [Cutting samples](https://gitlab.com/zzeebdrraa/tools/cl-audio-processing/aio/-/wikis/Workflows/Cutting-samples-from-videos-or-other-audio), [Processing Icecast recordings](https://gitlab.com/zzeebdrraa/tools/cl-audio-processing/aio/-/wikis/Workflows/Working-with-audio-files-recorded-by-an-icecast-server)
* Bugs

## Releases

* [CHANGELOG](https://gitlab.com/zzeebdrraa/tools/cl-audio-processing/aio/-/blob/master/CHANGELOG)

## Licence

This project is under the GPL3 License. See the [LICENSE](https://gitlab.com/zzeebdrraa/tools/cl-audio-processing/aio/-/blob/master/LICENSE) file for the full license text.
